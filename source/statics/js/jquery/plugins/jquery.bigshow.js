
var imgMaxWidth = function (imgs,max_width,fn){
	 max_width =  max_width || 600;
	 
	 var setWidth = function(img){

			var width = img.width,
				height = img.height;
				img.width = max_width;
				img.height = max_width / width * height
				fn && fn(img);

				// alert(width)
			
	  }
	 
	 for(var i=0, l = imgs.length; i<l;i++){
			var img = imgs[i];
			if(	img.width >= max_width){
				setWidth(img)
			} else {
				img.onload = function(){
					if(this.width >= max_width) setWidth(img);
					this.onload = null;
				}
		   }
	 }
};

/*
////测试：
function testShow(){
	imgMaxWidth( document.getElementById('content').getElementsByTagName('img') );
	imgMaxWidth(jQuery(".test img").get(), 600, function(bigImg){
		jQuery(bigImg).bigShow("src");
	});
	if (typeof jQuery == "undefined"){
		setTimeout( arguments.callee,200)
	}
};
*/

//图片预览
var initPreview = function(){

	var $preview = jQuery('<div id="image-preview"></div>').appendTo("body").hide(),
		imgLoaded = {}, // 储存图片地址
		last = '', //用于鼠标移除后取消图片显示事件
		mouse, // 储存最后的鼠标事件对象
		showImg = function(img){
			position(img);
			$preview.empty().append(img.elem).show();
		},
		// 计算和定位
		position = function(img){
		
			// 显示区域应该用 winWidth 和 clinetX 来计算而不是 pageX，窗口宽度可能小于 网页宽度
			
			var e = mouse,
				$img = jQuery(img.elem),
				imgWidth = img.w,
				imgHeight = img.h,
				imgRate = imgWidth/imgHeight,
			
				winWidth = jQuery(window).width(),
				winHeight = jQuery(window).height(),
				spaceX = 20,
				spaceY = 15,
				padding = 7, // 补正
				clientX = e.clientX,
				clientY = e.clientY,
				pageX = e.pageX,
				pageY = e.pageY,
				
				MINWIDTH = 300,
				// 判断窗口可显示区域的最大值，用于缩放
				maxWidth = Math.max(clientX -spaceX - padding*2, winWidth-clientX-spaceX - padding*2),
				
				// 缩放后的尺寸
				zoomWidth = imgWidth,
				zoomHeight = imgHeight;
			
			// 缩放图片
			if(imgWidth > maxWidth || imgHeight > winHeight){
				if( imgRate > maxWidth / winHeight) {
					zoomWidth = maxWidth;
					zoomHeight = zoomWidth / imgRate;
				} else {
					zoomHeight = winHeight;
					zoomWidth = zoomHeight * imgRate;
				}
			}
			
			// 缩放后小于最小宽度则重新调整
			if(imgWidth > MINWIDTH  && zoomWidth < MINWIDTH){
				zoomWidth = MINWIDTH;
				zoomHeight = zoomWidth / imgRate;
			}

			//@return 返回最终坐标
			//@do 先计算各宽度间的关系，赋予状态值。再根据状态转换显示位置。
			var pos = function(){ 
			
				// 为了显示上的统一性，只划分左右显示区域
				var xMode = clientX > winWidth / 2 ?  "left" : "right", 
					yMode;

				if(winHeight - clientY - spaceY > zoomHeight ) yMode = "base"; //显示在鼠标下方
				else if ( winHeight >= zoomHeight ) yMode = "bottom"; // 对齐窗口底部
				else yMode = "top" // 对齐窗口顶部

				var x = {
					right : pageX + spaceX ,
					left: pageX - spaceX - zoomWidth - padding
				}, y = {
					base : pageY+ spaceY,
					top : 0 ,
					bottom : pageY - clientY + winHeight - zoomHeight - padding
				};
				
				return {
					x : x[xMode],
					y : y[yMode],
					w : zoomWidth,
					h : zoomHeight
				}
			
			}()

			// 应用样式
			$img.css({
				width : img.w,
				height: img.h
			});
			$preview.css({
				position: 'absolute',
				zIndex: 999999,
				borderWidth: 2,
				borderStyle: 'solid',
				borderColor: '#999999',
				backgroundColor: '#fafafa',
				padding: 5,
				left : pos.x,
				top : pos.y
			});	
			
	}; 

	jQuery.fn.bigShow = function(rel, width, height){
			rel = rel || "preview"; // 保存大图地址的属性

			/**/
			this.live({
				mouseenter:
				function(e)
				{
					var $this = jQuery(this),
						src = $this.attr(rel),
						img = imgLoaded[src];

					mouse = e;
					last = src;
					
					if(img){
						showImg(img);
					} else {
						jQuery("<img>").load(function(){
							var wh = width; ht = height;
							if(width>0 || height>0) {
								if( (width>0 && width<this.width) || (height>0 && height<this.height) ) {
									if( (width>0 && !height) || (width>0 && height>0 && width>=height) ) {
										height = parseInt(this.height * width / this.width);
										if(height>ht && ht>0) {
											width = parseInt(ht * width / height);
											height = ht;
										}
									}
									else if( (!width && height>0) || (width>0 && height>0 && width<height) ) {
										width = parseInt(this.width * height / this.height);
										if(width>wh && wh>0) {
											height = parseInt(wh * height / width);
											width = wh;
										}
									}
								}
								else {
									width = this.width;
									height = this.height;
								}
								//alert('('+wh+':'+ht+') ' + width+':'+height + ' = '+ this.width+':'+this.height);
							}
							else {
								width = this.width;
								height = this.height;
							}
							imgLoaded[src] = { elem : this , w: width, h : height };
							if(last == src ) showImg(imgLoaded[src]);
						}).attr("src",src);
					}
				},
				mouseleave:
				function()
				{
					last = "";
					$preview.hide();
				},
				mousemove:
				function(e)
				{
					mouse = e;
					var $this = jQuery(this),
					src = $this.attr(rel),
					img = imgLoaded[src];
		
					img && position(img);
				}
			});
			

			/*
			this.hover(function(e){

				var $this = jQuery(this),
					src = $this.attr(rel),
					img = imgLoaded[src];

				mouse = e;
				last = src;
				
				if(img){
					showImg(img);
				} else {
					jQuery("<img>").load(function(){
						var wh = width; ht = height;
						if(width>0 || height>0) {
							if( (width>0 && width<this.width) || (height>0 && height<this.height) ) {
								if( (width>0 && !height) || (width>0 && height>0 && width>=height) ) {
									height = parseInt(this.height * width / this.width);
									if(height>ht && ht>0) {
										width = parseInt(ht * width / height);
										height = ht;
									}
								}
								else if( (!width && height>0) || (width>0 && height>0 && width<height) ) {
									width = parseInt(this.width * height / this.height);
									if(width>wh && wh>0) {
										height = parseInt(wh * height / width);
										width = wh;
									}
								}
							}
							else {
								width = this.width;
								height = this.height;
							}
							//alert('('+wh+':'+ht+') ' + width+':'+height + ' = '+ this.width+':'+this.height);
						}
						else {
							width = this.width;
							height = this.height;
						}
						imgLoaded[src] = { elem : this , w: width, h : height };
						if(last == src ) showImg(imgLoaded[src]);
					}).attr("src",src);
				}
				
			}, function(){
				last = "";
				$preview.hide();	
			}).mousemove(function(e){

				mouse = e;
				var $this = jQuery(this),
				src = $this.attr(rel),
				img = imgLoaded[src];
	
				img && position(img);
			});
			*/

	};
}

jQuery(document).ready(function(){
	initPreview();
	jQuery("img[preview]").bigShow();
});
 