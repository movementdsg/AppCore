目录结构：

api                            存放第三方API接口文件 （目前文件为空）
App                            应用程序文件存放目录，项目所有实现代码均放在这里。
Cache                          生成的缓存文件、日志等存放目录
Data                           数据库ACCESS存放目录
Inc                            程序核心运行文件，一般不能随便修改
res                            资源定向文件，其实就是url定向跳转文件 （可删除）
statics                        静态文件地址（img,js,css等文件），包括上传文件目录
Tester                         项目代码测试工具，项目正式上线后可删除。（可删除）
demo                           查看分页效果演示代码 （可删除）
index.asp                      程序单入口文件，系统所有均访问这个页面
admin.asp                      后台入口文件，其实就是一个url重定向

---------------

App - 应用程序文件存放目录，项目所有实现代码均放在这里。
这个目录是重中之重，基本修改代码均在这里。

App
 - Common           用户自定义函数
 - Conf             配置文件
 - Lang             语言文件
 - Lib              核心文件
 - Tpl              模板文件（视图层View）

Lib 目录用于存放 核心代码

	Lib
	 - Action   放置Controller类文件，里面具体行为(Action)函数，主要负责业务逻辑
	 - Model    (数据库表)Model层模型文件，基本上一个数据表对应一个模型文件
	 - Helper   Helper辅助类文件
	 - Widget   存放单元组件（比如广告组件）

--------------------------------------

MVC 框架的特点：
MVC 简写(Model、Controller、View)

1. 一般都是单入口，URL运行模式（路由功能），三个参数：m,c,a
举例URL访问地址： index.asp?m=admin&c=index&a=login
首先从 m=admin 定位到 /App/Lib/Action/Admin 目录下
其次从 c=index 定位到 /App/Lib/Action/Admin/indexAction.class.asp 文件
再次从 a=login 定位到 indexAction.class.asp 文件 里面的 Sub login() 函数块

注：
缺省了 m 参数，默认值为 home
缺省了 c 参数，默认值为 index
缺省了 a 参数，默认值为 index
所以访问 /index.asp 其实就等同访问 /index.asp?m=home&c=index&a=index

2. 分工明细，Model层主要处理数据操作，Controller层主要负责业务逻辑，View层载入模板文件。

--------------------------------------

其它说明：

Model 层文件可不必手动创建，添加数据表时，程序会自动创建。

Action如何载入模板：
例：
App.View.Display("home/index.html")

获取URL参数 和 表单元素，请统一用 App.Req() 函数获取，不要用 Request.QueryString 和 Request.Form 来获取，以免出现不必要的错误。
比如：URL GET中, App.Req("id") 相当于 Request.QueryString("id")
比如：Form POST中, App.Req("password") 相当于 Request.Form("password")





