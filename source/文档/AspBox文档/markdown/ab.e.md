# AspBox E 核心

<table>
	<tr>
		<td>文件：</td>
		<td>ab.e.asp</td>
	</tr>
	<tr>
		<td>类名：</td>
		<td>Class Cls_AB_E</td>
	</tr>
</table>


------------------------------------------------------------

**AB.E.Escape() 方法**

*语法*

> str = AB.E.Escape ( string )

*说明*

> 按Unicode编码用%编码特殊字符串

*参数*

> String string 			待编码的字符串

*举例*

> 	AB.C.Print AB.E.Escape("这是""AspBox""的演示文本。")
	'输出： %u8FD9%u662F%22AspBox%22%u7684%u6F14%u793A%u6587%u672C%u3002

*注释*

> 调用此方法将把string中的特殊字符按Unicode编码。

*返回值*

> [String (字符串)] 返回 %编码特殊字符串


------------------------------------------------------------

**AB.E.UnEscape() 方法**

*语法*

> str = AB.E.UnEscape ( string )

*说明*

> 把用Unicode编码的特殊字符解码为普通字符串

*参数*

> String string 			待解码的字符串

*举例*

> AB.C.Print AB.E.UnEscape("%u8FD9%u662F%22AspBox%22%u7684%u6F14%u793A%u6587%u672C%u3002")

*输出*

> 这是"AspBox"的演示文本。

*注释*

> 调用此方法将把str中的Unicode编码字符还原为普通字符串。

*返回值*

> [String (字符串)] 返回 解码后的字符串


------------------------------------------------------------