
<p><a name="cmstag"></a></p>

# CMS标签规则

------------------------------------------------------------

**标签规则**

> 	{cmstag:标识符 参数1=项值1 参数2=项值2 ...}
		[标识符:字段1]
		[标识符:字段2]
		..
		[标识符:字段n]
	{/cmstag:标识符}


**支持的参数**

> 	1、 sql: 直接书写SQL查询语句 （注：sql语句中#@符号代表数据表前缀，若sql语句必须普通#@符号时，可用加个\表示转义比如\#@）
	2、 table: 表示数据表，此参数缺省则表示数据表默认跟标识符一致
	3、 data: 若只含有data参数(不含有sql、table等参数), 表示数据来源从data参数值中获取，data参数里的值可以是 变量名，也可以是一个函数
	4、 field: 筛选字段，缺省 field="*"
	5、 where: 查询条件
	6、 order: 排序
	7、 rows|loop: 限制数据数，此参数缺省则等同loop=0, 从数据表中取出所有数据。
	8、 offset: 偏移量，offset和rows搭配 可筛选出 第x~第y条数据

**举例**

> 	{cmstag:user table="user" offset="6" rows="4"} .. {/cmstag:user}
	表示：从 user 表中 取出 第6+1=7条开始的4条数据，即取 第7~10条数据。

------------------------------------------------------------

*以下 只带data参数 的情况：*

> 	例1：数据来源是变量：
	{asp set rs_user = M('user')().fetch()}
	{cmstag:rs data="rs_user"}
		[rs:id] - [rs:username] <br />
	{/cmstag:rs}

> 	例2：数据来源是函数：
	{asp function getRs(tb) : set getRs = M(tb)().fetch() : end function }
	{cmstag:rs data="getRs('user')"}
		[rs:id] - [rs:username] <br />
	{/cmstag:rs}

------------------------------------------------------------

*例 1：*

> 	{cmstag:user sql="select * from #@user" }
		[user:id] - [user:username] <br />
	{/cmstag:user}

*例 2：*

> 	{cmstag:user sql="select * from #@user where email like '%@qq.com' " }
		[user:id] - [user:username] <br />
	{/cmstag:user}

*例 3：*

> 	sql语句中使用变量，形式:  ($var) 或 {$var}
	{asp str = "%@qq.com"}
	{cmstag:user sql="select * from #@user where email like '{$str}' " }
		[user:id] - [user:username] <br />
	{/cmstag:user}

------------------------------------------------------------

*例 4：*

> 	{cmstag:user table=user loop=10 }
		[user:id] - [user:username] <br />
	{/cmstag:user}

附： 子标签 `[user:username]` 还支持 参数
参数len=n ，表示 截取n个字符串, 比如`[user:username len=3]`表示截取3个字符串。
参数md5=32|16|8 ，表示对字段进行md5加密, 比如`[user:username md5=16]`表示对username字段其进行16位md5加密。
参数empty='为空默认值' ，设置为空代替值。比如`[user:username empty='为空时代替值']`表示username字段值为空时，用 “为空时代替值” 代替结果。

子标签 `[user:username]` 还支持 参数fun，表示自定义函数。
比如参数`fun='myfun(@me,1,2,3)'`
比如 `[user:username fun='len(@me)']` 等同效果： `<%=len(user("username"))%>`

为了解决两字段数据相互调用，特设了以下用法：
`[标识符:#]..这里书写asp语法..[/标识符:#]`
例：

> 	{cmstag:user table='user' loop=8 }
		[user:#] echo ~username~ & "-" & ~id~ [/user:#]
	{/cmstag:user}
	说明：~username~表示字段username，~id~表示字段id，若要~符作为普通字符，请用\~表示转义

------------------------------------------------------------

**参数可以指定返回值：**

当指定有参数return值时，作为模块返回值，可以对其进行引用：

*例 5：*

> 	{cmstag:user table=user loop=10 return='user'}
		[user:id] - [user:username] <br />
		--- 因为有了return返回值参数, 下面这些用法就变得可用：--- <br />
		{$user('id')} - {$user("username")} <br />
		{$user.id} - {$user.username} <br />
		{:user('id')} - {:user("username")} <br />
		{asp} echo user("id") & "-" & user("username") {/asp}
	{/cmstag:user}

------------------------------------------------------------

**标签相互嵌套：**

当页面 CMS标签 的 标识符 不重复时，可相互嵌套。比如多级分类的实现：

*例 5：*

> 	&lt;pre>
		{cmstag:class1 table=menu loop=10 where="pid=0" order="ordid,id" } 一级菜单： [class1:id] - [class1:name] <br />{/cmstag:class1}
		{cmstag:class1 table=menu loop=10 where="pid=0" order="ordid,id" }
		<!-- 一级菜单：--> [class1:id] - [class1:name]
		<!-- 二级菜单：--> 
			{cmstag:class2 table=menu loop=10 where="pid=[class1:id]" order="ordid,id" }
				[class2:id] - [class2:name]
		<!-- 三级菜单：--> 
				{cmstag:class3 table=menu loop=10 where="pid=[class2:id]" order="ordid,id" }
					[class3:id] - [class3:name]
		<!-- 四级菜单：--> 
					{cmstag:class4 table=menu loop=10 where="pid=[class3:id]" order="ordid,id" }
						[class4:id] - [class4:name]
					{/cmstag:class4}
				{/cmstag:class3}
			{/cmstag:class2}
		{/cmstag:class1}
	&lt;/pre>

------------------------------------------------------------

<p><a name="pagetag"></a></p>

# 分页标签

**标签规则：**

> 	{pagetag:标识符 参数1=项值1 参数2=项值2 ...}
		[标识符:字段1]
		[标识符:字段2]
		..
		[标识符:字段n]
	{/pagetag:标识符}

支持参数：`{pagetag:pager count='' data='' sql='' size='10' mark='p'} .. {/pagetag:pager}`

参数： 
> 	count 或 sql 或 data 三者之一，
	若设置 count 且 count > 0 则直接计算传递count为数据总数，
	否则，若设置 data 则传递 data属性里的 data 到 pager分页中，计算data的分页情况，
	否则，最后从 sql 中读取Rs记录集数据，再计算分页情况。
	当页面有cmstag标签里设置了pagesize参数时，可缺省这3个参数。(页面会自动判断)
	size, mark , 设置对应分页条的 pagesize, pagemark 属性。
	缺省继承 页面上CMS标签 设置的pagesize 和 pagemark，再者缺省继承 系统分页设置（默认 10）

*例 1：*

> 	{cmstag:user table=user pagesize=10 }
		[user:id] - [user:username] <br />
	{/cmstag:user}

> 	{pagetag:pager mark='p'}
		&lt;div id="wrapbox"><br />===========
		<br /><strong>第[pager:begin]条 - 第[pager:end]条</strong>
		<br />当前：<a href="/index.asp?a=test&p=[pager:no]">第[pager:no]页</a>
		<br />
		<!--
		<a href="/index.asp?a=test&p=[pager:first]">首页</a>  
		<a href="/index.asp?a=test&p=[pager:prev]">上一页</a>  
		<a href="/index.asp?a=test&p=[pager:next]">下一页</a>
		<a href="/index.asp?a=test&p=[pager:last]">末页</a>
		-->
		[pager:first_s] 
		[pager:prev_s]
		[pager:next_s]
		[pager:last_s]
		<br />===========
		<br />共[pager:total]条数据 当前: 第[pager:no]页/共[pager:count]页 每页显示:[pager:size]条
		<br />分页条：[pager:bar]
		<br />==========
		<br /></div>
	{/pagetag:pager}


------------------------------------------------------------

<p><a name="multidb"></a></p>

# 多库查询

在 CMS标签的参数中 中加入参数 conn，表示传递一个 Connection 对象的变量

> 	{asp}
	Dim db, newConn
	Set db = AB.db.New
	db.Conn = db.OpenConn(1, cfg_sitepath & "Data/#test.mdb", "")
	Set newConn = db.Conn
	{/asp}
	
> 	{cmstag:rs sql="select * from #@table1 where id<=1000000 order by id asc" rows="10" conn="newConn" return="rs"}
		{$rs("id")} - {$rs("aaaa")} - {$rs("bbbb")} - {$rs("cccc")} <br>
	{/cmstag:rs}
	{@empty for='rs'}暂无记录！{/@empty}


------------------------------------------------------------

系统标签
待整理..

