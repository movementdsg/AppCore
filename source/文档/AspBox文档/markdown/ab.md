# AspBox基类

<table>
	<tr>
		<td>文件：</td>
		<td>Cls_AB.asp</td>
	</tr>
	<tr>
		<td>类名：</td>
		<td>Class Cls_AB</td>
	</tr>
</table>


------------------------------------------------------------

**AB.SoftName() 属性**

*说明*

> 获取软件名称

*返回值*

> AspBox 

------------------------------------------------------------

**AB.Version() 属性**

*说明*

> 获取版本号

*返回结果*

> v1.4.0 Build 20140927

------------------------------------------------------------

**AB.RegID() 属性**

*说明*

> 获取版本注册ID， 用于版本升级比较

*返回结果*

> 1.40.140927.00

------------------------------------------------------------

**AB.CharSet() 属性**

*说明*

> 设置|获取 文件编码

*语法*

> AB.CharSet = "UTF-8"

*返回值*

> UTF-8

------------------------------------------------------------

**AB.ifCore() 属性**

*说明*

> 设置|获取 是否预加载核心

*语法*

> AB.ifCore = True

*返回值*

> [Boolean类型] ： <u>False</u> | True


------------------------------------------------------------

**AB.ifMvc() 属性**

*说明*

> 设置|获取 是否启用MVC模型

*语法*

> AB.ifMvc = True

*返回值*

> [Boolean类型] ： <u>False</u> | True


------------------------------------------------------------

**AB.Debug() 属性**

*说明*

> 设置|获取 打开开发者调试模式

*语法*

> AB.Debug = True

*返回值*

> [Boolean类型] ： <u>False</u> | True


------------------------------------------------------------

**AB.baseCore() 属性**

*说明*

> 设置|获取 配置预定义核心类

*语法*

> AB.baseCore = "C,D,H,E,A,DB"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.noCore() 属性**

*说明*

> 设置|获取 配置不加载的预定义核心类

*语法*

> AB.noCore = "DB,Dbo,SC,jsLib,Pager"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.BasePath() 属性**

*说明*

> 设置|获取 AspBox目录路径，以"/"开头

*语法*

> AB.BasePath = "/Inc/AspBox/"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.PluginPath() 属性**

*说明*

> 设置|获取 AspBox插件目录，一般缺省按默认配置

*语法*

> AB.PluginPath = AB.BasePath & "plugin/"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.CorePath() 属性**

*说明*

> 设置|获取 AspBox核心目录，一般缺省按默认配置

*语法*

> AB.CorePath = AB.BasePath & "library/"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.CachePath() 属性**

*说明*

> 设置|获取 AspBox缓存目录，一般缺省按默认配置

*语法*

> AB.CachePath = AB.BasePath & "_Cache/"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.FileBOM() 属性**

*说明*

> 设置|获取 如何处理载入的UTF-8文件的BOM信息(keep/remove/add)

*语法*

> AB.FileBOM = "remove"

*返回值*

> [String类型] : <u>remove</u> | keep | add


------------------------------------------------------------

**AB.tbPrefix() 属性**

*说明*

> 设置|获取 数据表前缀

*语法*

> AB.tbPrefix = ""

*返回值*

> [String类型]


------------------------------------------------------------

**AB.FsoName() 属性**

*说明*

> 设置|获取 FSO组件的名称（如果服务器上修改过）

*语法*

> AB.FsoName = "Scripting.FileSystemObject"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.SteamName() 属性**

*说明*

> 设置|获取 ADODB.Stream组件的名称（如果服务器上修改过）

*语法*

> AB.SteamName = "ADODB.Stream"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.DictName() 属性**

*说明*

> 设置|获取 Scripting.Dictionary字典组件的名称（如果服务器上修改过）

*语法*

> AB.DictName = "Scripting.Dictionary"

*返回值*

> [String类型]


------------------------------------------------------------

**AB.CookieEncode() 属性**

*说明*

> 设置|获取 Cookie是否加密

*语法*

> AB.CookieEncode = True

*返回值*

> [Boolean类型] ： <u>False</u> | True


------------------------------------------------------------

**AB.CacheType() 属性**

*说明*

>	设置|获取 缓存方式
	默认不缓存: 			0|空值|False
	app形式： 			1|app|True
	文件形式：			2|file
	数据库形式： 			3|data|db
	经测试：将值设置为 app 效率为最高

*语法*

> AB.CacheType = "app"

*返回值*

> [String类型] : <u>空值</u> | app | file | data


------------------------------------------------------------

**AB.ScriptTime() 属性**

*说明*

> 获取 计算脚本执行时间

*语法*

> timer = AB.ScriptTime

*返回结果*

> [String类型] ： 0.882


------------------------------------------------------------

**AB.dbQueryTimes() 属性**

*说明*

> 获取 计算数据库查询次数

*语法*

> times = AB.dbQueryTimes

*别名*

> times = AB.QT

*返回结果*

> [Integer类型] ： 4


------------------------------------------------------------

**AB.CacheClean() 方法**

*说明*

> 获取 清除AspBox缓存数据

*语法*

> AB.CacheClean()

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.HasCore() 方法**

*说明*

> 判断AspBox是否包含某个核心core

*语法*

> bool = AB.HasCore(core)

*参数*

> String core ： 核心名

*返回值*

> [Boolean类型] ： False | True


------------------------------------------------------------

**AB.DictAdd() 方法**

*说明*

> 新增AspBox字典库字典项

*语法*

> AB.DictAdd key, value

*参数*

> 	String key ： 键名
	MixType value ： 值

*别名*

> AB.DA key, value

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.DictDel() 方法**

*说明*

> 删除AspBox字典库字典项

*语法*

> AB.DictDel key

*别名*

> AB.DD key

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.DictClear() 方法**

*说明*

> 清空AspBox字典库所有字典项

*语法*

> AB.DictClear()

*别名*

> AB.Dc()

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Load() 方法**

*说明*

> 载入AspBox核心

*语法*

> AB.Load core

*参数*

> String core ： 要加载的核心名

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.ReLoad() 方法**

*说明*

> 重新载入AspBox核心

*语法*

> AB.ReLoad core

*参数*

> String core ： 要重新加载的核心名

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Lib() 方法**

*说明*

> 通过AB.Lib引入核心并赋予对象

*语法*

> Set obj = AB.Lib(core)

*参数*

> 	Object obj ： 创建的新对象
	String core ： 核心名

*返回值*

> [Object类型]


------------------------------------------------------------

**AB.Use() 方法**

*说明*

> 载入AspBox核心

*语法*

> AB.Use core

*参数*

> String core ： 要加载的核心名

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.ReUse() 方法**

*说明*

> 重新载入AspBox核心

*语法*

> AB.ReUse core

*参数*

> String core ： 要重新加载的核心名

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Ext() 方法**

*说明*

> 通过AB.Ext引入插件并赋予对象, 插件目录在 plugin 下

*语法*

> Set obj = AB.Ext(core)

*参数*

> 	Object obj ： 创建的新对象
	String core ： 核心名

*返回值*

> [Object类型]


------------------------------------------------------------

**AB.Drop() 方法**

*说明*

> 释放AspBox核心

*语法*

> AB.Drop core

*参数*

> String core ： 要释放的核心名

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Free() 方法**

*说明*

> 释放对象

*语法*

> AB.Free core

*参数*

> String core ： 要释放的对象名

*举例*

> AB.Free "DB"

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Trace() 方法**

*说明*

> 调试查看变量，若字典和rs对象，只输出前50条数据

*语法*

> AB.Trace var

*参数*

> MixType var ： 变量

*举例*

> AB.Trace Array(0,1,2)

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.TraceAll() 方法**

*说明*

> 调试查看变量，输出全部数据

*语法*

> AB.TraceAll var

*参数*

> MixType var ： 变量

*举例*

> AB.TraceAll Array(0,1,2)

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.FnInit() 方法**

*说明*

> 重置AB.Fn(一旦重置则以前增设的所有属性将全部失效)

*语法*

> AB.FnInit()

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.FnAdd() 方法**

*说明*

> 给AB.Fn拓展增加属性值(可以是 对象 或 字符串)

*语法*

> AB.FnAdd str, obj

*参数*

> 	String str ： 作为AB.Fn的变量属性名
	MixType obj ： 赋值

*举例*

> 	class cls_t: function foo() : foo = "hello!" : end function : end class
	class cls_a: function sum(a,b) : sum = a + b : end function : end class
	class cls_b: sub sayhello() : response.write("hi,jack") : end sub : end class
	dim o_t : set o_t = New cls_t
	dim o_a : set o_a = New cls_a
	dim o_b : set o_b = New cls_b
	ab.fnAdd "m", o_t
	ab.fnAdd "n", o_a
	AB.C.PrintCn ab.fn.m.foo() '输出：hello!
	AB.C.PrintCn ab.fn.n.sum(2,5) '输出：7
	AB.C.PrintCn ab.fnHas("m") '输出：True
	AB.C.PrintCn ab.fnHas("abc") '输出：False
	AB.Trace ab.fnItems '输出全部定义属性值
	AB.Trace ab.fnItem("m") '输出属性 m 的值
	ab.fnAdd "m", o_b '此属性 m 的值被覆盖，值变为 o_b 对象
	ab.fn.m.sayhello() '输出：hi,jack
	ab.fnAdd "n", "abcdefg" '此属性 n 的值被覆盖，值变为字符串 abcdefg
	AB.Trace ab.fnItem("n")
	ab.fnAdd "n", o_t '此属性 n 的值再次被覆盖，值又变为 o_t 对象
	AB.Trace ab.fn.n

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.FnDel() 方法**

*说明*

> 删除AB.Fn拓展的某个属性

*语法*

> AB.FnDel str

*参数*

> String str ： AB.Fn属性名

*举例*

> 	class cls_t: function foo() : foo = "hello!" : end function : end class
	class cls_a: function sum(a,b) : sum = a + b : end function : end class
	dim o_t : set o_t = New cls_t
	dim o_a : set o_a = New cls_a
	ab.fnAdd "m", o_t
	ab.fnAdd "n", o_a
	AB.C.PrintCn ab.fnHas("m") '输出：True
	If ab.fnHas("m") Then AB.C.PrintCn ab.fn.m.foo()
	ab.FnDel "m" '删除属性m
	AB.C.PrintCn ab.fnHas("m") '输出：False

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.FnHas() 方法**

*说明*

> 检测AB.Fn拓展是否含有某项属性

*语法*

> bool = AB.FnHas(str)

*参数*

> String str ： AB.Fn属性名

*举例*

> 	class cls_t: function foo() : foo = "hello!" : end function : end class
	dim o_t : set o_t = New cls_t
	ab.fnAdd "m", o_t
	AB.C.PrintCn ab.fnHas("m") '输出：True
	AB.C.PrintCn ab.fnHas("abc") '输出：False

*返回值*

> [Boolean (布尔值)]


------------------------------------------------------------

**AB.FnItem() 方法**

*说明*

> 获取AB.Fn拓展的某属性值

*语法*

> [Set] var = AB.FnItem(str)

*参数*

> String str ： AB.Fn属性名

*举例*

> 	class cls_t: function foo() : foo = "hello!" : end function : end class
	dim o_t : set o_t = New cls_t
	ab.fnAdd "m", o_t
	AB.Trace ab.fn.m '输出 ab.fn.m 的值
	AB.Trace ab.fnItem("m") '输出某项属性值,等同输出 ab.fn.m 的值

*返回值*

> [Anything (任何值)]


------------------------------------------------------------

**AB.FnItems() 方法**

*说明*

> 获取AB.Fn拓展的所有属性对象的集合信息（字典对象）

*语法*

> Set obj = AB.FnItems()

*举例*

> 	class cls_t: function foo() : foo = "hello!" : end function : end class
	class cls_a: function sum(a,b) : sum = a + b : end function : end class
	dim o_t : set o_t = New cls_t
	dim o_a : set o_a = New cls_a
	ab.fnAdd "m", o_t
	ab.fnAdd "n", o_a
	AB.C.PrintCn ab.fn.m.foo() '输出：hello!
	AB.C.PrintCn ab.fn.n.sum(2,5) '输出：7
	AB.C.PrintCn ab.fnHas("m") '输出：True
	AB.C.PrintCn ab.fnHas("abc") '输出：False
	AB.Trace ab.fnItems '输出全部定义属性值(字典对象)
	AB.Trace ab.fnItem("m") '输出属性 m 的值

*返回值*

> [Object Dictionary对象]


------------------------------------------------------------

**AB.setError() 方法**

*说明*

> AB抛出错误信息

*语法*

> AB.setError str, code

*参数*

> 	String str ： 抛错字符串
	Integer code ： 抛错代码

*举例*

> 	AB.Error(1) = "程序运行出错"
	AB.setError "(Error: 文件不存在)", 2

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.ShowErr() 方法**

*说明*

> AB直接中断输出错误信息

*语法*

> AB.ShowErr code, desc

*参数*

> 	Integer code ： 错误代码
	String desc ： 错误描述

*举例*

> AB.ShowErr 0, "程序运行出错。"

*返回值*

> [无返回值]


------------------------------------------------------------
