# AspBox C 核心

<table>
	<tr>
		<td>文件：</td>
		<td>ab.c.asp</td>
	</tr>
	<tr>
		<td>类名：</td>
		<td>Class Cls_AB_C</td>
	</tr>
</table>


------------------------------------------------------------

**AB.C.IIF() 方法**

*语法*

> bool = AB.Pub.IIF(condition, ifTrue, ifFalse)

*说明*

> 如果condition参数表达式为真则返回ifTrue参数的值，为假则返回ifFalse参数的值

*作用*

> 	如果condition参数表达式为真则返回ifTrue参数的值，为假则返回ifFalse参数的值
	调用此方法时，将首先判断第1个参数中的表达式是否为真，
	如果为真则返回第2个参数的表达式中的值，
	如果为假则返回第3个参数的表达式中的值。
	是If...Then...Else...End If的一种简写版本。类似于JavaScript中的 a ? b : c 表达式

*参数*

> 	Boolean condition          Expression (表达式)
	1、数值或字符串表达式，其运算结果是 True 或 False。
	如果 condition 是 Null，则 condition 被视为 False。
	2、形如 TypeOf objectname Is objecttype 的表达式。
	objectname 是任何对象的引用，而 objecttype 则是任何有效的对象类型。
	如果 objectname 是 objecttype 所指定的一种对象类型，则表达式为 True；否则为 False。
	Anything(任意值) ifTrue     如果 condition 为 True 时要返回的值
	Anything(任意值) ifFalse    如果 condition 为 False 时要返回的值

*举例*

> 	Result = AB.C.IIF(Instr("type","p")>0, "blue", "red")
	'如果Instr("type","p")的值大于0,则Result的值就为"blue"，反之则为"red"。

*返回值*

> [Anything(任意值)]


------------------------------------------------------------

**AB.C.ASCII() 方法**

*语法*

> str = AB.C.ASCII(string)

*说明*

> 转换字符串为HTML实体代码

*作用*

> 调用此方法可以把字符串转换为以 "&" 开头的HTML实体代码。

*参数*

> String string          待转换的字符串

*举例*

> 	Dim str
	str="I Like Asp!"
	AB.C.Print AB.C.ASCII(str) '输出： &#73;&#32;&#76;&#105;&#107;&#101;&#32;&#65;&#115;&#112;&#33;

*返回值*

> [String (字符串)] 返回在HTML源码中返回以"&" 开头的HTML实体代码


------------------------------------------------------------

**AB.C.CLeft() 方法**

*语法*

> str = AB.C.CLeft(string, separator)

*说明*

> 截取用某个特殊字符分隔的字符串的特殊字符左边部分

*参数*

> 	String string 			待转换的字符串
	String separator 		用来截取字符串的特殊分隔符号 

*举例*

> 	Dim str
	str = "hello:kitty_here"
	AB.C.Put AB.C.CLeft(str,":") '输出： hello
	AB.C.Put AB.C.CLeft(str,"_") '输出： hello:kitty

*返回值*

> [String (字符串)] 返回截取后的字符串


------------------------------------------------------------

**AB.C.CRight() 方法**

*语法*

> str = AB.C.CRight(string, separator)

*说明*

> 截取用某个特殊字符分隔的字符串的特殊字符右边部分

*参数*

> 	String string 			待转换的字符串
	String separator 		用来截取字符串的特殊分隔符号 

*举例*

> 	Dim str
	str = "hello:kitty_here"
	AB.C.WR AB.C.CRight(str,":") '输出： kitty_here
	AB.C.WR AB.C.CRight(str,"_") '输出： here


*返回值*

> [String (字符串)] 返回截取后的字符串


------------------------------------------------------------

**AB.C.CutStr() 方法**

*语法*

> str = AB.C.CutStr ( string, charNumber[:separator] )

*说明*

> 从左边截取指定数量的字符串，并以自定义的符号代替未显示部分

*参数*

> 	String string 			待截取的字符串
	Integer charNumber 		要保留的字数(包含 separator )，双字节字符（比如中文）算2个字符
	String separator 		自定义的代替符号 

*举例*

> 	Dim Str1, Str2
	Str1 = "这是一个测试用的字符串"
	Str2 = "This is a test 字符串"
	Response.write(AB.C.CutStr(Str1,10))   		'输出结果： 这是一个…
	Response.write(AB.C.CutStr(Str2,16))   		'输出结果： This is a test…
	Response.write(AB.C.CutStr(Str1,"10:~~"))   	'输出结果： 这是一个~~
	Response.write(AB.C.CutStr(Str1,"12:"))   	'输出结果： 这是一个测试

*注释*

> 	调用此方法将截取字符串的左边指定字数的部分，右边省略的部分以指定的符号（缺省为省略号“…”）代替。
	使用此方法需要注意的是，如果字符串是双字节字符（比如中文）则要算占用2个字符。 
	注意：参数中的长度值包括了替代符号的长度在内。


*返回值*

> [String (字符串)] 返回从左边截取指定数量的字符串


------------------------------------------------------------

**AB.C.Format() 方法**

*语法*

> str = AB.C.Format ( string, obj )

*说明*

> 将复杂的各类集合对象格式化为字符串

*参数*

> 	String string
	包含占位符的字符串，占位符用{}符号包含，视参数的不同，占位符中间可以是数字或者名称。
	如果占位符是数字，则编号从0开始表示第一个元素。如果字符串中本身就包含 { 字符，则需要用 \{ 进行转义。
	--------------
	Array|Object|Recordset|String obj
	用于格式化占位符的数据源，可以是以下类型：
	a. 字符串 - 只替换 {0} 占位符；
	b. 数组 - 依次替换占位符中的 {数字} 为对应的数组元素；
	c. 记录集(Recordset) - 依次替换占位符中的 {数字} 或 {列名} 为本条记录的相应值；
	d. 字典(Dictinary) - 依次替换占位符中的 {键名} 为字典中对应的值；
	e. Match集合 - 替换 {0} 为匹配本身，然后从 {1} 开始替换占位符中的数字为对应的子集合 (SubMatches)的值；
	f. SubMatches集合 - 依次替换占位符中的 {数字} 为本条集合中对应的值；
	g. AspBox List对象 - 依次替换占位符中的 {数字} 或 {Hash列名} 为数组中对应的值。

*举例*

> 	'字符串参数
	AB.C.Put AB.C.Format("This is a {0}.", "Text")
	'输出：This is a Text
	'-----
	'数组参数
	AB.C.Put AB.C.Format("name:{0}/sex:{1}/age:{2}", Array("Ertan", "Male", 38))
	'输出：name:Ertan/sex:Male/age:38
	'-----
	'格式化记录集值
	Dim rs
	Set rs = AB.db.GR("Users:ID,Name,QQ,Mail","","")
	While Not rs.EOF
	  '格式化记录集时，占位符可用数字和列名
	  AB.C.Put AB.C.Format("{0} / {1} / {qq} / {mail}",rs)
	  '输出： 10 / AlexFu / 8000121 / alex@alexfu.com
	  rs.MoveNext
	Wend
	AB.db.C(rs)
	'-----
	'格式化Dictionary值
	Dim d
	Set d = CreateObject("Scripting.Dictionary")
	d.Add "a","Athens"
	d.Add "b","Belgrade"
	d.Add "c","Cairo"
	'格式化字典对象时只能用列名
	AB.C.Put AB.C.Format("{a}/{c}",d)
	'输出： Athens/Cairo
	Set d = Nothing
	'-----
	'格式化List对象
	Dim list
	Set list = AB.List.New
	list.Hash = "name:Scott pwd:tiger age:39 job:CFO"
	'格式化list对象时，可以用数字，如果是Hash还可以用列名
	AB.C.Put AB.C.Format("{0} / {pwd} / {2} / {job}", list)
	'输出： Scott / tiger / 39 / CFO
	Set list = Nothing
	'-----
	'格式化Match对象
	Dim s,m,match
	s = "<em>Scott/Tiger</em><em>Smith/Cat</em>"
	Set m = AB.C.RegMatch(s, "<em>(\w+)/(\w+)</em>")
	For Each match In m
	  AB.C.Put AB.C.Format("{0} | {1}", match.SubMatches)
	  '输出： Scott | Tiger
	Next
	For Each match In m
	'如果直接用match作为参数，则第1个元素为match本身的值
	  AB.C.Put AB.C.Format("{0} &gt; {1} | {2}", match)
	  '输出： <em>Scott/Tiger</em> &gt; Scott | Tiger
	Next
	Set m = Nothing
	'-----
	'在占位符中使用参数格式化
	Dim a
	a = Array(.3453,2372291.876,-34.28,Now,"I'm Coldstone")
	'参数N表示格式化数字，语法为 "N[,或%或(][数字]"，后面两项均为可选:
	'("," - 用千位分隔符；"%" - 转换为百分比；"(" - 负数用括号包含；数字 - 小数位数)
	AB.C.Put AB.C.Format("{0:N} / {0:N3} / {0:N%2} / {1:N,} / {2:N(1}",a)
	'输出：0.3453 / 0.345 / 34.53% / 2,372,291.88 / (34.3)
	'-----
	'参数D表示格式化日期，用法和AB.C.DateTime一样
	'参数U表示转化为大写字母，参数L表示转化为小写字母
	AB.C.Put AB.C.Format("{3:Dy-mm-dd} / {4:U} / {4:L}",a)
	'输出：2010-08-18/I'M COLDSTONE/i'm coldstone
	'-----
	'参数E表示使用ASP语句表达式，用%s代替值在语句中的位置，AB.C的方法可省略 AB.C. 前缀
	AB.C.Put AB.C.Format("{4:ECLeft(%s,"" "")} / {3:EDateTime(%s,""星期w"")}",a)
	'输出：I'm / 星期三

*注释*

> 	调用此方法可以把各种带有集合特征的对象按字符串内占位符的形式把值替换到字符串中，
	能够很方便的用于各种字符串内变量的拼接。
	同时可在占位符中对要替换的值进行一些简单的格式化操作。
	与 AB.C.Str 方法不同的是，此方法内字符串占位符的下标由0开始

*返回值*

> [String (字符串)] 返回 格式化后的字符串


------------------------------------------------------------

**AB.C.HtmlDecode() 方法**

*语法*

> str = AB.C.HtmlDecode ( string )

*说明*

> HTML解码函数

*参数*

> String string 			待转换的字符串

*举例*

> 	AB.C.Print AB.C.HtmlDecode("这是一段&lt;em&gt;测试&lt;/em&gt;文字。<br />谢谢")
	'输出： 这是一段<em>测试</em>文字。<br />谢谢

*注释*

> 将字符串中的特殊字符转换为可识别的html代码。此方法同 AB.C.HtmlEncode 方法完全相反。

*返回值*

> [String (字符串)] 返回 经过转换的html字符串


------------------------------------------------------------

**AB.C.HtmlEncode() 方法**

*语法*

> str = AB.C.HtmlEncode ( string )

*说明*

> HTML编码函数(显示为HTML格式)

*参数*

> String string 			待转换的字符串

*举例*

> 	AB.C.Print AB.C.HtmlDecode("这是一段&lt;em&gt;测试&lt;/em&gt;文字。<br />谢谢")
	'输出： 这是一段<em>测试</em>文字。<br />谢谢

*注释*

> 	调用此方法将加码参数中的字符串，以方便阅读的HTML格式显示出来。
	例如将换行符替换为<br />，将<替换为&lt;等等。这也是一种安全处理字符串的方式。

*返回值*

> [String (字符串)] 返回 经过转换的html字符串


------------------------------------------------------------

**AB.C.HtmlFilter() 方法**

*语法*

> str = AB.C.HtmlFilter ( string )

*说明*

> 经过处理的字符串

*参数*

> String string 			待过滤的字符串

*注释*

> 调用此方法将过滤参数字符串中的所有HTML标签，返回纯文本。 

*返回值*

> [String (字符串)] 返回 经过转换的html字符串


------------------------------------------------------------

**AB.C.HtmlFormat() 方法**

*语法*

> str = AB.C.HtmlFormat ( string )

*说明*

> 简单格式化HTML文本（仅处理换行和空格）

*参数*

> String string 			待格式化的HTML文本

*举例*

> AB.C.Print AB.C.HtmlFormat("这是一段<b>测试</b>用的" & vbCrLf & "   <font color=""red"">文本</font>。")

*注释*

> 	此方法仅把字符串中的换行符和空格替换为浏览器可以显示的标签，其它的HTML标签不作处理。
	'输出： 这是一段<b>测试</b>用的<br />&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">文本</font>。


*返回值*

> [String (字符串)] 返回 经过格式化的字符串


------------------------------------------------------------

**AB.C.JsCode() 方法**

*语法*

> str = AB.C.JsCode ( string )

*说明*

> 返回用< script >包含的js代码字符串

*参数*

> String string 			javascript代码

*举例*

> AB.C.Print AB.C.JsCode("alert('hit me!');")

*注释*

> 调用此方法将返回一段标准的javascript代码片断，可用于在浏览器中输出

*返回值*

> [String (字符串)] 返回 一段由< script >标签包含的javascript代码


------------------------------------------------------------

**AB.C.JSEncode() 方法**

*语法*

> str = AB.C.JSEncode ( string )

*说明*

> 转换字符串为安全的JavaScript字符串

*参数*

> String string 			待转义的字符串

*举例*

> 	AB.C.Print AB.C.JSEncode("The Path is : '\test\path'")
	'输出： The Path is : \'\\test\\path\'

*注释*

> 调用此方法可以把一个字符串中的特殊字符转义为安全的JavaScript字符串。

*返回值*

> [String (字符串)] 返回 经过转义的字符串


------------------------------------------------------------

**AB.C.RandStr() 方法**

*语法*

> str = AB.C.RandStr ( charNumber[:range] | min-max | string[:range] )

*说明*

> 生成一个指定长度的随机字符串

*参数*

> 	Integer charNumber 			要生成的字符串的长度
	String range 				要生成的字符串的字符范围
	Integer min					生成随机数的最小值，必须和max配合使用
	Integer max					生成随机数的最大值，必须和min配合使用
	String string				要将随机字符串嵌入的字符串，可以嵌入随机字符串和随机数字：
	- 用"<n>"表示字符串中要加入的随机字符串的位数
	- 用"<min-max>"表示字符串中要加入的随机数字的最大值和最小值 

*举例*

> 	'生成一个12位的随机字符串
	AB.C.WR AB.C.RandStr(12)
	'生成一个6位的随机数
	AB.C.WR AB.C.RandStr("100000-999999")
	'生成一个包含特殊字符的8位数密码
	AB.C.WR AB.C.RandStr("8:0123456789abcdefghijklmnopqrstuvwxyz~!@#$%^&*_-+=")
	'在字符串内生成一个随机的16进制颜色代码
	AB.C.WR AB.C.RandStr("Random Color \: #<6>:0123456789ABCDEF")
	'生成一个CLSID(类ID)
	AB.C.WR AB.C.RandStr("{<8>-<4>-<4>-<4>-<12>}:0123456789ABCDEF")
	'生成一个自定义的包含固定编码部分的随机编码
	AB.C.WR AB.C.RandStr("CN-\<86\>-<6>-<10000-99999>")
	'生成一个3000以内的编号
	AB.C.WR AB.C.RandStr("No.<1-2999>")

*输出*

> 	a5Rg4Tam21Mw
	140548
	lw_s*v9c
	Random Color : #0FE329
	{3BCBF04B-BE10-9E3A-FEA4-7778414D1167}
	CN-<86>-vlP7YF-39532
	No.320

*说明*

> 	此方法根据参数的不同有三种用法:
	----
	第一，用charNumber和range参数将生成一个指定长度的随机字符串，其中包含大小写字母及数字。如果指定了range范围，则在指定的字符范围内生成随机字符串。
	----
	第二，用min和max参数将生成一个随机数，大小在min和max之间。
	----
	第三，用字符串作为参数，在字符串中嵌入形如“<n>”和“<min-max>”，可以在字符串内生成相应位数的随机字符串和随机数。
	如果同时指定了range范围，则此字符串内所有的随机字符串均在指定的字符范围内生成。如果字符串内本身含有“:”、“<”、“>”，则需要先在前面加“\”进行转义。

*返回值*

> [String (字符串)] 返回 生成一个指定长度的随机字符串


------------------------------------------------------------

**AB.C.RegMatch() 方法**

*语法*

> Set result = AB.C.RegMatch(string, rule)

*说明*

> 正则表达式编组捕获

*参数*

> 	Variable (变量) result 		返回值，返回的类型将是一个Matches集合
	String string 				待操作的字符串
	String rule 				包含编组的正则表达式

*注释*

> 	使用此方法可以对字符串中的正则编组进行捕获，
	返回一个Matches集合，可以循环此集合，然后用SubMatches集合对匹配的Match对象结果进行操作。

*返回值*

> [Object (对象)] 返回 捕获到的匹配Matches集合


------------------------------------------------------------

**AB.C.RegReplace() 方法**

*语法*

> Set result = AB.C.RegReplace(string, rule, result)

*说明*

> 根据正则表达式替换字符串内容

*参数*

> 	String string 				待替换的字符串
	String rule 				待替换的正则表达式规则
	String result 				需要替换为的结果

*注释*

> 调用此方法将把参数string中的匹配rule参数中正则表达式规则的字符串替换为参数result中的值。

*举例*

> AB.C.Print AB.C.RegReplace("AspBox的默认实例是AB。","A(sp)?[Bb]ox", "AspBox v2.0")

*输出*

> AspBox v2.0的默认实例是AspBox v2.0

*返回值*

> [String (字符串)] 返回 替换后的字符串


------------------------------------------------------------

**AB.C.RegReplaceM() 方法**

*语法*

> Set result = AB.C.RegReplaceM(string, rule, result)

*说明*

> 根据正则表达式替换字符串内容

*参数*

> 	String string 				待替换的字符串
	String rule 				待替换的正则表达式规则
	String result 				需要替换为的结果

*注释*

> 	调用此方法同 AB.C.RegReplace 方法基本一致，唯一的区别是正则表达式的Multiline(多行)属性为真。
	在多行模式下，正则表达式将在每一行中匹配结果，包括^（起始）和$（结束）标记。

*返回值*

> [String (字符串)] 返回 根据正则代表式替换文本（多行模式）


------------------------------------------------------------

**AB.C.Str() 方法**

*语法*

> Set result = AB.C.Str(string, array)

*说明*

> 格式化带变量的复杂字符串

*参数*

> 	String string 				包含占位符的字符串
	Array array 				占位符的实际值，{1}对应数组的第1个元素，以此类推

*注释*

> 	调用此方法可以将一个包含复杂变量的字符串通过直观的方式呈现。
	string参数中包含的{1}、{2}等占位符将被替换为array参数中相应的元素的值，并返回一个新的字符串

*举例*

> 	Dim a,b,c
	a = "魏小杠"
	b = "user/weixiaogang"
	c = "Miss"
	AB.C.Print AB.C.Str("<{4}><a href=""/{2}.html"">{3}.{1}</a></{4}>",Array(a,b,c,"div"))

*输出*

> <div><a href="/user/weixiaogang.html">Miss.魏小杠</a></div>

*返回值*

> [String (字符串)] 返回 重新组合后的新字符串


------------------------------------------------------------

**AB.C.Test() 方法**

*语法*

> bool = AB.C.Test(string, rule)

*说明*

> 根据正则表达式验证数据合法性

*参数*

> 	String string 		待验证的字符串
	String rule 		可以是正则表达式，也可以是以下字符串： 
						"date" - 验证是否为合法的日期格式 
						"idcard" - 验证是否为合法的身份证号码 
						"english" - 验证是否只包含英文字母 
						"chinese" - 验证是否只包含中文字符 
						"username" - 验证是否是合法的用户名（4-20位，只能是大小写字母数字及下划线且以字母开头） 
						"email" - 验证是否是合法的E-mail地址 
						"int" - 验证是否为整数 
						"number" - 验证是否为数字 
						"double" - 验证是否为双精度数字 
						"price" - 验证是否为价格格式 
						"zip" - 验证是否为合法的邮编 
						"qq" - 验证是否为合法的QQ号 
						"phone" - 验证是否为合法的电话号码 
						"mobile" - 验证是否为合法的手机号码 
						"url" - 验证是否为合法的URL地址 
						"ip" - 验证是否为合法的IP地址 

*注释*

> 	调用此方法将根据指定的规则验证参数string的合法性，验证通过则返回真(True)，不合法则返回假(False)。 
	P.S. 感谢我佛山人的正则验证代码，此方法中正则表达式均取自佛子的Validate v4.0)

*举例*

> 	Dim testDate, testStr
	testDate = "2008-2-30 12:00"
	testStr = "M20"
	AB.C.WR AB.C.Test(testDate,"date")		'输出False
	AB.C.WR AB.C.Test(testStr,"^\w[1-4][0-9]")	'输出True

*返回值*

> [String (字符串)] 返回 验证通过则返回真(True)，不合法则返回假(False)


------------------------------------------------------------