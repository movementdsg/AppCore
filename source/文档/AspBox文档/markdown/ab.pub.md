# AspBox Pub 核心

<table>
	<tr>
		<td>文件：</td>
		<td>Cls_AB.asp</td>
	</tr>
	<tr>
		<td>类名：</td>
		<td>Class Cls_AB_Pub</td>
	</tr>
</table>


------------------------------------------------------------

**AB.Pub.FixAbsPath() 方法**

*说明*

> 补充路径的前后 / 符号 （前后如果没有 / 的话）

*语法*

> path = AB.Pub.FixAbsPath(spath)

*参数*

> 	String spath ： 路径

*返回值*

> [String字符串]


------------------------------------------------------------

**AB.Pub.InArray() 方法**

*说明*

> 判断一个值是否存在于数组，若非数组则返回False

*语法*

> bool = AB.Pub.InArray(str,arr)

*参数*

> 	String str ： 字符串或数字
 	Array arr ： 数组

*举例*

> 	AB.C.Print AB.Pub.InArray("2", Array("1","2","3")) '输出：True

*返回值*

> [Boolean布尔值]


------------------------------------------------------------

**AB.Pub.Include() 方法**

*说明*

> 服务器端动态包含文件

*语法*

> AB.Pub.Include filePath

*别名*

> AB.C.Include filePath

*参数*

> 	String filePath ： 要包含的文件路径
	文件路径可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	此方法实现了ASP服务器端的动态包含，可以完全替换ASP中用<!--#include ... -->包含文件的方法，
	而且可以根据条件进行动态包含，不会预读任何页面信息。
	此方法有以下特点：
	1 - 支持动态包含ASP、Html、Txt等一切文本文件（相对路径和绝对路径均可），而且会自动执行其中的ASP语句（< % % >标记内的代码）；
	2 - 支持无限级的ASP原始预包含文件方法<!--#include ... -->，也就是#include中的#include（支持无限级#include）也可以自动包含；
	3 - 自动识别ASP原始预包含文件方法<!--#include ... -->中的相对路径和绝对路径；
	4 - 自动过滤被包含的ASP文件的源代码中出现的@LANGUAGE、@CODEPAGE及Option Explicit等会引起ASP报错的内容。
	如果要载入不同于当前文件编码的文档，可以先设置 AB.CharSet 属性为相应的编码类型。

*举例*

> 略

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Pub.xInclude() 方法**

*说明*

> 服务器端动态包含文件，用法和效果同 AB.Pub.Include()，唯一区别是文件路径若不存在不抛错。

*语法*

> AB.Pub.xInclude filePath

*返回值*

> [无返回值]


------------------------------------------------------------

**AB.Pub.IncCode() 方法**

*说明*

> 获取文件代码。

*语法*

> strCode = AB.Pub.IncCode ( filePath )

*参数*

> 	String filePath ： 文件路径

*返回值*

> [String字符串]


------------------------------------------------------------

**AB.Pub.IncXCode() 方法**

*说明*

> 获取文件代码。 与 AB.Pub.IncCode() 方法唯一区别是 返回 带 Abx_s_html 的赋值变量。

*语法*

> strCode = AB.Pub.IncXCode ( filePath )

*参数*

> 	String filePath ： 文件路径

*返回值*

> [String字符串]


------------------------------------------------------------

**AB.Pub.GetIncCode() 方法**

*说明*

> 获取文件代码。 

*语法*

> strCode = AB.Pub.GetIncCode ( filePath,  getHtml )

*参数*

> 	String filePath ： 文件路径
	Boolean getHtml : 是否返回带 Abx_s_html 的赋值变量
	当 getHtml = 1 ,等同： strCode = AB.Pub.IncXCode ( filePath )
	当 getHtml = 0 ,等同： strCode = AB.Pub.IncCode ( filePath )

*返回值*

> [String字符串]


------------------------------------------------------------

**AB.Pub.Read() 方法**

*说明*

> 获取文件内容Asp代码。 

*语法*

> strCode = AB.Pub.Read ( filePath )

*参数*

> 	String filePath ： 文件路径

*返回值*

> [String字符串]


------------------------------------------------------------

**AB.Pub.IncRead() 方法**

*说明*

> 	获取文件内容Asp代码。 
	区别 AB.Pub.Read() 方法 在于 递归替换所有 <!--#include (file|virtual)="路径"--> 里面的文件，直至最终的 Asp代码

*语法*

> strCode = AB.Pub.IncRead ( filePath )

*参数*

> 	String filePath ： 文件路径

*返回值*

> [String字符串]


------------------------------------------------------------