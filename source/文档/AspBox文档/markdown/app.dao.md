
# Active Record 数据查询


*引言：*

> 	很多PHP框都自带 Active Record 用于方便查询和更新数据，
	省去了写过多繁琐的原生态SQL查询语句，项目维护更加方便。
	现在我们也模拟应用到AppCore上，使用Active Record可以大大降低项目开发难度。

------------------------------------------------------------

## 获取查询数据 ##

<p><a name="getsql"></a></p>

- **App.Dao.getSQL() 方法：**

*语法*

> sql = App.Dao.getSQL()

*别名：*

> sql = App.Dao.lastSQL()

*说明：*

> 查看输出SQL语句

*返回值*

> [String (字符串)]

*例 ：*

> Response.Write M("user")().Top(10).getSQL

------------------------------------------------------------

<p><a name="query"></a></p>

- **App.Dao.Query() 方法：**

*语法*

> Set Rs = App.Dao.Query(sql)

*参数*

> String sql 			SQL查询语句

*说明：*

> 获取SQL语句的查询数据

*返回值*

> [Object (Rs记录集)]

*例 ：*

> 	dim rs
	Set rs = App.Dao.Query("select top 10 id,username from #@user")
	'等同于： Set rs = App.Dao.Query("select top 10 id,username from "& App.Dao.tbPrefix &"media")

------------------------------------------------------------

<p><a name="list"></a></p>

- **App.Dao.List() 方法：**

*语法*

> arr = App.Dao.List()

*说明：*

> 获取查询数据二维数组(Array)列表

*返回值*

> [Array (二维数组)]

*例 ：*

> 	dim list
	list = M("user")().select("id, username").top(10).List()
	ab.trace list '返回一个二维数组
	dim i, id, username
	For i = 0 To Ubound(list,2)
		id = list(0, i)
		username = list(1, i)
		Response.Write id & " : " & username & "<br>"
	Next

------------------------------------------------------------

<p><a name="result"></a></p>

- **App.Dao.Result() 方法：**

*语法*

> Set rs = App.Dao.Result()

*别名：*

> Set rs = App.Dao.GetRs() 或 Set rs = App.Dao.Fetch()

*说明：*

> 获取查询记录集

*返回值*

> [Object (Rs记录集)]

*例 ：*

> 	dim Rs
	Set Rs = M("user")().select("id, username").top(10).Result()
	ab.trace Rs
	Do While not Rs.eof
		Response.Write Rs("id") & " : " & Rs("username") & "<br>"
		Rs.MoveNext
	Loop

------------------------------------------------------------

<p><a name="row"></a></p>

- **App.Dao.Row() 方法：**

*语法*

> Set rs = App.Dao.Row(n)

*说明：*

> 取得第n+1行数据, n=0 表示取第一行数据

*参数*

> Integer n          参数n表示位置偏移量offset

*返回值*

> [Object (Rs记录集)]

*例 ：*

> 	dim Rs
	Set Rs = App.Dao.Sql("select top 10 id,username from #@user").Row(0) '获取第1行数据
	Set Rs = App.Dao.Sql("select top 10 id,username from #@user").Row(4) '获取第5行数据
	If Not Rs.Eof Then
		Response.Write Rs("id") & " : " & Rs("username")
	End If

------------------------------------------------------------

<p><a name="getfield"></a></p>

- **App.Dao.getField() 方法：**

*语法*

> val = App.Dao.getField(field)

*说明：*

> 取得某字段值

*参数*

> String field          字段名

*返回值*

> [Integer|String|Boolean (字段值)]

*例 ：*

> 	dim username
	'username = M("user")().Where("id=5").getField("username")
	username = M("user")().Find(5).getField("username")

------------------------------------------------------------
 
<p><a name="query_condition"></a></p>

## 查询条件 ##

AppCore内置了非常灵活的查询方法，可以快速的进行数据查询操作，查询条件可以用于CURD等任何操作，作为where方法的参数传入即可，下面来一一讲解查询语言的内涵。

### 查询方式 ###

AppCore可以支持直接使用字符串作为查询条件，但是大多数情况推荐使用数组或者字典来作为查询条件，代码逻辑会更清晰简洁。

**一、使用字符串作为查询条件**

这是最传统的方式，例如：

	Set Model = M("User") '实例化Model
	Set Rs = Model().Where("type=1 AND status=1").GetRs() 
	'最后生成的SQL语句是
	SELECT * FROM lx_user WHERE type=1 AND status=1

**二、使用数组作为查询条件**

使用数组方式，可以进行条件控制，书写代码逻辑更加清晰，例如：

*例 1：*

	Set Model = M("User") '实例化Model
	dim a_where(2)
	a_where(0) = "id>5"
	a_where(1) = "reg_time>#2014#"
	a_where(2) = "or score>10"
	'把查询条件传入查询方法
	Set Rs = Model().Where(a_where).Fetch()
	'最后生成的SQL语句是
	SELECT * FROM lx_user WHERE id>5 AND time>#2014# OR score>10

*例 2：*

	Set Model = M("User") '实例化Model
	dim a_where : a_where = Array()
	a_where = ab.a.push(a_where, "id>5")
	a_where = ab.a.push(a_where, "reg_time>#2014#")
	a_where = ab.a.push(a_where, "or score>10")
	'把查询条件传入查询方法
	Set Rs = Model().Where(a_where).Fetch()
	'最后生成的SQL语句是
	SELECT * FROM lx_user WHERE id>5 AND time>#2014# OR score>10

*例 3：*

	a_where = Array("id>5", "email:lajox@19www.com", "reg_time>#2014#")
	Set Rs = M("User")().Where(a_where).Fetch()
	'最后生成的SQL语句是
	SELECT * FROM lx_user WHERE id>5 AND email='lajox@19www.com' AND reg_time>#2014#

<table border="0" cellspacing="1" cellpadding="0">
    <tbody>
    <tr>
        <th>表达式</th>
        <th>含义</th>
    </tr>
    <tr>
        <td> : 或 = </td>
        <td>等于（=）</td>
    </tr>
    <tr>
        <td> <= </td>
        <td>小于等于（<=）</td>
    </tr>
    <tr>
        <td> >= </td>
        <td>大于等于（>=）</td>
    </tr>
    <tr>
        <td> <> 或 != </td>
        <td>不等于（<>）</td>
    </tr>
    <tr>
        <td> > </td>
        <td>大于（>）</td>
    </tr>
    <tr>
        <td> < </td>
        <td>小于（<）</td>
    </tr>
    </tbody>
</table>


**三、使用字典对象来查询**

使用字典方式，可以进行条件控制，书写代码逻辑更加清晰，例如：

	Set Model = M("User") '实例化Model
	Set d_where = AB.C.Dict() '创建字典对象
	d_where("reg_time<=") = "#"& CDate(Now()) & "#"
	d_where("status") = 1
	d_where("username[%]") = "%你%"
	'把查询条件传入查询方法
	Set Rs = Model().Where(d_where).Fetch()
	'最后生成的SQL语句是
	SELECT * FROM lx_user WHERE reg_time<=#2014/11/14# AND status=1 AND username LIKE '%你%'

<table border="0" cellspacing="1" cellpadding="0">
    <tbody>
    <tr>
        <th>表达式</th>
        <th>含义</th>
    </tr>
    <tr>
        <td>[:] 或 [=] 或 都不带</td>
        <td>等于（=）</td>
    </tr>
    <tr>
        <td>[<=]</td>
        <td>小于等于（<=）</td>
    </tr>
    <tr>
        <td>[>=]</td>
        <td>大于等于（>=）</td>
    </tr>
    <tr>
        <td>[<>] 或 [!=]</td>
        <td>不等于（<>）</td>
    </tr>
    <tr>
        <td>[>]</td>
        <td>大于（>）</td>
    </tr>
    <tr>
        <td>[<]</td>
        <td>小于（<）</td>
    </tr>
    <tr>
        <td>[like] 或 [%] 或 [*]</td>
        <td>LIKE模糊查询</td>
    </tr>
    <tr>
        <td>[in] 或 []</td>
        <td>区间查询</td>
    </tr>
    <tr>
        <td>+n （n为任一数字）</td>
        <td>表达式查询，支持SQL语法</td>
    </tr>
    </tbody>
</table>

--------------------------------------------

<p><a name="field"></a></p>

### 函数方法 ###

- **App.Dao.Field() 方法：**

*语法*

> Set dao = App.Dao.Field(field)

*别名*

> Set dao = App.Dao.Select(field)

*说明：*

> 构建Select查询条件

*参数*

> String field          字段名（多个字段名用 , 隔开； * 号为取出表的全部字段）

*返回值*

> [Object (模型对象)]

*例 ：*

> 	Set Rs = M("user")().Field("*").Fetch()
	Set Rs = M("user")().Field("id, username").Fetch()
	Set Rs = App.Dao.T("user").Field("id, username").Fetch()
	'最后生成的SQL语句是
	SELECT id, username FROM lx_user

<p><a name="from"></a></p>

- **App.Dao.From() 方法：**

*语法*

> Set dao = App.Dao.From(table)

*别名*

> Set dao = App.Dao.T(table)

*说明：*

> 构建FROM查询条件

*参数*

> String table          表名

*返回值*

> [Object (模型对象)]

*例 ：*

> 	dim dao
	'Set rs = M("")().Query("select * from #@user")
	Set rs = App.Dao.T("User").Fetch()
	'最后生成的SQL语句是
	SELECT * FROM lx_user

<p><a name="alias"></a></p>

- **App.Dao.Alias() 方法：**

*语法*

> Set dao = App.Dao.Alias(alias)

*别名*

> Set dao = App.Dao.A(alias)

*说明：*

> 用于设置表的别名

*参数*

> String alias          表的别名

*返回值*

> [Object (模型对象)]

*例 ：*

> 	Set rs = M("User")().A("u").Field("u.\*").Where("u.id<10").Fetch()
	'最后生成的SQL语句是
	SELECT u.\* FROM lx_user u WHERE u.id<10
	


