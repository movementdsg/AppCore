# AspBox A 核心

<table>
	<tr>
		<td>文件：</td>
		<td>ab.a.asp</td>
	</tr>
	<tr>
		<td>类名：</td>
		<td>Class Cls_AB_A</td>
	</tr>
</table>


------------------------------------------------------------

**AB.A.Max() 方法**

*语法*

> n = AB.A.Max(arr)

*说明*

> 取出数组元素最大值(所有数组元素均须为数字类型)

*参数*

> Array arr          数组

*举例*

> ab.c.print AB.A.Max(array(1, 5.5, 3.2)) '=> 5.5

*返回值*

> [数字类型]


------------------------------------------------------------

**AB.A.Min() 方法**

*语法*

> n = AB.A.Min(arr)

*说明*

> 取出数组元素最小值(所有数组元素均须为数字类型)

*参数*

> Array arr          数组

*举例*

> ab.c.print AB.A.Min(array(1, 5.5, 3.2)) '=> 1

*返回值*

> [数字类型]


------------------------------------------------------------

**AB.A.Sum() 方法**

*语法*

> n = AB.A.Sum(arr)

*说明*

> 计算所有数组元素相加总和

*参数*

> Array arr          数组

*举例*

> ab.c.print AB.A.Sum(array(1, 2.5, 1.6)) '=> 5.1

*返回值*

> [数字类型]


------------------------------------------------------------

**AB.A.Avg() 方法**

*语法*

> n = AB.A.Avg(arr)

*说明*

> 计算所有数组元素的平均数

*参数*

> Array arr          数组

*举例*

> ab.c.print AB.A.Avg(array(1, 2.5, 1.6)) '=> 1.7

*返回值*

> [数字类型]


------------------------------------------------------------

**AB.A.First() 方法**

*语法*

> n = AB.A.First(arr)

*说明*

> 取得数组第一个元素值

*参数*

> Array arr          数组

*举例*

> 	Dim o : Set o = AB.Dict '字典对象
	ab.trace AB.A.First(array("abcd", o, 5)) '=> "abcd"
	Dim x : Set x = AB.A.First(array(o, "abcd", 5)) '=> 对象 o
	AB.Trace x

*说明*

> 若参数为空数组或非数组则返回空值

*返回值*

> [Anything (任意值)]


------------------------------------------------------------

**AB.A.End() 方法**

*语法*

> n = AB.A.End(arr)

*说明*

> 取得数组最后一个元素值

*参数*

> Array arr          数组

*举例*

> 	Dim o : Set o = AB.Dict '字典对象
	ab.trace AB.A.First(array("abcd", o, 5)) '=> 5
	Dim x : Set x = AB.A.End(array("abcd", 5, o)) '=> 对象 o
	AB.Trace x

*说明*

> 若参数为空数组或非数组则返回空值

*返回值*

> [Anything (任意值)]


------------------------------------------------------------

**AB.A.Clone() 方法**

*语法*

> newArr = AB.A.Clone(arr)

*说明*

> 实现数组的克隆(拷贝)

*参数*

> Array arr          (原数组)

*举例*

> 	dim a1, a2
	a1 = array("a","b","c")
	a2 = array("x","y",3)
	a2 = ab.a.clone(a1)
	ab.trace(a2) '=> Array("a","b","c")

*说明*

> 对数组 arr 进行克隆，生成新数组(动态数组,可随时新增元素)

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.UnShift() 方法**

*语法*

> newArr = AB.A.UnShift(arr, var)

*说明*

> 向数组中从前压入元素

*参数*

> 	Array arr 			数组
	MixType var 		新元素

*举例*

> ab.trace ab.a.UnShift(array("a", "b", "c"), "d") '=> Array("d", "a", "b", "c")

*说明*

> 向数组中从前压入新元素(即新元素为新数组的第一个元素)，生成新数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Shift() 方法**

*语法*

> newArr = AB.A.Shift(arr)

*说明*

> 向数组中从前删除第一个元素

*参数*

> Array arr 			数组

*举例*

> ab.trace ab.a.Shift(array("a", "b", "c", "d")) '=> Array("b", "c", "d")

*说明*

> 向数组中从前删除元素，即删除数组的第一个元素，生成新数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Push() 方法**

*语法*

> newArr = AB.A.Push(arr, var)

*说明*

> 向数组中从后压入元素

*参数*

> 	Array arr 			数组
	MixType var 		新元素

*举例*

> ab.trace ab.a.Push(array("a", "b", "c"), "d") '=> Array("a", "b", "c", "d")

*说明*

> 向数组中从前删除元素，即删除数组的第一个元素，生成新数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Pop() 方法**

*语法*

> newArr = AB.A.Pop(arr)

*说明*

> 向数组中从后删除最后一个元素

*参数*

> Array arr 			数组

*举例*

> ab.trace ab.a.Pop(array("a", "b", "c", "d")) '=> Array("a", "b", "c")

*说明*

> 向数组中从后删除最后一个元素，生成新数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Strip() 方法**

*语法*

> arr = AB.A.Strip(str)

*说明*

> 返回以(,)分割成的数组

*参数*

> String str 			字符串

*举例*

> ab.trace ab.a.Strip("a,b,c,d,") '=> Array("a", "b", "c", "d")

*说明*

> 	去除字符串最右边和最左边的(,)符号,并返回以(,)分割成的数组
	去除如(1,2,3,)或(,1,2,3)最右边和最左边的(,)符号,并返回以(,)分割成的数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Walk() 方法**

*语法*

> newArr = AB.A.Walk(arr, f)

*说明*

> 对数组内元素执行函数后返回新数组

*参数*

> 	Array arr 			数组
	[string | mix-array] : (字符串 或 混合型数组)
	参数f是字符串，为要执行的函数名
	参数f是数组，则第一个元素为要执行的函数名，第二个元素做为执行函数的第二个以上参数数组

*举例*

> 	ab.trace ab.a.Walk(array("ab", "cde", "fghi"), "len") '=> Array(2, 3, 4)
	ab.trace ab.a.Walk(array("ab", "cde", "fghi"), array("len")) '=> Array(2, 3, 4)
	Function foo(a, b, c) : foo = a + b + c : End Function
	ab.trace ab.a.Walk(array(1, 2, 3), array("foo", array(2, 3)) ) '=> Array(6, 7, 8)

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Sub() 方法**

*语法*

> newArr = AB.A.Sub(arr, start, over)

*说明*

> 截取元素(按索引范围)

*参数*

> 	Array arr 			数组
	Integer start		start index
	Integer over		over index
	当结束索引 over 值为-1时, 表示截取元素范围从开始索引 index 开始直到结束。

*举例*

> 	ab.trace ab.a.Sub(array("a","b","c","d"), 0, 0) '=> Array("a")
	ab.trace ab.a.Sub(array("a","b","c","d"), 1, 2) '=> Array("b", "c")
	ab.trace ab.a.Sub(array("a","b","c","d"), -1, 2) '=> Array("a","b","c")
	ab.trace ab.a.Sub(array("a","b","c","d"), 0, -1) '=> Array("a", "b", "c", "d")

*说明*

> (根据索引范围:从开始索引 start 到 结束索引 over)从一个数组中截取一部分元素(一个或多个)并返回新数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Slice() 方法**

*语法*

> newArr = AB.A.Slice(arr, start, length)

*说明*

> 截取元素(从起始索引按长度截取)

*参数*

> 	Array arr 			数组
	Integer start		start index
	Integer over		over index
	当结束索引 over 值为-1时, 表示截取元素范围从开始索引 index 开始直到结束。

*举例*

> 	ab.trace ab.a.Slice(array("a","b","c","d"), 1, 2) '=> Array("b", "c")
	ab.trace ab.a.Slice(array("a","b","c","d"), 2, 5) '=> Array("c", "d")
	ab.trace ab.a.Slice(array("a","b","c","d"), 0, 2) '=> Array("a", "b")
	ab.trace ab.a.Slice(array("a","b","c","d"), 0, 0) '=> Array("a", "b", "c", "d")
	ab.trace ab.a.Slice(array("a","b","c","d","e"), 3, -2) '=> Array("d", "c", "b")

*说明*

> 	(根据指定的索引范围)从一个数组中截取一部分元素(一个或多个)并返回新数组
	start>=0时,为正常截取(从索引值为start处开始截取)返回数组
	strat<0时,从索引值[数组长度m-(-strat)处]开始截取返回数组
	length>0时,从索引值start处开始截取到start+length处截取返回数组
	length=0时,从索引值start处开始截取到结束截取返回数组
	length<0时,从索引值start处开始向左截取到start-(-length)结束截取返回数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Splice() 方法**

*语法*

> newArr = AB.A.Splice(arr, start, length)

*说明*

> 移除数组部分元素(根据指定的索引范围)

*参数*

> 	Array arr 			数组
	Integer start		start index
	Integer length		length
	当start=0 和 length=0 时，返回空数组。

*举例*

> 	ab.trace ab.a.Splice(array("a","b","c","d"), 1, 2) '=> Array("a", "d")
	ab.trace ab.a.Splice(array("a","b","c","d"), 2, 5) '=> Array("a", "b")
	ab.trace ab.a.Splice(array("a","b","c","d"), 0, 2) '=> Array("c", "d")
	ab.trace ab.a.Splice(array("a","b","c","d"), 0, 0) '=> Array()
	ab.trace ab.a.Splice(array("a","b","c","d","e"), 3, -1) '=> Array("a", "b", "e")

*说明*

> 	(根据指定的索引范围)从一个数组中移除一部分元素(一个或多个)并返回新数组
	start>=0时,为正常(从索引值为start处开始)部分删除后返回的数组
	start<0时,从索引值[数组长度m-(-start)处]开始部分删除后返回的数组
	length>0时,从索引值start处开始到start+length处截取返回的数组
	length=0时,从索引值start处开始到结束部分删除后返回的数组
	length<0时,从索引值start处开始向左到start-(-length)结束部分删除后返回的数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.SpliceX() 方法**

*语法*

> newArr = AB.A.SpliceX(arr, start, length, elem)

*说明*

> 替换数组部分元素(根据指定的索引范围)

*参数*

> 	Array arr 			数组
	Integer start		start index
	Integer length		length
	MixType elem		[Array | Object|String|Other.. | Null] 填充元素
	当 elem参数 为 Array 时, 表示填充批量元素组
	当 elem参数 为 Object|String|Other.. 时, 表示填充单个元素
	当 elem参数 为 null 时, 不填充元素, 等效于 AB.A.Splice(arr, start, length) 方法

*举例*

> 	ab.trace ab.a.SpliceX(array("a","b","c","d"), 1, 2, array("x","y")) '=> Array("a", "x", "y", "d")
	ab.trace ab.a.SpliceX(array("a","b","c","d"), 2, 5, array("x","y")) '=> Array("a", "b", "x", "y")
	ab.trace ab.a.SpliceX(array("a","b","c","d"), 0, 2, array("x","y")) '=> Array("x", "y", "c", "d")
	ab.trace ab.a.SpliceX(array("a","b","c","d"), 0, 0, array("x","y")) '=> Array("x", "y")
	ab.trace ab.a.SpliceX(array("a","b","c","d","e"), 3, -1, array("x","y")) '=> Array("a", "b", "x", "y", "e")
	ab.trace ab.a.SpliceX(array("a","b","c","d","e"), -1, 0, "x") '=> Array("a","b","c","d","x")
	ab.trace ab.a.Concat(array("a","b","c","d","e"), array("x","y")) '=> Array("a","b","c","d","e","x","y")
	ab.trace ab.a.Fill(array("a","b","c","d","e"), 1, "x") '=> Array("a","x","b","c","d","e")
	ab.trace ab.a.Fill(array("a","b","c","d","e"), 1, Array("x","y")) '=> Array("a","x","y","b","c","d","e")

*说明*

> 	(根据指定的索引范围)从一个数组中将一部分元素(一个或多个)用填充元素代替并返回新数组
	start>=0时,为正常(从索引值为start处开始)部分删除后返回的数组
	start<0时,从索引值[数组长度m-(-start)处]开始部分删除后返回的数组
	length>0时,从索引值start处开始到start+length处截取返回的数组
	length=0时,从索引值start处开始到结束部分删除后返回的数组
	length<0时,从索引值start处开始向左到start-(-length)结束部分删除后返回的数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Fill() 方法**

*语法*

> newArr = AB.A.Fill(arr, index, elem)

*说明*

> 插入元素到数组（填充数组）

*参数*

> 	Array arr 			数组
	Integer index		索引
	MixType elem		[Array | Object|String|Other.. | Null] 填充元素
	当 elem参数 为 Array 时, 表示填充批量元素组
	当 elem参数 为 Object|String|Other.. 时, 表示填充单个元素

*举例*

> 	ab.trace ab.a.Fill(array("a", "b", "c"), 1, "d") '=> Array("a", "d", "b", "c")
	ab.trace ab.a.Fill(array("a", "b", "c"), 5, "d") '=> Array("a", "b", "c", "", "", "d")
	ab.trace ab.a.Fill(array("a", "b", "c"), -2, "d") '=> Array("d", "", "a", "b", "c")

*说明*

> 	插入元素到数组（填充数组）
	当 elem参数 为 Array 时, 表示填充批量元素组
	当 elem参数 为 Object|String|Other.. 时, 表示填充单个元素

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Concat() 方法**

*语法*

> newArr = AB.A.Concat(arr1,arr2)

*说明*

> 将两个数组合并成一个数组(新数组)

*参数*

> 	Array arr1 			数组
	Array arr2 			数组

*举例*

> 	ab.trace ab.a.Concat(array("a","b","c"), array("c","d")) '=> Array("a","b","c","c","d")
	ab.trace ab.a.Concat(array("a","","","b"), array("d","","e")) '=> Array("a","","","b","d","","e")

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Merge() 方法**

*语法*

> newArr = AB.A.Merge(arr1,arr2)

*说明*

> 将两个数组合并成一个数组且交叉部分(相同元素部分)只保留一个(新数组)

*参数*

> 	Array arr1 			数组
	Array arr2 			数组

*举例*

> 	ab.trace ab.a.Merge(array("a","b","c"), array("c","d")) '=> Array("a","b","c","d")
	ab.trace ab.a.Merge(array("a","","","b"), array("d","","e")) '=> Array("a","","","b","d","e")

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Cover() 方法**

*语法*

> newArr = AB.A.Cover(arr1,arr2)

*说明*

> 由后面的数组arr2覆盖前面的数组arr1，成为新的数组

*参数*

> 	Array arr1 			数组
	Array arr2 			数组

*举例*

> 	ab.trace ab.a.cover(array("a","b","c"), array("d","e","f")) '=> Array("d","e","f")
	ab.trace ab.a.cover(array("a","b"), array("c","d","e")) '=> Array("c","d","e")
	ab.trace ab.a.cover(array("a","b","c"), array("d","e")) '=> Array("d","e","c")

*说明*

> 	由后面的数组arr2覆盖前面的数组arr1。
	当arr2数组元素个数不足arr1，保留arr1超过arr2上标部分
	当arr2数组元素个数超过arr1，函数返回arr2数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Remove() 方法**

*语法*

> newArr = AB.A.Remove(arr, elem)

*别名*

> newArr = AB.A.Del(arr, elem)

*说明*

> 按指定值移除数组元素

*参数*

> 	Array arr 			数组
	MixType elem		要移除的元素值

*举例*

> ab.trace ab.a.del(array("a", "b", "c", "b"),"b") '=> Array("a", "c")

*说明*

> 	由后面的数组arr2覆盖前面的数组arr1。
	当arr2数组元素个数不足arr1，保留arr1超过arr2上标部分
	当arr2数组元素个数超过arr1，函数返回arr2数组

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Unset() 方法**

*语法*

> newArr = AB.A.Unset(arr, index)

*说明*

> 按指定索引移除数组元素

*参数*

> 	Array arr 			数组
	Integer index		索引

*举例*

> ab.trace ab.a.unset(array("a", "b", "c", "b"),1) '=> Array("a", "c", "b")

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Unique() 方法**

*语法*

> newArr = AB.A.Unique(arr)

*说明*

> 移除重复(多余)的元素

*参数*

> Array arr 			数组

*举例*

> ab.trace ab.a.Unique(array("a", "b", "c", "b")) '=> Array("a", "b", "c")

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Reverse() 方法**

*语法*

> newArr = AB.A.Reverse(arr)

*说明*

> 数组元素反向排序（翻转数组）

*参数*

> Array arr 			数组

*举例*

> ab.trace ab.a.Reverse(array("a", "b", "c", "d")) '=> Array("d", "c", "b", "a")

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Range() 方法**

*语法*

> newArr = AB.A.Range(low, high, step)

*说明*

> 建立一个包含指定范围单元的数组

*参数*

> 	Array arr 			数组
	Integer low			从low开始
	Integer high		到high结束
	Integer step		(数字 或 空值) 作为单元之间的步进值，step 应该为正值。
						如果step值未指定(值为空)，则默认为 1

*举例*

> 	ab.trace ab.a.range(2, 5, 1) '=> Array(2, 3, 4, 5)
	ab.trace ab.a.range(1, 6, 2) '=> Array(1, 3, 5)
	ab.trace ab.a.range(6, 1, 2) '=> Array(6, 4, 2)
	ab.trace ab.a.range(6, 1, -2) '=> Array(6, 4, 2) '步进值虽为-2 实际上可理解为步退值2
	ab.trace ab.a.range("a", "e", 1) '=> Array("a", "b", "c", "d", "e")
	ab.trace ab.a.range("F", "A", 2) '=> Array("F","D","B") 即对于有序的ABCDEFG从F倒序到A,步阶为2

*说明*

> 	建立一个包含指定范围单元的数组, 步进值默认为 1
	返回数组中从 low 到 high 的单元，包括它们本身;
	如果 low > high，则序列将从 high 到 low
	如果 low > high，则序列将从 high 到 low

*返回值*

> [Array (数组)]


------------------------------------------------------------

**AB.A.Rand() 方法**

*语法*

> newArr = AB.A.Rand(arr, num)

*说明*

> 对数组进行乱序组合新的数组，可定义新数组元素个数

*参数*

> 	Array arr 			数组
	Integer num			新数组的元素个数

*举例*

> ab.trace ab.a.Rand(array("a", "b", "c"), 5) '=> Array("a", "a", "c", "a", "c")

*说明*

> 对数组进行乱序组合新的数组，可定义新数组元素个数

*返回值*

> [Array (数组)]


------------------------------------------------------------