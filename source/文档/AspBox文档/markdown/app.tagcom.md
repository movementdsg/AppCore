
# 公共静态标签

------------------------------------------------------------

<p><a name="nolayout"></a></p>

- **标签 {\_\_NOLAYOUT\_\_} 或 \_\_NOLAYOUT__**

> 除了在 layerout.html 以外的其他文件中设置，只要有这个标签，则不会载入 layerout.html 文件

*模板演示：*

> 	ACTION后台代码：
	App.View.Display("index.html")

> 	layerout.html 文件内容：
	{template 'common/header.html'}
	{\_\_CONTENT\_\_}
	{template 'common/footer.html'}

> 	index.html 文件内容：
	{\_\_NOLAYOUT\_\_}
	&lt;html xmlns="http://www.w3.org/1999/xhtml">
	&lt;head>
	&lt;title>测试地址&lt;/title>
	&lt;/head>
	&lt;body>
	内容...
	&lt;/body>
	&lt;/html>

------------------------------------------------------------

<p><a name="content"></a></p>

- **标签 {\_\_CONTENT\_\_} 或 \_\_CONTENT__**

> layerout.html 里面设置，表示正常的替代内容

------------------------------------------------------------

<p><a name="charset"></a></p>

- **标签 {\_\_CHARSET\_\_} 或 \_\_CHARSET__**

> 等效：{:App.CharSet}， 输出 页面编码

------------------------------------------------------------

<p><a name="url"></a></p>

- **标签 {\_\_URL\_\_} 或 \_\_URL__**

> 等效：{:URL()}， 输出 当前页相对地址，带页面Hash参数

------------------------------------------------------------

<p><a name="self"></a></p>

- **标签 {\_\_SELF\_\_} 或 \_\_SELF__**

> 等效：{:SELF()}， 输出 当前页相对地址，且不带页面Hash参数

------------------------------------------------------------

<p><a name="root"></a></p>

- **标签 {\_\_ROOT\_\_} 或 \_\_ROOT__**

> 等效：{:RootPath()}， 输出 获取整个项目所在目录，若在根路径则返回空值

------------------------------------------------------------

<p><a name="rooturl"></a></p>

- **标签 {\_\_ROOTURL\_\_} 或 \_\_ROOTURL__**

> 等效：{:RootURL()}， 输出 当前APP所在路径网址

------------------------------------------------------------

<p><a name="public"></a></p>

- **标签 {\_\_PUBLIC\_\_} 或 \_\_PUBLIC\_\_ 或者 {\_\_STATIC\_\_} 或 \_\_STATIC__**

> 等效：等效：{:App.StaticPath}， 输出：/statics， 文件静态目录

------------------------------------------------------------

<p><a name="js"></a></p>

- **标签 {\_\_JS\_\_} 或 \_\_JS__**

> 等效：{:App.JsPath}， 输出：/statics/js， js文件静态目录

------------------------------------------------------------

<p><a name="css"></a></p>

- **标签 {\_\_CSS\_\_} 或 \_\_CSS__**

> 等效：{:App.CssPath}， 输出：/statics/css， css文件静态目录

------------------------------------------------------------

<p><a name="img"></a></p>

- **标签 {\_\_IMG\_\_} 或 \_\_IMG__**

> 等效：{:App.ImgPath}， 输出：/statics/images， image文件静态目录

------------------------------------------------------------

<p><a name="upload"></a></p>

- **标签 {\_\_UPLOAD\_\_} 或 \_\_UPLOAD__**

> 等效：{:App.UploadPath}， 输出：/statics/uploads， 上传文件静态目录

------------------------------------------------------------

<p><a name="theme"></a></p>

- **标签 {\_\_THEME\_\_} 或 \_\_THEME__**

> 等效：{:App.View.Path_Theme}， 输出：/App/Tpl/default， 模板文件目录

------------------------------------------------------------

<p><a name="tmpl"></a></p>

- **标签 {\_\_TMPL\_\_} 或 \_\_TMPL__**

> 	等效：{:App.View.Path_TMPL}， 
	输出例：/App/Tpl/default/admin， /App/Tpl/default/home 等

------------------------------------------------------------

<p><a name="assets"></a></p>

- **标签 {\_\_ASSETS\_\_} 或 \_\_ASSETS__**

> 	等效：{:App.View.Path_Assets}， 
	输出例：
	/App/Tpl/default/admin/index （当 App.View.Locate = True 时，值包含 ACTION目录）
	或
	/App/Tpl/default/admin （当 App.View.Locate = False 时，值包不含 ACTION目录）

------------------------------------------------------------

<p><a name="m"></a></p>

- **标签 {\_\_M\_\_} 或 \_\_M__**

> 	输出：URL参数m (缺省值为 home)， 
	等同于全局变量 REQ_M 或 GROUP_NAME 或 APP_NAME
	模板里同样可引入标签 {REQ_M} 或 {GROUP_NAME} 或 {APP_NAME}

------------------------------------------------------------

<p><a name="c"></a></p>

- **标签 {\_\_C\_\_} 或 \_\_C__**

> 	输出：URL参数c (缺省值为 index)， 
	等同于全局变量 REQ_C 或 MODULE_NAME
	模板里同样可引入标签 {REQ_C} 或 {MODULE_NAME}

------------------------------------------------------------

<p><a name="a"></a></p>

- **标签 {\_\_A\_\_} 或 \_\_A__**

> 	输出：URL参数a (缺省值为 index)
	等同于全局变量 REQ_A 或 ACTION_NAME
	模板里同样可引入标签 {REQ_A} 或 {ACTION_NAME}

------------------------------------------------------------

<p><a name="token"></a></p>

- **标签 {\_\_TOKEN\_\_} 或 \_\_TOKEN__**

> 表单验证令牌标签


------------------------------------------------------------

<p><a name="note"></a></p>

# 常用模板标签：

- **注释标签：{@note}{/@note}**

> 	{\* 这里是模板单行注释，注意不能嵌套大括号 和 星号，不然不会解析。 \*}
	这行是正常内容。。
	{\* 又是一行注释. \*}
	{@note}
	这里都是多行注释 可以放任意字符 包括 大括号 { }
	...
	{/@note}
	这行是正常内容。。

<p><a name="asp"></a></p>

- **asp代码执行标签：{asp}{/asp}**

> 	{% 这里是单行asp代码.. %}

> 	{asp 这里是单行asp代码.. }

> 	{asp}
	这里写入多行 asp 代码
	{/asp}

> 	或者直接 用 <% %> 写入asp执行代码
	<%
	ab.c.w "hello world!"
	%>

<p><a name="literal"></a></p>

- **原样输出标签： {@literal}{/@literal}**

> `{@literal}{/@literal}`标签, 保持原样输出, 防止模板标签被解析

> 	例如模板里使用花括号可能被解析，要原样输出，可用 {@literal}..{/@literal} 引进来，
	或者用 \\{ \\} 等字符串表示转义。

> 	e.g. 
	{@literal}
	&lt;li>网站名称 <span class="highlight">{site_name}</span>&lt;/li>
	&lt;li>全局配置里面的Title <span class="highlight">{site_title}</span>&lt;/li>
	{/@literal}

<p><a name="func"></a></p>

- **函数标签： {: 函数块 }**

> 	{: ab.e.md5.to8( now() & "hash" ) } 等同于 {asp ab.c.echo ab.e.md5.to8( now() & "hash" ) }
	{:URL()} 等同于 {asp ab.c.echo URL()}
	{:ab.e.md5.to8('a')} 等同于 {asp ab.c.echo ab.e.md5.to8("a") }
	注：括号里的 '' 会智能替换为 ""

<p><a name="include"></a></p>

- **文件包含标签： include 标签 或 template 标签**

> {include 'a.tpl'} 或 {include('a.tpl')} 或 {template('a.tpl')}  或{template:a.tpl}

<p><a name="load"></a></p>

- **载入 css 或 js 标签： {@load href=''} {@css href=''} {@js href=''}**

> 	{@load href='1.css'}
	css 放置的位置 相对于 /statics/css 目录里
	js 放置的位置 相对于 /statics/js 目录里

> 	除此之外另外拓展了 两个标签
	{@css href='1.css'} 加载css
	{@js href='1.js'} 加载js

------------------------------------------------------------

<p><a name="autoplus"></a></p>

**常见标签**

- **a. 自增标签、自减标签**

> 	自增标签：
	{asp i++} {i++} {asp i = i + 1} <% i= i+1 %>
	
> 	自减标签：
	{asp i--} {i--} {asp i = i - 1} <% i= i-1 %>
	
> 	注：以下这个标签不是自增标签
	{i+1} 等同于 <% ab.c.echo (i+1) %>

<p><a name="if"></a></p>

- **b. if条件标签**

> 	{if 条件}
	..
	{else}
	..
	{/if}

> 	{if 条件}
	..
	{elseif 条件}
	..
	{else}
	..
	{/if}

*例 ：*

> 	注： 为了达到和 js形式的 if 语句块，
	特设了 && 等效代替 And
	特设了 || 等效代替 Or
	特设了 != 等效代替 <>
	特设了 == 等效代替 =
	以及 '' 智能代替为 ""

> 	例： {if i>5 && i!=7 || k=='on'} {i} {/if}
	解析为： <% if ( i>5  And  i<>7  Or  k="on" ) Then %> <% AB.C.Echo i %> <% End If %>

<p><a name="switch"></a></p>

- **c. switch开关控制标签**

> 	{switch expression}
	{case 条件值1}
	..
	{case 条件值2}
	..
	{case else}
	..
	{/switch}

> 	例： {asp k=3} {switch k}{case 1} 值为1 {case 2} 值为2 {case else} 值为{k} {/switch}
	解析为： <% k=3 %> <% Select Case k %><% Case 2 %> 值为2 <% Case Else %> 值为<% AB.C.Echo k %> <% End Select %>

<p><a name="foreach"></a></p>

- **d. foreach循环标签**（可循环获取 Array值，Dictionary字典对象 等）

> 	{foreach i d}..{/foreach}
	等效于：
	<%
	foreach i in d
	...
	next
	%>

> 	如循环输出数组信息：
	{asp ary = array(1,3,4,8)}
	{foreach t ary}
		{t}
	{/foreach}

<p><a name="while"></a></p>

- **e. while循环标签**

> 	{while 条件} .. {/while}
	等效于 
	<%
	while 条件
	..
	wend
	%>

<p><a name="loop"></a></p>

- **f. loop循环标签**

> 	{loop 条件} .. {/loop}

> 	{asp i=8}{loop i>0} {i}..{asp i--}{/loop}
	等效于
	<%
	dim i : i = 8
	Do While i>0
	AB.C.Echo i
	..
	i=i-1
	Loop
	%>

<p><a name="for"></a></p>

- **g. for循环标签**

> 	{for(i=0; i<10; i++)} .. {/for}
	等效于
	<%
	for i=0 to 10-1
	..
	next
	%>

> 	另外，
	{for(i=1; i<=10; i=i+2)} {i} .. {/for}
	等效于
	<%
	for i=1 to 10 step 2
	ab.c.echo i
	..
	next
	%>

> 	另外，
	{for(i=10; i<=1; i--)}
		{i}
	{/for}
	等效于
	<%
	for i=10 to 1 step -1
	ab.c.echo i
	..
	next
	%>

------------------------------------------------------------

<p><a name="sql"></a></p>

# sql标签块

**不设置分页情况**

- 【1】 带sql参数：

> 	{$rs sql="SQL语句"}
	  {$rs('id')} {$rs('title')}
	  {$rs['id']} {$rs['title']}
	  {rs('id')} {rs('title')}
	  {rs['id']} {rs['title']}
	{/$rs}

*注：SQL语句中 可以使用变量，形式:  ($var) 或 {$var}*

*e.g.*

> 	{asp str = 'a' }
	{$rs sql="select * from #@user where username like '%($str)%' "}
	  {$rs['id']} {$rs['username']}
	{/$rs}

- 【2】 或者只带table参数(不带sql参数)：

> 	{$rs table="mytable" field="*" where="name like '%a%'" order="id desc"}
		{$rs('id')} {$rs('title')}
	{/$rs}

> 	类似于 {$rs sql="select * from #@mytable where name like '%a%' order by id desc"} {rs['id']} {rs['title']} {/$rs}

*带table参数中，field参数、where参数、order参数 等可不写。*

- 【3】 或者只带data参数(不带sql、table参数)：

> 	表示数据来源从 data参数中获取，
	data参数里的值可以是 变量名，也可以是一个函数

*例，数据来源是变量：*

> 	{asp set rs_user = M('user')().fetch()}
	{$rs data="rs_user"}
		{$rs('id')} {$rs('username')}
	{/$rs}

*例，数据来源是函数：*

> 	{asp function getRs(tb) : set getRs = M(tb)().fetch() : end function}
	{$rs data="getRs('user')"}
		{$rs('id')} {$rs('username')}
	{/$rs}

- 【4】 此外还支持 offset 偏移量，rows 取数量 等参数：

> 	{$rs table="mytable" offset="6" rows="4"} .. {/$rs}
	表示：从 mytable 表中 取出 第6+1=7条开始的4条数据，即取 第7~10条数据。

*e.g.*

> 	{$rs table="user" field="*" where="id>0" order="id asc" offset="6" rows="4"}
		{$rs('id')} {$rs('username')}
	{/$rs}

*等同于：*

> 	{$rs sql="select * from #@user where id>0 order by id asc" offset="6" rows="4"}
		{$rs('id')} {$rs('username')}
	{/$rs}


**设置分页情况**

> 	必选参数： pagesize， 当 pagesize='0' 或为空 pagesize='' 则继承系统分页设置（默认 10）。
	可选参数： pagemark， 缺省默认为 p，表示URL参数以p作为分页标识符

*例：*

> 	{$rs sql="SQL语句" pagesize='10' pagemark='p'}
		{rs['id']} {rs['title']}
	{/$rs}
	分页条码： {@pager data='var' sql='' size='10' mark='p'}

> 	必选参数： count 或 sql 或 data 三者之一，
	若设置 count 且 count > 0 则直接计算传递count为数据总数，
	否则，若设置 data 则传递 data属性里的 data 到 pager分页中，计算data的分页情况，
	否则，最后从 sql 中读取Rs记录集数据，再计算分页情况。

> 	可选参数： size, mark , 设置对应分页条的 pagesize, pagemark 属性。
	缺省继承 var 变量设置的pagesize 和 pagemark，再者缺省继承 系统分页设置（默认 10）

*例：*

> 	{@pager data='dict'}
	&lt;div id="wrapbox"><br />===========
	<br /><strong>第{$pager("begin")}条 - 第{$pager("end")}条</strong>
	<br />当前：<a href="/index.asp?a=test&p={$pager("no")}">第{$pager("no")}页</a>
	<br />
	&lt;!--
	<a href="/index.asp?a=test&p={$pager("first")}">首页</a>  
	<a href="/index.asp?a=test&p={$pager("prev")}">上一页</a>  
	<a href="/index.asp?a=test&p={$pager("next")}">下一页</a>
	<a href="/index.asp?a=test&p={$pager("last")}">末页</a>
	-->
	{$pager("first_s")}
	{$pager("prev_s")}
	{$pager("next_s")}
	{$pager("last_s")}
	<br />===========
	<br />共{$pager("total")}条数据 当前: 第{$pager("no")}页/共{$pager("count")}页 每页显示:{$pager("size")}条
	<br />{$pager("bar")}
	<br />==========
	<br />&lt;/div>
	{/@pager}

------------------------------------------------------------

<p><a name="arr_rs_dict"></a></p>

# 循环标签获取 数组、字典对象、Rs记录对象

**不设置分页情况**

> 	要循环输出 Array, Dict, Rs 这些数据时， 必选设置必选参数 type=''
	type='arr' 或 type='array' 指定为 Array数组，（带有可选参数 key='i' 缺省为 i ）
	type='dict' 指定为 字典对象Dict，（带有可选参数 key='i' 缺省为 i ）
	type='rs' 指定为 Rs记录集对象，（没有其他参数）
	offset:偏移量，rows:取数量

*为了避免 和 pagesize 冲突，请在 不使用分页 pagesize 情况下才 设置索引。*

*举例 ：*

> 	{asp set rso = app.dao.query("select top 10 * from #@radio_radio where id>0") }
	{$rso type='rs' offset='0' rows='0' }
		{$rso('id')}
	{/$rso}

*例 1, 输出数组：*

> 	{$arr type='arr' key='j' }
		{$arr(j)} 
	{/$arr}

*例 2, 输出字典：*

> 	{$dict type='dict' key='j' pagesize='15' }
		{$dict(j)}
	{/$dict}

*例 3, 输出rs记录：*

> 	{asp set rso = app.dao.query("select top 10 * from #@radio_radio where id>0") }
	{$rso type='rs' pagesize='2' }
		{$rso('id')}
	{/$rso}


*下面分页显示（注 上面必选设置 pagesize 参数）：*

> 	{@pager data='dict'}
		&lt;div id="wrapbox">
		<a href="/index.asp?a=test&p={$pager("first")}">首页</a>  
		<a href="/index.asp?a=test&p={$pager("prev")}">上一页</a>  
		<a href="/index.asp?a=test&p={$pager("next")}">下一页</a>
		<a href="/index.asp?a=test&p={$pager("last")}">末页</a>
		&lt;p>共{$pager("total")}条数据 当前: 第{$pager("no")}页/共{$pager("count")}页 每页显示:{$pager("size")}条
		{$pager("bar")}
		&lt;/p>&lt;/div>
	{/@pager}

*输出rs记录集元素、字典元素 的项值：*

> 	标准写法：
	{$rs('title')} （推荐）
	
> 	拓展写法：
	{$rs.title} （推荐）
	{$rs[0]}
	{$rs(0)}
	{$rs['title']}
	{$rs[title]}

> 	调用函数写法：
	a.标准版：
	{:stdTime(rs('addtime'))} 

> 	b.拓展版，带参数：
	形式如： {$obj.att1.att2.att3...|userfunc=param1,param2,...,###,...,paramN}

> 	例：
	{$rs.addtime|stdTime=###}（推荐）

> 	当函数只有一个参数时，又可以省略(=)号后面的字符。例：
	{$add_time|stdTime=###} 可以省略成： {$add_time|stdTime}


**总结**

1、要循环输出dict字典对象，可以 foreach 标签 或 dict标签块
`{foreach k dict} .. {:dict(k)} .. {/foreach}`
`{$dict type='dict' key='i'} .. {:dict(i)} .. {/$dict}`

2、要循环输出arr数组，可以 foreach 标签 或 arr标签块
`{foreach i ary} .. {$i} .. {/foreach}`
`{$ary type='arr' key='j' } .. {ary(j)} .. {/$ary}`

3、要循环输出rs数据集，用rs标签块
`{$rs type='rs'} .. {$rs('id')} .. {/$rs}`


------------------------------------------------------------

<p><a name="empty"></a></p>

# 特殊标签用法

**a. {@empty} 、{@noempty} 指令标签**

> 	{@empty for='var'}..{/@empty}
	{@noempty for='var'}..{/@noempty}

*说明：*
> 	{@empty}用于对象为空 或 记录为空情况下，输出代替内容
	{@noempty}用于数据不为空情况下，输出内容
	必选参数 for=''

*例 ：*

> {@empty for='var'} 暂无记录！{/@empty}

*又例 ：*

> {@noempty for="rs('img')"}&lt;img src="{$rs('img')}" /&gt;{/@noempty}

*{@empty} {@noempty} 标签可以合并条件为：*

> 	{@empty for='var'}..{@else}..{/@empty}
	{@noempty for='var'}..{@else}..{/@noempty}

<p><a name="count"></a></p>

**b. {@count} 指令标签**

> 	{@count data=''}
	{@count sql=''}
	{@count table='' where=''}

*例 ：*

> 	{@count table='user' where='id>0'} 等同于 {@count sql='select * from #@user where id>0'}

> 	{asp set rs = App.Dao.Query("select top 10 * from #@radio_radio") }
	共 {@count data='rs'} 条记录
	电台总数( {@count sql='select * from #@radio_radio'} )

*注：sql语句中 含有#@符号 表示用于代替 数据表前缀*

<p><a name="assign"></a></p>

**c. {@assign} 标签，模板中赋值传递变量**

*用法：*

> 	这里写 按需加载显示的 js 文件...如：
	{if show_dialog=1}
	  {@load href="\_\_JS\_\_/jquery/plugins/jquery.artDialog.js"}
	{/if}
	{if show_validator=1}
	  {@load href="\_\_JS\_\_/jquery/plugins/formvalidator.js"}
	{/if}
	这里写 按需加载显示的 css 文件...如：
	{if show_css=1}
	{@load href="\_\_CSS\_\_/admin/style.css"}
	{/if}
	..
	..
	...
	{@assign var="show_dialog" value="1"}
	{@assign var="show_validator" value="0"}
	{@assign var="show_jqueryform" value="0"}
	{@assign var="show_css" value="1"}

------------------------------------------------------------

<p><a name="multidb"></a></p>

# 多库查询

**在 sql标签块 中加入参数 conn，表示传递一个 Connection 对象的变量**

> 	{asp}
	Dim db, newConn
	Set db = AB.db.New
	db.Conn = db.OpenConn(1, cfg_sitepath & "Data/#test.mdb", "")
	Set newConn = db.Conn
	{/asp}

> 	{$rs sql="select * from #@table1 where id<=1000000 order by id asc" pagesize="10" pagemark="p" offset="0" rows="0" conn="newConn"}
		{$rs("id")} - {$rs("aaaa")} - {$rs("bbbb")} - {$rs("cccc")}
	{/$rs}
	{@empty for='rs'}暂无记录！{/@empty}
