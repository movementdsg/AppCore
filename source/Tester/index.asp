<!--#include file="../Inc/sys.init.asp"-->
<%
Response.Charset="UTF-8"
'Response.CodePage=65001

Dim rqdo : rqdo = Request("do")
Dim c, str
On Error Goto 0
Select Case rqdo
	Case "exec" :
		c = Request("c")
		'AB.Use "tpl" : str = AB.Tpl.html_get(c, 1)
		AB.Use "Http"
		Dim h : Set h = AB.Http.New
		'h.SetHeader "content-type:application/x-www-form-urlencoded"
		'AB.Use "E" : c = AB.E.encodeURIComponent(c)
		AB.Use "E" : c = AB.E.escape(c)
		h.CharSet = "UTF-8"
		h.Data = "c="& (""&c)
		Dim qsurl : qsurl = AB.C.GetUrl(3) & "?do=runcode"
		str = h.Post(qsurl)
		Set h = Nothing
		'Response.Clear
		'Response.Charset="UTF-8"
		'Response.CodePage=65001
		'Response.Write "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Frameset//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><meta http-equiv='Content-Language' content='zh-cn' /><title>AspBox 代码测试工具</title></head><body>" & VbCrlf
		Response.Write "<textarea style='width:100%;height:100%;'>"&str&"</textarea>" & VbCrlf
		'Response.Write "</body></html>" & VbCrlf
		Response.End
	Case "runcode" :
		c = Request("c")
		AB.Use "tpl"
		c = AB.Tpl.vb_get(c, 1)
		Response.Clear
		On Error Goto 0
		On Error Resume Next
		Execute(c)
		If Err<>0 Then AB.ShowErr "<a href='errnum.html' title='查看错误代码表' target='_blank'>"& err.number &"</a>", err.description
		Response.End
	Case "load" :
		c = ""
		Response.Write c
		Response.End
	Case "exec2" :
		c = Request("c")
		AB.Use "tpl"
		c = AB.Tpl.vb_get(c, 1)
		Response.Clear
		Response.Write "<textarea style='width:100%;height:100%;'>"&c&"</textarea>"
		Response.End
End Select
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<title>AspBox 代码测试工具</title>
<link rel="stylesheet" type="text/css" href="runcode.css" />
<style type="text/css">
.on {
	background:#FFFF00;
}
</style>
</head>

<body id="editor">

<div id="header">
<h1>AspBox 代码测试工具</h1>
</div>

<div id="test" style="height:auto;">

<form name="form1" method="post" target="i">
<div id="butt">
<p><span><input name="submitbtn" type="submit" value="查看运行结果" onclick="openwin_run();"></input></span><span>（请在下面的文本框中编辑您的代码，然后单击此按钮测试结果。）</span></p>
</div>

<div id="code" style="width:49%">
<h2>编辑您的代码：</h2>
<textarea name="c">
可执行html代码:&lt;br /&gt;
&lt;font color=red&gt;红色字体&lt;/font&gt;
&lt;br /&gt;
&lt;br /&gt;
可执行Asp代码:&lt;br /&gt;
&lt;%
ab.c.wr "---"
ab.trace ab.dict
ab.c.wr "---"
%&gt;
</textarea>
</div>

<!---->
<div id="result">
<h2>查看结果: <input id="ea" type="button" value="运行结果" onclick="openwin_run();">&nbsp;&nbsp;<input id="eb" type="button" value="源代码" onclick="exec_run();">&nbsp;&nbsp;<a href="javascript:void(0)" onclick="opennew_run();">新窗口?</a></h2>
<div id="rs" style="width:100%;height:100%">
<iframe name="i" id="i" src="index.asp?do=load"></iframe>
</div>
</div>


<script type="text/javascript">
function exec_run() {
	document.getElementById("ea").className = '';
	document.getElementById("eb").className = 'on';
	document.form1.target='i';
	document.form1.action='index.asp?do=exec';
	document.form1.submit();
}
function openwin_run() {
	document.getElementById("ea").className = 'on';
	document.getElementById("eb").className = '';
	document.form1.target='i';
	document.form1.action='index.asp?do=runcode';
	document.form1.submit();
}
function opennew_run() {
	document.form1.target='_blank';
	document.form1.action='index.asp?do=runcode';
	document.form1.submit();
}
</script>

<!--
<script type="text/javascript">
function clearChilds(e){
  var childs = e.childNodes;
  for(var i=childs.length-1;i>=0;i--) {
	e.removeChild(childs[i]);
  }
}

function iframeInnerHtml(oframe)
{
	var iframeHTML = "";
	if (oframe.contentDocument) { iframeHTML = oframe.contentDocument.innerHTML; }
	else if (oframe.contentWindow) { iframeHTML = oframe.contentWindow.document.body.innerHTML; }
	else if (oframe.document) { iframeHTML = oframe.document.body.innerHTML; }
	return iframeHTML;
}

var timeHandle;
function setEvent() {
try{
	window.frames[0].document.body.onclick = function(){
		var area = document.createElement("textarea");
		area.setAttribute("id", "area");
		area.setAttribute("name", "area");
		area.setAttribute("style", "width:100%; height:90%;");
		area.value = iframeInnerHtml(window.frames[0]);
		//document.getElementById("rs").innerHTML = '';
		clearChilds(document.getElementById("rs"));
		document.getElementById("rs").appendChild(area);
	}
 }catch(e){}
 timeHandle = setTimeout(setEvent,200);
}
setEvent();
</script>
-->

</form>
</div>

<div id="footer">
<p>演示内容仅供测试。</p>
</div>

</body>
</html>

