<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Dim rqdo : rqdo = Request("do")
Dim c, str
On Error Goto 0

Select Case rqdo
	Case "exec" :
		c = Request("c")
		Response.Clear
		Response.Write c
		Response.End
	Case "load" :
		c = ""
		Response.Write c
		Response.End
End Select
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<title>HTML代码运行工具</title>
<link rel="stylesheet" type="text/css" href="runcode.css" />
</head>

<body id="editor" >

<div id="header">
<h1>HTML代码运行工具</h1>
</div>

<div id="test">

<form name="runform" action="run_html.asp?do=exec" method="post" target="i">
<div id="butt">
<p><span><input name="submit" type="submit" value="查看运行结果"></input></span><span>（请在下面的文本框中编辑您的代码，然后单击此按钮测试结果。）</span></p>
</div>

<div id="code">
<h2>编辑您的代码：</h2>
<textarea name="c">
&lt;html&gt;
&lt;body&gt;
&lt;script type="text/javascript"&gt;
var arr = new Array(6)
arr[0] = "George"
arr[1] = "John"
arr[2] = "Thomas"
arr[3] = "James"
arr[4] = "Adrew"
arr[5] = "Martin"

document.write(arr + "<br />")
arr.splice(2,3,"William")
document.write(arr)

&lt;/script&gt;

</body>
</html>
</textarea>
</div>

<div id="result">
<h2>查看结果:</h2>
<div id="rs" style="width:100%;height:100%"><iframe name="i" id="i" src="run_html.asp?do=load"></iframe></div>
</div>

</form>
</div>

<div id="footer">
<p>演示内容仅供测试。</p>
</div>

</body>
</html>