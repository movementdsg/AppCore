<!--#include file="config.asp"-->
<!--#include file="AspBox/Cls_AB.asp"-->
<!--#include file="AppCore/App.Init.asp"-->
<%
Dim App : Set App = New Cls_App_Init
'|* 配置app常量 *|
App.C("is_debug") = True '是否开启调试
App.C("is_cache") = False '是否开启缓存
App.C("site_path") = cfg_sitepath & "" '网站安装路径
' App.C("base_path") = App.C("site_path") & "Inc/AppCore/" '这可以由 App.BasePath = "/Inc/AppCore/" 指定
' App.C("app_path") = App.C("site_path") & "App/" '这可以由 App.AppPath = "/App/" 指定
' App.C("static_path") = App.C("site_path") & "statics/" '静态文件存放路径
' App.C("upload_path") = App.C("static_path") & "uploads/" '可用 App.UploadPath 读取上传目录
' App.C("css_path") = App.C("static_path") & "css/"
' App.C("js_path") = App.C("static_path") & "js/"
' App.C("img_path") = App.C("static_path") & "images/"
'|* 设置app目录 *|
' App.C("lib_path") = App.C("app_path") & "Lib/"
' App.C("conf_path") = App.C("app_path") & "Conf/" '配置文件存放路径
' App.C("lang_path") = App.C("app_path") & "Lang/"  '语言包存放路径
' App.C("tpl_path") = App.C("app_path") & "Tpl/" '模板存放路径
' App.C("common_path") = App.C("app_path") & "Common/" '自定义函数存放路径
'|* 设置app配置文件 *|
' App.C("baseconf") = "app"
' App.C("dbconf") = "db"
' App.C("conf_ext") = ".config.xml" '配置文件结尾，例：.config.xml，.xml.asp
'|* 其他app设置 *|
App.C("autoload") = True '是否自动加载自定义函数
App.C("autorun") = False '是否自动运行ACTION
App.Init()
'=========
%>