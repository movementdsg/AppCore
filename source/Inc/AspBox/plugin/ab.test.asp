<%
'#################################################################################
'##	ab.test.asp
'##	------------------------------------------------------------------------------
'##	Feature		:	AspBox Plugin Class Sample
'##	Version		:	v0.1
'##	Author		:	Lajox(lajox@19www.com)
'##	Update Date	:	2011/09/07 21:21
'##	Description	:	AspBox's plugin should be like this file as follow:
'					1.	File name should be like this: 'ab.***.asp'.  The '***'
'						is your plugin's name, with lower-case letters as better.
'					2.	Class's name should be like this: 'Cls_AB_***'. The '***'
'						is your plugin's name, lower-case letters after the '_'
'						are not required.
'					3.	You must put your file(s) in 'plugin' folder or any other
'						folder you setted with the property 'AB.PluginPath'.
'#################################################################################

Class Cls_AB_Test

	Private s_author, s_version

	Private Sub Class_Initialize()
		s_author 	= "lajox"
		s_version 	= "0.1"
		'Set Exception Info, please custom your ErrorCode such as 10001 or "test001".
		'Please try to keep your ErrorCode is not the same with other people.
		'Attention! You can use any character, not just numbers!
		AB.Error(10001) = "Error! Accept number only."
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'Set Property
	Public Property Get Author()
		Author = s_author
	End Property

	Public Property Get Version()
		Version = s_version
	End Property

	'Define a 'Sub'
	Public Sub helloWorld()
		AB.C.Print "Hello World!"
	End Sub

	'Define a 'Function'
	Public Function Return(ByVal s)
		Return = s
	End Function

	'Define default Function
	Public Default Function Fun(ByVal num)
		If Not isNumeric(num) Then
			'Raise a Exception Info When 'AB.Debug' is 'True'
			AB.Error.Msg = "<br />Your input is: " & num
			AB.Error.Raise 10001
		End If
		Fun = num
	End Function

End Class
%>