<%
'######################################################################
'## util.export.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc Export-Util Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2011/12/29 1:16
'## Description :   AspBox Mvc Export-Util Block(导出文件(Excel/Txt文件)工具拓展模块)
'######################################################################

Class Cls_Util_Export

	Private s_table, s_fields, s_fieldnames,oConn
	Public FileName		'设置生成的文件名

	Private Sub Class_Initialize()
		FileName = "data"
	End Sub

	Private Sub Class_Terminate()
		
	End Sub

	Public Property Let Conn(Byval connObj)
		Set oConn = connObj
	End Property

	'@ *****************************************************************************
	'@ 过程名:  Mvc.Util.Export.Excel(sSql,sFields)
	'@ 返  回:  无返回值
	'@ 作  用:  导出另存为Excel文件(xls格式)
	'==Param========================================================================
	'@ sSql  : 字符串 # [String] 执行的SQL语句
	'@ sFields  : 字符串 # [String] 设置Excel首行单元格,各个单元格名用(|)符号分割
	'@ 			  sFields为空则显示SQL语句删选的所有字段名
	'==DEMO=========================================================================
	'@ Mvc.Util.Use "Export"
	'@ Mvc.Util.Export.Conn = AB.db.Conn
	'@ Mvc.Util.Export.FileName = "data"
	'@ Mvc.Util.Export.Excel "SELECT top 10 * FROM LB_C_Media",""
	'@ Mvc.Util.Export.Excel "SELECT top 10 id,name,artist FROM LB_C_Media","tab1|tab2|tab3"
	'@ *****************************************************************************

	Function Excel(ByVal sSql, ByVal sFields)
		Dim rs,aAnother,i
		Response.Clear
		Response.Buffer = True
		Response.ContentType = "application/vnd.ms-excel"
		'Response.AddHeader "Content-Disposition", "inline; filename="&Me.FileName&".xls"
		Response.AddHeader "Content-Disposition", "attachment; filename="&Me.FileName&".xls"
		Response.Write "<table border=""1"">"
		Set rs = oConn.Execute(sSql)
		Response.Write("<tr>")
		If sFields = "" Then
			For i=0 To rs.fields.count-1
				Response.Write "<td>"&rs.Fields(i).Name&"</td>"
			Next
		Else
			aFields = Split(sFields,"|")
			For i=0 To uBound(aFields)
				Response.Write "<td>"&aFields(i)&"</td>"
			Next
		End If
		Response.Write("</tr>")
		Do While Not rs.eof
			Response.Write "<tr>"
			for i=0 to rs.fields.count-1
				If Len(rs(i))>12 And IsNumeric(rs(i)) Then
					Response.Write "<td>@"&rs(i)&"</td>"
				Else
					Response.Write "<td>"&rs(i)&"</td>"
				End If
			Next
			Response.Write "</tr>"
			rs.MoveNext
		Loop
		rs.close : Set rs = Nothing
		Response.Write "</table>"
		Response.End
	End Function

	'@ *****************************************************************************
	'@ 过程名:  Mvc.Util.Export.Txt(sSql,sFields)
	'@ 返  回:  无返回值
	'@ 作  用:  导出另存为TXT文本文件
	'==Param========================================================================
	'@ sSql  : 字符串 # [String] 执行的SQL语句
	'@ sFields  : 字符串 # [String] 设置Excel横向单元格名,各个单元格名用(|)符号分割
	'@ 			  sFields为空则显示SQL语句删选的所有字段名
	'==DEMO=========================================================================
	'@ Mvc.Util.Use "Export"
	'@ Mvc.Util.Export.Conn = AB.db.Conn
	'@ Mvc.Util.Export.FileName = "data"
	'@ Mvc.Util.Export.Txt "SELECT top 10 * FROM LB_C_Media",""
	'@ Mvc.Util.Export.Txt "SELECT top 10 id,name,artist FROM LB_C_Media","tab1|tab2|tab3"
	'@ *****************************************************************************

	Function Txt(ByVal sSql, ByVal sFields)
		Dim rs,aAnother,i
		Response.Clear
		Response.Buffer = True
		Response.ContentType = "application/octet-stream"
		'Response.AddHeader "Content-Disposition", "inline; filename="&Me.FileName&".txt"
		Response.AddHeader "Content-Disposition", "attachment; filename="&Me.FileName&".txt"
		Set rs = oConn.Execute(sSql)
		If sFields = "" Then
			For i=0 To rs.fields.count-1
				Response.Write rs.Fields(i).Name&Chr(9)
			Next
		Else
			aFields = Split(sFields,"|")
			For i=0 To uBound(aFields)
				Response.Write aFields(i)&Chr(9)
			Next
		End If
		Response.Write vbCrlf
		Do While Not rs.eof
			for i=0 to rs.fields.count-1
				If Len(rs(i))>12 And IsNumeric(rs(i)) Then
					Response.Write "@"&Replace(rs(i),Chr(9)," ")&Chr(9)
				Else
					Response.Write Replace(rs(i),Chr(9)," ")&Chr(9)
				End If
			Next
			Response.Write vbCrlf
			rs.MoveNext
		Loop
		rs.close : Set rs = Nothing
		Response.End
	End Function

End Class
%>