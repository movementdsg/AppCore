<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
AB.Use "jsLib"
Dim jsLib : Set jsLib = AB.jsLib.New
jsLib.Inc("aes.js")
Dim jso : Set jso = jsLib.Object
Dim str : str = ""

Dim password : password = "aspbox"
Dim plaintext : plaintext = "hello kitty"
Dim ciphertext : ciphertext = jso.Aes.Ctr.encrypt(plaintext, password, 256) '加密
Dim origtext : origtext = jso.Aes.Ctr.decrypt(ciphertext, password, 256) '还原(解密)

ab.c.printcn ciphertext '@return: rwKZQPfJL1Cg2FyxMn4jZ2osHA== (注：此值每刷新一次变化一次)
ab.c.printcn origtext '@return: hello kitty
%>