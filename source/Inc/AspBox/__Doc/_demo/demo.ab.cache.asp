<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
AB.Use "Cache"

'AB.C.RemoveAppAll

'不统计缓存总数(AB.Cache.Count将无法取到AB缓存总数)
'AB.Cache.CountEnabled = False

'文件缓存的保存路径(默认为/_cache)
'AB.Cache.SavePath = "/_cache"
'文件缓存的保存文件扩展名(默认为.cache)
AB.Cache.FileType = ".cache"

'统一缓存的保存时间，单位为分钟，不设置默认为5分钟
'AB.Cache.Expires = 5
'或设置为具体的过期时间：
'AB.Cache.Expires = "2010/04/01 08:00:00"

'缓存字符串到文件缓存
AB.C.WR "字符串缓存到文件缓存示例："
Dim tmp
'单独设置某一缓存的过期时间
AB.Cache("test").Expires = 10
AB.C.WR "此缓存的过期时间被设置为: " & AB.Cache("test").Expires & " 分钟后"
If AB.Cache("test").Ready Then
  '如果缓存存在且没有过期
  tmp = AB.Cache("test")
  AB.C.WR "已读取缓存(test):"
Else
  '如果缓存不存在或已过期
  '赋值给缓存对象
  tmp = "<i>测试字符串</i> 保存时间为 (" & Now() & ")"
  AB.Cache("test") = tmp
  '保存缓存到文件缓存（注意：保存为文件缓存还是内存缓存，区别只有一点，就是使用Save还是SaveApp，它们的获取方式是一样的）
  AB.Cache("test").Save
  AB.C.WR "已保存缓存(test)."
End If
AB.C.WR "---------"
AB.C.WR tmp
AB.C.WR "======================================"

'缓存记录集到文件缓存
AB.C.WR "记录集缓存到文件缓存示例："
Dim rs
'缓存过期时间为1分钟
AB.Cache("test/rs").Expires = 1
If AB.Cache("test/rs").Ready Then
  '读取文件缓存中的记录集(不需要Set)
  rs = AB.Cache("test/rs")
  AB.C.WR "已读取缓存(test/rs):"
Else
  Set rs = AB.db.GR("LB_C_Media:5","","ID Desc") '这里要换成你自己的数据库相关内容
  AB.Cache("test/rs") = rs
  '保存记录集到文件缓存，如果将记录集保存到内存缓存的话，会自动存为二维数组(GetRows)
  AB.Cache("test/rs").Save
  AB.C.WR "已保存缓存(test/rs)."
End If

AB.C.WR "---------"

If AB.C.Has(rs) Then
  While Not rs.EOF
    AB.C.WR "【" & rs(0) & "】" & rs(1) & " ( "&rs(2)&" )"
    rs.MoveNext
  Wend
Else
  AB.C.WR "记录集为空"
End If
AB.C.Close(rs)

AB.C.WR "======================================"

'缓存Dictionary对象到内存缓存
AB.C.WR "字典对象缓存示例："
Dim dict, key
AB.Cache("test/dict").Expires = 1
If AB.Cache("test/dict").Ready Then
  dict = AB.Cache("test/dict")
  AB.C.WR "已读取缓存(test/dict):"
Else
  Set dict = Server.CreateObject("Scripting.Dictionary")
  dict.add "a", "aaaaa"
  dict.add "b", "bbbbb"
  AB.Cache("test/dict") = dict
  '保存到内存缓存用SaveApp
  AB.Cache("test/dict").SaveApp
  AB.C.WR "已保存缓存(test/dict)."
End If

AB.C.WR "---------"

'列出字典内容
For Each key In dict
  AB.C.WR key & ":" & dict(key)
Next

AB.C.WR "======================================"
'缓存数量
AB.C.WR "总共有缓存：" & AB.Cache.Count & "个"
If AB.Cache.Count > 0 Then
	'遍历缓存名称
	Dim caches,ckey : Set caches = AB.C.GetApp("AB_Cache_Count")(1)
	For Each ckey In caches
	  AB.C.WR ckey
	Next
End If

AB.C.WR "------------------------------------"
AB.C.WR "页面执行时间： " & AB.C.GetScriptTime(0) & " 秒, 共查询数据库： " & AB.QT & " 次"
%>