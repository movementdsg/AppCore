<%
'######################################################################
'## ab.e.xor.asp
'## -------------------------------------------------------------------
'## Feature     :   XOR Encryption
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/04/22 18:46
'## Description :   AspBox XOR Encryption Block
'######################################################################

Class Cls_AB_E_XOR

	Private s_GenKey

	Private Sub Class_Initialize()
		s_GenKey = "AspBoxEncrypt"
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'-------------------------------------------------------------------------
	' @ 设置加密解密密钥，全局参数，读写
	'-------------------------------------------------------------------------
	' Desc: 设置密钥
	' e.g. AB.E.Xor.Password = "AspBoxEncrypt"
	'-------------------------------------------------------------------------

	Public Property Let Password(ByVal p)
		If Not IsNull(p) and p<>"" Then s_GenKey = p
	End Property

	Public Property Get Password()
		Password = s_GenKey
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.E.XOR.E(s) {简写为： AB.E.XOR(s) }
	'# @return: string
	'# @dowhat: 对字符串进行加密(使用XOR_ENS加密算法)
	'--DESC------------------------------------------------------------------------------------
	'# @param s: [string] (字符串)
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.print AB.E.XOR.E("aspbox") '=> 804606789D42843E0C480940
	'------------------------------------------------------------------------------------------

	Public Default Function E(Byval s)
		E = XOR_ENS(s, s_GenKey)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.E.XOR.D(s)
	'# @return: string
	'# @dowhat: <AB.E.XOR.E(s)加密算法>的解密函数
	'--DESC------------------------------------------------------------------------------------
	'# @param s: [string] (字符串)
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.printcn AB.E.XOR.D(AB.E.XOR.E("aspbox")) '=> aspbox
	'# ''----------------------------------------------
	'# Dim sPass,theStr,theEncStr,theDecStr
	'# ''sPass = AB.E.XOR.GetKeyGen(8)
	'# ''sPass = "u67(%#@d^J(>HTtdi"
	'# sPass = "C86HQ09Q"
	'# AB.E.XOR.Password = sPass
	'# theStr = "!#$%&"" '()*+,.-_/:;<=>?@[\]^`{|}~%中文"
	'# theEncStr = AB.E.XOR.E(theStr)
	'# theDecStr = AB.E.XOR.D(theEncStr)
	'# AB.C.PrintCn theStr & "<br>"
	'# AB.C.PrintCn theEncStr & "<br>"
	'# AB.C.PrintCn theDecStr & "<br>"
	'# AB.C.PrintCn "加密前后是否一致：" & (theStr = theDecStr) & "<br>"
	'------------------------------------------------------------------------------------------

	Public Function D(Byval s)
		D = XOR_DES(s, s_GenKey)
	End Function

	Public Function GetKeyGen(Byval iKeyLength) '产生密钥算法
		Dim k, iCount, strMyKey
		lowerbound = 35
		upperbound = 96
		Randomize
		For i = 1 To iKeyLength
			k = Int(((upperbound - lowerbound) + 1) * Rnd + lowerbound)
			strMyKey = strMyKey & Chr(k) & ""
		Next
		GetKeyGen = strMyKey
	End Function

	'---以下辅助函数---

		Private Function XOR_ENS(Byval Source, Byval Key) '加密函数
			IF IsNull(Key) Or Key="" Then Key = GetKeyGen(8)
			Dim Temp
			Dim i, iKey, iKeyLen
			Dim SSA, SSB, SSS
			Dim XOR_STR_A
			Select Case Len(Source)
			  Case 1
				  Source = Source & Chr(32) & Chr(32) & Chr(32) & Chr(32)
			  Case 2
				  Source = Source & Chr(32) & Chr(32) & Chr(32)
			  Case 3
				  Source = Source & Chr(32) & Chr(32)
			  Case 4
				  Source = Source & Chr(32)
			End Select

			XOR_STR_A = ""
			iKeyLen = Len(Key)
			iKey = 1
			'Source = StrConv(Source, vbFromUnicode)
			For i = 1 To LenB(Source)
				SSA = CInt(AscB(MidB(Source, i, 1)))
				SSB = CInt(Asc(Mid(Key, iKey, 1)))
				iKey = iKey + 1
				If iKey > iKeyLen Then
					iKey = 1
				End If
				SSS = SSA Xor SSB
				XOR_STR_A = XOR_STR_A & Right("0" & Hex(SSS), 2)
			Next
			Temp = XOR_STR_A
			Temp = Replace(Temp, "1", "*")
			Temp = Replace(Temp, "9", "1")
			Temp = Replace(Temp, "*", "9")
			Temp = Replace(Temp, "8", "#")
			Temp = Replace(Temp, "2", "8")
			Temp = Replace(Temp, "#", "2")
			Temp = Replace(Temp, "4", "#")
			Temp = Replace(Temp, "7", "4")
			Temp = Replace(Temp, "#", "7")
			Temp = Replace(Temp, "3", "#")
			Temp = Replace(Temp, "6", "3")
			Temp = Replace(Temp, "#", "6")
			Temp = Replace(Temp, "D", "#")
			Temp = Replace(Temp, "F", "D")
			Temp = Replace(Temp, "#", "F")
			Temp = Replace(Temp, "A", "#")
			Temp = Replace(Temp, "B", "A")
			Temp = Replace(Temp, "#", "B")
			XOR_ENS = Temp
		End Function

		Private Function XOR_DES(Byval Source, Byval Key)  ' 解密函数
			IF IsNull(Key) Or Key="" Then Key = GetKeyGen(8)
			Dim Temp
			Dim i, iKey, iKeyLen
			Dim SSA, SSB, SSS
			Dim XOR_STR_A
			XOR_STR_A = ""
			iKeyLen = Len(Key)
			iKey = 1
			XOR_STR_A = ""
			Source = Replace(Source, "1", "*")
			Source = Replace(Source, "9", "1")
			Source = Replace(Source, "*", "9")
			Source = Replace(Source, "8", "#")
			Source = Replace(Source, "2", "8")
			Source = Replace(Source, "#", "2")
			Source = Replace(Source, "4", "#")
			Source = Replace(Source, "7", "4")
			Source = Replace(Source, "#", "7")
			Source = Replace(Source, "3", "#")
			Source = Replace(Source, "6", "3")
			Source = Replace(Source, "#", "6")
			Source = Replace(Source, "D", "#")
			Source = Replace(Source, "F", "D")
			Source = Replace(Source, "#", "F")
			Source = Replace(Source, "A", "#")
			Source = Replace(Source, "B", "A")
			Source = Replace(Source, "#", "B")
			For i = 1 To Len(Source) Step 2
				SSA = CInt("&H" & (Mid(Source, i, 2)))
				SSB = (Asc(Mid(Key, iKey, 1)))
				iKey = iKey + 1
				If iKey > iKeyLen Then
					iKey = 1
				End If
				SSS = SSA Xor SSB
				XOR_STR_A = XOR_STR_A & ChrB(SSS)
			Next
			' Temp = StrConv(XOR_STR_A, vbUnicode)
			' Temp = replace(XOR_STR_A,vbcrlf,"",lenB(XOR_STR_A)-4)
			Temp=trim(XOR_STR_A)
			Temp=cstr(Temp)
			XOR_DES = Temp
		End Function

End Class
%>