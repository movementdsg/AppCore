<%
'######################################################################
'## ab.cookie.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Cookie Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/1/9 2:50
'## Description :   AspBox Cookie操作模块
'######################################################################

Class Cls_AB_Cookie

	Private	s_mark
	Private b_cooen
	Private i_expire

	Private Sub Class_Initialize()
		s_mark = ""
		b_cooen = AB.CookieEncode
		i_expire = 0
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Mark [= s]
	'@ 返  回:  --
	'@ 作  用:  设置/获取 Cookie键值前缀
	'==DESC=====================================================================================
	'@ 参数 s(可选): String (字符串)
	'==DEMO=====================================================================================
	'@ AB.Cookie.Mark = "abxcc_"
	'@ AB.C.PrintCn "cookie键值前缀:" & AB.Cookie.Mark
	'@ AB.Cookie.Set "temp","abcd",""
	'@ AB.C.PrintCn AB.Cookie("temp")
	'@ AB.C.PrintCn "若不对Cookie进行加密，这将等同于:" & Request.Cookies(AB.Cookie.Mark & "temp")
	'@ *****************************************************************************************

	Public Property Let Mark(Byval s)
		s_mark = s
	End Property

	Public Property Get Mark()
		Mark = s_mark
	End Property


	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Timeout = value
	'@ 别  名:  AB.Cookie.Expire = value
	'@ 返  回:  --
	'@ 作  用:  设置 Cookie的过期时间，缺省默认为 0 即浏览器进程。
	'==DESC=====================================================================================
	'@ 参数 value: Integer (整数)
	'==DEMO=====================================================================================
	'@ AB.Cookie.Timeout = 30 '设置30分钟后过期
	'@ *****************************************************************************************

    Public Property Let Timeout(Byval value)
		If Trim(value) <> "" Then
			If IsNumeric(i_expire) Then
				i_expire = CDbl(value)
			Else
				i_expire = CDate(value)
			End If
		End If
    End Property
    Public Property Let Expire(Byval value) : Timeout = value : End Property

	Public Property Get Timeout()
		Timeout = i_expire
	End Property
	Public Property Get Expire() : Expire = i_expire : End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.IfEncode [= b]
	'@ 返  回:  --
	'@ 作  用:  设置/取得 是否对Cookies值进行加密
	'==DESC=====================================================================================
	'@ 参数 b(可选): True/False (布尔值)
	'==DEMO=====================================================================================
	'@ AB.Cookie.IfEncode = True
	'@ AB.C.Print "对Cookie值是否启用加密: " & AB.Cookie.IfEncode
	'@ *****************************************************************************************

	Public Property Let IfEncode(ByVal b)
		b_cooen = b
	End Property

	Public Property Get IfEncode()
		IfEncode = b_cooen
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Find(key) 缩写形式 AB.Cookie(key)
	'@ 返  回:  Anything (任意值)
	'@ 作  用:  取Cookie值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ ab.c.print AB.Cookie("c_admin")
	'@ *****************************************************************************************

    Public Default Property Get Find(Byval key)
        Find = [Get](key)
    End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Set key, value, options
	'@ 返  回:  无返回值
	'@ 作  用:  设置一个Cookie值
	'@ 			注: AB.Cookie.Set 方法只适应于一般的Cookie设置,
	'@ 			    如果要更高级设置选项请 用 AB.C.SetCookie 方法
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'@ 参数 value: Anything (任意值)
	'@ 参数 options: Array/String (数组/字符串)
	'==DEMO=====================================================================================
	'@ AB.Cookie.Set "c_admin", "admin", ""
	'@ AB.Cookie.Set "c_admin", "admin", "2009-2-28 23:59:59"
	'@ AB.Cookie.Set "c_admin", "admin", Array("2009-2-28 23:59:59","/bbs","ambox.it")
	'@ *****************************************************************************************

	Sub [Set](Byval key, Byval value, Byval options)
		If b_cooen Then : AB.Use("E") : AB.E.Use("Aes") : value = AB.E.Aes.E(value) : End If
        Response.Cookies(Me.Mark & key) = value
        If Not (IsNull(options) Or IsEmpty(options)) Then
            If IsArray(options) Then
				AB.Use "A"
                Dim l : l = AB.A.Len(options)
				If (Trim(options(0))="" And Trim(i_expire)<>"0" And Trim(i_expire)<>"") Then options(0) = i_expire
				If l > 0 Then
					If AB.C.IsNum(options(0)) Then '分钟
						If CLng(options(0))>0 Then
							'Response.Cookies(Me.Mark & key).Expires = Now()+CLng(options(0))/60/24
							Response.Cookies(Me.Mark & key).Expires = DateAdd("n",CLng(options(0)),Now())
						End If
					Else '时间日期
						If (Trim(options(0))<>"" And Trim(options(0))<>"0") Then Response.Cookies(Me.Mark & key).Expires = cDate(options(0))
					End If
				End If
                If l > 1 Then If Trim(options(1)&"")<>"" Then Response.Cookies(Me.Mark & key).Path = options(1)
                If l > 2 Then If Trim(options(2)&"")<>"" Then Response.Cookies(Me.Mark & key).Domain = options(2)
            Else
				If (Trim(options)="" And Trim(i_expire)<>"0" And Trim(i_expire)<>"") Then options = i_expire
				If AB.C.IsNum(options) Then '分钟
					If CLng(options)>0 Then
						'Response.Cookies(Me.Mark & key).Expires = Now()+CLng(options)/60/24
						Response.Cookies(Me.Mark & key).Expires = DateAdd("n",CLng(options),Now())
					End If
				Else '时间日期
					If (Trim(options)<>"" And Trim(options)<>"0") Then Response.Cookies(Me.Mark & key).Expires = cDate(options)
				End If
            End If
        End If
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Get(key)
	'@ 返  回:  Anything (任意值)
	'@ 作  用:  或取Cookie值
	'@ 			注: AB.Cookie.Get 方法只适应于常规方法获取Cookie值,
	'@ 			    如果要更高级选项获取请 用 AB.C.Cookie 方法
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ ab.c.print AB.Cookie.Get("c_admin")
	'@ *****************************************************************************************

	Function [Get](Byval key)
		Dim value : value = Request.Cookies(Me.Mark & key)
		If b_cooen Then : AB.Use("E") : AB.E.Use("Aes") : value = AB.E.Aes.D(value) : End If
        [Get] = value
    End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Remove key
	'@ 返  回:  无返回值
	'@ 作  用:  删除某个Cookie值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ AB.Cookie.Remove "c_admin"
	'@ *****************************************************************************************

	Sub Remove(key)
		Response.Cookies(Me.Mark & key) = Empty
		Response.Cookies(Me.Mark & key).Expires = formatDateTime(Now()-1)
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.RemoveAll
	'@ 返  回:  无返回值
	'@ 作  用:  清空删除所有Cookie值
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ AB.Cookie.RemoveAll()
	'@ *****************************************************************************************

	Sub RemoveAll()
		Dim iCookie,Subkey
		For Each iCookie in Request.Cookies
			If Not(Request.Cookies(iCookie).HasKeys) Then
				Response.Cookies(iCookie) = Empty
			Else
				For Each Subkey In Request.Cookies(iCookie)
					Response.Cookies(iCookie)(Subkey) = Empty
				Next
			End if
			Response.Cookies(iCookie).Expires = formatDateTime(Now()-1)
		Next
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Cookie.Compare(key1, key2)
	'@ 返  回:  True/False 布尔值
	'@ 作  用:  比较两个Cookie值是否相等
	'==DESC=====================================================================================
	'@ 参数 key1: String (字符串) Cookie 1 的键值
	'@ 参数 key2: String (字符串) Cookie 2 的键值
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Cookie.Compare("c_v1", "c_v2")
	'@ *****************************************************************************************

	Function [Compare](Byval key1, Byval key2)
        Dim Cache1
        Cache1 = Me.[Get](key1)
        Dim Cache2
        Cache2 = Me.[Get](key2)
        If TypeName(Cache1) <> TypeName(Cache2) Then
            Compare = False
        Else
            If TypeName(Cache1) = "Object" Then
                Compare = (Cache1 Is Cache2)
            Else
                If TypeName(Cache1) = "Variant()" Then
                    Compare = (Join(Cache1, "^") = Join(Cache2, "^"))
                Else
                    Compare = (Cache1 = Cache2)
                End If
            End If
        End If
    End Function

End Class
%>