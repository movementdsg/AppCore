<%
'######################################################################
'## ab.jslib.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox JSLib(Load JS Libarary) Extensive Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/09/08 18:45
'## Description :   AspBox JSLib(JS核心库引用)JS脚本核心引用操作模块
'######################################################################

Class Cls_AB_jsLib

	Private s_path
	Private o_sc, o_jso, o_lib

	Private Sub Class_Initialize()
		On Error Resume Next
		s_path	= AB.BasePath & "jsLib/"
		AB.Use "Sc"
		Set o_sc = AB.Sc.New
		o_sc.Lang = "js"
		Set o_lib = Server.CreateObject(AB.dictName)
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		Set o_jso = Nothing
		Set o_sc = Nothing
		Set o_lib = Nothing
	End Sub

	Public Function [New]()
		Set [New] = New Cls_AB_jsLib
	End Function

	Public Property Let BasePath(ByVal p)
		s_path = Abx_FixAbsPath(p)
	End Property

	Public Property Get BasePath()
		BasePath = s_path
	End Property

	'-------------------------------------------------------------------------------------------
	'# AB.jsLib.Object()
	'# @alias: AB.jsLib.Mc()
	'# @return: 取得所有导入文件返回的总对象
	'# @dowhat: 返回总对象(共同体对象，可操作所有导入文件所有方法)
	'--DESC-------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.Use "jsLib"
	'# Dim jsLib : Set jsLib = AB.jsLib.New
	'# jsLib.Inc("a.js") '默认的文件目录：“AspBox/jsLib/”, 假设内容为: function myfunc() { return 'a'; }
	'# Dim jso : Set jso = jsLib.Object
	'# Dim temp : temp = jso.myfunc()
	'# ab.c.print temp '输出值：a
	'-------------------------------------------------------------------------------------------

	Public Function [Object]()
		On Error Resume Next:Set [Object] = o_sc.object:On Error GoTo 0
	End Function
	Public Function MC()
		On Error Resume Next:Set MC = o_sc.object:On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.jsLib.Get(js)
	'# @alias: AB.jsLib.Load(js)
	'# @alias: 简写为：AB.jsLib(js)
	'# @return: object
	'# @dowhat: 载入jsLib(JS核心库)
	'--DESC-------------------------------------------------------------------------------------
	'# @param js [string] : js文件名, 其中文件的扩展名(后缀名.js)可不必写出.
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.use "sc"
	'# Dim sc : Set sc = ab.sc.new
	'# sc.Lang = "js"
	'# sc.Add "function foo(){ var person = {name: {a:""zhangsan""}, pass: ""123"", fn: function(){alert(this.pass);} }; return person; }"
	'# Dim x : set x = sc.eval("foo()")
	'# ab.use "jsLib"
	'# Dim jso
	'# Set jso = ab.jsLib("json2.js") '该js文件在默认目录：“AspBox/jsLib/” 下
	'# Dim temp : temp = jso.JSON.stringify(x) '调用JSON.stringify()方法将JS对象转换成为 JSON
	'# ab.c.print temp
	'# Dim jsontext : jsontext = "{""name"":""testid"",""pass"":""123""}"
	'# ab.trace(jso.JSON.parse(jsontext))
	'-------------------------------------------------------------------------------------------

	Public Default Function [Get](ByVal f)
		On Error Resume Next
		Include(f)
		Set o_jso = o_sc.object
		Set [Get] = o_jso
		On Error GoTo 0
	End Function
	Public Function [Load](ByVal f): Set [Load] = [Get](f): End Function

	'-------------------------------------------------------------------------------------------
	'# AB.jsLib.Include(js)
	'# @alias: AB.jsLib.Inc(js)
	'# @alias: AB.jsLib.Import(js)
	'# @return: void
	'# @dowhat: 载入jsLib(JS核心库)
	'--DESC-------------------------------------------------------------------------------------
	'# @param js [string] : js文件的名字, 文件的扩展名(后缀名.js)可不必写出.
	'# 						默认的文件存放目录在：“AspBox/jsLib/” 下。
	'# 						可以通过 AB.jsLib.BasePath = "/yourJsFileDir/" 修改为指定目录。
	'# 						如: AB.jsLib.BasePath = AB.BasePath & "jsDir/" '目录改在：“AspBox/jsDir/” 下。
	'--DEMO-------------------------------------------------------------------------------------
	'# '__载入多个文件代码：
	'# AB.Use "jsLib"
	'# Dim jsLib : Set jsLib = AB.jsLib.New
	'# jsLib.Inc("a.js") '文件1内容为: function a() { return 'a'; }
	'# jsLib.Inc("b.js") '文件2内容为: function b() { return 'b'; }
	'# jsLib.Inc("c.js") '文件3内容为: function c() { return a() + ':' + b(); }
	'# Dim jso : Set jso = jsLib.Object
	'# Dim temp : temp = jso.c()
	'# ab.c.print temp '输值值：a:b
	'-------------------------------------------------------------------------------------------

	Public Function Include(ByVal f)
		On Error Resume Next
		Dim path : path = s_path & FixName(f)
		If Not HasLoad(f) Then
			If AB.C.isFile(path) Then
				o_sc.Include(path)
				o_lib.add LCase(FixName(f)), path
			End If
		End If
		On Error GoTo 0
	End Function
	Public Function Inc(Byval f): Include(f): End Function
	Public Function Import(Byval f): Include(f): End Function

	'-------------------------------------------------------------------------------------------
	'# AB.jsLib.HasLoad(js)
	'# @alias: AB.jsLib.Has(js)
	'# @return: Boolean (True / False)
	'# @dowhat: 检测是否已载入某JS核心
	'--DESC-------------------------------------------------------------------------------------
	'# @param js [string] : js文件的名字, 文件的扩展名(后缀名.js)可不必写出.
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.Use "jsLib"
	'# Dim jsLib : Set jsLib = AB.jsLib.New
	'# If Not AB.jsLib.Has("a.js") Then
	'# 	jsLib.Inc("a.js")
	'# End If
	'-------------------------------------------------------------------------------------------

	Public Function HasLoad(ByVal f)
		On Error Resume Next
		HasLoad = False
		f = LCase(FixName(f))
		If o_lib.Exists(f) Then
			HasLoad = True
		End IF
		On Error GoTo 0
	End Function
	Public Function Has(Byval f): Has = HasLoad(f): End Function

	Public Function FixName(ByVal f)
		If IsNull(f) Or Trim(f)="" Then f=""
		If InStr(f,".")<=0 Then
			f = f & ".js"
		Else
			If LCase(Mid(f,InstrRev(f,"."))) <> ".js" Then
				f = f & ".js"
			End If
		End If
		FixName = f
	End Function

	' ============== 以下辅助函数 ===================

		Private Function Abx_FixAbsPath(ByVal p)
			p = AB.C.IIF(Left(p,1)= "/", p, "/" & p)
			p = AB.C.IIF(Right(p,1)="/", p, p & "/")
			Abx_FixAbsPath = p
		End Function

End Class
%>