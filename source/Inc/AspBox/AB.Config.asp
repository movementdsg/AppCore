<%
'######################################################################
'## AspBox 应用配置文件
'## -------------------------------------------------------------------
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2015/12/05 14:48
'######################################################################

'Option Explicit
Dim Abx_Timer : Abx_Timer = Timer()
Dim Abx_DbQueryTimes : Abx_DbQueryTimes = 0
Dim Abx_s_html, Abx_o_ctrl
Dim AB : Set AB = New Cls_AB

'_必须正确设置AspBox目录在网站中的路径，以"/"开头:
AB.BasePath = cfg_sitepath & "Inc/AspBox/"

'_设置文件编码 (通常为"GBK"或"UTF-8")
AB.CharSet = "UTF-8"

'_设置数据表前缀
AB.tbPrefix = ""

'_设置缓存方式([0].不缓存(默认)：空值 或 False ; 1: app形式："app" ; [2].文件形式："file" ; [3].数据库形式："data")
'_经测试：将值设置为 "app" 效率为最高。
AB.CacheType = "app"

'_配置预定义核心类
'AB.baseCore = "C,D,H,[Error],E,A,DB,Dbo,Fso,List,Json,Cache,Http,Xml,Tpl,X,Up,Upload,Form,[Char],Time,Html,[Rnd],Mail,[Cookie],[Session],Url,SC,jsLib,Pager,Key"

'_配置不加载的预定义核心类
'AB.noCore = "DB,Dbo,SC,jsLib,Pager"

'_是否预加载核心
AB.ifCore = True

'_是否启用MVC模型
AB.ifMvc = False

'_打开开发者调试模式
AB.Debug = True

'_设置FSO组件的名称（如果服务器上修改过）:
'AB.FsoName = "Scripting.FileSystemObject"

'_设置ADODB.Stream组件的名称（如果服务器上修改过）:
'AB.SteamName = "ADODB.Stream"

'_设置Scripting.Dictionary组件的名称（如果服务器上修改过）:
'AB.DictName = "Scripting.Dictionary"

'_设置如何处理载入的UTF-8文件的BOM信息(keep/remove/add)：
'AB.FileBOM = "remove"

'_执行始函数
AB.Init()

'_配置数据库连接：
' AB.Da "dbtype", "acc" '@dbtype(acc/sql)
' AB.Da "accdb", "/data/data.mdb" '@acc.db
' AB.Da "accpwd", "" '@acc.pwd
' AB.Da "sqlserver", "127.0.0.1" '@sql.server
' AB.Da "sqldb", "Data" '@sql.db
' AB.Da "sqluid", "sa" '@sql.uid
' AB.Da "sqlpwd", "1234" '@sql.pwd
' Rem ----
' AB.ReUse "db"
' If LCase(AB.DT("dbtype"))="sql" Or LCase(AB.DT("dbtype"))="mssql" Then '@MS SQL Server
' 	AB.db.Conn = AB.db.OpenConn(0,AB.DT("sqldb"),""&AB.DT("sqluid")&":"&AB.DT("sqlpwd")&"@"&AB.DT("sqlserver")&"")
' Else '@Access
' 	AB.db.Conn = AB.db.OpenConn(1,AB.DT("accdb"),AB.DT("accpwd"))
' End If
' AB.ReUse "Dbo"
%>