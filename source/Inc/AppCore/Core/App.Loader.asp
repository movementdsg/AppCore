<%
'######################################################################
'## App.Loader.asp
'## -------------------------------------------------------------------
'## Feature     :   App Loader Block
'## Version     :   v0.1
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2014/2/15 2:18
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   App 加载器
'######################################################################

Class Cls_App_Loader

	Private s_dictName
	Private o_help_data, o_lib_data

	Private Sub Class_Initialize()
		On Error Resume Next
		s_dictName	= AB.dictName
		Set o_help_data = Server.CreateObject(s_dictName)
		Set o_lib_data = Server.CreateObject(s_dictName)
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		Set o_help_data = Nothing
		Set o_lib_data = Nothing
	End Sub

	'载入Helper小助手
	'e.g. App.Loader.Helper("tree") 简写：App.Helper("tree")
	Public Function Helper(ByVal s)
		On Error Resume Next
		Dim p, dir, f : p = AB.D.MCase(LCase(s))
		If IsObject(o_help_data(LCase(s))) And Not AB.C.isNul(o_help_data(LCase(s))) Then
			Set Helper = o_help_data(LCase(s))
		Else
			dir = "Helper"
			f = LCase(p) & ".helper.asp"
			If AB.C.isFile(App.BasePath & dir & "/" & f) Then
				App.Import("App.Helper."& p)
				Execute("Set Helper = New App_Helper_" & p)
			ElseIf AB.C.isFile(App.LibPath & dir & "/" & f) Then
				App.Import("@.Helper."& p)
				Execute("Set Helper = New App_Helper_" & p)
			End If
			If Err.Number=0 Then Set o_help_data(LCase(s)) = Helper
		End If
		If Err.Number<>0 Then Set Helper = Nothing
		On Error GoTo 0
	End Function

	'载入Library库文件
	'e.g. App.Loader.Library("tree") 简写：App.Library("tree")
	Public Function Library(ByVal s)
		On Error Resume Next
		Dim p, dir, f : p = AB.D.MCase(LCase(s))
		If IsObject(o_lib_data(LCase(s))) And Not AB.C.isNul(o_lib_data(LCase(s))) Then
			Set Library = o_lib_data(LCase(s))
		Else
			dir = "Library"
			f = LCase(p) & ".library.asp"
			If AB.C.isFile(App.BasePath & dir & "/" & f) Then
				App.Import("App.Library."& p)
				Execute("Set Library = New App_Library_" & p)
			ElseIf AB.C.isFile(App.LibPath & dir & "/" & f) Then
				App.Import("@.Library."& p)
				Execute("Set Library = New App_Library_" & p)
			End If
			If Err.Number=0 Then Set o_lib_data(LCase(s)) = Library
		End If
		If Err.Number<>0 Then Set Library = Nothing
		On Error GoTo 0
	End Function

End Class
%>