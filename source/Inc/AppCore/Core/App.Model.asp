<%
'######################################################################
'## Cls_App_Model.asp
'## -------------------------------------------------------------------
'## Feature     :   App Model
'## Version     :   v0.1
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2013/12/11 21:47
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   App 模型控制器
'######################################################################

Class Cls_App_Model

	Public Table

	Private Sub Class_Initialize()
		On Error Resume Next
		Table = "TABLE"
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	Public Function [New]()
		Set [New] = New Cls_App_Model
	End Function

	'缺省方法为Dao
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

End Class
%>