<%
'######################################################################
'## App.Error.asp
'## -------------------------------------------------------------------
'## Feature     :   App Error Core
'## Version     :   v0.1
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2013/08/30 18:00
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   App 错误处理
'######################################################################

Class Cls_App_Error

	Private s_dictName
	Private b_debug

	Private Sub Class_Initialize()
		On Error Resume Next
		b_debug = App.Debug
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	Public Property Let [Debug](ByVal b)
		b_debug = b
	End Property
	Public Property Get [Debug]
		[Debug] = b_debug
	End Property

	Public Default Property Get E(ByVal n)
		E = AB.Error(n)
	End Property
	Public Property Let E(ByVal n, ByVal s)
		If AB.C.Has(n) And AB.C.Has(s) Then
			AB.Error(CLng(n)) = s
		End If
	End Property

	Public Function [Set](ByVal n, ByVal s)
		If IsNumeric(n) Then
			AB.Error(CLng(n)) = s
		End If
	End Function

	Public Sub SetErr(ByVal Number, ByVal Description, ByVal Source)
		AB.Error.Set Number, Description, Source
	End Sub

	Public Sub ClearErr()
		AB.Error.ClearErr
	End Sub

	Public Sub Raise(ByVal n)
		If IsNumeric(n) Then
			AB.Error.Raise n
		End If
	End Sub

	Public Sub [Throw](ByVal n, ByVal p, ByVal s)
		Dim msg
		If b_debug Then
			If p<>"" And p<>0 Then msg = msg & "错误代码：" & p & "; <br />"
			msg = msg & "错误描述：" & s & "; "
			AB.Error.Msg = "" & msg & ""
			AB.Error.Raise n
		End If
	End Sub

End Class
%>