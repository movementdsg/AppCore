<%
'######################################################################
'## App.Init.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox AppCore
'## Version     :   v1.0.0
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2015/11/18 22:33
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   AspBox App主核心文件
'######################################################################

Class Cls_App_Init

	Public Cfg, [Error], Model, Dao, View, Out, Loader
	Private s_fsoName, s_dictName, s_steamName, s_charset, s_tbprefix, a_groupList
	Private o_base, o_cfg, o_cache_fs, o_cache_cfg, o_instance, o_var, o_db
	Private get_m, get_c, get_a
	Private b_debug, b_cache, b_asp, b_layerout, b_locate, b_hasrun
	'Private self

	Private Sub Class_Initialize()
		'Set self = Me
		s_charset = AB.CharSet
		s_tbprefix = ""
		s_fsoName = AB.fsoName
		s_dictName	= AB.dictName
		s_steamName	= AB.steamName
		a_groupList	= Array()
		b_debug = False
		b_cache = False '是否进行缓存
		b_asp = False
		b_layerout = False
		b_locate = False
		b_hasrun = False
		AB.Debug = b_debug
		AB.Error.Debug = b_debug
		AB.Use "A"
		AB.Use "Fso"
		AB.Use "Cache"
		AB.ReUse "db" : AB.db.Debug = b_debug
		Set o_cache_fs = AB.Cache.New : o_cache_fs.Expires = 60 * 24 '缓存1天
		Set o_cfg = Server.CreateObject(s_dictName) : Set Cfg = o_cfg
		Set o_base = Server.CreateObject(s_dictName)
		Set o_cache_cfg = Server.CreateObject(s_dictName)
		Set o_instance = Server.CreateObject(s_dictName)
		Set o_var = Server.CreateObject(s_dictName)
		get_m = Me.Req("m")
		get_c = Me.Req("c")
		get_a = Me.Req("a")
		Set Out = New Cls_App_Out
		'--- 以后可以通过 App.Error(n) 获取 AB.Error(n) 值
		AB.Error(401) = "程序运行出错；"
		AB.Error(402) = "类没有被定义；"
		AB.Error(403) = "创建实例失败；"
		AB.Error(506) = "类没有被定义；"
		AB.Error(999) = "数据库连接出错；"
	End Sub

	Private Sub InitConf()
		o_cfg("is_debug") = False
		o_cfg("is_cache") = False
		o_cfg("site_path") = "/"
		o_cfg("base_path") = Me.SitePath() & "Inc/AppCore/"
		o_cfg("core_path") = o_cfg("base_path") & "Core/"
		o_cfg("extend_path") = o_cfg("base_path") & "Extend/"
		o_cfg("app_path") = Me.SitePath() & "App/"
		o_cfg("cache_path") = Me.SitePath() & "Cache/"
		o_cfg("static_path") = Me.SitePath() & "statics/"
		o_cfg("upload_path") = Me.StaticPath() & "uploads/"
		o_cfg("css_path") = Me.StaticPath() & "css/"
		o_cfg("js_path") = Me.StaticPath() & "js/"
		o_cfg("img_path") = Me.StaticPath() & "images/"
		'---
		o_cfg("lib_path") = Me.AppPath() & "Lib/"
		o_cfg("conf_path") = Me.AppPath() & "Conf/"
		o_cfg("lang_path") = Me.AppPath() & "Lang/"
		o_cfg("tpl_path") = Me.AppPath() & "Tpl/"
		o_cfg("common_path") = Me.AppPath() & "Common/"
		'---
		o_cfg("default_document") = "index.asp" '默认文档
		o_cfg("display_document") = True '是否显示默认文档
		o_cfg("default_theme") = "default"
		o_cfg("default_group") = "home"
		o_cfg("baseconf") = "app"
		o_cfg("dbconf") = "db"
		o_cfg("conf_ext") = ".config.xml"
		o_cfg("autoload") = True '是否自动加载自定义函数
		o_cfg("autorun") = False '是否自动运行ACTION
		Set o_cfg("config:"& CfgVal("baseconf") &"") = Server.CreateObject(s_dictName)
	End Sub

	Public Sub Init()
		On Error Resume Next
		Dim fpath
		InitConf() '初始化配置
		LoadConf(CfgVal("baseconf")) '加载注配置文件
		LoadConf(CfgVal("dbconf")) '加载数据库配置文件
		If Not AB.C.isNul(CfgVal("is_debug")) Then
			b_debug = CBool(CfgVal("is_debug"))
			Me.Debug = b_debug
			AB.Debug = b_debug
			AB.db.Debug = b_debug
			AB.Error.Debug = b_debug
		End If
		If Not AB.C.isNul(CfgVal("is_cache")) Then
			b_cache = CBool(CfgVal("is_cache"))
		End If
		s_tbprefix = Me.tbPrefix
		AutoPress()
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		[Exit]()
	End Sub

	Public Sub [Exit]()
		Set o_cache_fs = Nothing
		Set o_cache_cfg = Nothing
		Set o_instance = Nothing
		Set o_var = Nothing
		Set o_cfg = Nothing
		Set o_base = Nothing
		Set Cfg = Nothing
		Set Model = Nothing
		Set View = Nothing
		Set Loader = Nothing
		'-- 关闭数据库连接, 释放资源.
		If IsObject(Dao) And LCase(TypeName(Dao)) = "cls_app_dao" Then
			If (IsObject(Dao.db) And LCase(TypeName(Dao.db)) = "cls_ab_db") Then
				If AB.C.IsConn(Dao.db.Conn) Then
					If Dao.db.Conn.State = 1 Then Dao.db.Conn.Close()
				End If
			End If
		End If
		If (IsObject(o_db) And TypeName(o_db) = "cls_ab_db") Then
			If AB.C.IsConn(o_db.Conn) Then
				If o_db.Conn.State = 1 Then o_db.Conn.Close()
			End If
		End If
		Set o_db = Nothing
		Set Dao = Nothing
		AB.C.End()
	End Sub

	Public Property Get SoftName()
		SoftName = "AppCore"
	End Property
	Public Property Get Version()
		Version = "v1.0.0Alpha"
	End Property
	Public Property Get Release()
		Release = "20151118"
	End Property

	Public Property Let [Debug](ByVal b)
		b_debug = b
		AB.Debug = b_debug
		AB.db.Debug = b_debug
		AB.Error.Debug = b_debug
	End Property
	Public Property Get [Debug]
		[Debug] = b_debug
	End Property

	Public Property Let Cache(ByVal b)
		b_cache = b
	End Property
	Public Property Get Cache
		Cache = b_cache
	End Property

	Public Property Get AspEnable
		AspEnable = b_asp
	End Property
	Public Property Let AspEnable(ByVal b)
		b_asp = b
	End Property

	Public Property Let Locate(ByVal b)
		b_locate = b
	End Property
	Public Property Get Locate
		Locate = b_locate
	End Property

	Public Property Let LayerOut(ByVal b)
		b_layerout = b
	End Property
	Public Property Get LayerOut
		LayerOut = b_layerout
	End Property

	Public Property Let [CharSet](ByVal s)
		s_charset = Ucase(s)
	End Property
	Public Property Get [CharSet]()
		[CharSet] = s_charset
	End Property

	Public Property Let tbPrefix(ByVal s)
		Me.C("table_prefix") = Trim(s)
		s_tbprefix = s
	End Property
	Public Property Get tbPrefix()
		On Error Resume Next
		Dim d : If AB.C.IsDict(dbCfg()) Then Set d = dbCfg()
		If o_base.Exists("table_prefix") Or o_cfg.Exists("table_prefix") Then
			s_tbprefix = Trim(CfgVal("table_prefix"))
		ElseIf AB.C.IsDict(d) Then
			If d.Exists("table_prefix") Then
				s_tbprefix = Trim(d("table_prefix"))
			Else
				s_tbprefix = s_tbprefix & ""
			End If
		Else
			s_tbprefix = s_tbprefix & ""
		End If
		tbPrefix = s_tbprefix
		On Error Goto 0
	End Property

	Public Property Let C(ByVal p, ByVal s)
		AB.C.SetDict o_base, LCase(p), s
		ReCfg()
	End Property
	Public Property Get C(ByVal p)
		If IsObject(CfgVal(p)) Then Set C = CfgVal(p) Else C = CfgVal(p)
		ReCfg()
	End Property

	'刷新Cfg(注：要读取Cfg之前调用 ReCfg()方法 )
	'e.g. App.ReCfg : AB.Trace App.Cfg
	Public Sub ReCfg()
		On Error Resume Next
		For Each i In o_cfg
			i = LCase(i)
			If o_base.Exists(i) Then
				If IsObject(o_base(i)) Then Set o_cfg(i) = o_base(i) Else o_cfg(i) = o_base(i)
			End If
		Next
		On Error GoTo 0
	End Sub

	'获取配置项值
	Public Function CfgVal(ByVal s)
		Dim Matches, Match, t, tmp, bfg, mfg
		If Not AB.C.IsDict(o_base) Then Set o_base = AB.C.Dict()
		s = LCase(s)
		If o_base.Exists(s) Then
			If IsObject(o_base(s)) Then Set tmp = o_base(s) Else tmp = o_base(s)
		ElseIf o_cfg.Exists(s) Then
			If IsObject(o_cfg(s)) Then Set tmp = o_cfg(s) Else tmp = o_cfg(s)
		Else
			bfg = AB.C.IIF(Trim(CfgVal("baseconf"))<>"",CfgVal("baseconf"),"app")
			get_m = Lcase(Trim(Me.Req("m"))) : get_m = AB.C.IIF(get_m<>"", get_m, Me.Default_Group)
			If AB.C.IsDict(o_cfg("config:"& Lcase(get_m) &"")) Then
				mfg = Lcase(get_m)
				If o_cfg("config:"& mfg &"").Exists(s) Then
					If IsObject(o_cfg("config:"& mfg &"").Item(s)) Then Set tmp = o_cfg("config:"& mfg &"").Item(s) Else tmp = o_cfg("config:"& mfg &"").Item(s)
				ElseIf o_cfg("config:"& bfg &"").Exists(s) Then
					If IsObject(o_cfg("config:"& bfg &"").Item(s)) Then Set tmp = o_cfg("config:"& bfg &"").Item(s) Else tmp = o_cfg("config:"& bfg &"").Item(s)
				Else
					tmp = Empty
				End If
			Else
				If o_cfg("config:"& bfg &"").Exists(s) Then
					If IsObject(o_cfg("config:"& bfg &"").Item(s)) Then Set tmp = o_cfg("config:"& bfg &"").Item(s) Else tmp = o_cfg("config:"& bfg &"").Item(s)
				Else
					tmp = Empty
				End If
			End If
		End If
		If IsObject(tmp) Then Set CfgVal = tmp Else CfgVal = tmp
	End Function

	Public Function IsVal(ByVal p, ByVal s)
		Dim t, b : b = False
		p = LCase(p)
		If o_base.Exists(p) Then
			t = o_base(p)
			If CBool( LCase(t)=LCase(s) ) Then b = True
		ElseIf o_cfg.Exists(p) Then
			t = o_cfg(p)
			If CBool( LCase(t)=LCase(s) ) Then b = True
		Else
			b = False
		End If
		IsVal = b
	End Function

	'整个网站项目所在URL地址(绝对地址)
	Public Property Get RootURL()
		RootURL = AB.C.GetUrl("-1") & Me.RootPath()
	End Property

	'整个网站项目所在路径
	Public Property Get SitePath()
		SitePath = FixAbsPath(Trim(CfgVal("site_path")))
	End Property

	'获取整个网站项目所在目录，若在根路径则返回空值
	Public Property Get RootPath()
		Dim spath : spath = Trim(CfgVal("site_path"))
		RootPath = ""
		If spath<>"" Then
			If Right(spath,1)="/" Then
				RootPath = Left(spath, Len(spath)-1)
			Else
				RootPath = spath
			End If
		End If
	End Property

	'App缺省主题
	Public Property Let Default_Theme(ByVal s)
		Me.C("default_theme") = s
	End Property
	Public Property Get Default_Theme()
		Default_Theme = CfgVal("default_theme")
	End Property

	'App默认文档
	Public Property Let Default_Document(ByVal s)
		Me.C("default_document") = AB.C.IIF(Trim(s)<>"",Trim(s),"index.asp")
	End Property
	Public Property Get Default_Document()
		Default_Document = CfgVal("default_document")
	End Property

	'是否显示默认文档
	Public Property Let Display_Document(ByVal s)
		Me.C("display_document") = CBool(s)
	End Property
	Public Property Get Display_Document()
		Display_Document = CfgVal("display_document")
	End Property

	'App缺省Group
	Public Property Let Default_Group(ByVal s)
		Me.C("default_group") = s
	End Property
	Public Property Get Default_Group()
		Default_Group = AB.C.IIF(Trim(CfgVal("default_group"))<>"", Trim(CfgVal("default_group")), "home")
	End Property

	'App所在路径
	Public Property Let AppPath(ByVal s)
		Me.C("app_path") = FixAbsPath(s)
	End Property
	Public Property Get AppPath()
		AppPath = FixAbsPath(CfgVal("app_path"))
	End Property

	Public Property Let BasePath(ByVal s)
		Me.C("base_path") = FixAbsPath(s)
	End Property
	Public Property Get BasePath()
		BasePath = FixAbsPath(CfgVal("base_path"))
	End Property

	Public Property Let CorePath(ByVal s)
		Me.C("core_path") = FixAbsPath(s)
	End Property
	Public Property Get CorePath()
		CorePath = FixAbsPath(CfgVal("core_path"))
	End Property

	Public Property Let ExtendPath(ByVal s)
		Me.C("extend_path") = FixAbsPath(s)
	End Property
	Public Property Get ExtendPath()
		ExtendPath = FixAbsPath(CfgVal("extend_path"))
	End Property

	Public Property Let CachePath(ByVal s)
		Me.C("cache_path") = FixAbsPath(s)
	End Property
	Public Property Get CachePath()
		CachePath = FixAbsPath(CfgVal("cache_path"))
	End Property

	Public Function WorkPath(ByVal p)
		Dim path
		If Trim(p)="" Or Trim(p)="/" Then
			BasePath()
		Else
			path = BasePath() & p & "/"
		End If
		WorkPath = FixAbsPath(path)
	End Function

	Public Function UserPath(ByVal p)
		Dim path
		If Trim(p)="" Or Trim(p)="/" Then
			path = LibPath() & ""
		ElseIf Trim(p)<>"" Then
			path = LibPath() & "" & p & "/"
		Else
			path = LibPath() & ""
		End If
		UserPath = FixAbsPath(path)
	End Function

	Public Function LibPath()
		LibPath = FixAbsPath(CfgVal("lib_path"))
	End Function

	Public Function ConfPath()
		ConfPath = FixAbsPath(CfgVal("conf_path"))
	End Function

	Public Function LangPath()
		LangPath = FixAbsPath(CfgVal("lang_path"))
	End Function

	Public Function TplPath()
		TplPath = FixAbsPath(CfgVal("tpl_path"))
	End Function

	Public Function CommonPath()
		CommonPath = FixAbsPath(CfgVal("common_path"))
	End Function

	Public Function ActionPath()
		ActionPath = FixAbsPath(CfgVal("lib_path") & "Action/")
	End Function

	Public Function ModelPath()
		ModelPath = FixAbsPath(CfgVal("lib_path") & "Model/")
	End Function

	Public Function WidgetPath()
		WidgetPath = FixAbsPath(CfgVal("lib_path") & "Widget/")
	End Function

	Public Function StaticPath()
		StaticPath = FixAbsPath(CfgVal("static_path"))
	End Function

	Public Function UploadPath()
		UploadPath = FixAbsPath(CfgVal("upload_path"))
	End Function

	Public Function CssPath()
		CssPath = FixAbsPath(CfgVal("css_path"))
	End Function

	Public Function JsPath()
		JsPath = FixAbsPath(CfgVal("js_path"))
	End Function

	Public Function ImgPath()
		ImgPath = FixAbsPath(CfgVal("img_path"))
	End Function

	'获取数据库配置信息
	Public Function dbCfg()
		On Error Resume Next
		Dim t, dbfg : dbfg = AB.C.IIF(Trim(CfgVal("dbconf"))<>"",Trim(CfgVal("dbconf")),"db")
		t = LCase("config:"& dbfg )
		If dbfg<>"" Then
			If IsObject(CfgVal(t)) Then
				Set dbCfg = CfgVal(t)
			Else
				dbCfg = CfgVal(t)
			End If
		Else
			dbCfg = Empty
		End If
		On Error Goto 0
	End Function

	'存取db数据
	Public Property Let db(ByVal p)
		If IsObject(p) Then Set o_db = p
	End Property
	Public Property Get db()
		If IsObject(o_db) Then
			Set db = o_db
		Else
			db = Empty
		End If
	End Property

	Private Sub AutoPress()
		On Error Resume Next
		LoadBaseCommon__() '加载内置系统函数
		LoadBaseCore__() '加载核心
		LoadUserModel__() '加载用户模型并实例化
		LoadUserCommon__() '加载用户函数
		LoadCore("Base")
		'LoadCore("Home/Base; Home/Index")
		'LoadCore("Admin/Base")
		If IsVal("autorun", "true") Or IsVal("autorun", "1") Then AutoRun() '自动运行app核心
		On Error GoTo 0
	End Sub

	Public Sub AutoLoad(ByVal p)
		Dim absPath,Flist,i,path : absPath = FixAbsPath(p)
		Flist = AB.Fso.Lists(absPath,0)
		If Not AB.C.IsNul(Flist) Then
			For i = 0 To UBound(Flist,2)
				path = Flist(0,i)
				If AB.C.isFile(path) And LCase(AB.Fso.ExtOf(path))=".asp" Then
					ReadCache path
					If Err.Number <> 0 Then Me.Error.Throw 401, Err.Number, Err.Description
				End If
			Next
		End If
	End Sub

	Public Function IsLoad(ByVal p)
		On Error Resume Next
		If Mid(p,2,1)<>":" Then p = Server.MapPath(p)
		Dim b : b = False
		If o_cache_fs(LCase(p)).Ready Then
			b = True
		End If
		IsLoad = b
		On Error Goto 0
	End Function

	Private Sub LoadConf(ByVal p)
		On Error Resume Next
		Dim a, b, path, o, tmp, i, k, aid, s, t, c, v : a = Array()
		If IsArray(p) Then
			a = AB.A.Unique(p)
		Else
			b = Split(Trim(p),",")
			For Each t In b
				t = Trim(t&"")
				If t<>"" Then
					If Not AB.A.InArray(LCase(t),a) Then a = AB.A.Push(a,LCase(t))
				End If
			Next
		End If
		AB.Use "Xml"
		a = AB.A.Unique(a) '删除重复元素
		For i=0 To UBound(a)
			path = Me.ConfPath & Trim(a(i)) & CfgVal("conf_ext")
			If AB.C.isFile(path) Then
				c = Lcase(Trim(a(i)))
				t = "config:"& c
				If Not o_cfg.Exists(t) Or Not IsObject(o_cfg(t)) Then
					Set o_cfg(t) = Server.CreateObject(s_dictName)
				End If
				If Not o_cache_cfg.Exists(c) Then
					AB.Xml.Load path&">utf-8"
					o_cache_cfg(c) = path
					If AB.Xml("site").Length > 0 Then
						For k = 0 To AB.Xml("site")(0).ChildNodes.Length-1
							tmp = Trim(AB.Xml("site")(0).Child(k).Name)
							If tmp<>"" and tmp <> "name" Then
								v = AB.Xml("site")(0).Child(k).Value
								v = NodeStr(v)
								o_cfg(t)(tmp) = v
							End If
						Next
						Set o = AB.Xml("site")(0).Find("name[alias]")
						For k = 0 To o.Length-1
							aid = Trim(o(k).Attr("alias")&"")
							If aid<>"" Then
								If IsObject(o(k)) Then
									v = o(k).Value
									v = NodeStr(v)
									o_cfg(t)(LCase(aid)) = v
								End If
							End If
						Next
					End If
				End If
			End If
		Next
		On Error Goto 0
	End Sub

	Private Sub LoadCore(ByVal p)
		On Error Resume Next
		Dim a, b, path, o, tmp, i, k, t, dir, fname, msg, code : a = Array()
		If VarType(p) = vbString Then
			If IsArray(p) Then
				a = AB.A.Unique(p)
			Else
				p = Replace(p,"|",";")
				b = Split(Trim(p),";")
				For Each t In b
					t = Trim(t&"")
					If t<>"" Then
						If Not AB.A.InArray(LCase(t),a) Then a = AB.A.Push(a,LCase(t))
					End If
				Next
			End If
		ElseIf IsArray(p) Then
			a = p
		End If
		For i=0 To UBound(a)
			tmp = a(i)
			If Instr(tmp,"/")>0 Then
				dir = Trim(AB.C.CLeft(tmp, "/"))
				fname = AB.C.CRight(tmp, "/")
			Else
				dir = "" : fname = tmp
			End If
			path = Me.ActionPath & AB.C.IIf(dir="","",dir&"/") & AB.D.MCase(fname) & "Action.class.asp"
			If AB.C.isFile(path) Then
				If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
				ReadCache path
				InstantCore fname, dir '实例化Core
				If Err.Number <> 0 Then
					Me.Error.Throw 401, Err.Number, Err.Description
				End If
			End If
		Next
		On Error Goto 0
	End Sub

	'载入核心文件
	'e.g. App.Import("@.Helper.Page") : page_bar = App.Helper.Page.GetHtml()
	'e.g. App.Import("App.Core.Dao")
	Public Sub Import(ByVal p)
		On Error Resume Next
		Dim t, a, path, dir, fn, i : p = Trim(p)
		If LCase(p) = ":all" Then
			AutoLoad(Me.BasePath)
			'AutoLoad(Me.WorkPath("Core"))
			Exit Sub
		ElseIf InStr(p,"@.")>0 Then '加载用户添加的核心
			t = Right(p, Len(p)-2)
			a = Split(t, ".")
			For i=0 To UBound(a)-1
				dir = dir & a(i) & "/"
			Next
			fn = a(UBound(a)) & "."& a(UBound(a)-1) &".asp"
			path = Me.LibPath & dir & fn
		ElseIf InStr(Trim(LCase(p)),"app.")=1 Then '加载app核心
			t = Right(p, Len(p)-4)
			a = Split(t, ".")
			For i=0 To UBound(a)-1
				dir = dir & a(i) & "/"
			Next
			If LCase(a(0))="core" Then
				fn = "App."& a(UBound(a)) &".asp"
			Else
				fn = a(UBound(a)) & "."& a(UBound(a)-1) &".asp"
			End If
			path = Me.BasePath & dir & fn
		End If
		If AB.C.isFile(path) And LCase(AB.Fso.ExtOf(path))=".asp" Then
			ReadCache path
			If Err.Number <> 0 Then Me.Error.Throw 401, Err.Number, Err.Description
		End If
		On Error Goto 0
	End Sub

	'载入Helper小助手
	Public Function Helper(ByVal p)
		Set Helper = App.Loader.Helper(p)
	End Function

	'载入Library库文件
	Public Function Library(ByVal p)
		Set Library = App.Loader.Library(p)
	End Function

	'运行URL模式
	Public Sub Run()
		If Not b_hasrun Then
			Me.RunA get_a, get_c &":"& get_m
			b_hasrun = True
		End If
	End Sub

	Public Sub AutoRun()
		Me.Run()
	End Sub

	'自动创建模型类文件
	Public Sub AutoCreateModel()
		Dim tables, t, f, c, tb, m, arr, i, j, tmp
		tables = AB.C.GetTables() '自动获取数据库所有表名信息
		For i = 0 To Ubound(tables)
			tb = tables(i)
			If AB.C.RegTest(tb, "^[a-zA-Z]\w*$") And AB.C.RegTest(tb, "^"& App.Dao.tbPrefix &"\w+$") Then '筛选出符合规则的表：只能是英文+数字组合，并且是以数据表前缀开头的表
				m = AB.C.RegReplace(tb, "^"& App.Dao.tbPrefix &"", "") '清除数据表前缀
				't = AB.D.MCase(Lcase(m))
				arr = Array()
				tmp = Split(m,"_")
				For j=0 To UBound(tmp)
					arr = AB.A.Push(arr, AB.D.MCase(LCase(tmp(j))))
				Next
				t = Join(arr,"_")
				f = Me.ModelPath & LCase(t) &"_model.class.asp"
				If Not AB.C.isFile(f) Then
					c = "<"&"%" & VbCrlf & "" &_
						"Class Model_"& t &"" & VbCrlf & "" &_
						"" & VbCrlf & "" &_
						"	Public Table '数据表" & VbCrlf & "" &_
						"" & VbCrlf & "" &_
						"	Private Sub Class_Initialize()" & VbCrlf & "" &_
						"		Table = """& LCase(t) &"""" & VbCrlf & "" &_
						"	End Sub" & VbCrlf & "" &_
						"" & VbCrlf & "" &_
						"	'默认缺省方法为Dao，注意：此方法必不可少！" & VbCrlf & "" &_
						"	Public Default Function Dao()" & VbCrlf & "" &_
						"		Set Dao = App.Dao.New" & VbCrlf & "" &_
						"		Dao.Table = Table" & VbCrlf & "" &_
						"	End Function" & VbCrlf & "" &_
						"" & VbCrlf & "" &_
						"End Class" & VbCrlf & "" &_
						"%"&">"
					AB.Fso.CreateFile f, c
				End If
			End If
		Next
	End Sub

	'实例化Core
	Public Sub InstantCore(ByVal p, ByVal d)
		On Error Resume Next
		Dim t, b, f, tempCode, errNo, errTxt, errSrc
		b = AB.C.IIf(d="","", AB.D.MCase(d) & "_") & AB.D.MCase(p)
		t = AB.D.MCase(p) &"Action" & AB.C.IIf(d="","", "_" & AB.D.MCase(d))
		If Not o_instance.Exists(LCase(b)) Then
			'ExecuteGlobal("Set "& b &" = App.ActionNew("""& p &""","""& d &""")")
			ExecuteGlobal("Set "& b &" = New "& t &"")
			o_instance(LCase(b)) = t
			If Err.Number=506 Then '类出错或未定义
				errNo = Err.Number : errTxt = Err.Description : errSrc = Err.Source
				Me.Error.setErr errNo, errTxt, errSrc
				'Me.Error.Throw 506, errNo, errTxt
				f = Me.ActionPath() & AB.C.IIf(d="", AB.D.MCase(p) &"Action.class.asp", d & "/" & AB.D.MCase(p) &"Action.class.asp")
				f = Server.MapPath(f)
				If o_cache_fs.Items.Exists( LCase(f) ) Then
					tempCode = Trim(AB.C.IncCode(f))
					If AB.C.RegTest(tempCode,"Class[\s\t]+"& t & "[\n\s\:]+[\s\S]*[\n\s\:]+End[\s\t]+Class") Then
						Me.Error.Throw 403, errNo, "类代码内部出错"
					Else
						Me.Error.Throw 506, errNo, errTxt
					End If
				Else
					'Me.Error.Throw 506, errNo, errTxt
				End If
			End If
		End If
		On Error Goto 0
	End Sub

	'实例化Model
	Public Sub InstantModel(ByVal p)
		On Error Resume Next
		Dim tables, t, tmp, tb, m, arr, i, j : arr = Array()
		If Trim(p)="" Or p = ":all" Then '全部实例化
			tables = AB.C.GetTables() '自动获取数据库所有表名信息
			If Not AB.C.IsNul(tables) Then
				For i = 0 To Ubound(tables)
					tb = tables(i)
					If AB.C.RegTest(tb, "^[a-zA-Z]\w*$") Then '筛选出符合规则得表面：只能是英文+数字
						If AB.C.RegTest(tb, "^" & App.Dao.tbPrefix & "\w+$") Then '只筛选有数据表前缀的表名
							m = AB.C.RegReplace(tb, "^"& App.Dao.tbPrefix &"", "") '清除数据表前缀
							't = AB.D.MCase(Lcase(m))
							arr = Array()
							tmp = Split(m,"_")
							For j=0 To UBound(tmp)
								arr = AB.A.Push(arr, AB.D.MCase(LCase(tmp(j))))
							Next
							t = Join(arr,"_")
							If Not o_instance(LCase(t&"_Model")) Then
								ExecuteGlobal("Set "& t &"_Model = New Model_"& t &" : "& t &"_Model.Table = """ & LCase(m) & """")
								o_instance(LCase(t&"_Model")) = "Model_"& t &""
								If Err Or (Not IsObject(Eval(t &"_Model"))) Or LCase(TypeName(Eval(t &"_Model")))<>Lcase("Model_"& t) Then
									Err.Clear()
									ExecuteGlobal("Set "& t &"_Model = App.Dao.New : "& t &"_Model.Table = """ & LCase(m) & """")
									o_instance(LCase(t&"_Model")) = "Cls_App_Model"
								End If
							End If
							Err.Clear()
						End If
					End If
				Next
			End If
		Else '按需实例化
			't = AB.D.MCase(Lcase(p))
			arr = Array()
			tmp = Split(p,"_")
			For j=0 To UBound(tmp)
				arr = AB.A.Push(arr, AB.D.MCase(LCase(tmp(j))))
			Next
			t = Join(arr,"")
			If Not o_instance(LCase(t&"Model")) Then
				ExecuteGlobal("Set "& t &"Model = New Model_"& t &" : "& t &"Model.Table = """ & p & """")
				o_instance(LCase(t&"Model")) = "Model_"& t &""
				If Err Or (Not IsObject(Eval(t &"Model"))) Or LCase(TypeName(Eval(t &"Model")))<>Lcase("Model_"& t) Then
					Err.Clear()
					ExecuteGlobal("Set "& t &"Model = App.Dao.New : "& t &"Model.Table = """ & p & """")
					o_instance(LCase(t&"Model")) = "Cls_App_Model"
				End If
			End If
		End If
		On Error Goto 0
	End Sub

	Public Sub ActionRun(ByVal a, ByVal c)
		On Error Resume Next
		Dim o, um, uc, ua, errnum, msg, path, base, t, b, arr, i, tmp : ua = a : errnum = 0
		If Trim(c&"")="" Then c="Index:Home"
		If Trim(ua&"")="" Then ua="Index"
		If Instr(c,":")>0 Then
			uc = AB.C.CLeft(c,":")
			um = AB.C.CRight(c,":")
		Else
			um = "Home" : uc = Trim(c)
		End If
		If Trim(um&"")="" Then um="Home"
		If Trim(uc&"")="" Then uc="Index"
		If AB.A.Len(a_groupList)>0 Then
			If Not AB.A.InArray( LCase(um), a_groupList ) Then
				Me.Error.Throw 401, 0, "此App Group被禁用。"
			End If
		End If
		path = Me.ActionPath & AB.C.IIf(um="","",um&"/") & uc & "Action.class.asp"
		base = Me.ActionPath & AB.C.IIf(um="","",um&"/") & "Base" & "Action.class.asp"
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		If Mid(base,2,1)<>":" Then base = Server.MapPath(base)
		ReadCache base '优先载入Base
		ReadCache path
		InstantCore "Base", um '优先实例化Base
		InstantCore uc, um '实例化Action对象
		Err.Clear
		Set o = ActionNew(uc,um)
		If Err.Number=0 and Not (o Is Nothing) Then
			Execute "o."&ua&"()"
			errnum = Err.Number
			If errnum<>0 Then
				Err.Clear
				If errnum=438 Then '对象不支持属性或方法
					Execute "o.empty()"
				End If
				If Err Then
					Err.Clear
					Set o = ActionNew("Base",um)
					If Err.Number=0 and Not (o Is Nothing) Then
						Err.Clear
						Execute "o.empty()"
						If Err Then
							Err.Clear
							Set o = ActionNew("Base","")
							If Not (o Is Nothing) Then
								Execute "o.index()"
							End If
						End If
					Else
						Err.Clear
						Set o = ActionNew("Base","")
						If Not (o Is Nothing) Then
							Execute "o.index()"
						End If
					End If
				End If
				If Err.Number <> 0 Then Me.Error.Throw 401, Err.Number, Err.Description
			End If
		Else
			Err.Clear
			Set o = ActionNew("Base",um)
			If Err.Number=0 and Not (o Is Nothing) Then
				Execute "o."&ua&"()"
				If Err.Number=438 Then '对象不支持属性或方法
					Err.Clear : Execute "o.empty()"
				End If
				If Err Then
					Err.Clear
					Set o = ActionNew("Base","")
					If Not (o Is Nothing) Then
						Execute "o.index()"
					End If
				End If
			Else
				Err.Clear
				Set o = ActionNew("Base","")
				If Err.Number=0 and Not (o Is Nothing) Then
					Execute "o.index()"
				Else
					'If Err Then Me.Error.Throw 401, Err.Number, Err.Description
					Me.Error.Throw 401, 506, "无Controller，且基类没有被定义。"
				End If
			End If
		End If
		Err.Clear
		Set o = Nothing
		On Error GoTo 0
	End Sub
	Public Sub ActRun(ByVal a, ByVal c) : ActionRun a, c : End Sub
	Public Sub RunA(ByVal a, ByVal c) : ActionRun a, c : End Sub

	Public Function ActionNew(ByVal p, ByVal c)
		On Error Resume Next
		If Err.Number<>0 Then Err.Clear
		Dim o, act, msg, path : act = p&"Action" & AB.C.IIf(c="","","_"&c)
		path = Me.ActionPath & AB.C.IIf(c="","",c&"/") & p & "Action.class.asp"
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		ReadCache path
		Dim t : t = Eval("LCase(TypeName(New "&act&"))")
		Set ActionNew = Nothing
		If Err.Number=0 and t = LCase(act) Then
			Execute "Set o = New "&act&""
			If Err.Number=0 and IsObject(o) Then
				Set ActionNew = o
				'If Err.Number <> 0 Then Me.Error.Throw 401, Err.Number, Err.Description
			End If
		Else
			'If Err.Number <> 0 Then Me.Error.Throw 401, Err.Number, Err.Description
		End If
		Err.Clear
		On Error GoTo 0
	End Function

	Public Sub CreateCaches(ByVal p, ByVal c)
		Dim Flist, i, path, newfile, context, cache_dir, resouce_dir : resouce_dir = Array()
		If LCase(p)="core" Or LCase(p)="common" Or LCase(p)="model" Then 'core
			'resouce_dir = AB.A.Push(resouce_dir, Me.WorkPath("Core"))
			If LCase(p)="model" Then
				resouce_dir = AB.A.Push(resouce_dir, Me.ModelPath)
			ElseIf LCase(p)="common" Then
				resouce_dir = AB.A.Push(resouce_dir, Me.BasePath & AB.D.MCase(p))
				If IsVal("autoload", "true") Or IsVal("autoload", "1") Then resouce_dir = AB.A.Push(resouce_dir, Me.CommonPath)
			Else
				resouce_dir = AB.A.Push(resouce_dir, Me.BasePath & AB.D.MCase(p))
			End If
			cache_dir = Me.CachePath & "data/"
			newfile = "_"& AB.D.MCase(p) &".asp"
			context = ""
			If Not AB.C.IsNul(resouce_dir) Then
				For Each dir In resouce_dir
					Flist = AB.Fso.Lists(FixAbsPath(dir),0)
					For i = 0 To UBound(Flist,2)
						path = Flist(0,i)
						'fname = LCase(AB.Fso.NameOf(path))
						If AB.C.isFile(path) And LCase(AB.Fso.ExtOf(path))=".asp" Then
							context = context & AB.C.IncCode(path) & VBCrlf
						End If
					Next
				Next
			End If
			AB.Fso.CreateFile cache_dir & newfile, "<"&"% " & context & "%"&">"
		ElseIf AB.C.IsFolder(p) Then '目录
			context = ""
			Flist = AB.Fso.Lists(p,0)
			For i = 0 To UBound(Flist,2)
				path = Flist(0,i)
				If AB.C.isFile(path) And LCase(AB.Fso.ExtOf(path))=".asp" Then
					context = context & AB.C.IncCode(path) & VBCrlf
				End If
			Next
			AB.Fso.CreateFile c, "<"&"% " & context & "%"&">"
		' ElseIf AB.C.IsFile(p) Then '文件
			' AB.Fso.CreateFile p, "<"&"% " & c & "%"&">"
		Else
			AB.Fso.CreateFile p, "<"&"% " & c & "%"&">"
		End If
	End Sub

	Public Function ReadCache(ByVal p)
		On Error Resume Next
		If Mid(p,2,1)<>":" Then p = Server.MapPath(p)
		'AB.C.Include(p)
		Dim info : info = AB.Pub.FileInfo(p)
		Dim dict : Set dict = AB.C.Dict()
		Dim tempCode
		If AB.C.isFile(p) Then
			If o_cache_fs(LCase(p)).Ready Then
				Dim o : Set o = o_cache_fs(LCase(p)).Value
				If o("lasttime") = info(5) Then
					tempCode = Trim(o("content"))
				Else
					tempCode = Trim(AB.C.IncCode(p))
					dict("lasttime") = info(5)
					dict("content") = tempCode
					o_cache_fs(LCase(p)).Del
					o_cache_fs(LCase(p)) = dict
					o_cache_fs(LCase(p)).SaveApp
				End If
			Else
				tempCode = Trim(AB.C.IncCode(p))
				dict("lasttime") = info(5)
				dict("content") = tempCode
				o_cache_fs(LCase(p)).Del
				o_cache_fs(LCase(p)) = dict
				o_cache_fs(LCase(p)).SaveApp
			End If
			ExecuteGlobal tempCode
		End If
		'If Err.Number<>0 Then : ReadCache = False : Exit Function : End If
		If Err Then Err.Clear()
		ReadCache = True
		On Error Goto 0
	End Function

	Public Function FixAbsPath(ByVal p)
		p = AB.C.IIF(Left(p,1)= "/", p, "/" & p)
		p = AB.C.IIF(Right(p,1)="/", p, p & "/")
		FixAbsPath = p
	End Function

	Public Function CheckToken() '表单验证Token, 防多次提交
		Dim name, key, value, a, bool : bool = True
		If CfgVal("token_on")="true" Then
			AB.Use "Form"
			name = CfgVal("token_name")
			If Not ( AB.Form.Has(name) And AB.C.HasCookie(name,"") ) Then
				bool = False
			Else
				a = Split(AB.Form(name),"@#@")
				key = a(0)
				If AB.A.Len(a)>1 Then value = Cstr(a(1))
				If Cstr(Request.Cookies(name)(key))<>"" And Cstr(Request.Cookies(name)(key)) = value Then
					Response.Cookies(name)(key) = Empty '验证完即销毁
					bool = True
				Else
					bool = False
				End If
			End If
		End If
		CheckToken = bool
	End Function

	Public Sub ReadFile(ByVal p)
		ReadCache p
	End Sub

	'设置传参全局变量
	Public Sub SetReq(ByVal p, ByVal v)
		p = LCase(Trim(p))
		o_var(p) = v
		If p="a" Then ExecuteGlobal "REQ_A = """& v &""" : ACTION_NAME = """& v &""" "
		If p="c" Then ExecuteGlobal "REQ_C = """& v &""" : MODULE_NAME = """& v &""" "
		If p="m" Then ExecuteGlobal "REQ_M = """& v &""" : GROUP_NAME = """& v &""" : APP_NAME = """& v &""" "
	End Sub
	Public Sub SetVar(ByVal p, ByVal v) : SetReq p, v : End Sub

	'判断Request.QueryString是否包含某参数
	Public Function QsHas(ByVal p)
		Dim i, bool : bool = False
		For Each i In Request.QueryString
			If LCase(i)=LCase(p) Then
				bool = True
				Exit For
			End If
		Next
		QsHas = bool
	End Function

	'判断Request数据中是否包含某参数
	Public Function ReqHas(ByVal p)
		Dim bool : bool = False
		AB.Use "Form"
		If Me.QsHas(p) Then
			bool = True
		ElseIf AB.Form.Has(p) Then
			bool = True
		End If
		ReqHas = bool
	End Function

	'全局获取URL参数或表单元素
	'1.若URL参数含某参数，优先获取URL参数、
	'2.若没URL参数，进一步从Form表单元素中获取。
	'3.支持 获取表单enctype="multipart/form-data" 提交方式的数据。
	Public Function Req(ByVal p)
		On Error Resume Next
		If o_var.Exists(LCase(p)) Then
			Req = o_var(LCase(p))
		Else
			If Me.QsHas(p) Then
				Req = AB.C.Get(p)
			Else
				'Req = AB.C.Req(p)
				If Request.ServerVariables("REQUEST_METHOD")="POST" Then
					AB.Use "Form"
					IF Instr(Request.ServerVariables("CONTENT_TYPE"),"multipart/form-data")>0 Then
						'Req = AB.Form.FormVar(p)
						If AB.Form.Count>0 Then
							If AB.Form.Has(p) Then
								Req = AB.Form.FormVar(p)
							Else
								Req = Empty
								If IsObject(Me.ReqMulti(p)) Then Set Req=Me.ReqMulti(p) Else Req=Me.ReqMulti(p)
							End If
						Else
							Req = Empty
						End If
					Else
						If AB.Form.Has(p) Then
							Req = AB.Form.FormVar(p)
						Else
							Req = Empty
							If IsObject(Me.ReqMulti(p)) Then Set Req=Me.ReqMulti(p) Else Req=Me.ReqMulti(p)
						End If
					End If
				Else
					Req = AB.C.Req(p)
				End If
			End If
		End If
		If AB.C.IsNum(Req) Then Req = CLng(Req)
		On Error Goto 0
	End Function

	'获取表单enctype="multipart/form-data"提交方式的数据
	Public Function ReqData(ByVal p)
		On Error Resume Next
		If o_var.Exists(LCase(p)) Then
			ReqData = o_var(LCase(p))
		Else
			If Request.ServerVariables("REQUEST_METHOD")="POST" Then
				IF Instr(Request.ServerVariables("CONTENT_TYPE"),"multipart/form-data")>0 Then
					AB.Use "Form"
					'ReqData = AB.Form.Get(p)
					ReqData = AB.Form.FormVar(p)
				Else
					ReqData = AB.C.Req(p)
				End If
			Else
				ReqData = AB.C.Req(p)
			End If
		End If
		If AB.C.IsNum(p) Then ReqData = CLng(ReqData)
		On Error Goto 0
	End Function

	'获取多个类似元素
	'比如表单元素：<input name="setting[title]" /><input name="setting[keywords]" />
	'则可用：App.ReqMulti("setting") 获取，值返回 字典对象。匹配不到则返回 Empty
	Public Function ReqMulti(ByVal p)
		On Error Resume Next
		Dim d, m, n, i, f, v, o, regex, match, a, b
		Set d = AB.C.Dict() : m = 0 : n = 0
		Set o = AB.C.Dict()
		AB.Use "Form"
		For Each i In AB.Form.Items
			f = i
			v = AB.Form.Item(i)
			If AB.C.RegTest(f,"^"&p&"\[(\w*)\](\[\w*\])*$") Then
				Set regex = AB.C.RegMatch(f, "^"&p&"\[(\w*)\]\[(\w*)\]$")
				For Each match In regex
					a = match.SubMatches(0)
					b = match.SubMatches(1)
					If AB.C.IsNul(o(a)) Then Set o(a) = AB.C.Dict()
					If b="" Then
						b = m : m = m + 1
					End If
					o(a)(b) = v
					If a="" Then
						a = n : n = n + 1
					End If
					Set d(a) = o(a)
					f = AB.C.RP(f, match.Value, "")
				Next
				Set regex = Nothing
				Set regex = AB.C.RegMatch(f, "^"&p&"\[(\w*)\]$")
				For Each match In regex
					a = match.SubMatches(0)
					If a="" Then
						a = n : n = n + 1
					End If
					d(a) = v
				Next
			End If
		Next
		If Err.Number=0 And Not AB.C.IsNul(d) Then Set ReqMulti=d Else ReqMulti = Empty
		Set o = Nothing : Set d = Nothing
		On Error Goto 0
	End Function

	Public Function ReqTrim(ByVal p)
		ReqTrim = Trim(Me.Req(p))
	End Function

	Public Function ReqSet(ByVal p, ByVal s)
		Dim t : t = Me.Req(p)
		If Trim(t)="" Then t = s
		If AB.C.IsNum(t) Then t = CLng(t)
		ReqSet = t
	End Function

	Public Function ReqInt(ByVal p, ByVal s)
		Dim t : t = Me.ReqSet(p,s)
		If AB.C.IsNum(t) Then
			t = CLng(t)
		Else
			t = 0
		End If
		ReqInt = t
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d, i, f, v, spl : Set d = AB.C.Dict()
		AB.Use "Form"
		'AB.Form.Fun = "AB.E.UnEscape" '设置Form函数
		For i=0 To AB.Form.Count-1
			f = AB.Form(i).Name
			v = AB.Form(i).Value
			d(f) = v
		Next
		Set dictForm = d
		On Error Goto 0
	End Function

	Public Sub CC()
		AB.C.Clear()
	End Sub

	Public Sub noCache()
		AB.C.noCache()
	End Sub

	'---------待完善 begin ---------

		'URL跳转（支持路由跳转）
		Public Sub RR(ByVal p)
			Dim url
			'Dim self : self = AB.C.SelfName() '自身文件名
			If InStr(LCase(p), "url:") > 0 Then
				url = AB.C.CRight(p, "url:")
			Else
				url = U(p)
			End If
			AB.C.RR(url)
		End Sub

	'---------待完善 end ---------

		'=====辅助函数======

		Private Sub PassURL()
			get_a = AB.C.IfHas(Trim(get_a), "index")
			get_c = AB.C.IfHas(Trim(get_c), "index")
			get_m = AB.C.IfHas(Trim(get_m), Me.Default_Group)
			'-- 检查合法a,c,m参数
			get_a = AB.C.IIF(AB.C.RegTest(get_a, "^[a-zA-Z][a-zA-Z0-9_]*$"), get_a, "index")
			get_c = AB.C.IIF(AB.C.RegTest(get_c, "^[a-zA-Z][a-zA-Z0-9_]*$"), get_c, "index")
			get_m = AB.C.IIF(AB.C.RegTest(get_m, "^[a-zA-Z][a-zA-Z0-9_]*$"), get_m, Me.Default_Group)
			get_a = LCase(get_a) : get_c = LCase(get_c) : get_m = LCase(get_m)
			o_var("a") = get_a : o_var("c") = get_c : o_var("m") = get_m
			ExecuteGlobal "Dim REQ_A : REQ_A = """& get_a &""" "
			ExecuteGlobal "Dim REQ_C : REQ_C = """& get_c &""" "
			ExecuteGlobal "Dim REQ_M : REQ_M = """& get_m &""" "
			ExecuteGlobal "Dim ACTION_NAME : ACTION_NAME = """& get_a &""" "
			ExecuteGlobal "Dim MODULE_NAME : MODULE_NAME = """& get_c &""" "
			ExecuteGlobal "Dim GROUP_NAME : GROUP_NAME = """& get_m &""" "
			ExecuteGlobal "Dim APP_NAME : APP_NAME = """& get_m &""" "
			ExecuteGlobal "Dim REQ_WAY : REQ_WAY = """& Request.ServerVariables("REQUEST_METHOD") &""" "
			ExecuteGlobal "Dim REQUEST_METHOD : REQUEST_METHOD = """& Request.ServerVariables("REQUEST_METHOD") &""" "
			ExecuteGlobal "Dim IS_POST : IS_POST = """& (Request.ServerVariables("REQUEST_METHOD")="POST") &""" "
			ExecuteGlobal "Dim IS_GET : IS_GET = """& (Request.ServerVariables("REQUEST_METHOD")="GET") &""" "
		End Sub

		Private Function FixPath(ByVal p)
			p = AB.C.IIF(Right(p,1)="/", p, p & "/")
			FixPath = p
		End Function

		Private Sub LoadBaseCommon__()
			Dim fpath
			AutoLoad(Me.BasePath & "Common/")
		End Sub

		Private Sub LoadUserCommon__()
			Dim fpath
			If Not b_cache Then
				If IsVal("autoload", "true") Or IsVal("autoload", "1") Then AutoLoad(Me.CommonPath) '通用common
			Else
				fpath = Me.CachePath & "data/" & "_Common.asp"
				If Mid(fpath,2,1)<>":" Then fpath = Server.MapPath(fpath)
				If Not AB.C.IsFile(fpath) Then CreateCaches "Common", "" '缓存Common
				AB.C.xInclude(fpath)
			End If
		End Sub

		Private Sub LoadBaseCore__()
			On Error Resume Next
			Dim fpath, i, bfg, mfg : bfg = AB.C.IIF(Trim(CfgVal("baseconf"))<>"",CfgVal("baseconf"),"app")
			Dim a, b, j, s_groupList : a = Array()
			If o_cfg("config:"& bfg &"").Exists("app_group_list") Then s_groupList = Trim(o_cfg("config:"& bfg &"")("app_group_list"))
			If AB.C.IsDict(o_cfg("config:"& bfg &"")) Then
				For Each i In o_cfg("config:"& bfg &"")
					o_cfg(i) = o_cfg("config:"& bfg &"").Item(i)
				Next
			End If
			get_m = Lcase(Trim(Me.Req("m"))) : get_m = AB.C.IIF(get_m<>"", get_m, Me.Default_Group)
			mfg = Lcase(get_m)
			''a_groupList = AB.A.Push(a_groupList, mfg)
			b = AB.C.Split(s_groupList, ",")
			If AB.A.Len(b)>0 Then
				For j=0 To UBound(b)
					If Not AB.A.InArray( LCase(b(j)), a ) Then a = AB.A.Push( a, LCase(b(j)) )
				Next
			End If
			a_groupList = AB.A.Unique(a) '配置中的组（配置中没包含此组则表示禁用该组）
			If Not AB.C.IsNul(s_groupList) Then
				'LoadConf(s_groupList)
				If AB.A.InArray( mfg, a_groupList ) Then
					LoadConf(mfg)
				End If
			End If
			If AB.C.IsDict(o_cfg("config:"& mfg &"")) Then
				For Each i In o_cfg("config:"& mfg &"")
					o_cfg(i) = o_cfg("config:"& mfg &"").Item(i)
				Next
			End If
			PassURL() '解析URL中的m,c,a参数
			If Not b_cache Then
				'Import(":all")
				Import("App.Core.Error") : Set [Error] = New Cls_App_Error
				Import("App.Core.Dao") : Set Dao = New Cls_App_Dao
				Import("App.Core.Model") : Set Model = New Cls_App_Model
				Import("App.Core.View") : Set View = New Cls_App_View
				Import("App.Core.Loader") : Set Loader = New Cls_App_Loader
			Else
				fpath = Me.CachePath & "data/" & "_Core.asp"
				If Mid(fpath,2,1)<>":" Then fpath = Server.MapPath(fpath)
				If Not AB.C.IsFile(fpath) Then CreateCaches "Core", "" '缓存Core
				AB.C.xInclude(fpath)
				Set [Error] = New Cls_App_Error
				Set Dao = New Cls_App_Dao
				Set Model = New Cls_App_Model
				Set View = New Cls_App_View : View.Init()
			End If
			On Error Goto 0
		End Sub

		Private Sub LoadUserModel__()
			Dim fpath
			If Not b_cache Then
				AutoCreateModel() '自动创建模型类文件
				AutoLoad(Me.ModelPath) '自动加载模型类文件
				InstantModel(":all") '自动实例化Model
			Else
				AutoCreateModel() '自动创建模型类文件
				fpath = Me.CachePath & "data/" & "_Model.asp"
				If Mid(fpath,2,1)<>":" Then fpath = Server.MapPath(fpath)
				If Not AB.C.IsFile(fpath) Then CreateCaches "Model", "" '缓存Core
				AB.C.xInclude(fpath)
				InstantModel(":all") '自动实例化Model
			End If
		End Sub

		Private Function NodeStr(ByVal s)
			Dim Matches, Match, t, v, tmp : tmp = s
			tmp = AB.C.RP(tmp,"&amp;","&")
			If AB.C.RegTest(tmp, "\{%([\w\_\-]+)%\}") Then '引用配置
				Set Matches = AB.C.RegMatch(tmp, "\{%([\w\_\-]+)%\}")
				For Each Match In Matches
					t = Match.SubMatches(0)
					v = CfgVal(t)
					tmp = AB.C.RegReplace(tmp, "\{%([\w\_\-]+)%\}", v)
				Next
				Set Matches = Nothing
			End If
			If AB.C.RegTest(tmp, "\{\$([\w\_]+)\}") Then '引用全局变量
				Set Matches = AB.C.RegMatch(tmp, "\{\$([\w\_]+)\}")
				For Each Match In Matches
					t = Match.SubMatches(0)
					v = Eval(t)
					tmp = AB.C.RegReplace(tmp, "\{\$([\w\_]+)\}", v)
				Next
				Set Matches = Nothing
			End If
			NodeStr = tmp
		End Function

End Class

Class Cls_App_Out

	Public Sub W(ByVal s) : AB.C.W s : End Sub
	Public Sub WR(ByVal s) : AB.C.WR s : End Sub
	Public Sub Print(ByVal s) : AB.C.W s : End Sub
	Public Sub Echo(ByVal s) : AB.C.Echo s : End Sub
	Public Sub Put(ByVal s) : AB.C.Echo s : App.Exit() : End Sub
	Public Sub Clear() : AB.C.Clear() : End Sub
	Public Sub [End]() : App.Exit() : End Sub
	Public Sub Redirect(ByVal s) : AB.C.RR s : End Sub
	Public Sub RR(ByVal s) : AB.C.RR s : End Sub
	Public Sub Flush() : AB.C.Flush() : End Sub

End Class
%>