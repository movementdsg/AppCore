<%
'--------------------------------------------------------------------
'系统内置函数，不懂的不要乱做修改，否则可能影响整个程序系统。
'--------------------------------------------------------------------

'获取子级分类id(数组)
Function arr_child_ids(ByVal id, ByVal self)
	Dim ids : ids = App_System_Data.get_child_ids(id, CBool(self))
	arr_child_ids = ids
End Function

'获取子级分类id
Function child_ids(ByVal id, ByVal self)
	Dim ids : ids = App_System_Data.get_child_ids(id, CBool(self))
	child_ids = Join(ids,",")
End Function

'获取所有父类ID
Function parent_ids(ByVal id, ByVal self)
	Dim spid, ids, arr, i : arr = Array() : self = CBool(self)
	AB.Use "A"
	If CBool(self) Then spid = App_System_Data.get_ssid(id) Else spid = App_System_Data.get_spid(id)
	If spid<>"" Then
		For Each i In Split(spid,"|")
			If Trim(i)<>"" Then arr = AB.A.Push(arr, Trim(i))
		Next
		arr = AB.A.Unique(arr) '移除相同元素
	End If
	parent_ids = Join(arr,",")
End Function

'是否为某栏目的子栏目
Function isChild(ByVal id, ByVal parentid)
	Dim ids : ids = App_System_Data.get_child_ids(parentid, true)
	AB.Use "A"
	isChild = AB.A.InArray(id, ids)
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''' App System Data
''''''''''''''''''''''''''''''''''''''''''''''''''
Dim App_System_Data : Set App_System_Data = New Cls_App_System_Data
Class Cls_App_System_Data

	Private Sub Class_Initialize()
		AB.Use "A"
	End Sub

	Public Function get_child_ids(ByVal id, ByVal self)
		Dim a : a = get_child__("", id)
		a = AB.A.Push(a, intval(id))
		a = AB.A.Unique(a)
		If self = False Then a = AB.A.Del(a, id)
        get_child_ids = a
	End Function

		Private Function get_child__(ByRef a, ByVal id)
			Dim table : table = "class"
			Dim ids, i, rs : AB.Use "A" : id = intval(id)
			If AB.C.IsNul(a) Then a = Array()
			Set rs = M(table)().Field("id").Where("pid="& id &"").Fetch()
			If rs.RecordCount>0 Then
				Do While Not rs.Eof
					ids = get_child__(a, rs("id").Value)
					For Each i In ids
						a = AB.A.Push(a, i)
					Next
					rs.MoveNext
				Loop
			End If
			rs.Close() : Set rs = Nothing
			a = AB.A.Push(a, id)
			a = AB.A.Unique(a)
			get_child__ = a
		End Function

	'根据pid获取spid
	Public Function get_ssid(Byval pid)
		Dim spid : spid = get_spid(pid)
		If AB.C.IsNul(spid) Then spid = "0"
		spid = AB.C.IIF(spid="" Or spid="0", pid & "|", spid & pid & "|")
		If IntVal(pid)=0 Then spid = 0
		get_ssid = spid
	End Function

	'根据id获取spid
	Public Function get_spid(Byval id)
		Dim table : table = "class"
		Dim rs, str : id = intval(id)
		Set rs = M(table)().Where("id=" & id).Fetch()
		If rs.RecordCount>0 Then
			If rs("pid")>0 Then
				id = IntVal(rs("pid"))
				str = id & "|" & str
				If get_spid(id)<>"0" Then str = get_spid(id) & str
			Else
				str = "0|" & str
			End If
		Else
			If id=0 Then str = "0|" & str
		End If
		str = AB.C.RegReplace(str, "^0\|", "")
		If str="" Then str = "0"
		rs.Close() : Set rs = Nothing
		get_spid = str
	End Function

End Class
%>