<%
'######################################################################
'## mail.library.asp
'## -------------------------------------------------------------------
'## Feature     :   App Mail Library
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/5/21 15:40
'## Description :   App Mail Class(App邮件操作)
'## ========================================================================================
'## Examples :
'##     Dim oMail
'##     'Set oMail= App.Loader.Library("mail")
'##     Set oMail= App.Library("mail")
'##     oMail.Object 		= 1 '指定发送邮件组件(1=Jmail,2=Aspemail,3=Cdonts,4=Cdoemail)
'##     oMail.SMTP 			= "smtp.163.com" 'SMTP地址(邮件服务器)
'##     oMail.LoginName		= "******@163.com" '身份验证用户名(邮件服务器登录名)
'##     oMail.LoginPass		= "******" '身份验证密码(邮件服务器登录密码)
'##     oMail.FromMail		= "******@163.com" '发送人邮箱
'##     oMail.FromName 		= "虚幻" '发送人名字信息
'##     '== a.使用 oMail.Send 方法
'##     oMail.ToMail 		= "******@126.com" '收信人邮件列表
'##     oMail.Subject 		= "邮件标题" '邮件主题
'##     oMail.Content 		= "邮件内容" '邮件内容
'##     oMail.Send 			'执行发送
'##     '== b.使用 oMail.SendMail 方法
'##     Call oMail.SendMail("******@126.com","邮件标题","邮件内容")
'##     If oMail.isOk Then AB.C.Print "邮件发送成功" Else AB.C.Print oMail.Msg
'######################################################################

Class App_Library_Mail

	Private oMail

	Private Sub Class_Initialize()
		On Error Resume Next
		AB.Use "Mvc" : AB.Mvc.Ctrl.Use "mail"
		Set oMail = AB.Mvc.Ctrl.Mail
		Me.[Object] = 1
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set oMail = Nothing
	End Sub

	'设置SMTP邮件服务器地址(可读/可写)
	Public Property Let SMTP(Byval s)
		oMail.SMTP = s
	End Property
	Public Property Get SMTP()
		SMTP = oMail.SMTP
	End Property

	'设置您的邮件服务器登录名(可读/可写)
	Public Property Let LoginName(Byval s)
		oMail.LoginName = s
	End Property
	Public Property Get LoginName()
		LoginName = oMail.LoginName
	End Property

	'设置登录密码(可读/可写)
	Public Property Let LoginPass(Byval s)
		oMail.LoginPass = s
	End Property
	Public Property Get LoginPass()
		LoginPass = oMail.LoginPass
	End Property

	'设置发件人的邮件地址(可读/写)
	Public Property Let FromMail(Byval s)
		oMail.FromMail = s
	End Property
	Public Property Get FromMail()
		FromMail = oMail.FromMail
	End Property

	'设置发送人名称(可读/写)
	Public Property Let FromName(Byval s)
		oMail.FromName = s
	End Property
	Public Property Get FromName()
		FromName = oMail.FromName
	End Property

	'设置邮件类型(可读/写)
	Public Property Let ContentType(Byval s)
		oMail.ContentType = s
	End Property
	Public Property Get ContentType()
		ContentType = oMail.ContentType
	End Property

	'设置编码类型(可读/写)
	Public Property Let Charset(Byval s)
		oMail.Charset = Cstr(s)
	End Property
	Public Property Get Charset()
		Charset = oMail.Charset
	End Property

	'设置选取组件(可读/写) 1=Jmail,2=Aspemail,3=Cdonts,4=Cdoemail
	Public Property Let [Object](Byval s)
		oMail.Object = s
	End Property
	Public Property Get [Object]()
		[Object] = oMail.Object
	End Property

	'获取返回信息代码和信息内容(只读)
	Public Property Get Msg()
		Msg = oMail.Msg
	End Property
	Public Property Get Code()
		Code = oMail.Code
	End Property

	'设置收件人的邮件地址(可读/可写)
	Public Property Let ToMail(Byval s)
		oMail.ToMail = s
	End Property
	Public Property Get ToMail()
		ToMail = oMail.ToMail
	End Property

	'设置发送邮件主题(可读/可写)
	Public Property Let Subject(Byval s)
		oMail.Subject = s
	End Property
	Public Property Get Subject()
		Subject = oMail.Subject
	End Property

	'设置发送邮件内容(可读/可写)
	Public Property Let Content(Byval s)
		oMail.Content = s
	End Property
	Public Property Get Content()
		Content = oMail.Content
	End Property

	'检查是否发送成功
	Public Property Get isOk()
		isOk = oMail.isOk
	End Property

	Public Sub Send()
		oMail.Send()
	End Sub

	Public Sub SendMail(Byval email,Byval subject,Byval content)
		Call oMail.SendMail(email, subject, content)
	End Sub

	Public Function MailArray(Byval mails, Byval p)
		MailArray = oMail.MailArray(mails, p)
	End Function

End Class
%>