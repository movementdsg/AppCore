<%
'######################################################################
'## App.TagUser.asp
'## -------------------------------------------------------------------
'## Feature     :   AppCore TagUser
'## Version     :   v0.1
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2014/12/01 21:26
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   AppCore 用户自定义标签解析
'######################################################################

Class Cls_App_TagUser

	Private b_debug
	Private o_ds

	Private Sub Class_Initialize()
		On Error Resume Next
		b_debug = App.Debug
		Set o_ds = AB.C.Dict()
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		Set o_ds = Nothing
	End Sub

	'标签解析入口函数
	Public Function Parse(ByVal str)
		On Error Resume Next
		If Err Then Err.Clear
		str = tag_hello(str)
		Parse = str
		On Error GoTo 0
	End Function

	'演示创建hello标签： $hello$
	Public Function tag_hello(ByVal str)
		On Error Resume Next
		str = AB.C.RP(str,"$hello$","hello world!")
		tag_hello = str
		On Error GoTo 0
	End Function

End Class
%>