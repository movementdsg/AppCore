<%
'######################################################################
'## App.TagSys.asp
'## -------------------------------------------------------------------
'## Feature     :   AppCore TagSys
'## Version     :   v0.1
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2014/12/02 21:27
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   AppCore 拓展系统标签解析
'######################################################################

Class Cls_App_TagSys

	Private s_dictName, s_tbPrefix
	Private b_debug
	Private o_db, o_ds
	Private model, module_id, group

	Private Sub Class_Initialize()
		On Error Resume Next
		b_debug = App.Debug
		Set o_ds = AB.C.Dict()
		s_tbPrefix = App.Dao.tbPrefix '数据表前缀
		Set o_db = App.Dao.db() '数据库驱动托管于App.Dao.db
		AB.Use "E" : AB.E.Use "Md5"
		AB.Use "A"
		group = LCase(GROUP_NAME)
		If group="news" Then group = "article" '新闻频道->文章模型
		If group="photo" Then group = "picture" '图片频道
		If AB.A.InArray(group, App.Dao.GetTables()) Then
			Set model = M(group)
			module_id = M("modules")().Where("name='"& group &"'").getField("id")
		End If
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		Set o_ds = Nothing
	End Sub

	'标签解析入口函数
	Public Function Parse(ByVal str)
		On Error Resume Next
		If Err Then Err.Clear
		Dim i:i=0
		str = Me.ListTag(str)
		str = Me.CmsTag(str)
		Parse = str
		On Error GoTo 0
	End Function

	'list标签(待完善..)
	Public Function ListTag(ByVal str)
		On Error Resume Next
		Dim RegEx, Match, RegAttrs, attr, rule, attrs, tmp
		Dim sql, var, md5, cvar, cval, inner
		rule = "{list\s+([\s\S]*?)}([\s\S]*?){/list}"
		Set RegEx = AB.C.RegMatch(str, rule)
		For Each Match In RegEx
			o_ds.RemoveAll
			attrs = Match.SubMatches(0) '各项属性
			'attrs = AB.C.RP(attrs, Array("\\","\@","\#"), Array(Chr(17),Chr(18),Chr(19)))
			inner = Match.SubMatches(1)
			Set RegAttrs = AB.C.RegMatch(attrs,"([a-zA-Z0-9_]+)=(['""]?)([^'""\s]*|[^\2]*?)\2")
			For Each attr In RegAttrs
				cvar = Trim(attr.SubMatches(0))
				cval = attr.SubMatches(2)
				o_ds(LCase(cvar)) = cval
			Next
			'str = AB.C.RP(str, Array(Chr(17),Chr(18),Chr(19)), Array("\","@","#"))
			Select Case o_ds("action")
				Case "module" : '模块
					'..
				Case "related" : '相关文章
					'..
				Case "category" : '栏目
					'..
				Case "sql" : '直接sql查询
					'..
			End Select
		Next
		Set RegAttrs = Nothing
		Set RegEx = Nothing
		ListTag = str
		On Error GoTo 0
	End Function

	'cms标签
	Public Function CmsTag(ByVal str)
		On Error Resume Next
		Dim RegEx, Match, RegAttrs, attr, rule, mark, attrs
		Dim sql, var, md5, cvar, cval
		Dim result, id, catid, childs, empty_str, a_where
		Dim prev_page, next_page, link
		Dim tag, arr, tmp
		Dim prev_rs, next_rs
		rule = "{cms\:(\w+)\s*([\s\S]*?)}"
		Set RegEx = AB.C.RegMatch(str, rule)
		For Each Match In RegEx
			o_ds.RemoveAll
			mark = Match.SubMatches(0) '标识符
			attrs = Match.SubMatches(1) '各项属性
			Set RegAttrs = AB.C.RegMatch(attrs,"([a-zA-Z0-9_]+)=(['""]?)([^'""\s]*|[^\2]*?)\2")
			For Each attr In RegAttrs
				cvar = Trim(attr.SubMatches(0))
				cval = attr.SubMatches(2)
				o_ds(LCase(cvar)) = cval
			Next
			Select Case mark
				Case "prev_page", "next_page" : '上一篇 {cms:prev_page} 、下一篇 {cms:next_page}
					If LCase(ACTION_NAME)="show" Then
						id = App.ReqInt("id",0) : empty_str = "没有了"
						If o_ds.Exists("empty") Then empty_str = o_ds("empty")
						If id>=0 And Not AB.C.isNul(model) Then
							catid = model().Find(id).getField("classid")
							'上一篇
							a_where = Array("id<"&id, "status=1")
							If Not AB.C.isNul(catid) Then a_where = AB.A.Push(a_where, "classid in( "& catid &" )")
							Set prev_rs =  model() _
											.Field("title,id") _
											.Where( a_where ) _
											.Order("id desc").Limit(0,1) _
											.Fetch()
							'下一篇
							a_where = Array("id>"&id, "status=1")
							If Not AB.C.isNul(catid) Then a_where = AB.A.Push(a_where, "classid in( "& catid &" )")
							Set next_rs =  model() _
											.Field("title,id") _
											.Where( a_where ) _
											.Order("id asc").Limit(0,1) _
											.Fetch()
							prev_page = "<a href="""& getArcUrl(prev_rs("id")) &""" title="""& prev_rs("title") &""">"& prev_rs("title") &"</a>"
							next_page = "<a href="""& getArcUrl(next_rs("id")) &""" title="""& next_rs("title") &""">"& next_rs("title") &"</a>"
							If mark = "prev_page" Then
								If Not AB.C.isNul(prev_rs) Then result = prev_page Else result = empty_str
							Else
								If Not AB.C.isNul(next_rs) Then result = next_page Else result = empty_str
							End If
						Else
							result = empty_str
						End If
					Else
						result = ""
					End If
				Case "tag" : '标签 {cms:tag}
					If IntVal(module_id)<=0 Then module_id=1
					If Not AB.C.isNul(model) And module_id>0 Then
						arr = Array()
						id = App.ReqInt("id",0)
						a_where = Array("module_id="& module_id)
						If LCase(ACTION_NAME)="show" And id>=0 Then
							a_where = AB.A.Push(a_where, "arcid="& id)
						ElseIf LCase(ACTION_NAME)="category" Then
							childs = child_ids(id,true)
							ids = model().Where("classid in("& childs & ")").getField("id:1")
							If Not AB.C.isNul(ids) Then a_where = AB.A.Push(a_where, "arcid in("& Join(ids,",") & ")")
						End If
						Set tag = M("tag")().Field("name").Where( a_where ).Group("name").Fetch()
						Do While Not tag.Eof
							name = tag("name")
							tmp = "<a href="""& getTagUrl(name) &""">"& name &"</a>"
							arr = AB.A.Push(arr, tmp)
							tag.MoveNext
						Loop
						result = Join(arr, " ")
					End If
				Case "hottag" : '全站热门标签 {cms:hottag}
					If IntVal(module_id)<=0 Then module_id=1
					If Not AB.C.isNul(model) And module_id>0 Then
						arr = Array()
						count = 30 '取数量
						If o_ds.Exists("count") Then count = IntVal(o_ds("count"))
						a_where = Array("module_id="& module_id)
						ids = ""
						If LCase(ACTION_NAME)="show" And id>=0 Then
							catid = model().Find(id).getField("classid")
							childs = child_ids(catid,true)
							ids = model().Where("classid in("& childs & ")").getField("id:1")
						ElseIf LCase(ACTION_NAME)="category" Then
							childs = child_ids(id,true)
							ids = model().Where("classid in("& childs & ")").getField("id:1")
						End If
						If IsArray(ids) Then ids = Join(ids,",")
						If Not AB.C.isNul(ids) Then a_where = AB.A.Push(a_where, "arcid in("& ids & ")") '文档多的话,in语句貌似效率很低，待优化..
						Set tag = M("tag")().Field("COUNT(id) AS [count],name").Where(a_where).Group("name").Order("COUNT(id) desc").Limit(0,count).Fetch()
						Do While Not tag.Eof
							name = tag("name")
							tmp = "<a href="""& getTagUrl(name) &""">"& name &"</a>"
							arr = AB.A.Push(arr, tmp)
							tag.MoveNext
						Loop
						Set tag = Nothing
						result = Join(arr, " ")
					End If
			End Select
			str = AB.C.RP(str, Match.Value, result)
		Next
		Set RegAttrs = Nothing
		Set RegEx = Nothing
		CmsTag = str
		On Error GoTo 0
	End Function

	'获取当前模型文档url链接
	Function getArcUrl(ByVal id)
		getArcUrl = U(GROUP_NAME&":index/show?id=" & IntVal(id))
	End Function

	'获取当前模型文档url链接
	Function getTagUrl(ByVal name)
		getTagUrl = U(GROUP_NAME&":index/tag?name=" & Server.UrlEncode(name))
	End Function

End Class
%>