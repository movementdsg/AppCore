<!--#include file="../inc.asp" -->
<style>
<!--
body,div,ul,ol,dl,dt,dd{margin:0;padding:0;font-size:100%;}
ol,ul{list-style:none;}
*:focus{outline:0;}
body{font:75%/1.5 "Lucida Grande",Helvetica,Arial,sans-serif;}

#wrapbox{
	width:800px; height:auto; margin:0 auto;
}

/*= Pagination =*/

.pagerbar{margin-bottom:10px;line-height:20px;float:right;}
.pagerbar a,.pagerbar span{
	color:#555;
	float:left;display:inline-block;
	margin-left:4px;padding:0 8px;border:1px solid #ccc;
	background:#fff no-repeat 100% 100%;
	white-space:nowrap;
}

.pagerbar a:link,.pagerbar a:visited{color:#ab0000;text-decoration:none;}
.pagerbar a:hover,.pagerbar a:active{color:#ab0000;text-decoration:underline;}

.pagerbar select {
	display:inline-block; float:left; background:#fff;
	height:20px; color:#000; width:auto;
	border:1px solid #f00;
	margin-top: 0px; margin-left:4px;
}
.pagerbar select option.current{
	background:#5DB0F4;
}
.pagerbar em {
	float:left; display:inline-block; height:20px; line-height:20px;
	font-style:normal;
	margin-top: 2px; margin-left:4px;
}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs 'Pager.Dom传值对象是：rs记录集对象
pager.PageSize = 3

pager.Format = "<div class=""pagerbar"">{first} {prev} {next} {last} <em class=""txt"">转到：</em>{select}</div>"
pager.Style = ""
pager.addStyle "first{display:show;class:first;dis-tag:span;text:首 页;}"
pager.addStyle "prev{display:show;class:prev;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:show;class:next;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;}"
pager.addStyle "list{display:show;curr-tag:span;curr-class:current;link-text:'{n}';link-class:'';link-space:' ';dot-val:'..';dot-for:a#xy;index:2|2;}"
pager.addStyle "select{display:show;name:'';text:'第$n页';class:'';id:'';curr-class:'current';curr-id:'';}"
'pager.addStyle "select{}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>