<!--#include file="../inc.asp" -->
<style>
body {
    color: #4c4c4c;
    font-family: "Microsoft YaHei","Arial Narrow";
    font-size: 12px;
    line-height: 22px;
}

.postpage {
	margin: 30px 20px 0 15px;
	padding: 5px 5px;
	text-align: center;
	border: 0;
	display: none
}

.postpage a {
	font-size: 16px;
	background: #fff;
	margin: 3px 3px;
	padding: 3px 10px;
	color: #525252;
	text-decoration: none;
	border: #347dcb 1px solid;
	line-height: 30px;
	box-shadow: 0 2px 2px #abaaaa;
	font-weight: 100
}

.postpage a:hover {
	font-size: 16px;
	margin: 3px 3px;
	background: #347dcb;
	color: #fff
}

.postpage .pagecurr {
	background: #347dcb;
	color: #fff;
	margin: 3px 3px;
	font-size: 16px;
	/*
	padding: 3px 5px;
	font-size: 14px;
	*/
	padding: 3px 10px;
	text-decoration: none;
	border: #347dcb 1px solid;
	line-height: 30px;
	box-shadow: 0 2px 2px #abaaaa;
	font-weight: 100
}

.postpage .sinfo{
	background: #fff;
	font-size: 16px;
	margin: 3px 3px;
	padding: 3px 10px;
	color: #525252;
	text-decoration: none;
	border: #347dcb 1px solid;
	line-height: 30px;
	box-shadow: 0 2px 2px #abaaaa;
	font-weight: 100
}
</style>

<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div style=""margin-top: 10px; display: block;"" class=""postpage"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:show;dis-tag:span;dis-href:js;text:首页;dis-class:sinfo;}"
pager.addStyle "prev{display:show;dis-tag:span;dis-href:js;text:上一页;dis-class:sinfo;}"
pager.addStyle "next{display:show;dis-tag:span;dis-href:#;text:下一页;dis-class:sinfo;}"
pager.addStyle "last{display:show;dis-tag:span;dis-href:#;text:尾页;dis-class:sinfo;}"
pager.addStyle "list{curr-tag:span;curr-class:pagecurr;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div class=""J_page_wrap"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr "<br>"
ab.c.wr "<div class='page_wrap'>"
ab.c.w pager.html
ab.c.wr "</div>"
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>