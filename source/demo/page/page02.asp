<!--#include file="../inc.asp" -->
<style>
<!--
*{margin:0;padding:0;word-wrap:break-word;}
body{background:#FFF url("../images/background.png") repeat-x 0 0;}
body{font:12px/1.5 Tahoma,Helvetica,SimSun,sans-serif;color:#444;}
ul li{list-style:none;}
em,cite{font-style:normal;}
a{color:#333;text-decoration:none;}
a:hover{text-decoration:underline;}
a img{border:none;}

.pg{float:right; margin-right:10px;}
.pg,.pgb{line-height:26px;}
.pg a,.pg a.current,.pg span,.pgb a{
	float:left;display:inline;margin-left:4px;padding:0 8px;height:26px;
	border:1px solid;border-color:#C2D5E3;background-repeat:no-repeat;
	color:#333;overflow:hidden;text-decoration:none;
}
.pg a.next,.pgb a{padding:0 10px;}
.pg a:hover,.pgb a:hover{border-color:#369;color:#369;}
.pg a.next{padding-right:25px;background-image:url(./images/arw_r.gif);background-position:90% 50%;}
.pg a.prev{background-image:url(./images/arw_l.gif);background-position:10% 50%;padding-left:25px;}
.pg a.current{background-color:#E5EDF2;}
.pgb a{padding-left:25px;background-image:url(./images/arw_l.gif);background-position:10px 50%;}
#wrapbox .pg, #wrapbox .pgb{margin-top:5px;}

.pg span{border:1px solid #ddd; background-color:#fff;}

.y{float:right;}
.tb .y{float:right;margin-right:0;}.tb .y a{border:none;background:transparent;}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""pg"">{first} {prev} {list} {next} {last}</div> <span class='pgb y'><a href='./'>返&nbsp;回</a></span>"
pager.Style = ""
pager.addStyle "first{display:show;class:first;dis-tag:span;text:首 页;}"
pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;}"
'pager.addStyle "list{display:show;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:a#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>