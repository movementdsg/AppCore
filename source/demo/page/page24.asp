<!--#include file="../inc.asp" -->
<style>
html,body,div,span,object,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,samp,small,strong,sub,sup,tt,var,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,figcaption,figure,footer,header,hgroup,menu,nav,section,summary,time,mark,audio,video{margin:0;padding:0;vertical-align:baseline;outline:none;font-size:100%;background:transparent;border:none;text-decoration:none}
div { font-family: Georgia, Times, 'Times New Roman', serif; }

/**
* Pagination and prev/next links
**************************************/
.pagination, .page-links, .comment-pagination {
	overflow: hidden;
	clear: both;
	font-size: 13px;
	font-weight: bold;
	line-height: 13px;
	margin: 0 0 25px 0;
	}
.pagination { /* Leave room for box-shadow on page numbers. */
	margin-bottom: 23px;
	padding-bottom: 2px;
	}
.page-links {
	padding: 7px 0 0 0;
	}
.pagination .pagenum, .pagination label {
	float: left;
	display: inline-block;
	margin: 0 15px 0 0px;
	padding: 11px 15px;
	font-weight: bold;
	color: #fff;
	background: #04648D;
	-moz-box-shadow: 2px 2px 5px #999;
	-webkit-box-shadow: 2px 2px 5px #999;
	box-shadow: 2px 2px 5px #999;
	}
.page-links a {
	float: none;
	margin: 0 7px;
	padding: 8px 12px;
	display: inline-block;
	font-weight: bold;
	color: #fff;
	background: #04648D;
	}
.entry-summary .page-links a {
	margin: 0 4px;
	padding: 5px 9px;
	}
.pagination a:hover, .page-links a:hover {
	background: #ce3000;
	}
.pagination .current {
	background: #ce3000;
	}
</style>

<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div style=""margin-top: 10px; display: block;"" class=""postpage"">{prev} {list} {next}</div>"
pager.Style = ""
pager.addStyle "prev{display:auto;text:'← Previous';class:pagenum;}"
pager.addStyle "next{display:auto;text:'Next →';class:pagenum;}"
pager.addStyle "list{curr-tag:span;curr-class:'current';link-text:'$n';link-class:'pagenum';link-space:' ';dot-val:'..';dot-for:label#xy;index:1|1;min:3;}"

ab.c.w "<div class=""J_page_wrap"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr "<br>"
ab.c.wr "<div class='pagination'>"
ab.c.w pager.html
ab.c.wr "</div>"
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>

