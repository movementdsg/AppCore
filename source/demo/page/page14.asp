<!--#include file="../inc.asp" -->
<style>
<!--
/*公共*/
BODY {
FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif, Verdana; list-style:none;
}
A {TEXT-DECORATION: none;}
/*CSS yellow style pagination*/
DIV.yellow {
PADDING: 7px 7px; MARGIN: 3px; TEXT-ALIGN: center
}
DIV.yellow A {
BORDER: #ccc 1px solid; 
PADDING: 0 5px;
MARGIN-RIGHT: 4px; 
COLOR: #000; 
height:18px;
line-height:18px;
float:left;
}
DIV.yellow A:hover {
BORDER: #f0f0f0 1px solid; COLOR: #000;
}
DIV.yellow A:active {
BORDER: #f0f0f0 1px solid; COLOR: #000;
}
DIV.yellow A.current {
BORDER: #d9d300 1px solid; 
PADDING: 0 5px;
MARGIN-RIGHT: 4px; 
COLOR: #fff; BACKGROUND-COLOR: #d9d300; FONT-WEIGHT: bold;
float:left;
}
DIV.yellow SPAN.prevPage,DIV.yellow SPAN.nextPage {
BORDER: #d9d300 1px solid;
PADDING: 2px 5px 2px 5px; 
MARGIN-RIGHT: 4px; 
COLOR: #ddd;
float:left;
}
DIV.yellow A.prevPage,DIV.yellow A.nextPage {
PADDING: 0 5px; 
MARGIN-RIGHT: 4px; 
COLOR: #444;
height:18px;
line-height:18px;
float:left;
}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""yellow"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:hide;}"
pager.addStyle "prev{display:show;class:prevPage;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:show;class:nextPage;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:hide;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'num';link-space:' ';dot-val:'..';dot-for:a#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>