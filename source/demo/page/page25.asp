<!--#include file="../inc.asp" -->
<style>
body {
    color: #888;
    font: 14px/1.5 "微软雅黑","宋体","Verdana",sans-serif;
    text-align: left;
}
a {
    color: #1f4f88;
    cursor: pointer;
    outline: 0 none;
    text-decoration: none;
    transition: all 0.2s linear 0s;
}

/* 分页 */
.pagination {text-align:center;height:32px;line-height:32px;margin-top:20px;}
.pagination a {padding:0px 12px;display:inline-block;background-color:#f5f5f5; margin-right:2px; color:#777; text-decoration:none; }
.pagination a:hover { background-color:#63b8ff; color:#fff;text-shadow:1px 1px 1px #000;}
.pagination strong { 
	font-weight:normal;
	padding:0px 12px; 
	height:32px;
	line-height:32px;
	display:inline-block;
	background-color:#63b8ff; 
	margin-right:2px; 
	color:#fff; 
	text-shadow:1px 1px 1px #000;
}
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""pagination"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:show;class:first;dis-tag:a;text:首页;}"
pager.addStyle "prev{display:show;class:prev;dis-tag:a;text:上一页;}"
pager.addStyle "next{display:show;class:next;dis-tag:a;text:下一页;}"
pager.addStyle "last{display:show;class:last;dis-tag:a;text:尾页;}"
pager.addStyle "list{curr-tag:strong;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>