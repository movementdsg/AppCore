<!--#include file="../inc.asp" -->
<style>
div {
    word-break: break-all;
    word-wrap: break-word;
}
.pagerbar a, .pagerbar span {
    background: url("images/core_bg.png") repeat scroll 0 0 rgba(0, 0, 0, 0);
}

.page_wrap .pagerbar {
    float: left;
	font-size:12px;
}
.pagerbar a,.pagerbar span{ 
	display:inline-block;line-height:13px;padding:6px 10px !important;border:1px solid;border-color:#c9c9c9 #bdbdbd #b0b0b0 #bdbdbd;background-color:#f8f8f8;text-decoration:none;color:#666666;margin-right:3px;border-radius:3px;vertical-align:top;*font-family:Simsun;
}
.pagerbar a:hover{ color:#fff;background:#488fcf !important;border-color:#2470b5 #488fcf #488fcf #488fcf;text-decoration:none; }
.pagerbar span,.pagerbar a.current{ color:#fff !important;background:#488fcf !important;border-color:#2470b5 #488fcf #488fcf #488fcf; font-style:normal; }
.pagerbar span{ padding:0 10px; }
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""pagerbar"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:hide;}"
pager.addStyle "prev{display:auto;dis-tag:a;dis-href:js;text:&#171\;&nbsp\;上一页;}"
pager.addStyle "next{display:auto;dis-tag:a;dis-href:#;text:下一页&nbsp\;&#187\;;}"
pager.addStyle "last{display:hide;}"
pager.addStyle "list{curr-tag:span;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div class=""J_page_wrap"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr "<br>"
ab.c.wr "<div class='page_wrap'><div style='margin-right:3px;' class='pagerbar'><a class='pages_pre' href='#'>« 返回列表</a></div>"
ab.c.w pager.html
ab.c.wr "</div>"
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>