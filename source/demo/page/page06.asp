<!--#include file="../inc.asp" -->
<style>
<!--
*{ margin:0; padding:0;} 
body{font-size:12px; font-family:Verdana;} 
a{color:#333; text-decoration:none;} 
ul{list-style:none;} 
#pagelist { width:600px; margin:30px auto; padding:6px 0px; height:20px;} 
#pagelist ul li { float:left; border:1px solid #5d9cdf; height:20px; line-height:20px; margin:0px 2px;} 
#pagelist ul li a { display:block; padding:0px 6px; background:#e6f2fe;} 

#pagelist ul li.current span { background:#a9d2ff; display:block; padding:0px 6px; font-weight:bold;}
#pagelist ul li.current a { background:#e6f2fe; display:block; padding:0px 6px; font-weight:bold;}

#pagelist ul li.first a, #pagelist ul li.prev a, #pagelist ul li.next a, #pagelist ul li.last a{ background:#e6f2fe; padding:0px 6px;  color:#333; text-decoration:none;  display:block; }
#pagelist ul li.first span, #pagelist ul li.prev span, #pagelist ul li.next span, #pagelist ul li.last span{ background:#e6f2fe; padding:0px 6px; color:#333; font-weight:normal; display:block; }

#pagelist ul li.pageinfo { display:block; padding:0px 6px; background:#e6f2fe; color:#555;} 
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div id=""pagelist""><ul>\n{first}\n{prev}\n{list}\n{next}\n{last}\n</ul></div>"
pager.Style = ""
pager.addStyle "first{display:show;class:'';dis-tag:span;text:首 页;wrap-tag:li;wrap-class:first;wrap-id:'';}"
pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;wrap-tag:li;wrap-class:prev;wrap-id:'';}"
pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;wrap-tag:li;wrap-class:next;wrap-id:'';}"
pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;wrap-tag:li;wrap-class:last;wrap-id:'';}"

' <li class="first"><span>首 页</span></li> 
' <li class="prev"><span>上一页</span></li> 
' <li class="current"><span>1</span></li> 
' <li class="num"><a href="/help/index.asp?item=pageshow&part=6&page=2">2</a></li> 
' <li class="num"><a href="/help/index.asp?item=pageshow&part=6&page=3">3</a></li> 
' <li class="num"><a href="/help/index.asp?item=pageshow&part=6&page=4">4</a></li> 
' <li class="next"><a href="/help/index.asp?item=pageshow&part=6&page=2">下一页</a></li> 
' <li class="last"><a href="/help/index.asp?item=pageshow&part=6&page=103">末 页</a></li>

'pager.addStyle "list{dot-val:'..';dot-for:a#xy;}"
pager.addStyle "list{curr-tag:span;curr-class:'';link-text:'$n';link-class:'';link-space:'\n';index:4|4;min:9;}"
pager.addStyle "list{curr-wrap-tag:li;curr-wrap-class:'current';curr-wrap-id:'';}"
pager.addStyle "list{link-wrap-tag:li;link-wrap-class:num;link-wrap-id:'';}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wn "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wn pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>