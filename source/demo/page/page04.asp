<!--#include file="../inc.asp" -->
<style>
<!--
.page_cut { clear:both; padding:10px; text-align:left; clear:both; overflow:hidden; font-size:11px; font-family:Arial; _height:10px; _overflow:inherit;}
.page_cut a {padding:0 6px; float:left; background:#fff; border:1px #ccc solid; width:auto; height:17px; line-height:17px; margin-right:5px; text-align:center; color:#000!important; text-decoration:none; display:block;}
.page_cut a:hover {border:1px #000 solid; background:#f2f2f2; color:#fb7b00!important; text-decoration:none;}
.page_cut span {float:left; margin-right:5px; text-align:center; height:17px; line-height:17px; font-weight:bold;}
.page_cut .current {padding:0 6px; background:#fb7b00; border:1px #E37206 solid; color:#fff;}
.page_cut .break {font-size:12px; font-weight:bold; color:#666;}
.page_cut .page_info {font-size:12px; font-weight:normal; line-height:19px;}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""page_cut"">{first} {prev} {list} {next} {last}</div>"
pager.Style = "first{display:hide;}prev{display:hide;}next{display:hide;}last{display:hide;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'<span class=""break"">..</span> ';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>