<!--#include file="../inc.asp" -->
<style>
<!--
/*文章分页样式*/
#fenye{clear:both;display:block;width:600px; height:30px;}
#fenye a{text-decoration:none;font-family:Arial;font-size:12px;}
#fenye a.first,#fenye a.prev,#fenye a.next,#fenye a.last{width:52px; text-align:center;}
#fenye span.first,#fenye span.prev,#fenye span.next,#fenye span.last{width:52px; text-align:center; color:#999;}
#fenye span.current{width:22px;background:#1f3a87; border:1px solid #dcdddd; font-weight:bold; text-align:center; color:#f00;}
#fenye a.current{width:22px;background:#1f3a87; border:1px solid #dcdddd; font-weight:bold; text-align:center; color:#f00;}
#fenye a.current:visited {color:#f00;}
#fenye span.current{width:22px;background:#1f3a87; border:1px solid #dcdddd; font-weight:bold; text-align:center; color:#f00;}
#fenye a{margin:5px 4px 0 0; color:#1E50A2;background:#fff; display:inline; border:1px solid #dcdddd; float:left; text-align:center;height:22px;line-height:22px}
#fenye a.num{width:22px;}
#fenye a:visited{color:#1f3a87;} 
#fenye a:hover{color:#fff; background:#1E50A2; border:1px solid #1E50A2;float:left;}
#fenye span{display:block;font-family:Arial;font-size:12px;}
#fenye span{margin:5px 4px 0 0; color:#1E50A2;background:#fff; display:inline; border:1px solid #dcdddd; float:left; text-align:center;height:22px;line-height:22px}
#fenye span.break{border:0;width:5px;}
/*文章分页样式结束*/
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div id=""fenye"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:show;class:first;dis-tag:span;text:首 页;}"
pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'num';link-space:' ';dot-val:'<span class=""break"">..</span>';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

'rs.Close
'Set rs = Nothing
%>