<!--#include file="../inc.asp" -->
<style>
<!--
* {margin: 0;}
body,div,td,th { font-family: simsun,'宋体','新宋体',sans-serif;font-size:12px;color: #333;line-height: 180%; }
div,form,img{padding:0;border:0;}
dl,dt,dd,ul,ol,li{padding:0;border:0;list-style-type:none;list-style-position:outside;}
dl,ul{clear:left;text-align:left;}
dt,dd,li{display: block;}
select,input{font-size:9pt;}
em { font-style: normal;}
a{text-decoration:none;}
a:link,a:visited{color: #333;}
a:hover,a:active{color:#06C;text-decoration:underline;}
.fix{ zoom:1; overflow:hidden;}

/*翻页*/
#listPagerWrapper{ width:auto; padding:10px; margin:10px auto; background:#FFF; height:45px; }
#listPagerWrapper .tip{ 
	background:#EDF2F9; color:#135B96; border:none; font-weight:700; font-size:12px; 
	float:left; margin:0 5px; padding:0 5px; height:25px; line-height:22px; 
	border:1px #F1F1F1 solid;
}

#listPager{  margin:0 auto 10px; height:25px; overflow:hidden; text-align:center; background:#FFF; float:left;}
#listPager a{ display:block; float:left; padding:0 5px; border:1px #DEDEDE solid; margin:0 3px; height:22px; line-height:22px; background:#FFF; font-weight:700; color:#666}
#listPager a:hover{ background:#FEFAD8; text-decoration:none}
#listPager span{ display:block; float:left;  padding:0 5px; margin:0 3px;  height:22px; line-height:22px; border:1px #F1F1F1 solid; font-weight:700; color:#666; background:#EDF2F9; }
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<DIV id='listPager'>{first} {prev} {list} {next} {last}</DIV>"
pager.Style = ""
pager.addStyle "first{display:show;dis-tag:span;text:'&lt\;&lt\;';}"
pager.addStyle "prev{display:show;dis-tag:span;text:'&lt\;';}"
pager.addStyle "next{display:show;dis-tag:span;text:'&gt\;';}"
pager.addStyle "last{display:show;dis-tag:span;text:'&gt\;&gt\;';}"
pager.addStyle "list{curr-tag:span;curr-class:current;link-text:'$n';link-class:'num';link-space:' ';dot-val:'';index:4|4;min:10;}"

ab.c.wr "<div>"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="

ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr "</div>"

ab.c.wn "<DIV id='pagewrap'>"
ab.c.wn "<form name='pageForm' id='pageForm' action='' method='post'>"
ab.c.wn "" &pager.html& ""
'pager.Show()
ab.c.wn "	<span>共" & pager.PageCount & "页</span>"
ab.c.wn "	<span>直接到</span><input id=idarticlekey name=idarticlekey class=pagenum size=4 type=text><span>页</span>"
ab.c.wn "	<input id=button2 onclick=jumpPage() value=确定 type=submit name=button2>"
ab.c.wn "</form>"
ab.c.wn "</DIV>"

ab.c.wr "=========="

rs.Close
Set rs = Nothing
%>
	<script>
	function jumpPage() 
		{
			var idNum=document.pageForm.idarticlekey.value ;
			if(!isNaN(idNum)){
				if( idNum><%=pager.PageCount%>) idNum=<%=pager.PageCount%>;
				if( idNum<1) idNum=1;
				document.pageForm.action= '<%=AB.C.GetUrlWith("-page","page=")%>' + idNum+'';
				document.pageForm.submit();
			}
			else {
				alert("请输入正确的页数!");
				return;
			}
		}
	</script>