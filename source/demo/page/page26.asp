<!--#include file="../inc.asp" -->
<style>
body {
    font: 12px/1.5 "Microsoft YaHei";
}
a {
    color: #1f4f88;
    text-decoration: none;
}
a:hover {
    color: #c00;
    text-decoration: underline;
}
a:hover {
    color: #c00;
    text-decoration: underline;
}

.comment-pages .icon {
	background: url(images/comment-bg.png) no-repeat;
	_background: url(images/comment-bg-ie6.png) no-repeat;
}

.comment-pages {
	padding: 27px 0;
	text-align: center;
	font-family: "Microsoft Yahei","\5FAE\8F6F\96C5\9ED1";
}

.comment-pages a,.comment-pages span,.comment-pages em {
	display: inline-block;
	margin: 0 2px;
	line-height: 36px;
}

.comment-pages a,.comment-pages span {
	width: 36px;
	text-align: center;
}

.comment-pages a {
	color: #666;
	background-color: #f5f5f5;
}

.comment-pages a:hover,.comment-pages span {
	color: #fff;
	background-color: #3ea0e6;
	text-decoration: none;
}

.comment-pages em {
	color: #222;
}

.comment-pages .icon {
	position: absolute;
	top: 13px;
	width: 7px;
	height: 11px;
	font: 0/0 Arial;
}

.comment-pages .prev,.comment-pages .next {
	width: 66px;
	position: relative;
	zoom: 1;
}

.comment-pages .prev {
	padding: 0 0 0 14px;
}

.comment-pages .next {
	padding: 0 14px 0 0;
}

.comment-pages .prev .icon {
	background-position: -40px -330px;
	left: 13px;
}

.comment-pages .next .icon {
	background-position: -50px -330px;
	right: 13px;
}

.comment-pages .prev:hover .icon {
	background-position: -40px -345px;
}

.comment-pages .next:hover .icon {
	background-position: -50px -345px;
}
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""comment-pages"">{prev} {list} {next}</div>"
pager.addStyle "prev{display:auto;class:prev;dis-tag:a;text:上一页<i class=""icon""></i>;}"
pager.addStyle "next{display:auto;class:next;dis-tag:a;text:下一页<i class=""icon""></i>;}"
pager.addStyle "list{curr-tag:span;curr-class:now;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:em#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>