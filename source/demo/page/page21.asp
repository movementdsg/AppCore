<!--#include file="../inc.asp" -->
<style>
input, textarea, select, a {
    outline: 0 none;
}
a {
    color: #717171;
    text-decoration: none;
}
em, i, strong, b {
    font-style: normal;
    font-weight: 400;
}

.page{text-align:center;padding:16px 0;height:30px;line-height:20px;font-size:12px;margin: 0;color: #717171;}
.page a,.page span,.page em{display:inline-block;padding:5px 0;height:20px}
.page a{border:1px solid #d4d4d4;background-color:#f0f0f0;padding:0 10px}
.page a.up,.page a.next{_font-size:12px}
.page em,.page a:hover{border:1px solid #e60013;color:#fff;padding:0 10px;background-color:#e60013;text-decoration:none}
.page span{padding:0 10px}
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""page"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:hide;}"
pager.addStyle "prev{display:show;dis-tag:a;dis-href:js;text:上一页;}"
pager.addStyle "next{display:show;dis-tag:a;dis-href:js;text:下一页;}"
pager.addStyle "last{display:hide;}"
pager.addStyle "list{curr-tag:em;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>