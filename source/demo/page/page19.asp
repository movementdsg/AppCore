<!--#include file="../inc.asp" -->
<style>
<!--
html, body { background: #f6f9fb; color: #555; line-height:18px; }
html, body, h1, h2, h3, h4, ul, li, dl, input { font-family:Arial, Helvetica, sans-serif;  font-size:12px; list-style:none; margin:0px; padding:0px; }
div, p{ font-family:Arial, Helvetica, sans-serif;  list-style:none; margin:0px; padding:0px; }
div, table {margin: 0 auto; }
a { color:#538FC4; text-decoration: none; }
a:hover { color:#f60; text-decoration: underline; }

/*CSS sabrosus style pagination*/
.pagebox a.num, .pagebox .num { display:block; border:1px solid #B2BFCF; background:#fff; float:left; margin-right:4px; height:18px; line-height:18px; padding:0 5px; }
.pagebox a.num:hover { border:1px solid #577DA8; background:#FFFFE9; text-decoration:none}
.pagebox a.first, .pagebox a.last, .pagebox a.prevPage, .pagebox a.nextPage { display:block; border:1px solid #B2BFCF; background:#fff; float:left; margin-right:4px; height:18px; line-height:18px; padding:0 5px; }
.pagebox a.first:hover, .pagebox a.last:hover, .pagebox a.prevPage:hover, .pagebox a.nextPage:hover { border:1px solid #577DA8; background:#FFFFE9; text-decoration:none}
.pagebox a.current { border:1px solid #1D619C; background:#1972BD; color:#fff; }
.pagebox .num input {margin:-2px -6px 0 -4px;border:0px; background:none; height:12px;line-height:12px; width:14px;}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""pagebox"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:hide;}"
pager.addStyle "prev{display:show;class:prevPage;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:show;class:nextPage;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:hide;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'num';link-space:' ';dot-val:'..';dot-for:a#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>