<!--#include file="../inc.asp" -->
<style>
<!--
/*公共*/
BODY {
FONT-SIZE: 12px;FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;WIDTH: 60%; PADDING-LEFT: 25px;
}
/*CSS sabrosus style pagination*/
DIV.sabrosus {
PADDING: 3px 3px; MARGIN: 3px; TEXT-ALIGN: center;
}
DIV.sabrosus A {
BORDER: #9aafe5 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: -1px; 
COLOR: #2e6ab1; TEXT-DECORATION: none;
}
DIV.sabrosus A:hover {
BORDER: #2b66a5 1px solid; COLOR: #000; BACKGROUND-COLOR: lightyellow;
}
DIV.pagination A:active {
BORDER: #2b66a5 1px solid; COLOR: #000; BACKGROUND-COLOR: lightyellow;
}
DIV.sabrosus A.current {
BORDER: navy 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 0px; 
COLOR: #fff; FONT-WEIGHT: bold; BACKGROUND-COLOR: #2e6ab1;
}
DIV.sabrosus SPAN.prevPage,DIV.sabrosus SPAN.nextPage {
BORDER: #929292 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 0px;
COLOR: #929292;
height:13px;
}
DIV.sabrosus A.prevPage,DIV.sabrosus A.nextPage {
PADDING: 2px 5px 2px 5px; MARGIN: 0px;
height:13px;
}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""sabrosus"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:hide;}"
pager.addStyle "prev{display:show;class:prevPage;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:show;class:nextPage;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:hide;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'num';link-space:' ';dot-val:'<span class=""break"">..</span>';dot-for:txt#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>