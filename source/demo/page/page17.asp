<!--#include file="../inc.asp" -->
<style>
<!--
/*公共*/
BODY {
FONT-SIZE: 12px;FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;WIDTH: 60%; PADDING-LEFT: 25px;
}
/*CSS viciao style pagination*/
DIV.viciao {
MARGIN-TOP: 20px; MARGIN-BOTTOM: 10px
}
DIV.viciao A {
BORDER: #8db5d7 1px solid; 
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 0px; 
COLOR: #000; TEXT-DECORATION: none
}
DIV.viciao A:hover {
BORDER: #f00 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 2px;
}
DIV.viciao A:active {
BORDER: #f00 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 2px;
}
DIV.viciao A.current {
BORDER: #e89954 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 0px;
FONT-WEIGHT: bold; COLOR: #000; BACKGROUND-COLOR: #ffca7d
}
DIV.viciao SPAN.prevPage,DIV.viciao SPAN.nextPage {
BORDER: #ccc 1px solid;
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 0px;
}
DIV.viciao A.prevPage,DIV.viciao A.nextPage {
PADDING: 2px 5px 2px 5px; MARGIN-RIGHT: 0px;
height:13px;
}
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""viciao"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:hide;}"
pager.addStyle "prev{display:show;class:prevPage;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:show;class:nextPage;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:hide;}"
pager.addStyle "list{curr-tag:a;curr-class:current;link-text:'$n';link-class:'num';link-space:' ';dot-val:'..';dot-for:a#xy;index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>