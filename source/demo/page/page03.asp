<!--#include file="../inc.asp" -->
<style>
<!--
/*****_____ 分页样式CSS定义 Begin _____*****/
.pagelist { width:auto;  margin-top:15px; margin-bottom:15px; }
.pagelist span.info{float:left; color:#7d7d7d; font-size:14px; margin-top:5px; }
/***通用***/
.pagebox{overflow:hidden; zoom:1; font-size:12px; font-family:"宋体",sans-serif; color:#7d7d7d; float:right; }
.pagebox span{overflow:hidden; background:#fff; margin-left:2px; margin-right:2px; float:left; display:block; }
.pagebox span a{display:block; overflow:hidden; zoom:1; _float:left;}
.pagebox A{overflow:hidden; background:#fff; margin-left:2px; margin-right:2px; float:left; }
.pagebox A{display:block; overflow:hidden; zoom:1; _float:left;}
/***首页***/
.pagebox A.first,.pagebox A.first:visited,.pagebox A.next,.pagebox A.next:visited{width:auto; border:1px #cfcbc2 solid; color:#7d7b6f; text-decoration:none; text-align:center; cursor:pointer; height:21px; line-height:21px;}
.pagebox SPAN.first{padding:0 7px; color:#3568b9; height:23px; border:1px #ddd solid; height:21px; line-height:21px; text-align:center; color:#999; cursor:default; font-style:normal;}
/***上一页***/
.pagebox A.prev,.pagebox A.prev:visited{width:auto; border:1px #cfcbc2 solid; color:#7d7b6f; text-decoration:none; text-align:center; cursor:pointer; height:21px; line-height:21px;}
.pagebox A.prev:hover,.pagebox A.prev:active{color:#7d766c; border:1px #7d766c solid;}
.pagebox SPAN.prev{padding:0 7px; color:#3568b9; border:1px #ddd solid; height:21px; line-height:21px; text-align:center; color:#999; cursor:default; font-style:normal;}
/**数字列表***/
.pagebox A,.pagebox A:visited{border:1px #cecbc2 solid; color:#83766d; text-decoration:none; padding:0 8px; cursor:pointer; height:21px; line-height:21px;}
.pagebox A:hover,.pagebox A:active{border:1px #7d766c solid;color:#363636;}
.pagebox SPAN.num{color:#83766d; height:22px; color:#393733; width:22px; background:none; line-height:22px; font-style:normal;}
/**当前页面***/
.pagebox SPAN.current{padding:0 7px; border:1px #aaa solid; height:21px; line-height:21px; color:#fff; cursor:default; background:#ACA899; font-weight:bold;}
/***下一页***/
.pagebox A.next,.pagebox A.next:visited{width:auto; border:1px #cfcbc2 solid; color:#7d7b6f; text-decoration:none; text-align:center; cursor:pointer; height:21px; line-height:21px;}
.pagebox A.next:hover,.pagebox A.next:active{color:#7d766c; border:1px #7d766c solid;}
.pagebox SPAN.next{color:#3568b9; border:1px #ddd solid; height:21px; line-height:21px; text-align:center; color:#999; cursor:default; font-style:normal;}
/***尾页***/
.pagebox A.last,.pagebox A.last:visited{width:auto; border:1px #cfcbc2 solid; color:#7d7b6f; text-decoration:none; text-align:center; cursor:pointer; height:21px; line-height:21px;}
.pagebox SPAN.last{padding:0 7px;width:auto; color:#3568b9; border:1px #ddd solid; height:21px; line-height:21px; text-align:center; color:#999; cursor:default; font-style:normal;}
/*****_____ 分页样式CSS定义 End _____*****/
-->
</style>
<%
Dim pager
set pager = ab.lib("pager")
pager.Param = "page"
pager.Url = ""
'pager.Total = rs.RecordCount
pager.dom = rs
pager.PageSize = 3

pager.Format = "<div class=""pagebox"">{first} {prev} {list} {next} {last}</div>"
pager.Style = ""
pager.addStyle "first{display:show;class:first;dis-tag:span;text:首 页;}"
pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;}"
pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;}"
pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;}"
pager.addStyle "list{display:show;curr-tag:span;curr-class:current;link-text:'$n';link-class:'';link-space:' ';index:4|4;min:9;}"

ab.c.w "<div id=""wrapbox"">"
ab.c.wr "==========="
ab.c.wr "<strong>第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
Dim i, n, m
n = pager.Begin
m = pager.End
Dim k : k = 1
If Rs.State = 1 Then 'rs对象是打开的
	If Not Rs.Eof Then
		For i = n To m
			rs.MoveFirst
			rs.Move i-1
			If Not Rs.Eof Then ab.c.wn "<li>" & rs("id") & " : " & rs("name") & "</li>"
		Next
	End If
End If
ab.c.wr "==========="
ab.c.wr "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
ab.c.wr pager.html
'pager.Show()
ab.c.wr "=========="
ab.c.wr "</div>"

rs.Close
Set rs = Nothing
%>