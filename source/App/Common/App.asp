<%
'App项目函数

'获取产品名称
Function getProductName(ByVal id)
	getProductName = M("product")().Find(id).getField("name")
End Function

'获取付款方式
Function getPayWay(ByVal n)
	Select Case LCase(n)
		Case "1" : getPayWay = "货到付款"
		Case "2" : getPayWay = "银行汇款"
	End Select
End Function
%>