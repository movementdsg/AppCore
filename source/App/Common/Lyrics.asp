<%
'' 歌词转文本 相关函数

'取得歌词最大可能的行数目
Function getLrcMaxLine(ByVal lrc)
  Dim Matches, RegEx
  Set RegEx = New RegExp
  RegEx.Pattern = "\[(\d{1,2}:\d{1,2}\.{0,1}\d{1,2})\]"
  RegEx.Global = True
  Set Matches = RegEx.Execute(lrc)
  getLrcMaxLine = Matches.Count
  Set Matches = Nothing
  Set RegEx = Nothing
End Function

'歌词修正函数
Function ReviseLrc(ByVal lrc)
	On Error Resume Next
	Dim RegEx,Match,RegEx2,Match2,rule,line,tmp,str : str = lrc
	str = AB.C.RP(str,""," ") '类空格特殊符号转为英文空格
	str = AB.C.RP(str,"　"," ") '全角空格转为英文空格
	rule = "(\[(\d{1,2}:\d{1,2}\.{0,1}\d{1,2})\](.*))"
	Set RegEx = AB.C.RegMatch(str,rule) '检查每一行
	For Each Match In RegEx
		line = Trim(Match.SubMatches(0))
		str = AB.C.RP(str,Match.value,line&"")
		Set RegEx2 = AB.C.RegMatch(line,"(((\[\d{1,2}:\d{1,2}\.{0,1}\d{1,2}\]){1,})(((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*))")
		For Each Match2 In RegEx2
			str = AB.C.RP(str, Match2.value, Trim(Match2.value) & "" & VBCrlf )
		Next
		Set RegEx2 = Nothing
	Next
	Set RegEx = Nothing
	str = AB.D.DelSpace(str, 6) '清除空白行及空格开头结尾等
	tmp = ""
	'rule = "((\[\d{1,2}:\d{1,2}\.{0,1}\d{1,2}\]){1,}((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*)"
	rule = "((\[\d{1,2}:\d{1,2}\.{0,1}\d{1,2}\]){1,}((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*|(\[[a-z0-9A-Z]+:[^]]*\]){1,}((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*)"
	Set RegEx = AB.C.RegMatch(str,rule) '检查每一行
	For Each Match In RegEx
		line = Trim(Match.SubMatches(0))
		line = AB.C.RP(line,VBCrlf,"")
		line = AB.C.RP(line,Chr(13),"")
		line = AB.C.RP(line,Chr(10),"")
		IF Trim(line)<>"" Then
			tmp = tmp & line & VBCrlf
		End IF
	Next
	Set RegEx = Nothing
	str = tmp
	str = AB.D.DelSpace(str, 6) '清除空白行及空格开头结尾等
	ReviseLrc = str
End function

'把时间类型转换成整数类型, 方便排序
Function convertTime2Int(ByVal strTime)
  Dim strTmp, regEx
  If InStr(strTime, ".")=0 Then
	strTime = strTime & ".00"
  End If
  Set regEx = New RegExp
  regEx.Pattern = "(\d{1,}):(\d{1,2})\.(\d{1,2})"
  convertTime2Int = regEx.Replace(strTime, "$1")
  strTmp = regEx.Replace(strTime, "$2")
  If Len(Trim(strTmp))=1 Then
	strTmp = "0" & strTmp
  End If
  convertTime2Int = convertTime2Int & strTmp
  strTmp = regEx.Replace(strTime, "$3")
  Set regEx = Nothing
  If Len(Trim(strTmp))=1 Then
	strTmp = "0" & strTmp
  End If
  convertTime2Int = CLng(convertTime2Int & strTmp)
End Function

'取得歌词文本数据(数组)
Function lrc2arr(ByVal lrc)
	Dim line, idx, arr() : idx = 0
	lrc = ReviseLrc(lrc) '歌词修正
	line = getLrcMaxLine(lrc)
	ReDim arr(line, 3)
	Call ParseLyric(lrc, arr, idx)
	Call SortLyric(arr)
	lrc2arr = arr
End function

'歌词转文本
Function lrc2txt(ByVal lrc)
	Dim arr, i, str : str = ""
	arr = lrc2arr(lrc)
	For i=0 To UBound(arr, 1)-1
		'''str = str & arr(i, 0) & " : " & arr(i, 1) & "<br/>" & VBCrlf
		str = str & arr(i, 1) & "<br/>" & VBCrlf
	Next
	lrc2txt = str
End function

'开始正则匹配
Sub ParseLyric(ByVal lrc, ByRef arr, ByRef idx)
  Dim Matches, RegEx, i
  Set RegEx = New RegExp
  RegEx.Pattern = "\[(\d{1,2}:\d{1,2}\.{0,1}\d{1,2})\](.*)"
  'RegEx.Pattern = "\[(\d{1,2}:\d{1,2}\.{0,1}\d{1,2})\](((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*)"
  RegEx.Global = True
  Set Matches = RegEx.Execute(lrc)
  For i=0 To Matches.Count-1
	Call ReverseLyric(Matches(i), arr, idx)
  Next
  Set RegEx = Nothing
End Sub

'遍历匹配的歌词, 并取得各个时间轴和与之对应的歌词
Sub ReverseLyric(ByVal lrc, ByRef arr, ByRef idx)
	Dim Matches, RegEx, RegEx2, strTime, strWords, str
	Set RegEx = New RegExp
	RegEx.Pattern = "\[(\d{1,2}:\d{1,2}\.{0,1}\d{1,2})\](.*)"
	'RegEx.Pattern = "\[(\d{1,2}:\d{1,2}\.{0,1}\d{1,2})\](((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*)"
	RegEx.Global = True
	Set RegEx2 = New RegExp
	RegEx2.Pattern = "((\[\d{1,2}:\d{1,2}\.{0,1}\d{1,2}\]){1,})(.*)"
	'RegEx2.Pattern = "((\[\d{1,2}:\d{1,2}\.{0,1}\d{1,2}\]){1,})(((?!\[[a-z0-9A-Z]*:[^]]*\])[\s\S])*)"
	RegEx2.Global = True
	If RegEx.Test(lrc) Then
		strTime = RegEx.Replace(lrc, "$1")
		str = RegEx.Replace(lrc, "$2")
		strWords = RegEx2.Replace(str,"$3")
		arr(idx, 0) = strTime
		arr(idx, 1) = strWords
		arr(idx, 2) = convertTime2Int(strTime)
		idx = idx + 1
		If (Trim(str)<>"" AND RegEx.Test(str)) Then
			Call ReverseLyric(str, arr, idx)
		Else
			Exit Sub
		End If
	Else
		Exit Sub
	End If
	Set RegEx = Nothing
	Set RegEx2 = Nothing
End Sub

'歌词排序
Sub SortLyric(ByRef arr)
  Dim intLoop, intReLoop, strWords, strTime, intTime
  For intLoop=0 To UBound(arr, 1)-1
	  For intReLoop=intLoop+1 To UBound(arr, 1)-1
		  If arr(intLoop, 2) > arr(intReLoop, 2) Then
			  strTime = arr(intLoop, 0)
			  strWords = arr(intLoop, 1)
			  intTime = arr(intLoop, 2)
			  arr(intLoop, 0) = arr(intReLoop, 0)
			  arr(intLoop, 1) = arr(intReLoop, 1)
			  arr(intLoop, 2) = arr(intReLoop, 2)
			  arr(intReLoop, 0) = strTime
			  arr(intReLoop, 1) = strWords
			  arr(intReLoop, 2) = intTime
		  End If
	  Next
  Next
End Sub
%>