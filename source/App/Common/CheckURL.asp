<%
Function JsEnCodeURI(ByVal str)
	AB.Use "E"
	str = AB.E.encodeURIComponent(AB.E.encodeURIComponent(str))
	JsEnCodeURI = str
End Function

Function JsDeCodeURI(ByVal str)
	AB.Use "E"
	str = AB.E.decodeURIComponent(AB.E.decodeURIComponent(str))
	JsDeCodeURI = str
End Function

Function GetRealURL(ByVal url) '获取绝对地址
	Dim HostUrl, HttpBef : HttpBef = "http://"
	HostUrl = Request.ServerVariables("HTTP_HOST") ' 服务器域名(可以得到非80的Server_Port端口) (返回: localhost:8080)
	If LCase(Request.ServerVariables("HTTPS")) <> "off" Then HttpBef = "https://"
	If Instr(url,"://")>0 Then
		url = url
	ElseIf Instr(url,":")>0 Then
		url = "file:///" & url & ""
	ElseIf InStr(url,"/")>0 Then ' 如果是以/开头 就补全 网站服务器地址
		url = HttpBef & HostUrl & "" & url
	Else
		url = App.RootPath() & "/" & url
	End If
	GetRealURL = url
End Function

Function GetFileType(ByVal url) '根据文件路径获取文件名后缀(类型/格式)
	Dim fpath,fname,ext
	url = Replace(Trim(url), "\", "/")
	fpath = Left( url , Len(url)-(Len(Split(url,"/")(Ubound(Split(url,"/"))))+1) )
	fname = Split(url,"/")(Ubound(Split(url,"/")))
	ext = Lcase(Mid(fname, InstrRev(fname,".")+1))
	GetFileType = ext
End Function

Function GetRemoteFileType(ByVal url)
	Dim temp, ftype, arrExt
	arrExt = Split("mp3,wma,wmv,mid,wav,asf,avi,swf,flv,rmvb,ra,rm,mov,qt,mpeg,mpg,dat,mp4,3gp,vob,flac",",")
	ftype = GetFileType(url)
	AB.Use "A"
	If Not AB.C.isNul(url)  Then
		Select Case ftype
			Case AB.A.InArray(ftype,arrExt) : temp = "FileExt:"& ftype
			Case Else : 
				If Instr(LCase(url),"file:///") Then
					temp = "Error"
				Else
					If Not CheckURL(url,"media") Then
						temp = "Error"
					Else
						temp = "Content-Type:" & GetXttpResponseHeaders(url,"media") & ""
					End If
				End If
		End Select
	Else
		temp = "Error"
	End If
	GetRemoteFileType = temp
End Function

Function CheckURL(ByVal A_strUrl,ByVal A_strItem) 'ASP通用地址死链检测函数
	'ASP通用地址死链检测函数,使用/调用方法:
	'用于网页文档地址检测: CheckURL(URL,"html")
	'用于媒体地址检测: CheckURL(URL,"media")
	'用于图片地址检测: CheckURL(URL,"image")
	On Error Resume Next
	Dim A_ServUrl:A_ServUrl = Mid(A_strUrl,1,Instr(8,A_strUrl,"/"))
	Dim XMLHTTP
	'Set XMLHTTP=Server.CreateObject("Microsoft.XMLHTTP")
	Set XMLHTTP=Server.CreateObject("MSXML2.ServerXMLHTTP")
	'Set XMLHTTP=Server.CreateObject("MSXML2.XMLHTTP.4.0")
	With XMLHTTP
	'.setTimeOuts 10000,10000,10000,30000
	'解析响应时间, 连接响应时间, 发送请求数据响应时间, 接受数据响应时间
	Select Case LCase(A_strItem)
		Case "html"
			'.open "HEAD",A_strUrl,False,"",""
			.open "GET",A_strUrl,False
		Case "media"
			.open "HEAD",A_strUrl,False
			'.open "GET",A_strUrl,False,"",""
		Case "image"
			'.open "HEAD",A_strUrl,False,"",""
			.open "GET",A_strUrl,False
		Case Else
			.open "HEAD",A_strUrl,False
			'.open "GET",A_strUrl,False,"",""
	End Select
	'.setRequestHeader "content-type","application/x-www-form-urlencoded"
	.setRequestHeader "Referer", A_strUrl
	.send()
	End With
	If XMLHTTP.readyState=4 Then
		If XMLHTTP.Status=404 Or XMLHTTP.Status=403 Or XMLHTTP.Status=500 Then
			CheckURL = False
		ElseIf XMLHTTP.Status=200 Or XMLHTTP.Status=0 Then
			Select Case LCase(A_strItem)
				Case "html" ' 用于网页文档地址检测
					'If Len(XMLHTTP.getResponseHeader("X-Powered-By"))>0 Then
					'If Len(XMLHTTP.getResponseHeader("Content-Type"))>0 Then
					'If Len(XMLHTTP.ResponseBody)>0 Then
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If Len(XMLHTTP.ResponseBody)>0 Then
							'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
							IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
								CheckURL = True
							Else
								CheckURL = False
							End IF
						Else
							CheckURL = False
						End If
					Else
						CheckURL = False
					End IF
				Case "media" ' 用于媒体地址检测
					'If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'If Len(XMLHTTP.ResponseBody)>0 Then
							If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
								CheckURL = False
							Else
								'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
								'IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
									CheckURL = True
								'Else
									'CheckURL = False
								'End IF
							End If
						'Else
							'CheckURL = False
						'End If
					'Else
						'CheckURL = False
					'End IF
				Case "image" ' 用于图片地址检测
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
							CheckURL = False
						Else
							'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
							IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
								CheckURL = True
							Else
								CheckURL = False
							End IF
						End If
					Else
						CheckURL = False
					End IF
				Case Else
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If XMLHTTP.getResponseHeader("Content-Type")="" Then '获取不到Content-Type
							CheckURL = False
						Else
							CheckURL = True
						End If
					Else
						CheckURL = False
					End IF
			End Select
		Else
			Select Case A_strItem
				Case "html" ' 用于网页文档地址检测
					'If Len(XMLHTTP.getResponseHeader("X-Powered-By"))>0 Then
					'If Len(XMLHTTP.getResponseHeader("Content-Type"))>0 Then
					'If Len(XMLHTTP.ResponseBody)>0 Then
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'If Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
						IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
							CheckURL = True
						Else
							CheckURL = False
						End If
					Else
						CheckURL = False
					End IF
				Case "media" ' 用于媒体地址检测
					'If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'If Len(XMLHTTP.ResponseBody)>0 Then
							If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
								CheckURL = False
							Else
								'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
								'IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
									CheckURL = True
								'Else
									'CheckURL = False
								'End IF
							End If
						'Else
							'CheckURL = False
						'End If
					'Else
						'CheckURL = False
					'End IF
				Case "image" ' 用于图片地址检测
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
							CheckURL = False
						Else
							'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
							IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
								CheckURL = True
							Else
								CheckURL = False
							End IF
						End If
					Else
						CheckURL = False
					End IF
				Case Else
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If XMLHTTP.getResponseHeader("Content-Type")="" Then '获取不到Content-Type
							CheckURL = False
						Else
							CheckURL = True
						End If
					Else
						CheckURL = False
					End IF
			End Select
		End If
	Else
		CheckURL=False
		'Exit Function
	End If
	Set XMLHTTP=Nothing
	If Err.Number<>0 Then Err.Clear
End Function

Function GetXttpResponseHeaders(ByVal A_strUrl,ByVal A_strItem)
'-----------------------------------------------------------------------------
	' 一些常见格式返回值如下:
	' 11.mp3 返回 "audio/mpeg"
	' 11.wma 返回 "audio/x-ms-wma"
	' 11.wmv 返回 "video/x-ms-wmv"
	' 11.mid 返回 "audio/mid"
	' 11.wav 返回 "audio/wav"
	' 11.asf 返回 "video/x-ms-asf"
	' 11.avi 返回 "video/avi"
	' 11.swf 返回 "application/x-shockwave-flash"
	' 11.flv 返回 "application/octet-stream"
	' 11.ra 返回 "audio/x-pn-realaudio"
	' 11.rm 返回 "application/octet-stream"
	' 11.rmvb 返回 "application/octet-stream"
	' 11.mov 返回 "video/quicktime"
	' 11.qt 返回 "video/quicktime"
	' 11.mpg 返回 "video/mpeg"
	' 11.mpeg 返回 "video/mpeg"
	' 11.dat 返回 "application/octet-stream"
	' 11.mp4 返回 "application/octet-stream"
	' 11.3gp 返回 "application/octet-stream"
	' 11.vob 返回 "application/octet-stream"
	' 11.flac 返回 "application/octet-stream"
'-----------------------------------------------------------------------------
	On Error Resume Next
	Dim A_ServUrl:A_ServUrl = Mid(A_strUrl,1,Instr(8,A_strUrl,"/"))
	Dim XMLHTTP
	'Set XMLHTTP=Server.CreateObject("Microsoft.XMLHTTP")
	Set XMLHTTP=Server.CreateObject("MSXML2.ServerXMLHTTP")
	'Set XMLHTTP=Server.CreateObject("MSXML2.XMLHTTP.4.0")
	With XMLHTTP
	'.setTimeOuts 10000,10000,10000,30000
	'解析响应时间, 连接响应时间, 发送请求数据响应时间, 接受数据响应时间
	Select Case A_strItem
		Case "html"
			'.open "HEAD",A_strUrl,False,"",""
			.open "GET",A_strUrl,False
		Case "media"
			.open "HEAD",A_strUrl,False
			'.open "GET",A_strUrl,False,"",""
		Case "image"
			'.open "HEAD",A_strUrl,False,"",""
			.open "GET",A_strUrl,False
		Case Else
			.open "HEAD",A_strUrl,False
			'.open "GET",A_strUrl,False,"",""
	End Select
	'.setRequestHeader "content-type","application/x-www-form-urlencoded"
	.setRequestHeader "Referer", A_ServUrl
	'.setRequestHeader "Cookie", ""
	.send()
	End With
	If XMLHTTP.readyState=4 Then
		If XMLHTTP.Status=404 Or XMLHTTP.Status=403 Or XMLHTTP.Status=500 Then
			GetXttpResponseHeaders = False
		ElseIf XMLHTTP.Status=200 Or XMLHTTP.Status=0 Then
			Select Case A_strItem
				Case "html" ' 用于网页文档地址检测
					'If Len(XMLHTTP.getResponseHeader("X-Powered-By"))>0 Then
					'If Len(XMLHTTP.getResponseHeader("Content-Type"))>0 Then
					'If Len(XMLHTTP.ResponseBody)>0 Then
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If Len(XMLHTTP.ResponseBody)>0 Then
							'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
							IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
								GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
							Else
								GetXttpResponseHeaders = False
							End IF
						Else
							GetXttpResponseHeaders = False
						End If
					Else
						GetXttpResponseHeaders = False
					End IF
				Case "media" ' 用于媒体地址检测
					'If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'If Len(XMLHTTP.ResponseBody)>0 Then
							If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
								GetXttpResponseHeaders = False
							Else
								'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
								'IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
									GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
								'Else
									'GetXttpResponseHeaders = False
								'End IF
							End If
						'Else
							'GetXttpResponseHeaders = False
						'End If
					'Else
						'GetXttpResponseHeaders = False
					'End IF
				Case "image" ' 用于图片地址检测
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
							GetXttpResponseHeaders = False
						Else
							'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
							IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
								GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
							Else
								GetXttpResponseHeaders = False
							End IF
						End If
					Else
						GetXttpResponseHeaders = False
					End IF
				Case Else
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
						IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
							GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
						Else
							GetXttpResponseHeaders = False
						End IF
					Else
						GetXttpResponseHeaders = False
					End IF
			End Select
		Else
			Select Case A_strItem
				Case "html" ' 用于网页文档地址检测
					'If Len(XMLHTTP.getResponseHeader("X-Powered-By"))>0 Then
					'If Len(XMLHTTP.getResponseHeader("Content-Type"))>0 Then
					'If Len(XMLHTTP.ResponseBody)>0 Then
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'If Len(XMLHTTP.ResponseBody)>0 Then
						IF Not IsEmpty(XMLHTTP.ResponseBody) Then
							GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
						Else
							GetXttpResponseHeaders = False
						End If
					Else
						GetXttpResponseHeaders = False
					End IF
				Case "media" ' 用于媒体地址检测
					'If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'If Len(XMLHTTP.ResponseBody)>0 Then
						'IF Not IsEmpty(XMLHTTP.ResponseBody) Then
							If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
								GetXttpResponseHeaders = False
							Else
								'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
								'IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
									GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
								'Else
									'GetXttpResponseHeaders = False
								'End IF
							End If
						'Else
							'GetXttpResponseHeaders = False
						'End If
					'Else
						'GetXttpResponseHeaders = False
					'End IF
				Case "image" ' 用于图片地址检测
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
							GetXttpResponseHeaders = False
						Else
							'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
							IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
								GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
							Else
								GetXttpResponseHeaders = False
							End IF
						End If
					Else
						GetXttpResponseHeaders = False
					End IF
				Case Else
					If Len(XMLHTTP.getAllResponseHeaders())>0 Then '是否有响应站点
						'IF Len(XMLHTTP.ResponseBody)>0 Then '文件大小不能是0字节
						IF Not IsEmpty(XMLHTTP.ResponseBody) Then '能够获取到响应内容
							GetXttpResponseHeaders = XMLHTTP.getResponseHeader("Content-Type")
						Else
							GetXttpResponseHeaders = False
						End IF
					Else
						GetXttpResponseHeaders = False
					End IF
			End Select
		End If
	Else
		GetXttpResponseHeaders=False
		Exit Function
	End If
	Set XMLHTTP=Nothing
	If Instr(GetXttpResponseHeaders,";")>0 Then GetXttpResponseHeaders = Trim(Split(GetXttpResponseHeaders,";")(0))
	If Err.Number<>0 Then Err.Clear
End Function

Function getContentType(ByVal A_strUrl) '获取文档的Content-Type类型
	On Error Resume Next
	Dim A_ServUrl:A_ServUrl = Mid(A_strUrl,1,Instr(8,A_strUrl,"/"))
	Dim XMLHTTP
	'Set XMLHTTP=Server.CreateObject("Microsoft.XMLHTTP")
	Set XMLHTTP=Server.CreateObject("MSXML2.ServerXMLHTTP")
	'Set XMLHTTP=Server.CreateObject("MSXML2.XMLHTTP.4.0")
	With XMLHTTP
	'.setTimeOuts 10000,10000,10000,30000
	'解析响应时间, 连接响应时间, 发送请求数据响应时间, 接受数据响应时间
	.open "HEAD",A_strUrl,False
	'.open "GET",A_strUrl,False
	'.setRequestHeader "content-type","application/x-www-form-urlencoded"
	.setRequestHeader "Referer", A_ServUrl
	.send()
	End With
	If XMLHTTP.readyState=4 Then
		If XMLHTTP.Status=404 Then
			getContentType = ""
		ElseIf XMLHTTP.Status=200 Or XMLHTTP.Status=0 Then
			getContentType = XMLHTTP.getResponseHeader("Content-Type")
		Else
			If Len(XMLHTTP.getAllResponseHeaders())>0 Then
			'If Len(xmlHttp.getResponseHeader("X-Powered-By"))>0 Then
			'If Len(XMLHTTP.getResponseHeader("Content-Type"))>0 Then
			'If Len(XMLHTTP.ResponseBody)>0 Then
				If XMLHTTP.getResponseHeader("Content-Type")="text/html" Then '对于文档类型（包括404返回页等）
					getContentType = "text/html"
				Else
					getContentType = XMLHTTP.getResponseHeader("Content-Type")
				End If
			Else
				getContentType = ""
			End If
		End If
	Else
		getContentType = ""
		Exit Function
	End If
	Set XMLHTTP=Nothing
	If Err.Number<>0 Then Err.Clear
End Function

Function getImageType(Byval sURL) '获取图像的Content-Type类型
	' 常见的图像的Content-Type类型有:
	' "image/gif" 'GIF images(.gif)
	' "image/jpeg" 'JPEG images(.jpeg ,.jpg ,.jpe)
	' "image/png" 'PNG images(.png)
	' "image/bmp" '(.bmp)
	' "image/x-icon" ' (.ico)
	' "image/x-xbitmap" ' (.xbm)
	' "image/x-xpixmap" ' (.xpm)
	' "image/tiff" 'TIFF images(.tiff ,.tif)
	' "application/octet-stream" ' 未知格式
	Dim spType, sFileType
	sFileType = getContentType(sURL)
	Select Case sFileType
		Case "image/gif","image/jpeg","image/png","image/bmp","image/x-icon","image/x-xbitmap","image/x-xpixmap","image/tiff"
			spType = sFileType
		Case Else
			spType = "application/octet-stream" '未知类型
	End Select
	getImageType = spType
End Function
%>