<%
'--------定义部份------------------
Dim Neeao_Application_Value
Dim Neeao_Post,Neeao_Get,Neeao_Status,Neeao_Inject,Neeao_Inject_Keyword,Neeao_Kill_IP,Neeao_Write_Data
Dim Neeao_Alert_Url,Neeao_Alert_Info,Neeao_Kill_Info,Neeao_Alert_Type
Dim Neeao_Sec_Forms,Neeao_Sec_Form_open,Neeao_Sec_Form,Neeao_Sec_Modules,Neeao_Sec_Module_Open


'SQL防注入函数
Sub Stop_SqlIn()
	If IsArray(Application("Neeao_config_info"))=False Then Call PutApplicationValue()
	Neeao_Application_Value = Application("Neeao_config_info")
	'获取配置信息
	Neeao_Status = Neeao_Application_Value(0)
	Neeao_Inject = Neeao_Application_Value(1)
	Neeao_Kill_IP = Neeao_Application_Value(2)
	Neeao_Write_Data = Neeao_Application_Value(3)
	Neeao_Alert_Url = Neeao_Application_Value(4)
	Neeao_Alert_Info = Neeao_Application_Value(5)
	Neeao_Kill_Info = Neeao_Application_Value(6)
	Neeao_Alert_Type = Neeao_Application_Value(7)
	Neeao_Sec_Forms = Neeao_Application_Value(8)
	Neeao_Sec_Form_open = Neeao_Application_Value(9)
	Neeao_Sec_modules = Neeao_Application_Value(10)
	Neeao_Sec_Module_Open = Neeao_Application_Value(11) '是否开启安全模块

	'安全表单参数
	Neeao_Sec_Form = split(Neeao_Sec_Forms,"|")
	Neeao_Inject_Keyword = split(Neeao_Inject,"|")

	If Neeao_Status=1 Then
		If Not Secure_Modules_Allow(Neeao_Sec_modules, Neeao_Sec_Module_Open) Then
			If Neeao_Kill_IP=1 Then Stop_IP
			If Request.Form<>"" Then Call StopInjection(Request.Form,1)
			If Request.QueryString<>"" Then Call StopInjection(Request.QueryString,2)
			If Request.Cookies<>"" Then Call StopInjection(Request.Cookies,3)
		End If
	End If
End Sub

'安全模块绕过验证
Function Secure_Modules_Allow(secrue_modules, secrue_module_open)
	Dim is_arrow, this_module, allow_modules, allow_item, allow_m, allow_c, allow_a, t, p
	is_arrow = False
	AB.Use "A"
	If Neeao_Sec_Module_Open = 1 Then '开启安全模块
		secrue_modules = Trim(secrue_modules)
		If secrue_modules<>"" Then
			secrue_modules = Trim(AB.C.RP(secrue_modules, Array(VbCrlf,VbCr,VbLf), ";"))
			arr_modules = Split(secrue_modules,";")
			allow_modules = Array()
			For Each item In arr_modules
				item = Trim(item)
				If item<>"" Then
					If InStr(item,":")>0 Then
						t = Split(item,":")
						allow_m = LCase(AB.C.IIF((Trim(t(0))="" Or Trim(t(0))="*"), GROUP_NAME, Trim(t(0))))
						p = Trim(t(1))
						If p="" Or p="*" Then
							allow_c = LCase(MODULE_NAME) : allow_a = LCase(ACTION_NAME)
						ElseIf InStr(p,"/") Then
							t = Split(p,"/")
							allow_c = LCase(AB.C.IIF((Trim(t(0))="" Or Trim(t(0))="*"), MODULE_NAME, Trim(t(0))))
							allow_a = LCase(AB.C.IIF((Trim(t(1))="" Or Trim(t(1))="*"), ACTION_NAME, Trim(t(1))))
						End If
					ElseIf InStr(item,"/") Then
						allow_m = LCase(GROUP_NAME)
						t = Split(item,"/")
						allow_c = LCase(AB.C.IIF((Trim(t(0))="" Or Trim(t(0))="*"), MODULE_NAME, Trim(t(0))))
						allow_a = LCase(AB.C.IIF((Trim(t(1))="" Or Trim(t(1))="*"), ACTION_NAME, Trim(t(1))))
					Else
						allow_m = LCase(GROUP_NAME) : allow_c = LCase(MODULE_NAME)
						allow_a = LCase(AB.C.IIF((item="" Or item="*"), ACTION_NAME, item))
					End If
					allow_item = allow_m & ":" & allow_c & "/" & allow_a
					allow_modules = AB.A.Push(allow_modules, allow_item)
				End If
			Next
			If Not AB.C.isNul(allow_modules) Then
				this_module = LCase(GROUP_NAME) & ":" & LCase(MODULE_NAME) & "/" & LCase(ACTION_NAME)
				If AB.A.InArray(this_module, allow_modules) Then
					is_arrow = True
				End If
			End If
		End If
	End If
	Secure_Modules_Allow = is_arrow
End Function

Function Stop_IP()
	Dim Sqlin_IP,rsKill_IP,Kill_IPsql
	Sqlin_IP=Request.ServerVariables("REMOTE_ADDR")
	Kill_IPsql="SELECT sqlin_ip FROM [#@sqlin] where sqlin_ip='"&Sqlin_IP&"' AND kill_ip=1"
	Set rsKill_IP = App.Dao.Query(Kill_IPsql)
	If Not(rsKill_IP.eof or rsKill_IP.bof) Then
		N_Alert(Neeao_Kill_Info)
	Response.End
	End If
	rsKill_IP.close
End Function

'sql通用防注入主函数
Function StopInjection(values,n)
	Dim Neeao_Get,Neeao_i
	For Each Neeao_Get In values
		'安全表单功能
		If Neeao_Sec_Form_open = 1 Then
			For Neeao_i=0 To UBound(Neeao_Sec_Form)
				If LCase(Neeao_Get)=LCase(Neeao_Sec_Form(Neeao_i)) Then
					Exit Function
				else
					Call Select_BadChar(values,Neeao_Get)
				End If
			Next
		Else
			Call Select_BadChar(values,Neeao_Get)
		End If
	Next
End Function

'查找关键字
Function Select_BadChar(values,Neeao_Get)
	Dim Neeao_Xh
	Dim Neeao_ip,Neeao_url,Neeao_sql
	'Neeao_ip = Request.ServerVariables("REMOTE_ADDR")
	Neeao_ip = AB.C.GetIp()
	Neeao_url = Request.ServerVariables("URL")
	For Neeao_Xh=0 To Ubound(Neeao_Inject_Keyword)
		If Instr(LCase(values(Neeao_Get)),Neeao_Inject_Keyword(Neeao_Xh))<>0 Then
			If Neeao_Write_Data = 1 Then
				On Error Resume Next
				Neeao_sql = "INSERT INTO [#@sqlin](sqlin_ip,sqlin_web,sqlin_fs,sqlin_cs,sqlin_sj) VALUES('"&Neeao_ip&"','"&Neeao_url&"','"&N_intype(values)&"','"&Neeao_Get&"','"&N_Replace(values(Neeao_Get))&"')"
				'response.write Neeao_sql
				App.Dao.Execute(Neeao_sql)
				If Err Then
					'Response.Write "无法写入SQL防注入数据库"
					'Response.End
					Err.Clear
				End IF
			End If
			N_Alert(Neeao_Alert_Info)
			Response.End
		End If
	Next
End Function

'输出警告信息
Function N_Alert(Neeao_Alert_Info)
	Dim str
	str = str & "<"&"script type=""text/javascript"""&">"
	str = str & "function closewin() {"
	str = str & "	var userAgent = navigator.userAgent;"
	str = str & "	if (userAgent.indexOf('Firefox') != -1 || userAgent.indexOf('Presto') != -1) {"
	str = str & "		window.location.replace('about:blank');"
	str = str & "	} else {"
	str = str & "		window.opener = null;"
	str = str & "		window.open('', '_self');"
	str = str & "		window.close();"
	str = str & "	}"
	str = str & "}"
	Select Case Neeao_Alert_Type
		Case 1
			str = str & "window.opener=null; closewin();"
		Case 2
			str = str & "alert('"&Neeao_Alert_Info&"'); closewin();"
		Case 3
			str = str & "location.href='"&Neeao_Alert_Url&"';"
		Case 4
			str = str & "alert('"&Neeao_Alert_Info&"');location.href='"&Neeao_Alert_Url&"';"
	end Select
	str = str & "<"&"/script"&">"
	response.write  str
End Function

'判断注入类型函数
Function N_intype(values)
	Select Case values
		Case Request.Form
			N_intype = "Post"
		Case Request.QueryString
			N_intype = "Get"
		Case Request.Cookies
			N_intype = "Cookies"
	end Select
End Function

'干掉xss脚本
Function N_Replace(N_urlString)
	N_urlString = Replace(N_urlString,"'","''")
    N_urlString = Replace(N_urlString, ">", "&gt;")
    N_urlString = Replace(N_urlString, "<", "&lt;")
    N_Replace = N_urlString
End Function

Sub  PutApplicationValue()
	dim  infosql,rsinfo, sql
	sql = "SELECT status,filter,kill_ip,makelog,alert_url,alert_info,kill_info,handle_type,secure_forms,secure_form_open,secure_modules,secure_module_open FROM [#@sqlin_config]"
	set rsinfo = App.Dao.Query(sql)
	Redim ApplicationValue(11)
	dim i
	for i=0 to 11
		ApplicationValue(i) = rsinfo(i)
	next
	set rsinfo=nothing
	Application.Lock
	set Application("Neeao_config_info")=nothing
	Application("Neeao_config_info")=ApplicationValue
	Application.unlock
end Sub
%>