<%
'Mini电台Action层
Class MiniAction_Radio

	Private radio_model, url_model
	Private module_id, topid

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		App.View.LayerFile = "mini/layout.html" '设置layerout模板
		Response.Charset = App.CharSet
		Set radio_model = M("radio")
		Set url_model = M("radio_url")
		module_id : module_id = M("modules")().Where("name='radio'").getField("id")
		topid = IntVal( M("class")().Where("typeid=0 and module_id="& module_id &" and pid=0").getField("id") )
		App.View.Assign "module_id", module_id '电台模型id
		App.View.Assign "topid", topid '顶级栏目id
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set radio_model = Nothing
		Set url_model = Nothing
	End Sub

	'View: index.asp?m=radio&c=mini&a=all
    Public Sub all()
		App.View.Assign "lmid", IntVal(App.Req("id"))
		App.View.Assign "page", IntVal(App.Req("p"))
		App.View.Display("all.html")
    End Sub

	'View: index.asp?m=radio&c=mini&a=search&keyword=aa
    Public Sub search()
		Dim keyword
		keyword = Trim(App.Req("keyword"))
		keyword = Replace(keyword,"'","''")
		App.View.Assign "keyword", keyword
		App.View.Display("search.html")
    End Sub

	'View: index.asp?m=radio&c=mini&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=radio&c=mini&a=menu
    Public Sub menu()
		App.View.Display("menu.html")
    End Sub

	'View: index.asp?m=radio&c=mini&a=show
    Public Sub show()
		App.View.Display("show.html")
    End Sub

	'View: index.asp?m=radio&c=mini&a=list
    Public Sub list()
		'App.Tpl.TplFile = "list.html"
		'App.Tpl.Display
		App.View.Display("list.html")
    End Sub

	'View: index.asp?m=radio&c=mini&a=radio&id=1
	'View: index.asp?m=radio&c=mini&a=radio&o=main
    Public Sub radio()
		On Error Resume Next
		Dim o, id, Rs, host, quote : o = App.Req("o")
		host = AB.C.RqSv("HTTP_HOST")
		If o = "main" Then
			id = IntVal(SiteCfgData()("radio_web_radioid"))
		Else
			id = IntVal(App.Req("id"))
			quote = "&nbsp;&nbsp;&nbsp;&nbsp;<span class='quotetxt'><a href='"& App.RootPath &"/res/radio/quote.asp?id="& id &"' target='_blank'>引用</a></span>"
		End If
		Set Rs = radio_model().Top(1).Where("id = "& id).Result()
		Dim radio_name, radio_txt, player, showtxt
		If Not (Rs.Bof And Rs.Eof) Then
			radio_name = Rs("name")
			radio_txt = Rs("content")
			player = Rs("player")
			showtxt = radio_name & " &nbsp;&nbsp;<a href=""res/radio/"&"error.asp?id="& id &""" target=""_blank"">&nbsp;&nbsp;<font color=#765545>听不了？</font></a>"
			showtxt = showtxt& "<span style='display: none;'>"& radio_txt &"</span>"
			App.View.Assign "radio_name", radio_name
			App.View.Assign "radio_txt", radio_txt
			App.View.Assign "player", player
			App.View.Assign "showtxt", showtxt
			Rs("hits") = IntVal(Rs("hits"))+1
			Rs.update
			If Err.Number <> 0 Then App.Error.Throw 401, Err.Number, Err.Description '出错处理，例如数据不可更新
		End If
		App.View.Assign "host", host
		App.View.Assign "quote", quote
		App.View.Assign "id", id
		'App.View.Assign "o", o
		App.View.Display("radio.html") '显示模板
		On Error Goto 0
    End Sub

	'View: index.asp?m=radio&c=mini&a=url&id=1
    Public Sub url()
		App.noCache() '强制不缓存
		Dim webname, http, id, i, no, Rs, player, radio_name
		webname = SiteCfgData()("radio_title")
		http = SiteCfgData()("site_url")
		id = IntVal(App.Req("id"))
		Set Rs = radio_model().Find(id).Result()
		If Not (Rs.Bof And Rs.Eof) Then
			player = Rs("player")
			radio_name = Rs("name")
			Rs.Close() : Set Rs = Nothing
			Set RsUrl = url_model().Top(1).Where("radioid = "& id).Result()
			If intval(player)=1 Then
				Response.ContentType = "video/x-ms-asf"
				App.Out.Print "<ASX Version=""3.0"">"&VbCrlf
				no = RsUrl.RecordCount
				For i=1 To no
					App.Out.Print "<Entry>"&VbCrlf
					App.Out.Print "<Title>"& radio_name &"</Title>"&VbCrlf
					App.Out.Print "<Author>更多精彩电台尽在 "&http&"</Author>"&VbCrlf
					App.Out.Print "<Copyright>"&webname&"</Copyright>"&VbCrlf
					App.Out.Print "<Ref href="""&RsUrl("url")&"""/>"&VbCrlf
					App.Out.Print "</Entry>"&VbCrlf
					RsUrl.movenext
				Next
				App.Out.Print "</ASX>"&VbCrlf
			ElseIf intval(player)=2 Then
				'Response.ContentType = "audio/x-pn-realaudio"
				Response.ContentType = "audio/x-mpegurl"
				no = RsUrl.RecordCount
				For i=1 To no
					App.Out.Print RsUrl("url")&VbCrlf
					RsUrl.movenext
				Next
			Else
				App.Out.Print "出错：未设置播放类型!"
			End If
		Else
			App.Out.Print "未找到该项数据!"
		End If
    End Sub

	'View: index.asp?m=radio&c=mini&a=error&id=1
    Public Sub [error]()
		On Error Resume Next
		Dim id, Rs, radio_name, radio_text, ccc, code : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		Set Rs = radio_model().Find(id).Result()
		If Not (Rs.Bof And Rs.Eof) Then
			radio_name = Rs("name")
			radio_text = Rs("content")
			App.View.Assign "radio_name", radio_name
			App.View.Assign "radio_text", radio_text
			if App.Req("code")<>"" and App.Req("code") = AB.Cookie("tingError") then
				if Rs("error")<>"" then
					Rs("error").value = Rs("error").value+1
				else    
					Rs("error") = 1
				end if
				Rs.update
				AB.Cookie.Set "tingError", Cstr(id), ""
			end if
		End If
		If AB.Cookie("tingError") <> Cstr(id) then
			code = Now()*2/2
			AB.Cookie.Set "tingError", code, ""
		Else
			ccc = 1
		End If
		App.View.Assign "code", code
		App.View.Assign "ccc", ccc
		App.View.Display("error.html") '显示模板
		On Error Goto 0
    End Sub

	'View: index.asp?m=radio&c=mini&a=quote&id=1
    Public Sub quote()
		On Error Resume Next
		Dim id, Rs, host, radio_name, radio_text : id = IntVal(App.Req("id"))
		host = AB.C.RqSv("HTTP_HOST")
		App.View.Assign "id", id
		App.View.Assign "host", host
		Set Rs = radio_model().Find(id).Result()
		If Not (Rs.Bof And Rs.Eof) Then
			radio_name = Rs("name")
			radio_text = Rs("content")
			App.View.Assign "radio_name", radio_name
			App.View.Assign "radio_text", radio_text
		End If
		App.View.Display("quote.html") '显示模板
		On Error Goto 0
    End Sub

	'View: index.asp?m=radio&c=mini&a=asx&u=sssss
    Public Sub asx()
		Dim webname, http, tmp, url, Rs, u, i : u = App.Req("u")
		App.noCache() '强制不缓存
		Response.ContentType = "video/x-ms-asf"
		App.Out.Print "<ASX Version=""3.0"">"& VbCrlf
		webname = SiteCfgData()("radio_title")
		http = SiteCfgData()("site_url")
		If Trim(u)<>"" Then
			AB.Use "E"
			If Instr(u,"$$")>0 Then
				tmp = Split(u, "$$")
				For i=0 To UBound(tmp)
					url = AB.E.URLDecode(tmp(i))
					App.Out.Print "<Entry>"& VbCrlf
					App.Out.Print "<Title>节目 "& (i+1) &"</Title>"& VbCrlf
					App.Out.Print "<Author>更多精彩电台尽在 "& http &"</Author>"& VbCrlf
					App.Out.Print "<Copyright>"& webname &"</Copyright>"& VbCrlf
					App.Out.Print "<Ref href="""& url &"""/>"& VbCrlf
					App.Out.Print "</Entry>"& VbCrlf
				Next
			Else
				App.Out.Print "<Entry>"& VbCrlf
				App.Out.Print "<Title>节目 1</Title>"& VbCrlf
				App.Out.Print "<Author>更多精彩电台尽在 "& http &"</Author>"& VbCrlf
				App.Out.Print "<Copyright>"& webname &"</Copyright>"& VbCrlf
				App.Out.Print "<Ref href="""& AB.E.URLDecode(u) &"""/>"& VbCrlf
				App.Out.Print "</Entry>"& VbCrlf
			End If
		End If
		App.Out.Print "</ASX>"
		App.Out.End
    End Sub

    Public Sub [empty]()
		App.Out.Print "Radio Mini Empty Action"
    End Sub

End Class
%>