<%
Class ApiAction_Public

	Public model, cate_model
	Public table, module_id, topid

	Private Sub Class_Initialize()
		On Error Resume Next
		Response.Charset = App.CharSet
		module_id = App.ReqInt("module_id",0)
		Set cate_model = M("class")
		If module_id>0 Then
			table = M("modules")().Where("id="& module_id &"").getField("name")
		Else
			table = "article"
			module_id = M("modules")().Where("name='"& table &"'").getField("id") '当前模型id
		End If
		Set model = M(table)
		topid = cate_model().Where("typeid=0 and module_id="& module_id &" and pid=0").getField("id")
		App.View.Assign "module_id", IntVal(module_id)
		App.View.Assign "topid", IntVal(topid)
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
		Set cate_model = Nothing
	End Sub

	'View: index.asp?m=public&c=api&a=hits&id=1&module_id=1
    Public Sub hits()
		Dim id, num : num = 0
		id = IntVal(App.Req("id"))
		model().Find(id).setInc "hits", 1 '点击数+1
		num = model().Find(id).getField("hits")
		App.Out.Put IntVal(num)
    End Sub

	'View: index.asp?m=public&c=api&a=show_hits&id=1&module_id=1
    Public Sub show_hits()
		Dim id, num : num = 0
		id = IntVal(App.Req("id"))
		model().Find(id).setInc "hits", 1 '点击数+1
		num = model().Find(id).getField("hits")
		App.Out.Put "document.write('"& IntVal(num) &"');"
    End Sub

	'View: index.asp?m=public&c=api&a=api&module_id=1&f=userinfo
    Public Sub api()
		Dim page, f : f = App.Req("f")
		Select Case LCase(f)
			Case "userinfo" : page = "api_userinfo.html"
			Case Else : page = "api_index.html"
		End Select
		App.View.Display(page)
    End Sub

	'社会化登录接口 View: index.asp?m=public&c=api&a=oauth
    Public Sub oauth()
		'..
    End Sub

    Public Sub [empty]()
		App.Out.Print "Empty Action"
    End Sub

End Class
%>