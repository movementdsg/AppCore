<%
Class IndexAction_Public

	Private s_dictName
	Private i_vcode_type

	Private Sub Class_Initialize()
		On Error Resume Next
		s_dictName	= AB.dictName
		i_vcode_type = 1
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'
	End Sub

	'设置验证码类型
	Public Property Get VcodeType
		VcodeType = i_vcode_type
	End Property
	Public Property Let VcodeType(ByVal n)
		If IsNumeric(n) Then i_vcode_type = CInt(n)
	End Property

	'View: index.asp?m=public&c=index&a=index
    Public Sub index()
		App.Out.Print "Public: Index Action"
    End Sub

	'View: index.asp?m=public&c=index&a=verify_code
    Public Sub verify_code()
		App.noCache()
		If i_vcode_type = 1 Then
			AB.C.Include(App.ActionPath & "Public/Inc/verify_code.asp")
		Else
			AB.C.Include(App.ActionPath & "Public/Inc/verify_code2.asp")
		End If
    End Sub

	'View: index.asp?m=public&c=index&a=rss
    Public Sub rss()
		App.Out.Print "Public: Rss Action"
    End Sub

    Public Sub [empty]()
		App.Out.Print "Public: Empty Action"
    End Sub

End Class
%>