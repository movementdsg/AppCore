<%
Class PlayerAction_Public

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'
	End Sub

	'View: index.asp?m=public&c=player&a=common
    Public Sub common()
		Dim pt : pt = App.Req("pt")
		Select Case LCase(pt)
			Case "kmp" : App.View.Display("common/kmp.html")
			Case "vcastr" : App.View.Display("common/vcastr.html")
			Case "vcastr_swfobject" : App.View.Display("common/vcastr_swfobject.html")
			Case "media" : App.View.Display("common/mediaplayer.html")
			Case "jwplayer" : App.View.Display("common/jwplayer.html")
			Case "jwplayer5" : App.View.Display("common/jwplayer5.html")
			Case "cuplayer" : App.View.Display("common/cuplayer.html")
			Case Else : R("index") '跳转到 index()
		End Select
    End Sub

	'View: index.asp?m=public&c=player&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

    Public Sub [empty]()
		App.Out.Print "Public: Empty Action"
    End Sub

End Class
%>