<%
'前台News模块
Class IndexAction_News

	Public model, cate_model
	Public table, module_id, topid, navid, meta_title
	Private search_max_length, search_min_length

	Private Sub Class_Initialize()
		On Error Resume Next
		Response.Charset = App.CharSet
		table = "article" '表名
		Set model = M(table)
		Set cate_model = M("class")
		module_id = M("modules")().Where("name='"& table &"'").getField("id") '当前模型id
		topid = cate_model().Where("typeid=0 and module_id="& module_id &" and pid=0").getField("id")
		App.View.Assign "module_id", IntVal(module_id)
		App.View.Assign "topid", IntVal(topid)
		navid = M("nav")().Where(" alias='新闻' ").getField("id") '当前导航栏目id
		App.View.Assign "navid", IntVal(navid)
		meta_title = "新闻频道" '标题
		App.View.Assign "meta_title", meta_title
		search_max_length = 20 '允许搜索的最大长度
		search_min_length = 4 '允许搜索的最小长度
		AB.Use "A"
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
		Set cate_model = Nothing
	End Sub

	'View: index.asp?m=news&c=index&a=index
    Public Sub index()
		''ab.trace News_Base.Config_Seo(array(1,2,5), array(3,4))
		meta_title = "新闻频道" '标题
		App.View.Assign "meta_title", meta_title
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=news&c=index&a=show&id=6
    Public Sub category()
		Dim cat, id, fields, f, catid, childs, tpl
		id = IntVal(App.Req("id"))
		'-- 设置分页
		App.View.PageSize = 20 
		App.View.PageMark = "p"
		Set cat = cate_model().Field("id,pid,name").Where("id="&id).Fetch()
		childs = child_ids(id,false)
		If Not AB.C.IsNul(childs) Then tpl = "category_index.html" Else tpl = "category_list.html"
		App.View.Assign "catid", id
		App.View.Assign "cat", cat
		App.View.Assign "pid", cat("pid")
		childs = child_ids(id,true)
		childs = AB.C.IfHas(childs,-1)
		App.View.Assign "childs", childs
		App.View.Display(tpl)
    End Sub

	'View: index.asp?m=news&c=index&a=show&id=6
    Public Sub show()
		Dim rs, id, fields, f, v, t, catid, relate
		Dim pn, pnum, spl : pnum = 1 : pn = IntVal(App.Req("p")) '内容分页的页码
		id = IntVal(App.Req("id"))
		Set rs = model().Field("*").Where("id="&id).Fetch()
		fields = App.Dao.RsFields(rs)
		'-- 设置分页
		App.View.PageSize = 1
		App.View.PageMark = "p"
		App.View.Pager.Format = "{prev} {list} {next}"
		App.View.Pager.addStyle "prev{display:auto;class:prev;dis-tag:a;dis-href:js;text:上一页;}"
		App.View.Pager.addStyle "next{display:auto;class:next;dis-tag:a;dis-href:js;text:下一页;}"
		App.View.Pager.addStyle "list{display:auto;curr-tag:a;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:2|2;min:5;}"
		spl = "[page]"
		For Each f In fields
			v = rs(f).Value
			If LCase(f)="content" AND InStr(v,spl)>0 Then
				'v = AB.C.RP(v, "<hr style=""page-break-after:always;"" class=""ke-pagebreak"" />", spl)
				t = Split(v, spl)
				pnum = UBound(t)+1
				pn = AB.C.IIF(pn<=0, 1, AB.C.IIF(pn>pnum, pnum, pn) )
				v = t(pn-1)
			End If
			App.View.Assign f, v
		Next
		App.View.Assign "content_pnum", pnum
		catid = model().Find(id).getField("classid")
		App.View.Assign "catid", catid
		Set relate = get_relate_list(id,10)
		App.View.Assign "relate", relate '相关新闻
		App.View.Display("show.html")
    End Sub

	'View: index.asp?m=news&c=index&a=tag&name=cms
    Public Sub tag()
		Dim tagsql
		name = Trim(App.Req("name"))
		tagsql = "SELECT arc.* FROM ["& GetTable(table) &"] arc LEFT JOIN ["& GetTable("tag") &"] tag ON arc.id = tag.arcid" & _
				" WHERE tag.module_id="& module_id & " AND tag.name='"& name & "' "
		App.View.Assign "tagsql", tagsql
		App.View.Display("tag.html")
    End Sub

	'View: index.asp?m=news&c=index&a=search&keyword=cms
    Public Sub search()
		On Error Resume Next
		Dim contentid, keyword : keyword = Trim(App.Req("keyword"))
		Dim key, subkey, i, a_where : a_where = Array()
		'-- 设置分页
		App.View.PageSize = 10
		App.View.PageMark = "p"
		'关键字个数判断
		'keyword = AB.C.RP(keyword, "%20", " ")
		If search_max_length>0 And AB.C.strLen(keyword)>search_max_length Then
			Alert "关键字不得大于"& search_max_length &"个字符（"& (search_max_length/2) &"个汉字）"
		End If
		If search_min_length>0 And AB.C.strLen(keyword)<search_min_length Then
			Alert "关键字不得少于"& search_min_length &"个字符（"& (search_min_length/2) &"个汉字）"
		End If
		keyword = AB.C.RP(keyword, Array("'","\",">","<","=",Chr(34)), "") '安全过滤
		For Each key In Split(keyword," ")
			key = Trim(key)
			If key<>"" Then
				'a_where = AB.A.Push(a_where, "( [title] LIKE '%"& key &"%' OR [intro] LIKE '%"& key &"%' OR [content] LIKE '%"& key &"%' )")
				a_where = AB.A.Push(a_where, "( [title] LIKE '%"& key &"%' OR [intro] LIKE '%"& key &"%' )")
			End If
			' If Len(key)>1 Then '智能搜索
				' For i=1 To Len(key)-1
					' subkey = Mid(key,i,2)
					' If Trim(subkey)<>"" Then
						' a_where = AB.A.Push(a_where, "( [title] LIKE '%"& Trim(subkey) &"%' OR [intro] LIKE '%"& key &"%' OR [content] LIKE '%"& Trim(subkey) &"%' )")
					' End If
				' Next
			' End If
		Next
		a_where = Join(a_where," OR ")
		contentid = model().Where(a_where).getField("id:1") '加:1表示返回多条记录(数组)
		If IsArray(contentid) Then contentid = Join(contentid,",")
		App.View.Assign "keyword", keyword
		App.View.Assign "contentid", contentid
		App.View.Display("search.html")
		On Error Goto 0
    End Sub

    Public Sub [empty]()
		App.Out.Print "News Module Error !"
    End Sub

End Class
%>