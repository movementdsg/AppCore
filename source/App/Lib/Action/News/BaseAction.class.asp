<%
Class BaseAction_News

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		App.View.LayerOut = True
		App.View.Locate = True
		'App.View.Theme = "default"
		App.View.LayerFile = "layout.html" '设置layerout模板
		'-- 分页设置
		App.View.PageSize = 20
		App.View.PageMark = "p"
		App.View.Pager.Format = "{first} {prev} {list} {next} {last}"
		App.View.Pager.addStyle "first{display:auto;class:first;text:首 页;}"
		App.View.Pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;}"
		App.View.Pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;}"
		App.View.Pager.addStyle "last{display:auto;class:last;dis-tag:span;text:尾 页;}"
		App.View.Pager.addStyle "list{display:auto;curr-tag:span;curr-class:current;link-text:'$n';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:2|2;min:5;}"
		Base.LoadCfg() '加载站点配置
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	Public Function Config_Seo(ByVal arr1, ByVal arr2)
		AB.Use "A"
		Dim a : a = AB.A.Cover(arr1, arr2)
		Config_Seo = a
	End Function

    Public Sub index()
		AB.C.Print "缺省模块 缺省Action"
    End Sub

    Public Sub [empty]()
		AB.C.Print "缺省模块 Empty Action"
    End Sub

End Class
%>