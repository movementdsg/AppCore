<%
Class IndexAction_Music

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("music_music")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=music&c=index&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=music&c=index&a=sort_list
    Public Sub sort_list()
		Dim str
		str = str & "var BigClassName_ID42 = '流行歌曲';" & VbCrlf
		'str = str & "var BigClassName_ID"& id &" = '" & name & "';" & VbCrlf
		App.Out.Put(str)
    End Sub

	'View: index.asp?m=music&c=index&a=data_list
    Public Sub data_list()
		Dim rs, artist
		Set rs = model().Order("id asc").Fetch()
		Do While Not rs.Eof
			artist = model().Find(IntVal(rs("artist_id"))).getField("artist_name")
			App.Out.Print "var url_"&rs("id")&" = '"&AB.C.RP(rs("mp3url"),"'","\'")&"'; "&vbCrLf
			App.Out.Print "var music_"&rs("id")&" = '"&AB.C.RP(rs("song_name"),"'","\'")&"'; "&vbCrLf
			App.Out.Print "var singer_"&rs("id")&" = '"&AB.C.RP(artist,"'","\'")&"'; "&vbCrLf
			App.Out.Print "var ID_"&rs("id")&" = "&rs("id")&" ; "&vbCrLf
			App.Out.Print "var BgColor_"&rs("id")&" = '"&rs("colors")&"'; "&vbCrLf
			App.Out.Print "var PlayerType_"&rs("id")&" = '"&rs("player")&"'; "&vbCrLf &vbCrLf
			rs.MoveNext
		Loop
		App.Exit()
    End Sub

	'View: index.asp?m=music&c=index&a=play_list
    Public Sub play_list()
		Dim rs, i, listid, s_where, s_order
		listid = App.Req("listid")
		s_order = "id asc"
		If listid<>"" Then listid = IntVal(listid)
		If App.Req("item")="top" and listid="" then
			s_where = ""
			s_order = "id asc"
		Else
			If listid<>"" Then
				s_where = "cate_id="& listid
				s_order = "cate_id asc, id asc"
			Else
				s_where = ""
				s_order = "id asc"
			End if
		End if
		Set rs = model().Where(s_where).Order(s_order).Fetch()
		Do While Not rs.Eof
			i = rs("id")
			App.Out.Print "InsertList(url_"&i&", music_"&i&", singer_"&i&", ID_"&i&", BgColor_"&i&", PlayerType_"&i&"); "&vbCrLf
			Rs.movenext
		Loop
		rs.Close
		Set rs = Nothing
		App.Out.Print("doplay();")
		App.Exit()
    End Sub

	'View: index.asp?m=music&c=index&a=lrc_show&id=1
    Public Sub lrc_show()
		App.View.Display("musicbox/lrc_show.html")
    End Sub

	'View: index.asp?m=music&c=index&a=lrc_txt&id=1
    Public Sub lrc_txt()
		Dim rs, lyrics, id : id = IntVal(App.Req("id"))
		Set rs = model().Field("song_name, artist_id, lyrics").Find(id).Fetch()
		lyrics = rs("lyrics")
		song = rs("song_name")
		artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
		App.View.Assign "song", song
		App.View.Assign "artist", artist
		App.View.Assign "lyrics", lyrics
		App.View.Display("musicbox/lrc_txt.html")
    End Sub

	'View: index.asp?m=music&c=index&a=down&id=1
    Public Sub down()
		setData()
		App.View.Display("musicbox/down.html")
    End Sub

	'View: index.asp?m=music&c=index&a=intro&id=1
    Public Sub intro()
		setData()
		App.View.Display("musicbox/intro.html")
    End Sub

	'查看动态歌词
	'View: index.asp?m=music&c=index&a=lyrics&id=1
    Public Sub lyrics()
		Dim rs, song, artist, fname, lrctxt, str, id : id = IntVal(App.Req("id"))
		Set rs = model().Top(1).Find(id).Field("lyrics,song_name,artist_id").Fetch()
		If Not rs.Eof Then
			song = rs("song_name")
			artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
			lrctxt = rs("lyrics")
		End If
		rs.close
		Set rs = Nothing
		str = lrctxt
		str = ReviseLrc(str) '歌词修正
		App.Out.Print str '输出歌词文本
		App.Exit()
    End Sub

	'下载动态歌词
	'View: index.asp?m=music&c=index&a=lyrics_down&id=1
    Public Sub lyrics_down()
		Dim rs, song, artist, fname, lrctxt, str, id : id = IntVal(App.Req("id"))
		Set rs = model().Find(id).Fetch()
		If Not rs.Eof Then
			song = rs("song_name")
			artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
			lrctxt = rs("lyrics")
		End If
		rs.close
		Set rs = Nothing
		str = lrctxt
		str = ReviseLrc(str) '歌词修正
		'fname = ( song&" - "& artist & ".LRC" )
		'fname = ( "" & id & ".LRC" )
		fname = ("" & song &" - "& artist & "["& id &"].LRC")
		Response.Charset = "UTF-8"
		Response.ContentType = "application/octet-stream"
		Response.AddHeader "Content-Disposition", "attachment; filename=" & fname
		App.Out.Print str '输出歌词文本
		App.Exit()
    End Sub

	'下载歌曲
	'View: index.asp?m=music&c=index&a=song_down&id=1
    Public Sub song_down()
		Dim url, id : id = IntVal(App.Req("id"))
		url = model().Find(id).getField("mp3url")
		App.Out.Put url
    End Sub

	'出错处理
	'View: index.asp?m=music&c=index&a=handle&id=1
    Public Sub handle()
		Dim id, st : id = App.Req("id")
		st = App.Req("st")
		If st="hits" Then
			model().Find(id).setInc "hits",1
		ElseIf st="downs" Then
			model().Find(id).setInc "downs",1
		ElseIf st="error" Then
			model().Find(id).setInc "hits",1
		End If
		App.Out.Put "1"
    End Sub

	'View: index.asp?m=music&c=index&a=playcx&id=1
    Public Sub playcx()
		setData()
		App.View.Display("musicbox/playcx.html")
    End Sub

	'View: index.asp?m=music&c=index&a=lrcmp&id=1
    Public Sub lrcmp()
		setData()
		App.View.Display("musicbox/lrcmp.html")
    End Sub

	'View: index.asp?m=music&c=index&a=box_main
    Public Sub box_main()
		App.View.Display("musicbox/box_main.html")
    End Sub

	'View: index.asp?m=music&c=index&a=checkurl?enurl=
    Public Sub checkurl()
		Dim url
		If App.Req("enurl")<>"" Then
			AB.Use "E" : AB.E.Use "Base64"
			url = AB.E.Base64.D(App.Req("enurl"))
		Else
			url = App.Req("url")
		End If
		url = GetRealUrl(url) '获取完整地址(补全地址)
		App.Out.Put GetRemoteFileType(url)
    End Sub

	'View: index.asp?m=music&c=index&a=checkurl_cross?url=
    Public Sub checkurl_cross()
		Dim url, ftype, ptype, songid
		ptype = Trim(App.Req("ptype"))
		songid = Trim(App.Req("SongID"))
		url = App.Req("url")
		url = JsDeCodeURI(url) '解码
		url = GetRealUrl(url) '获取完整地址(补全地址)
		ftype = GetRemoteFileType(url)
		url = JsEnCodeURI(url) '编码
		App.Out.Print "cb_url = '" & url & "';" & VBCrlf
		App.Out.Print "cb_ext = '" & ftype & "';" & VBCrlf
		App.Out.Print "cb_type = '" & ptype & "';" & VBCrlf
		App.Out.Print "cb_songid = '" & songid & "';" & VBCrlf
		App.Exit()
    End Sub

	'View: index.asp?m=music&c=index&a=checkurl_jsonp?url=
    Public Sub checkurl_jsonp()
		Dim url, ftype, ptype, songid
		ptype = Trim(App.Req("ptype"))
		songid = Trim(App.Req("SongID"))
		url = App.Req("url")
		url = JsDeCodeURI(url) '解码
		url = GetRealUrl(url) '获取完整地址(补全地址)
		ftype = GetRemoteFileType(url)
		url = JsEnCodeURI(url) '编码
		App.Out.Put "jsonpHandle('" & url & "' ,'" & ftype & "' ,'" & ptype & "' ,'" & songid &"');"
    End Sub

	Private Sub setData()
		Dim rs, id : id = App.Req("id")
		Dim song, artist, lrctxt, player, mp3url
		Set rs = model().Find(id).Fetch()
		If Not rs.Eof Then
			song = rs("song_name")
			artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
			lrctxt = rs("lyrics")
			player = rs("player")
			mp3url = rs("mp3url")
		End If
		rs.close
		Set rs = Nothing
		lrctxt = ReviseLrc(lrctxt) '歌词修正
		App.View.Assign "id", id
		App.View.Assign "song", song
		App.View.Assign "artist", artist
		App.View.Assign "lrctxt", lrctxt
		App.View.Assign "player", player
		App.View.Assign "mp3url", mp3url
	End Sub

    Public Sub [empty]()
		App.Out.Print GROUP_NAME & " Empty Action"
    End Sub

End Class
%>