<%
Class IndexAction_Music

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("music_music")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=music&c=index&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=music&c=index&a=play
    Public Sub play()
		App.View.Display("play.html")
    End Sub

	'View: index.asp?m=music&c=index&a=init_url
    Public Sub init_url()
		App.View.Display("playbox/init_url.html")
    End Sub

	'View: index.asp?m=music&c=index&a=playlist
    Public Sub playlist()
		Dim rs, i, listid, s_where, s_order
		s_where = "" : s_order = "mu.id asc"
		Set rs = model().A("mu")_
				.Field("mu.*, art.id as artist_id, art.artist_name")_
				.Join("#@music_artist art")_
				.On("art.id = mu.artist_id")_
				.Where(s_where)_
				.Order(s_order)_
				.Limit(8,5)_
				.Fetch()
		App.View.Assign "songs", rs
		App.View.Display("playbox/playlist.html")
    End Sub

	'View: index.asp?m=music&c=index&a=songlist
    Public Sub songlist()
		Dim rs, i, listid, s_where, s_order
		Dim fk, op : op = App.Req("op")
		s_where = "" : s_order = "mu.id asc"
		Set rs = model().A("mu")_
				.Field("mu.*, art.id as artist_id, art.artist_name")_
				.Join("#@music_artist art")_
				.On("art.id = mu.artist_id")_
				.Where(s_where)_
				.Order(s_order)_
				.Limit(0,0)_
				.Fetch()
		App.View.Assign "songs", rs
		Select Case LCase(op)
			Case "rnddata" : fk = 4
			Case "newdata" : fk = 5
			Case "hotdata" : fk = 6
			Case Else : fk = 1
		End Select
		App.View.Assign "fk", fk
		Response.CharSet = "UTF-8"
		App.View.Display("playbox/songlist.html")
    End Sub

	'View: index.asp?m=music&c=index&a=songdata&id=1
    Public Sub songdata()
		Dim rs, i, listid, s_where, s_order, id : id = IntVal(App.Req("id"))
		s_where = "mu.id = " & id : s_order = "mu.id asc"
		Set rs = model().A("mu")_
				.Field("mu.*, art.id as artist_id, art.artist_name")_
				.Join("#@music_artist art")_
				.On("art.id = mu.artist_id")_
				.Where(s_where)_
				.Order(s_order)_
				.Top(1)_
				.Fetch()
		App.View.Assign "song", rs
		App.View.Display("playbox/songdata.html")
    End Sub

	'View: index.asp?m=music&c=index&a=down&id=1
    Public Sub down()
		setData()
		App.View.Display("playbox/down.html")
    End Sub

	'View: index.asp?m=music&c=index&a=lrc_js&id=1
    Public Sub lrc_js()
		setData()
		App.View.Display("playbox/lrc_js.html")
    End Sub

	'View: index.asp?m=music&c=index&a=lrc_txt&id=1
    Public Sub lrc_txt()
		setData()
		App.View.Display("playbox/lrc_txt.html")
    End Sub

	'查看动态歌词
	'View: index.asp?m=music&c=index&a=lyrics&id=1
    Public Sub lyrics()
		Dim rs, song, artist, fname, lrctxt, str, id : id = IntVal(App.Req("id"))
		Set rs = model().Find(id).Field("lyrics,song_name,artist_id").Fetch()
		If Not rs.Eof Then
			song = rs("song_name")
			artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
			lrctxt = rs("lyrics")
		End If
		rs.close
		Set rs = Nothing
		str = lrctxt
		str = ReviseLrc(str) '歌词修正
		App.Out.Print str '输出歌词文本
		App.Exit()
    End Sub

	'下载动态歌词
	'View: index.asp?m=music&c=index&a=lyrics_down&id=1
    Public Sub lyrics_down()
		Dim rs, song, artist, fname, lrctxt, str, id : id = IntVal(App.Req("id"))
		Set rs = model().Find(id).Fetch()
		If Not rs.Eof Then
			song = rs("song_name")
			artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
			lrctxt = rs("lyrics")
		End If
		rs.close
		Set rs = Nothing
		str = lrctxt
		str = ReviseLrc(str) '歌词修正
		'fname = ( song&" - "& artist & ".LRC" )
		'fname = ( "" & id & ".LRC" )
		fname = ("" & song &" - "& artist & "["& id &"].LRC")
		Response.Charset = "UTF-8"
		Response.ContentType = "application/octet-stream"
		Response.AddHeader "Content-Disposition", "attachment; filename=" & fname
		App.Out.Print str '输出歌词文本
		App.Exit()
    End Sub

	'下载歌曲
	'View: index.asp?m=music&c=index&a=song_down&id=1
    Public Sub song_down()
		Dim url, id : id = IntVal(App.Req("id"))
		url = model().Find(id).getField("mp3url")
		App.Out.Print url
		App.RR url
    End Sub

	'出错处理
	'View: index.asp?m=music&c=index&a=handle&st=hits&id=1
    Public Sub handle()
		Dim id, st, count : id = App.Req("id")
		st = App.Req("st")
		If st="hits" Then
			model().Find(id).setInc "hits",1
			count = model().Find(id).getField("hits")
		ElseIf st="downs" Then
			model().Find(id).setInc "downs",1
			count = model().Find(id).getField("downs")
		ElseIf st="errors" Then
			model().Find(id).setInc "errors",1
			count = model().Find(id).getField("errors")
		End If
		count = IntVal(count)
		App.Out.Put count
    End Sub

	'View: index.asp?m=music&c=index&a=playcx&id=1
    Public Sub playcx()
		setData()
		App.View.Display("playbox/playcx.html")
    End Sub

	'View: index.asp?m=music&c=index&a=lrcmp&id=1
    Public Sub lrcmp()
		setData()
		App.View.Display("playbox/lrcmp.html")
    End Sub

	Private Sub setData()
		Dim rs, id : id = App.Req("id")
		Dim song, artist, lyrics, lrctxt, player, mp3url
		Set rs = model().Find(id).Fetch()
		If Not rs.Eof Then
			song = rs("song_name")
			artist = M("music_artist")().Find( IntVal(rs("artist_id")) ).getField("artist_name")
			lyrics = rs("lyrics")
			player = rs("player")
			mp3url = rs("mp3url")
		End If
		rs.close
		Set rs = Nothing
		lyrics = AB.C.RP(lyrics,Array("""","'"),Array("”","’"))
		lrctxt = ReviseLrc(lyrics) '歌词修正
		App.View.Assign "id", id
		App.View.Assign "song", song
		App.View.Assign "artist", artist
		App.View.Assign "lyrics", lyrics
		App.View.Assign "lrctxt", lrctxt
		App.View.Assign "player", player
		App.View.Assign "mp3url", mp3url
	End Sub

    Public Sub [empty]()
		App.Out.Print GROUP_NAME & " Empty Action"
    End Sub

End Class
%>