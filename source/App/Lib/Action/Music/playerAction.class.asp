<%
Class PlayerAction_Music

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=home&c=index&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=music&c=player&a=play&fk=1
    Public Sub play()
		Dim fk : fk = App.Req("fk")
		App.View.Display("play"& fk &".html")
    End Sub

    Public Sub [empty]()
		App.Out.Print GROUP_NAME & " Empty Action"
    End Sub

End Class
%>