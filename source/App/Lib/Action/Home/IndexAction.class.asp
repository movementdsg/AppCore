<%
'前台Action层

Class IndexAction_Home

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=home&c=index&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=home&c=index&a=test
    Public Sub test()
		Dim db : Set db = AB.db.New
		db.Conn = db.OpenConn(1, cfg_sitepath & "Data/#test.mdb", "")
		App.View.Assign "newConn", db.Conn
		App.View.Display("test.html")
    End Sub

	'View: index.asp?a=ttt
    Public Sub ttt()
		App.View.Display("ttt.html")
    End Sub

	'View: index.asp?m=home&c=index&a=sayhello
    Public Sub sayhello()
		App.CC
		App.Out.W "Hello World!"
    End Sub

    Public Sub [empty]()
		App.Out.Print "Home Empty Action"
    End Sub

End Class
%>