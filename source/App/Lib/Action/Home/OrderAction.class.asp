<%
'订单
Class OrderAction_Home

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=home&c=order&a=saveOrder
    Public Sub saveOrder()
		Dim model
		If IS_POST Then
			Set d = dictForm()
			d("add_time") = Now()
			If d("name")="" Then
				warnTip "请输入您的姓名", -1, "", ""
			End If
			If d("phone")="" Then
				warnTip "请输入您的手机号", -1, "", ""
			End If
			Set model = M("order")
			If Err.Number=0 And model().Add(d) Then
				winTip "操作成功", U("index"), "", ""
			Else
				warnTip "操作失败", -1, "", ""
			End If
		End If
    End Sub

End Class
%>