<%
'微信Action层

Class ApiAction_Wx

	Private Wechat, Xml, Keyword
	Public Data

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		AB.Use "Xml" : Set Xml = AB.Xml.New
		Set Data = AB.C.Dict()
		Set Wechat = App.Helper("Wechat")
		Wechat.Token = "aspwx"
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=wx&c=api&a=index
    Public Sub index()
		Dim Content
		If IS_POST Then
			InitData() '初始化数据
			Wechat.toUser = Me.Data("ToUserName") 
			Wechat.fromUser = Me.Data("FromUserName")
			Keyword = Me.Data("Content")
			If Keyword="你好" Then
				Content = "我也很好"
			Else
				Content = Keyword
			End If
			Wechat.Content = Content
			Wechat.WriteText()
		Else
			Dim echostr : echostr = App.Req("echostr")
			'If Wechat.checkSignature Then
				If Trim(echostr)<>"" Then App.Out.Put echostr
			'End If
		End If
    End Sub

	'View: index.asp?m=wx&c=api&a=test
    Public Sub test()
		' InitData() '初始化数据
		' Wechat.toUser = Me.Data("ToUserName")
		' Wechat.fromUser = Me.Data("FromUserName")
		' Wechat.Content = Me.Data("Content")
		' Wechat.WriteText()
		' Dim strxml
		' strxml=strxml& "<xml>" & VbCrlf
		' strxml=strxml& "<ToUserName><![CDATA["& Me.Data("FromUserName") &"]]></ToUserName>" & VbCrlf
		' strxml=strxml& "<FromUserName><![CDATA["& Me.Data("ToUserName") &"]]></FromUserName> " & VbCrlf
		' strxml=strxml& "<CreateTime>1</CreateTime>" & VbCrlf
		' strxml=strxml& "<MsgType><![CDATA[text]]></MsgType>" & VbCrlf
		' strxml=strxml& "<Content><![CDATA[aaa]]></Content>" & VbCrlf
		' strxml=strxml& "<FuncFlag>0</FuncFlag>" & VbCrlf
		' strxml=strxml& "</xml>"
		' App.Out.Put strxml
    End Sub

	'初始化数据
	Private Sub InitData()
		If IS_POST Then
			Set Data = Wechat.GetXmlData()
			' Dim XmlDom
			' Set XmlDom = Server.CreateObject("MSXML2.DOMDocument")'获取API信息
			' XmlDom.load Request
			' Me.Data("ToUserName") = XmlDom.getElementsByTagName("ToUserName").Item(0).Text
			' Me.Data("FromUserName") = XmlDom.getElementsByTagName("FromUserName").Item(0).Text
			' Me.Data("MsgType") = XmlDom.getElementsByTagName("MsgType").Item(0).Text
			' Me.Data("Content") = XmlDom.getElementsByTagName("Content").Item(0).Text
		End If
	End Sub

End Class
%>