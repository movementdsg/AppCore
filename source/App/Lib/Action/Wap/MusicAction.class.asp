<%
'WAP之Music操作模块

Class MusicAction_Wap

	Private Sub Class_Initialize()
		On Error Resume Next
		'-- 视图设置
		'App.View.LayerOut = False
		App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=wap&c=muisc&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=wap&c=muisc&a=so
    Public Sub so()
		App.View.Display("so.html")
    End Sub

	'View: index.asp?m=wap&c=music&a=play&id=1
    Public Sub play()
		Dim id : id = App.ReqInt("id",0)
		Dim songName, album, artist, artistPic, mp3path, duration
		Dim rs, rs1, rs2 : Set rs = M("music_music")().Find(id).Fetch()
		If Not (rs.Bof And rs.Eof) Then
			songName = rs("song_name")
			mp3path = rs("mp3url")
			duration = rs("duration")
			Set rs1 = M("music_album")().Find( rs("album_id").Value ).Fetch()
			Set rs2 = M("music_artist")().Find( rs("artist_id").Value ).Fetch()
			If Not rs1.Eof Then
				album = rs1("album_name")
			End If
			If Not rs2.Eof Then
				artist = rs2("artist_name")
				artistPic = rs2("artist_pic")
			End If
			App.Out.Print "{'ip':'0','rid':'"& id &"','songName':'"& songName &"','album':'"& album &"','artist':'"& artist &"','duration':'"& duration &"','artistPic':'"& artistPic &"','mp3path':'"& mp3path &"'}"
			Set rs1 = Nothing : Set rs2 = Nothing
		Else
			App.Out.Print "error, data no find!"
		End If
		Set rs = Nothing
    End Sub

	'View: index.asp?m=wap&c=music&a=lyric&id=1
    Public Sub lyric()
		Dim id : id = App.ReqInt("id",0)
		Dim rs : Set rs = M("music_lyric")().Find(id).Fetch()
		Dim lyric
		If Not (rs.Bof And rs.Eof) Then
			lyric = rs("lyric_lrc")
		End If
		App.Out.Print "$song_Lrc["& id &"] = """& AB.C.RP(lyric,"""","”") &""";"
		Set rs = Nothing
    End Sub

    Public Sub [empty]()
		App.Out.Print "Empty Action"
    End Sub

End Class
%>