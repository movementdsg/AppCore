<%
Class IndexAction_Admin

	Private Sub Class_Initialize()
		On Error Resume Next
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=index&a=index
    Public Sub index()
		Admin_Base.LoginCheck() '验证登陆
		Dim menu_data
		AB.Use "Json"
		menu_data = AB.Json.toJson(Menu_Model.get_menu_data())
		App.View.Assign "menu_data", menu_data
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=index&a=left
    Public Sub left()
		Admin_Base.LoginCheck() '验证登陆
		Dim often_menu : Set often_menu = M("menu")().Where("often=1").Fetch()
		App.View.Assign "topid", IntVal(App.Req("menuid"))
		App.View.Assign "often_menu", often_menu
		App.View.Display("left.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=index&a=panel
    Public Sub panel()
		Admin_Base.LoginCheck() '验证登陆
		App.View.Display("panel.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=index&a=map
    Public Sub map()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		dim list : Set list = M("menu").admin_menu(0)
        App.View.Assign "list", list
		App.View.Display("map.html")
		App.Exit()
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=index&a=login
    Public Sub login()
		App.noCache()
		Dim username, password, backurl, login_token
		If IS_POST And App.Req("do")="login" Then
			'增加防跨站攻击CSRF的Token检测
			If Not App.CheckToken() Then
				'AlertGo "非法来源或重复提交", "-1"
				warnTip "非法来源或重复提交", U("index/login"), "", ""
			End If
			If App.Req("username")="" Then warnTip "请输入用户名", -1, "", ""
			If App.Req("password")="" Then warnTip "请输入密码", -1, "", ""
			If C("verify_code_on")="true" Then '若开启了验证码
				If LCase(App.Req("verify_code"))<>LCase(AB.Session("getcode")) Then
					warnTip "验证码输入错误", -1, "", ""
				End If
			End If
			username = App.Req("username")
			password = App.Req("password")
			If Not ChkLogin(username, password) Then
				warnTip "请输入正确的用户名和密码", -1, "", ""
			Else
				'App.RR( U("index/index") )
				backurl = AB.E.URLDecode(App.Req("backurl"))
				If backurl<>"" Then
					winTip "登陆成功", backurl, "", ""
				Else
					winTip "登陆成功", U("index/index"), "", ""
				End If
			End If
		Else
			App.View.Display("login.html")
		End If
    End Sub

	'退出操作
	'View: index.asp?m=admin&c=index&a=logout
    Public Sub logout()
		'AB.Cookie.RemoveAll() '清空所有cookie
		AB.Cookie.Remove "admin_name"
		AB.Cookie.Remove "admin_id"
		AB.Cookie.Remove "admin_role_id"
		AB.Cookie.Remove "admin_role_name"
		'App.RR U("index/login")
		winTip "您已成功退出", U("index/login"), "", ""
    End Sub

	'验证通过后登陆
    Private Function ChkLogin(uid, pwd)
		On Error Resume Next
		Dim rs, d, tm, status, user_name, user_id, role_id, role_name
		pwd = AB.E.Md5.To32(pwd)
		Set rs = M("admin")().Where(" username='"& uid &"' and password='"& pwd &"' ").Fetch()
		if rs.eof and rs.bof then
			rs.close
			set rs = nothing
			status = False
		else
			tm = now()
			rs("login_count")=rs("login_count")+1
			rs("last_time")=rs("login_time")
			rs("last_ip")=rs("login_ip")
			rs("login_time")=tm
			rs("login_ip")=AB.C.GetIP()
			rs.Update
			user_name = rs("username")
			user_id = rs("id")
			role_id = CInt(rs("role_id"))
			set rs = nothing
			'--记录日志
			Set d = AB.C.Dict()
			d("uid") = user_id
			d("login_ip") = AB.C.GetIP()
			d("login_time") = tm
			d("useragent") = AB.C.getOS() & "; " & AB.C.IfHas(App.Req("browser"), AB.C.getBS())
			M("admin_log")().Add(d)
			'--记录登陆 Cookie
			role_name = M("admin_role")().where("id="& role_id).getField("name")
			AB.Cookie.Set "admin_name", user_name, ""
			AB.Cookie.Set "admin_id", user_id, ""
			AB.Cookie.Set "admin_role_id", role_id, ""
			AB.Cookie.Set "admin_role_name", role_name, ""
			status = True
		end if
		ChkLogin = status
    End Function

    Public Sub [empty]()
		App.Out.Print "Empty Action"
    End Sub

End Class
%>