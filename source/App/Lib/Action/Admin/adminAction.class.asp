<%
Class AdminAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("admin")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
	End Sub

	'View: index.asp?m=admin&c=admin&a=index
    Public Sub index()
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加管理员"
		big_menu("iframe") = U("admin/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 210
		App.View.Assign "big_menu", big_menu
		'-- e --
		Dim rs
		'Set rs = model().Fetch()
		'Set rs = model().Field("#@admin.*, #@admin_role.name as role_name").Relation( Array("admin_role", "#@admin.role_id = #@admin_role.id", true) ).Fetch()
		Set rs = model().A("admin").Field("admin.*, role.name as role_name").Relation( Array("admin_role role", "admin.role_id = role.id", true) ).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=admin&a=add
    Public Sub add()
		On Error Resume Next
		Dim d, role
		If IS_POST Then '提交方式：POST
			If Trim(AB.Form("password"))="" Then App.Out.Put dataReturn(0, "密码不能为空", "", "add", "")
			Set d = model.dictForm()
			d("status") = 1
			d("login_count") = 0
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			Set role = M("admin_role")().where("status=1").Fetch()
			App.View.Assign "role", role
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=admin&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id, role : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = model.dictForm()
			If d.Exists("username") Then
				If model.name_exists(d("username"), id) Then
					App.Out.Put dataReturn(0, "操作失败，管理员名重复", "", "edit", "")
				End If
			End If
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			Set role = M("admin_role")().where("status=1").Fetch()
			App.View.Assign "role", role
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="username" Then
			If model.name_exists(val, id) Then
				ajaxReturn 0, "管理员名重复"
			End If
		End If
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("username"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "管理员已存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

End Class
%>