<%
Class ArticleAction_Admin

	Private model, cate_model, page_model, tag_model
	Private module_id, topid

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("article")
		Set cate_model = M("class")
		Set page_model = M("page")
		Set tag_model = M("tag")
		module_id = M("modules")().Where("name='article'").getField("id") '当前模型id
		topid = IntVal( M("class")().Where("typeid=0 and module_id="& module_id &" and pid=0").getField("id") )
		App.View.Assign "module_id", IntVal(module_id)
		App.View.Assign "topid", topid '顶级栏目id
		AB.Use "A"
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
		Set cate_model = Nothing
		Set page_model = Nothing
		Set tag_model = Nothing
	End Sub

	'View: index.asp?m=admin&c=article&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, res, cate_list, p, d_where, s_order : Set d_where = AB.C.Dict()
		Dim search,time_start,time_end,status,keyword,posid,classid,selected_ids
		Set res = cate_model().Field("id,name").Where("status=1 AND module_id="& module_id).Fetch()
		Set cate_list = AB.C.Dict()
		If res.RecordCount>0 Then
			Do While Not res.Eof
				cate_list(CLng(res("id").Value)) = res("name").Value
				res.MoveNext
			Loop
		End If
		App.View.Assign "cate_list", cate_list
		p = App.ReqInt("p",1)
        App.View.Assign "p", p
        App.View.Assign "k", 1
		'--条件
		time_start = Trim(App.Req("time_start"))
		time_end = Trim(App.Req("time_end"))
		status = Trim(App.Req("status"))
		keyword = Trim(App.Req("keyword"))
		classid = Trim(App.Req("classid"))
		posid = Trim(App.Req("posid"))
		If time_start<>"" Then d_where("add_time>=") = "#"& CDate(time_start) & "#"
		If time_end<>"" Then d_where("add_time<=") = "#"& CDate(time_end) & "#"
		If status<>"" Then d_where("status") = IntVal(status)
		If posid<>"" Then d_where("posid") = IntVal(posid)
		If keyword<>"" Then d_where("title[%]") = "%"& keyword & "%"
		selected_ids = ""
		If IntVal(classid)>0 Then
            id_arr = cate_model.get_child_ids(classid, true)
			'd_where("classid") = "In(" & Join(id_arr, ",") & ")"
			d_where("classid[in]") = id_arr
            selected_ids = cate_model.get_ssid(IntVal(classid))
        End If
		Set search = AB.C.Dict()
		search("time_start") = time_start
		search("time_end") = time_end
		search("classid") = classid
		search("selected_ids") = selected_ids
		search("status") = status
		search("keyword") = keyword
		search("posid") = posid
		s_order = Admin_Base.GetOrderBy("article")
		'--
		Set rs = model().Where(d_where).Order(s_order).Fetch()
		App.View.Assign "search", search
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=article&a=add
    Public Sub add()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, author, arr, tags, tag
		If IS_POST Then '提交方式：POST
			'--上传 Process
			Dim Upload, upimg, file, savepath, rndDir, result
			Set Upload = Admin_Base.Upload.Uploader
			If Upload.ErrorID>0 then
				warnTip Upload.Description, -1, "", ""
			Else
				rndDir = Year(Now()) & "/" & right("0"& Month(Now()),2) & right("0"& Day(Now()),2)
				savepath = AB.Pub.FixAbsPath(App.UploadPath) & "post/" & rndDir & "/" '上传目录
				Set file = upload.files("img")
				if not(file is nothing) then
					result = file.saveToFile(savepath,0,true)
					if result then
						upimg = savepath & file.filename
					else
						warnTip file.Exception, -1, "", ""
					end if
				end if
			End If
			Set Upload = Nothing
			'------
			Set d = model.dictForm()
			If upimg<>"" Then d("thumb") = upimg
			If Err.Number=0 And model().Add(d) Then
				id = model().LastId '上次插入id
				tags = Trim(App.Req("tags"))
				If tags<>"" Then
					arr = Array()
					For Each tag In Split(tags, " ")
						If Trim(tag)<>"" Then arr = AB.A.Push(arr, Trim(tag))
					Next
					arr = AB.A.Unique(arr) '移除相同元素
					tag_model().Where("module_id="& module_id &" AND arcid="& id &"").Del()
					For Each tag In arr
						If tag<>"" Then tag_model().Add( Array("module_id:"& module_id, "arcid:"& id, "name:"& tag) )
					Next
				End If
				winTip "操作成功", U("article/index"), "", ""
			Else
				warnTip "操作失败", -1, "", ""
			End If
		Else
			author = AB.Cookie("admin_name")
			App.View.Assign "author", author
			App.View.Assign "module_name", MODULE_NAME
			App.View.Assign "action_name", ACTION_NAME
			App.View.Display("add.html")
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=article&a=edit&id=1
    Public Sub edit()
		On Error Resume Next
		Dim rs, d, id, classid, selected_ids, arr, tags, tag, spl
		If IS_POST Then '提交方式：POST
			'--上传 Process
			Dim Upload, upimg, file, savepath, rndDir, result
			Set Upload = Admin_Base.Upload.Uploader
			If Upload.ErrorID>0 then
				warnTip Upload.Description, -1, "", ""
			Else
				rndDir = Year(Now()) & "/" & right("0"& Month(Now()),2) & right("0"& Day(Now()),2)
				savepath = AB.Pub.FixAbsPath(App.UploadPath) & "post/" & rndDir & "/" '上传目录
				Set file = upload.files("img")
				if not(file is nothing) then
					result = file.saveToFile(savepath,0,true)
					if result then
						upimg = savepath & file.filename
					else
						warnTip file.Exception, -1, "", ""
					end if
				end if
			End If
			Set Upload = Nothing
			'------
			Set d = model.dictForm()
			If upimg<>"" Then d("thumb") = upimg
			If Err.Number=0 And model().Find(d("id")).Update(d) Then
				id = App.ReqInt("id",0)
				tags = Trim(App.Req("tags"))
				If tags<>"" Then
					arr = Array()
					For Each tag In Split(tags, " ")
						If Trim(tag)<>"" Then arr = AB.A.Push(arr, Trim(tag))
					Next
					arr = AB.A.Unique(arr) '移除相同元素
					tag_model().Where("module_id="& module_id &" AND arcid="& id &"").Del()
					For Each tag In arr
						If tag<>"" Then tag_model().Add( Array("module_id:"& module_id, "arcid:"& id, "name:"& tag) )
					Next
				End If
				winTip "操作成功", U("article/index"), "", ""
			Else
				warnTip "操作失败", -1, "", ""
			End If
		Else
			id = App.ReqInt("id",0)
			Set rs = model().Find(id).Fetch()
			spl = "<hr class=""ke-pagebreak"" style=""page-break-after:always;"" />" '分页分隔符
			App.View.Assign "spl", spl
			App.View.Assign "rs", rs
			classid = model().Where("id="& id).getField("classid")
			selected_ids = cate_model.get_ssid(IntVal(classid))
			App.View.Assign "selected_ids", selected_ids
			App.View.Assign "module_name", MODULE_NAME
			App.View.Assign "action_name", ACTION_NAME
			tags = M("tag")().Where("module_id="& module_id &" AND arcid="& id &"").getField("name:1") '后面加:1表示返回数组(多个纪录)
			If IsArray(tags) Then tags = Join(tags," ")
			App.View.Assign "tags", tags
			App.View.Display("edit.html")
		End If
		On Error Goto 0
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub ajax_gettags()
		App.noCache() '强制不缓存
		Dim tags, s_where, title : title = Trim(App.Req("title"))
		title = AB.C.RP(title,"'","")
		s_where = "module_id="& module_id &" AND INSTR('"& title &"', [name] )>0" 'ACCESS语句
		tags = M("tag")().Where(s_where).Order("id asc").getField("name:1")
		If IsArray(tags) Then tags = Join(tags, " ")
		App.Out.Put dataReturn(1, "操作成功", tags, "", "")
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print "Article Empty Action"
    End Sub

End Class
%>