<%
Class RadioAction_Admin

	Private model, cate_model, url_model
	Private module_id, topid

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("radio")
		Set cate_model = M("class")
		Set url_model = M("radio_url")
		module_id = M("modules")().Where("name='radio'").getField("id")
		topid = IntVal( M("class")().Where("typeid=0 and module_id="& module_id &" and pid=0").getField("id") )
		App.View.Assign "module_id", module_id '电台模型id
		App.View.Assign "topid", topid '顶级栏目id
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=radio&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, d_where, s_order : Set d_where = AB.C.Dict()
		Dim search,status,keyword,classid,selected_ids
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加电台"
		big_menu("iframe") = U("radio/add")
		big_menu("id") = "add"
		big_menu("width") = 550
		big_menu("height") = 200
		App.View.Assign "big_menu", big_menu
		'-- e --
		'--条件
		status = Trim(App.Req("status"))
		keyword = Trim(App.Req("keyword"))
		classid = Trim(App.Req("classid"))
		If status<>"" Then d_where("status") = IntVal(status)
		If keyword<>"" Then d_where("name[%]") = "%"& keyword & "%"
		selected_ids = ""
		If IntVal(classid)>0 Then
			d_where("_string") = " ','+[classid]+',' LIKE '%,"& classid &",%' "
            selected_ids = cate_model.get_ssid(IntVal(classid))
        End If
		Set search = AB.C.Dict()
		search("classid") = classid
		search("selected_ids") = selected_ids
		search("status") = status
		search("keyword") = keyword
		s_order = Admin_Base.GetOrderBy("radio")
		's_order = "ordid asc, id desc"
		Set rs = model().Where(d_where).Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Assign "search", search
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=admin&c=radio&a=add
    Public Sub add()
		On Error Resume Next
		Dim d, id, num, i, url, arr, total : arr = Array()
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("ordid") = 255
			num = App.ReqInt("num",0)
			If num>0 Then
				AB.Use "A"
				For i=1 To num
					url = Trim(App.Req("url"& i))
					If url<>"" Then arr = AB.A.Push(arr, url)
				Next
			End If
			If Err.Number=0 And model().Add(d) Then
				id = model().LastId '上次插入Id
				total = AB.A.Len(arr)
				For i=1 To total
					url = arr(i-1)
					url_model().Add( Array("radioid:"&id, "url:"& url) )
				Next
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=radio&a=edit&id=1
    Public Sub edit()
		Dim rs, rsurl, d, id, num, i, url, arr, count, total : id = IntVal(App.Req("id")) : arr = Array()
		App.View.Assign "id", id
		Set rsurl = url_model().Where("radioid="&id).Fetch()
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			num = App.ReqInt("num",0)
			If num>0 Then
				AB.Use "A"
				For i=1 To num
					url = Trim(App.Req("url"& i))
					If url<>"" Then arr = AB.A.Push(arr, url)
				Next
			End If
			If Err.Number=0 And model().Find(id).Update(d) Then
				If AB.A.Len(arr)<=0 Then
					url_model().Where("radioid="&id).Del()
				Else
					count = rsurl.RecordCount
					total = AB.A.Len(arr)
					i = 0
					Do While Not rsurl.Eof
						i = i+1
						If i<=total Then rsurl("url") = arr(i-1)
						rsurl.Update
						rsurl.MoveNext
					Loop
					If count>=total Then
						rsurl.MoveFirst
						i = 0
						Do While Not rsurl.Eof
							i = i+1
							If i>total Then rsurl.Delete
							rsurl.MoveNext
						Loop
					Else
						For i=count+1 To total
							url = arr(i-1)
							url_model().Add( Array("radioid:"&id, "url:"& url) )
						Next
					End If
				End If
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			App.View.Assign "rsurl", rsurl
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			url_model().Where("radioid in("&id&")").Del()
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub drop_error()
		Dim id : id = App.Req("id")
		If model().Find(id).Update("error:0") Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

	Public Sub select_cate()
		Dim list, rs, id, pid, check_ids
		If IS_POST Then
			'App.Out.Put dataReturn(1, "操作成功", "", "", "")
		Else
			AB.Use "A"
			'Set list = cate_model().Field("*").Where("module_id="& module_id &" and pid=0 and typeid=0").Fetch()
			Set list = model.get_list_tree(topid,0)
			App.View.Assign "list", list
			If App.Req("pid")<>"" Then
				check_ids = AB.A.Fetch(App.Req("pid"),",")
			ElseIf App.Req("id")<>"" Then
				id = App.ReqInt("id",0)
				If id>0 Then
					Set rs = model().Field("id,name,classid").Find(id).Fetch()
					check_ids = AB.A.Fetch(rs("classid"),",")
				End If
			End If
			App.View.Assign "check_ids", check_ids
			DisplayTpl "select_cate.html", 1
		End If
	End Sub

	Public Sub ajax_load_pid()
		Dim str, pid, name, arr : arr = Array()
		pid = Trim(App.Req("pid"))
		If pid<>"" Then
			AB.Use "A"
			For Each id In Split(pid,",")
				If IntVal(id)>0 Then
					name = cate_model().Where("pid<>"& topid).Find( IntVal(id) ).getField("name")
					If name<>"" Then arr = AB.A.Push(arr, name)
				End If
			Next
			str = Join(arr, ", ")
		End If
		App.Out.Put str
	End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("name"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "电台名称已经存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>