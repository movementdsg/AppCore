<%
Class seoAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("setting")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=seo&a=page
    Public Sub page()
		On Error Resume Next
		Dim seo_config, setting, o, str, key
		If App.Req("isok")=1 Then
			winTip "更新成功", U("seo/page"), "", ""
		End If
		If IS_POST Then '提交方式：POST
			App.Out.Flush()
			msgTip "正在更新中，请稍后.."
			App.Out.Flush()
			AB.Use "json"
			If IsObject(App.Req("seo_config")) Then
				str = AB.Json.toJson( App.Req("seo_config") )
			Else
				str = App.Req("seo_config")
			End If
			If model().Where("name='seo_config'").Count()>0 Then
				Set o = AB.C.Dict()
				o("data") = str
				model().Where("name='seo_config'").Update(o)
			Else
				Set o = AB.C.Dict()
				o("name") = "seo_config"
				o("data") = str
				model().Add(o)
			End If
			If IsObject(App.Req("setting")) Then
				Set setting = App.Req("setting")
				For Each key In setting
					If IsObject(setting(key)) Then
						str = AB.Json.toJson( setting(key) )
					Else
						str = setting(key)
					End If
					If model().Where("name='"& key &"'").Count()>0 Then
						Set o = AB.C.Dict()
						o("data") = str
						model().Where("name='"& key &"'").Update(o)
					Else
						Set o = AB.C.Dict()
						o("name") = key
						o("data") = str
						model().Add(o)
					End If
				Next
			End If
			App.Out.Echo "<script type='text/javascript'>location='"& U("seo/page?isok=1") &"';</script>"
			App.Out.Flush()
		Else
			seo_config = model().Where("name='seo_config'").getField("data")
			If ( Not (seo_config=False) And Not AB.C.IsNul(seo_config) ) Then
				Set seo_config = AB.Json.toObject( seo_config )
			End If
			App.View.Assign "seo_config", seo_config
			App.View.Display("page.html")
			App.Exit()
		End If
		On Error Goto 0
    End Sub

End Class
%>