<%
Class IpbanAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("ipban")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=ipban&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, t, s_where, a_where, s_order : a_where = Array()
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加黑名单"
		big_menu("iframe") = U("ipban/add")
		big_menu("id") = "add"
		big_menu("width") = 400
		big_menu("height") = 130
		App.View.Assign "big_menu", big_menu
		'-- e --
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(AB.E.URLDecode(App.Req("keyword")))
			s_where = " name LIKE '%"& t &"%' "
			a_where = AB.A.Push(a_where, s_where)
		End If
		s_order = Admin_Base.GetOrderBy("ipban")
		Set rs = model().Where(a_where).Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=admin&c=ipban&a=add
    Public Sub add()
		On Error Resume Next
		Dim Rs, d
		If IS_POST Then '提交方式：POST
			If Trim(AB.Form("name"))="" Then App.Out.Put dataReturn(0, "内容不能为空", "", "add", "")
			Set d = model.dictForm()
			If model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=ipban&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = model.dictForm()
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		model().Find(id).setField field, val
		ajaxReturn 1, ""
    End Sub

End Class
%>