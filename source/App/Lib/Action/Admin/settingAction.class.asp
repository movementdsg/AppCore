<%
Class SettingAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("setting")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=setting&a=index
    Public Sub index()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim page : page = "index.html"
		If App.Req("type")<>"" Then page = LCase(App.Req("type"))&".html"
		App.View.Display(page)
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=setting&a=follow
    Public Sub follow()
		F("index?type=follow") '定向
    End Sub

	'View: index.asp?m=admin&c=setting&a=index
    Public Sub user()
		F("index?type=user") '定向
    End Sub

	'POST: index.asp?m=admin&c=setting&a=edit
    Public Sub edit()
		On Error Resume Next
		If IS_POST Then '提交方式：POST
			Dim d, k, o, str : Set d = model.dictForm()
			AB.Use "json"
			For Each k In d
				If IsObject(d(k)) Then
					str = AB.Json.toJson(d(k))
				Else
					str = d(k)
				End If
				If model().Where("name='"& k & "'").Count()>0 Then
					Set o = AB.C.Dict()
					o("data") = str
					model().Where("name='"& k & "'").Update(o)
				Else
					Set o = AB.C.Dict()
					o("name") = k
					o("data") = str
					model().Add(o)
				End If
			Next
			Set o = Nothing
			SiteCfgDataRefresh() '刷新SiteCfg数据
			winTip "更新成功", U("setting/index?menuid="& App.Req("menuid")), "", ""
		Else
			App.Out.Put "Request method 'GET' not supported"
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=setting&a=ajax_mail_test&email=azsmfff@163.com
    Public Sub ajax_mail_test()
		On Error Resume Next
		Dim email, oMail, send_mode
		email = App.ReqTrim("email")
		If email="" Then ajaxReturn 0, "测试邮件地址不能为空"
		Set oMail= App.Library("mail")
		send_mode : send_mode = siteCfg("mail_server.mode")
		'If send_mode="" Then send_mode = "jmail"
		oMail.Object 		= send_mode '指定发送邮件组件(1=Jmail,2=Aspemail,3=Cdonts,4=Cdoemail)
		oMail.SMTP 			= siteCfg("mail_server.host") 'SMTP地址(邮件服务器)
		oMail.LoginName		= siteCfg("mail_server.auth_username") '身份验证用户名(邮件服务器登录名)
		oMail.LoginPass		= siteCfg("mail_server.auth_password") '身份验证密码(邮件服务器登录密码)
		oMail.FromMail		= siteCfg("mail_server.from")
		oMail.FromName 		= siteCfg("mail_server.auth_username")
		oMail.SendMail email, L("send_test_email_subject"), L("send_test_email_body")
		If oMail.isOk Then
			ajaxReturn 1, "邮件发送成功"
		Else
			ajaxReturn 0, oMail.Msg
		End If
		On Error Goto 0
    End Sub

End Class
%>