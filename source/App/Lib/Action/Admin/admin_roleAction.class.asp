<%
Class Admin_RoleAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("admin_role")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
	End Sub

	'View: index.asp?m=admin&c=admin_role&a=index
    Public Sub index()
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加角色"
		big_menu("iframe") = U("admin_role/add")
		big_menu("id") = "add"
		big_menu("width") = 450
		big_menu("height") = 180
		App.View.Assign "big_menu", big_menu
		'-- e --
		Dim rs : Set rs = model().Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=admin_role&a=add
    Public Sub add()
		On Error Resume Next
		Dim d, role
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=admin_role&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id, role : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If d.Exists("name") Then
				If model.name_exists(d("name"), id) Then
					App.Out.Put dataReturn(0, "操作失败，角色名重复", "", "edit", "")
				End If
			End If
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="name" Then
			If model.name_exists(val, id) Then
				ajaxReturn 0, "角色名重复"
			End If
		End If
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub auth()
		App.noCache() '强制不缓存
		Dim auth_mod, id, menu_id, e : Set auth_mod = M("admin_auth")
		AB.Use "Form" : AB.Use "A"
		If AB.Form.Has("dosubmit") Then
			id = IntVal(AB.Form("id"))
			auth_mod().where("role_id="& id).Del()
			menu_id = AB.Form("menu_id").Value
			If Trim(menu_id)<>"" Then
				menu_id = Split(menu_id, ",")
				For Each e in menu_id
					If Trim(e)<>"" Then
						auth_mod().add( Array("role_id:"& id, "menu_id:"& Trim(e)) )
					End If
				Next
			End If
			'winTip "提交成功", U("admin_role/auth?id="& id), "", ""
			winTip "提交成功", U("admin_role/index"), "", ""
		Else
            id = IntVal(App.Req("id"))
			If id<=0 Then App.Out.Put("参数错误！")
			Dim priv_ids : priv_ids = Array()
			Dim list : Set list = model.get_list_tree(0,0)
			Dim auth_data : Set auth_data = model().Field("#@admin_auth.menu_id as menu_id").Relation( Array("admin_auth", "#@admin_auth.role_id = #@admin_role.id", true) ).Find(id).Fetch()
			Do While Not auth_data.eof
				priv_ids = AB.A.Push(priv_ids, auth_data("menu_id").Value)
				auth_data.MoveNext
			Loop
			Dim role : Set role = model().Field("id, name").Find(id).Fetch()
			App.View.Assign "role", role
			App.View.Assign "priv_ids", priv_ids
			App.View.Assign "list", list
            App.View.Display("auth.html")
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("name"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "角色名已存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

End Class
%>