<%
Class SqlinAction_Admin

	Private model, model_config

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("sqlin")
		Set model_config = M("sqlin_config")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=sqlin&a=index
    Public Sub index()
		Dim rs, s_where, s_order
		s_order = Admin_Base.GetOrderBy("sqlin")
		Set rs = model().Order(s_order).Where(s_where).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=sqlin&a=config
    Public Sub config()
		Dim rs, d
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("alert_info") = AB.C.RP(d("alert_info"),Array("'",""""),Array("’","“"))
			d("kill_info") = AB.C.RP(d("kill_info"),Array("'",""""),Array("’","“"))
			d("kill_ip") = IntVal(d("kill_ip"))
			If Err.Number=0 And model_config().Update(d) Then
				Set Application("Neeao_config_info") = Nothing
				winTip "操作成功", U("sqlin/config"), "", 1
			Else
				warnTip "操作失败", -1, "", ""
			End If
		Else
			Set rs = model_config().Fetch()
			App.View.Assign "rs", rs
			App.View.Display("config.html")
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub lockip()
		Dim ids, i, ip, id : id = App.Req("id")
		AB.Use "A"
		ids = AB.A.Fetch(id,",")
		If Not AB.C.IsNul(ids) Then
			For Each i In ids
				ip = model().Find(i).getField("sqlin_ip")
				model().Where("sqlin_ip='"& ip &"'").Update(Array("kill_ip:1"))
			Next
		End If
		If Err.Number=0 Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub unlockip()
		Dim ids, i, ip, id : id = App.Req("id")
		ids = AB.A.Fetch(id,",")
		If Not AB.C.IsNul(ids) Then
			For Each i In ids
				ip = model().Find(i).getField("sqlin_ip")
				model().Where("sqlin_ip='"& ip &"'").Update(Array("kill_ip:0"))
			Next
		End If
		If Err.Number=0 Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val, ip
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			If field="kill_ip" Then
				ip = model().Find(id).getField("sqlin_ip")
				'model().Where("sqlin_ip='"& ip &"'").setField field, val
				model().Where("sqlin_ip='"& ip &"'").Update(Array("kill_ip:"& val))
			End If
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

End Class
%>