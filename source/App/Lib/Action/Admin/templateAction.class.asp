<%
Class TemplateAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		AB.Use "Fso"
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=radio&a=index
    Public Sub index()
		Dim config_file, str, tpl_dir, theme_dir, dirname, theme_cfg_file, f_list, i, p
		Dim default_theme, template_list, k, theme_cfg : k = 0
		App.noCache() '强制不缓存
        config_file = App.ConfPath & "home.config.xml"
		default_theme = "default" '默认主题
		If IS_POST Then
			dirname = App.Req("dirname")
			If dirname<>"" Then
                '..save to config_file
			End If
			winTip "操作成功", U("template/index"), "", ""
		Else
			tpl_dir = App.TplPath & ""
			Set template_list = AB.C.Dict()
			f_list = AB.Fso.Dir(tpl_dir)
			For i = 0 To UBound(f_list,2)
				p = f_list(0,i)
				If Right(p,1)="/" Then '文件夹
					dirname = AB.C.CLeft(p,"/")
					theme_dir = tpl_dir & p & "home/"
					theme_cfg_file = theme_dir & "config.asp"
					Set theme_cfg = AB.C.Dict()
					If AB.C.isFile(theme_cfg_file) Then
						str = AB.C.IncCode(theme_cfg_file) : Execute(str)
					End If
					If AB.C.isDict(theme_cfg) Then
						theme_cfg("dirname") = dirname
						theme_cfg("themedir") = theme_dir
					End If
					template_list.Add k, theme_cfg
					k = k + 1
				End If
			Next
			App.View.Assign "default_theme", default_theme
			App.View.Assign "template_list", template_list
			App.View.Display("index.html")
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>