<%
Class ClassAction_Admin

	Private model, module_model, page_model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("class")
		Set module_model = M("modules")
		Set page_model = M("page")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=class&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加栏目"
		big_menu("iframe") = U("class/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 380
		App.View.Assign "big_menu", big_menu
		'-- e --
		Dim cat : Set cat = model.get_tree(0,0)
		App.View.Assign "cat", cat
		App.View.Assign "module_name", MODULE_NAME
		App.View.Assign "action_name", ACTION_NAME
		Dim module_list : Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
		App.View.Assign "module_list", module_list
		App.View.Assign "module_id", App.ReqInt("module_id",0)
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=music&a=add
    Public Sub add()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, pid, spid, cat, e, str, selected, color, module_list : pid = App.ReqInt("pid",0) : str = ""
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If model.name_exists(d("name"),d("pid"),0) Then
				ajaxReturn 0, "操作失败"
			End If
			d("spid") = model.get_ssid(d("pid"))
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "添加成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			If pid>0 Then
				spid = model.get_ssid(pid)
			End If
			App.View.Assign "spid", spid
			Set cat = model.get_tree(0,0)
			For Each e In cat
				selected = AB.C.IIF(cat(e)("id")=pid, "selected", "")
				str = str & "<option value='"& cat(e)("id") &"' "& selected &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
			Next
			App.View.Assign "select_menus", str
			Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
			App.View.Assign "module_list", module_list
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=music&a=edit&id=1
    Public Sub edit()
		Dim rs, d, dd, id, spid_arr, module_list
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("pid") = IntVal(d("pid"))
			If model.name_exists(d("name"),d("pid"),d("id")) Then
				ajaxReturn 0, "操作失败"
			End If
			Set rs = model().Field("img,pid").Find("id="& d("id")).Fetch()
			If rs.RecordCount>0 Then
				AB.Use "A"
				If d("pid") <> rs("pid") Then
					spid_arr = model.get_child_ids(d("id"),true)
					If AB.A.InArray(d("pid"), spid_arr) Then
						ajaxReturn 0, "不能移动到本分类或者子分类下面"
					End If
				End If
				d("spid") = model.get_ssid(d("pid"))
			End If
			If Err.Number=0 And model().Find(d("id")).Update(d) Then
				'Set dd = AB.C.Dict() : dd("spid") = model.get_spid(d("id"))
				'model().Find(d("id")).Update(dd)
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			id = App.ReqInt("id",0)
			Set rs = model().Find(id).Fetch()
			'spid = rs("spid")
			spid = model.get_spid(id)
			App.View.Assign "spid", spid
			App.View.Assign "id", id
			App.View.Assign "rs", rs
			Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
			App.View.Assign "module_list", module_list
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		On Error Resume Next
		Dim id, typeid, module_id, module_name, child_ids
		id = IntVal(App.Req("id"))
		typeid = model().Find(id).getField("typeid")
		module_id = model().Find(id).getField("module_id")
		module_name = module_model().Find(module_id).getField("name")
		If model().Delete(id) Then
			child_ids = model.get_child_ids(id, false)
			If Not AB.C.IsNul(child_ids) Then
				model().Where( "id IN("& Join(child_ids,",") &")" ).Update("pid:0")
			End If
			If typeid=1 Then '单页
				page_model().Where("classid="& id).Del()
			End If
			If App.Dao.HasTable(module_name) Then
				M(module_name)().Where("classid="& id).Update( Array("classid:0") ) '移动当前模型下的数据分类为未分类(classid:0)
			End If
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
		On Error Goto 0
    End Sub

    Public Sub ajax_upload_img()
		Dim Upload, upimg, file, savepath, rndDir, result
		Set Upload = Admin_Base.Upload.Uploader
		If Upload.ErrorID>0 then
			ajaxReturn 0, Upload.Description
		Else
			rndDir = Year(Now()) & "/" & right("0"& Month(Now()),2) & right("0"& Day(Now()),2)
			savepath = AB.Pub.FixAbsPath(App.UploadPath) & "post/" & rndDir & "/" '上传目录
			Set file = upload.files("img")
			if not(file is nothing) then
				result = file.saveToFile(savepath,0,true)
				if result then
					upimg = savepath & file.filename
					App.Out.Put dataReturn(1, "操作成功", upimg, "", "")
				else
					ajaxReturn 0, file.Exception
				end if
			end if
			ajaxReturn 0, "操作失败"
		End If
		Set Upload = Nothing
    End Sub

    Public Sub ajax_getchilds()
		Dim rs, where : where = "1=1 AND status=1"
		If App.Req("pid")<>"" Then
			where = where & " AND pid="& App.ReqInt("pid",0)
		ElseIf App.Req("id")<>"" Then
			where = where & " AND pid="& App.ReqInt("id",0)
		End If
		If App.ReqInt("module_id",0)>0 Then where = where & " AND module_id="& App.ReqInt("module_id",0)
		If App.Req("typeid")<>"" Then where = where & " AND typeid="& App.ReqInt("typeid",0)
		Set rs = model().Field("id,name").Where(where).Fetch()
		If rs.RecordCount>0 Then
			App.Out.Put dataReturn(1, "操作成功", rs, "", "")
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

	'获取栏目模型id
    Public Sub ajax_getcatmodid()
		Dim module_id, id : id = App.ReqInt("id",0)
		module_id = model().Where("id="& id).getField("module_id")
		If module_id>0 Then
			App.Out.Put dataReturn(1, "操作成功", module_id, "", "")
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>