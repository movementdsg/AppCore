<%
Class adboardAction_Admin

	Private model
	Private tpl_list

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Set model = M("adboard")
		Set tpl_list = model.get_tpl_list()
		App.View.Assign "tpl_list", tpl_list
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=adboard&a=index
    Public Sub index()
		Dim rs, s_order
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加广告位"
		big_menu("iframe") = U("adboard/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 280
		App.View.Assign "big_menu", big_menu
		'-- e --
		s_order = "id desc"
		Set rs = model().Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=admin&c=adboard&a=add
    Public Sub add()
		Dim d
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If d.Exists("name") Then
				If model.name_exists(d("name"), 0) Then
					App.Out.Put dataReturn(0, "广告位名称已存在", "", "add", "")
				End If
			End If
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
    End Sub

	'View: index.asp?m=admin&c=adboard&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If d.Exists("name") Then
				If model.name_exists(d("name"), id) Then
					App.Out.Put dataReturn(0, "广告位名称已存在", "", "edit", "")
				End If
			End If
			If Err.Number=0 And model().Find(id).Set(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="name" Then
			If model.name_exists(val, id) Then
				ajaxReturn 0, "广告位名称重复"
			End If
		End If
		model().Find(id).setField field, val
		ajaxReturn 1, ""
    End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("name"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "广告位名称已经存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>