<%
Class Custom_labelAction_Admin

	Private model, model_sort

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("custom_label")
		Set model_sort = M("custom_labelsort")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=custom_label&a=index
    Public Sub index()
		Dim rs, rssort, s_order, s_where
		s_order = Admin_Base.GetOrderBy("custom_label")
		If App.Req("sortid")<>"" Then
			s_where = "sortid=" & IntVal(App.Req("sortid")) & ""
		End If
		Set rs = model().Order(s_order).Where(s_where).Order("[level] desc, id desc").Fetch()
		Set rssort = model_sort().Where("status=1").Order("id asc").Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Assign "rssort", rssort
		App.View.Show()
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=custom_label&a=addtag
    Public Sub addtag()
		On Error Resume Next
		Dim d, rssort, sortname, sortid
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("update_time") = Now()
			sortname = Trim(d("sortname"))
			If sortname<>"" Then
				If model_sort().Where("name='"& sortname & "'").Count() <= 0 Then
					model_sort().Add(Array("name:"& sortname, "status:1")) '新增分类
					sortid = model_sort().LastId()
					d("sortid") = sortid
				Else
					d("sortid") = IntVal(model_sort().Where("name='"& sortname & "'").getField("id"))
				End If
			Else
				d("sortid") = 0
			End If
			If Err.Number=0 And model().Add(d) Then
				winTip "操作成功", U(MODULE_NAME &"/index"), "", 1
			Else
				warnTip "操作失败", -1, "", ""
			End If
		Else
			Set rssort = model_sort().Where("status=1").Order("id asc").Fetch()
			App.View.Assign "rssort", rssort
			App.View.Show()
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=custom_label&a=edittag&id=1
    Public Sub edittag()
		Dim rs, d, id, rssort, sortname, sortid : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("update_time") = Now()
			sortname = Trim(d("sortname"))
			If sortname<>"" Then
				If model_sort().Where("name='"& sortname & "'").Count() <= 0 Then
					model_sort().Add(Array("name:"& sortname, "status:1")) '新增分类
					sortid = model_sort().LastId()
					d("sortid") = sortid
				Else
					d("sortid") = IntVal(model_sort().Where("name='"& sortname & "'").getField("id"))
				End If
			Else
				d("sortid") = 0
			End If
			If Err.Number=0 And model().Find(id).Update(d) Then
				winTip "操作成功", U(MODULE_NAME &"/index"), "", 1
			Else
				warnTip "操作失败", -1, "", ""
			End If
		Else
			Set rssort = model_sort().Where("status=1").Order("id asc").Fetch()
			App.View.Assign "rssort", rssort
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			App.View.Show()
		End If
    End Sub

    Public Sub deltag()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			winTip "操作成功", U(MODULE_NAME &"/index"), "", 1
		Else
			warnTip "操作失败", -1, "", ""
		End If
    End Sub

    Public Sub closetag()
		Dim id : id = App.Req("id")
		If model().Find(id).Update(Array("status:0")) Then
			winTip "操作成功", U(MODULE_NAME &"/index"), "", 1
		Else
			warnTip "操作失败", -1, "", ""
		End If
    End Sub

    Public Sub opentag()
		Dim id : id = App.Req("id")
		If model().Find(id).Update(Array("status:1")) Then
			winTip "操作成功", U(MODULE_NAME &"/index"), "", 1
		Else
			warnTip "操作失败", -1, "", ""
		End If
    End Sub

	'View: index.asp?m=admin&c=custom_label&a=sortlist
    Public Sub sortlist()
		Dim rs
		Set rs = model_sort().Where("status=1").Order("id asc").Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Show()
		App.Exit()
    End Sub

    Public Sub deltagsort()
		Dim id : id = App.Req("id")
		If model_sort().Delete(id) Then
			model().Where("sortid="& id).Update(Array("sortid:0"))
			winTip "操作成功", U(MODULE_NAME &"/sortlist"), "", 1
		Else
			warnTip "操作失败", -1, "", ""
		End If
    End Sub

	'Post: index.asp?m=admin&c=custom_label&a=addtagsort
    Public Sub addtagsort()
		On Error Resume Next
		Dim d, sortname
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Trim(d("name"))="" Then
				warnTip "分类名称不能为空", -1, "", ""
			End If
			d("status") = 1
			sortname = Trim(d("name"))
			If model_sort().Where("name='"& sortname & "'").Count() > 0 Then
				warnTip "该标签分类已存在！", -1, "", ""
			End If
			If Err.Number=0 And model_sort().Add(d) Then
				winTip "操作成功", U(MODULE_NAME &"/sortlist"), "", 1
			Else
				warnTip "操作失败", -1, "", ""
			End If
		End If
		On Error Goto 0
    End Sub

	'Post: index.asp?m=admin&c=custom_label&a=addtagsort
    Public Sub edittagsort()
		On Error Resume Next
		Dim rs, d, id, sortname : id = IntVal(App.Req("id"))
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			sortname = Trim(d("name"))
			If model_sort().Where("name='"& sortname & "' AND id<>"& id).Count() > 0 Then
				warnTip "该标签分类已存在！", -1, "", ""
			End If
			If Err.Number=0 And model_sort().Find(id).Update(d) Then
				winTip "操作成功", U(MODULE_NAME &"/sortlist"), "", 1
			Else
				warnTip "操作失败", -1, "", ""
			End If
		Else
			Set rs = model_sort().Find(id).Fetch()
			App.View.Assign "rs", rs
			App.View.Show()
		End If
		On Error Goto 0
    End Sub

End Class
%>