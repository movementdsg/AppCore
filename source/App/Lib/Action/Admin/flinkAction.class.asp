<%
Class FlinkAction_Admin

	Private model, cate_model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("flink")
		Set cate_model = M("flink_cate")
		AB.Use "A"
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=flink&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim search, rs, rs_cate, t, s_where, a_where, s_order : a_where = Array()
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = L("add_flink")
		big_menu("iframe") = U("flink/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 260
		App.View.Assign "big_menu", big_menu
		'-- e --
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(App.Req("keyword"))
			't = AB.E.URLDecode(t)
			s_where = " fk.name LIKE '%"& t &"%' "
			a_where = AB.A.Push(a_where, s_where)
		End If
		If IntVal(App.Req("cate_id"))>0 Then
			s_where = " fk.cate_id In(" & IntVal(App.Req("cate_id")) & ")"
			a_where = AB.A.Push(a_where, s_where)
        End If
		Set search = AB.C.Dict()
		search("cate_id") = IntVal(App.Req("cate_id"))
		search("keyword") = Trim(App.Req("keyword"))
		s_order = Admin_Base.GetOrderBy("flink fk")
		'联表查询(Join)：
		'Set rs = model().Alias("fk").Field("fk.*, cate.name as cate_name").Join("#@flink_cate cate").On("fk.cate_id = cate.id").Where(a_where).Order(s_order).Fetch()
		'用Relation方法代替：
		Set rs = model().A("fk").Field("fk.*, cate.name as cate_name").Relation( Array("flink_cate cate", "fk.cate_id = cate.id", true) ).Where(a_where).Order(s_order).Fetch()
		Set rs_cate = cate_model().Where("status=1").Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Assign "search", search
		App.View.Assign "rs_cate", rs_cate
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=flink&a=add
    Public Sub add()
		On Error Resume Next
		Dim rs_cate, d
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("cate_id") = IntVal(d("cate_id"))
			d("ordid") = IntVal(d("ordid"))
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			Set rs_cate = cate_model().Where("status=1").Fetch()
			App.View.Assign "rs_cate", rs_cate
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=flink&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("cate_id") = IntVal(d("cate_id"))
			d("ordid") = IntVal(d("ordid"))
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			Set rs_cate = cate_model().Where("status=1").Fetch()
			App.View.Assign "rs", rs
			App.View.Assign "rs_cate", rs_cate
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_upload_img()
		Dim Upload, upimg, file, savepath, result
		Set Upload = Admin_Base.Upload.Uploader
		If Upload.ErrorID>0 then
			ajaxReturn 0, Upload.Description
		Else
			savepath = AB.Pub.FixAbsPath(App.UploadPath) & "flink/" '上传目录
			Set file = upload.files("img")
			if not(file is nothing) then
				result = file.saveToFile(savepath,0,true)
				if result then
					'upimg = savepath & file.filename
					upimg = file.filename
					App.Out.Put dataReturn(1, "操作成功", upimg, "", "")
				else
					ajaxReturn 0, file.Exception
				end if
			end if
			ajaxReturn 0, "操作失败"
		End If
		Set Upload = Nothing
    End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("name"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "链接名称已经存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>