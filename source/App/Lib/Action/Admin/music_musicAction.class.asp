<%
Class Music_MusicAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("music_music")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=music_music&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, t, s_where, a_where, s_order : a_where = Array()
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(AB.E.URLDecode(App.Req("keyword")))
			s_where = " [song_name] LIKE '%"& t &"%' "
			's_where = s_where & " OR [email] LIKE '%"& t &"%' "
			a_where = AB.A.Push(a_where, s_where)
		End If
		s_order = Admin_Base.GetOrderBy("music_music")
		Set rs = model().Where(a_where).Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=music_music&a=artist_select&pid=0
    Public Sub artist_select()
		App.noCache() '强制不缓存
		Dim rs, t, s_where, a_where : a_where = Array()
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(AB.E.URLDecode(App.Req("keyword")))
			s_where = " [artist_name] LIKE '%"& t &"%' "
			's_where = s_where & " OR [email] LIKE '%"& t &"%' "
			a_where = AB.A.Push(a_where, s_where)
		End If
		Set rs = M("music_artist")().Where(a_where).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		DisplayTpl "artist_select.html", 1
    End Sub

	'View: index.asp?m=admin&c=music_music&a=artist_add
    Public Sub artist_add()
		On Error Resume Next
		If IS_POST Then '提交方式：POST
			'..
		Else
			DisplayTpl "artist_add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=music_music&a=add
    Public Sub add()
		On Error Resume Next
		Dim checkArr
		If IS_POST Then '提交方式：POST
			'..
		Else
			checkArr = Array()
			cate_tree = get_cate_tree( get_cate_data( M("music_cate"),0), checkArr)
			App.View.Assign "cate_tree", cate_tree
			App.View.Display("add.html")
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=music_music&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			App.View.Display("edit.html")
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

	Public Sub select_cate()
		Dim list, rs, id, pid, check_ids
		If IS_POST Then
			'App.Out.Put dataReturn(1, "操作成功", "", "", "")
		Else
			AB.Use "A"
			DisplayTpl "select_cate.html", 1
		End If
	End Sub

	Public Sub ajax_load_pid()
		Dim str, pid, name, arr : arr = Array()
		pid = Trim(App.Req("pid"))
		If pid<>"" Then
			AB.Use "A"
			For Each id In Split(pid,",")
				If IntVal(id)>0 Then
					name = cate_model().Where("pid<>"& topid).Find( IntVal(id) ).getField("name")
					If name<>"" Then arr = AB.A.Push(arr, name)
				End If
			Next
			str = Join(arr, ", ")
		End If
		App.Out.Put str
	End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>