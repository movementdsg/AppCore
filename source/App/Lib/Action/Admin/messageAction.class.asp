<%
Class MessageAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("message")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=message&a=index
    Public Sub index()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, pt, s_order
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "发送通知"
		big_menu("iframe") = U("message/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 320
		App.View.Assign "big_menu", big_menu
		'-- e --
		Set d = AB.C.Dict()
		If Trim(App.Req("time_start"))<>"" Then d("add_time>=") = CDate(Trim(App.Req("time_start")))
		If Trim(App.Req("time_end"))<>"" Then d("add_time<") = CDate(App.Req("time_end"))
		If Trim(App.Req("keyword"))<>"" Then d("keyword[%]") = "%"& Trim(App.Req("keyword")) &"%"
		If Trim(App.Req("from_name"))<>"" Then d("from_name[%]") = "%"& Trim(App.Req("from_name")) &"%"
		If Trim(App.Req("to_name"))<>"" Then d("to_name[%]") = "%"& Trim(App.Req("to_name")) &"%"
		If Trim(App.Req("type"))<>"" Then
			pt = IntVal(Trim(App.Req("type")))
			If pt=1 Then
				d("from_id") = 0
			ElseIf pt=2 Then
				d("from_id>") = 0
			End If
		End If
		s_order = Admin_Base.GetOrderBy("message")
		Set rs = model().Where(d).Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=message&a=add
    Public Sub add()
		On Error Resume Next
		Dim Rs, d, tpl_list
		If IS_POST Then '提交方式：POST
			If Trim(AB.Form("to_name"))="" Then App.Out.Put dataReturn(0, "接收人必选填写", "", "add", "")
			Set d = Admin_Base.dictForm()
			If model().Add(d) And False Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
            Set tpl_list = M("message_tpl")().Field("id,alias,name").where("type='msg' And is_sys=0").Fetch()
			App.View.Assign "tpl_list", tpl_list
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

End Class
%>