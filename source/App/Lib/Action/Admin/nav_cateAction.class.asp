<%
Class Nav_CateAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("nav_cate")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=nav_cate&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, s_order
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加分类"
		big_menu("iframe") = U("nav_cate/add")
		big_menu("id") = "add"
		big_menu("width") = 420
		big_menu("height") = 90
		App.View.Assign "big_menu", big_menu
		'-- e --
		's_order = Admin_Base.GetOrderBy("nav_cate")
		s_order = "ordid asc, id asc"
		Set rs = model().Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=nav_cate&a=add
    Public Sub add()
		On Error Resume Next
		Dim d
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("ordid") = 0
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=nav_cate&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="name" Then
			If model.name_exists(val, id) Then
				ajaxReturn 0, "英文名称重复"
			End If
		End If
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("name"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "英文名称已经存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>