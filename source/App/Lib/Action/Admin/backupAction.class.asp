<%
Class BackupAction_Admin

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=backup&a=index
    Public Sub index()
		App.noCache()
		AB.Use "E" : AB.E.Use "Md5"
		App.View.Assign "dbtype", AB.DT("dbtype")
		App.View.Assign "accdb", AB.DT("accdb")
		App.View.Assign "SitePath", App.RootPath & "/"
		App.View.Assign "BakFolder", "backup"
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=admin&c=backup&a=restore
    Public Sub restore()
		R("index") '跳转
    End Sub

End Class
%>