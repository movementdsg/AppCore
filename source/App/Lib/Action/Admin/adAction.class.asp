<%
Class adAction_Admin

	Private model, adboard_model
	Private ad_type, img_dir

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Set model = M("ad")
		Set adboard_model = M("adboard")
		Set ad_type = AB.C.Dict()
		ad_type("image") = "图片" : ad_type("code") = "代码" : ad_type("flash") = "Flash" : ad_type("text") = "文字"
		App.View.Assign "ad_type_arr", ad_type
		img_dir = App.UploadPath & MODULE_NAME & "/"
		App.View.Assign "img_dir", img_dir
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=ad&a=index
    Public Sub index()
		Dim rs, res, s_order, d_where, search : Set d_where = AB.C.Dict()
		Dim board_list
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加广告"
		big_menu("iframe") = U("ad/add")
		big_menu("id") = "add"
		big_menu("width") = 550
		big_menu("height") = 410
		App.View.Assign "big_menu", big_menu
		'-- e --
		Set res = adboard_model().Field("id,name").Order("id desc").Fetch()
        Set board_list = AB.C.Dict()
		Do While Not res.Eof
			board_list(res("id").Value) = res("name").Value
			res.MoveNext
		Loop
		res.Close() : Set res = Nothing
		'--条件
		start_time_min = Trim(App.Req("start_time_min"))
		start_time_max = Trim(App.Req("start_time_max"))
		end_time_min = Trim(App.Req("end_time_min"))
		end_time_max = Trim(App.Req("end_time_max"))
		board_id = IntVal(App.Req("board_id"))
		style = Trim(App.Req("style"))
		keyword = Trim(App.Req("keyword"))
		' If start_time_min<>"" Then d_where("start_time>=") = "#"& CDate(start_time_min) & "#"
		' If start_time_max<>"" Then d_where("start_time<=") = "#"& CDate(start_time_max) & "#"
		' If end_time_min<>"" Then d_where("end_time>=") = "#"& CDate(end_time_min) & "#"
		' If end_time_max<>"" Then d_where("end_time<=") = "#"& CDate(end_time_max) & "#"
		If start_time_min<>"" Then d_where("+1") = "( (start_time >= #"& CDate(start_time_min) & "#) OR (start_time IS NULL) )"
		If start_time_max<>"" Then d_where("+2") = "( (start_time <= #"& CDate(start_time_max) & "#) OR (start_time IS NULL) )"
		If end_time_min<>"" Then d_where("+3") = "( (end_time >= #"& CDate(end_time_min) & "#) OR (end_time IS NULL) )"
		If end_time_max<>"" Then d_where("+4") = "( (end_time <= #"& CDate(end_time_max) & "#) OR (end_time IS NULL) )"
		If board_id>0 Then d_where("board_id") = board_id
		If style<>"" Then d_where("type") = style
		If keyword<>"" Then d_where("name[%]") = "%"& keyword & "%"
		Set search = AB.C.Dict()
		search("start_time_min") = start_time_min
		search("start_time_max") = start_time_max
		search("end_time_min") = end_time_min
		search("end_time_max") = end_time_max
		search("board_id") = board_id
		search("style") = style
		search("keyword") = keyword
		s_order = Admin_Base.GetOrderBy("ad")
		's_order = "ordid asc, id desc"
		Set rs = model().Where(d_where).Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Assign "board_list", board_list
		App.View.Assign "search", search
		App.View.Display("index.html")
    End Sub

	'View: index.asp?m=admin&c=ad&a=add
    Public Sub add()
		Dim d, s_type, content, img : s_type = App.Req("type")
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Not AB.C.isNul(d("start_time")) And Not AB.C.isNul(d("end_time")) Then
				If CDate(d("start_time")) >= CDate(d("end_time")) Then
					App.Out.Put dataReturn(0, "开始时间必须小于结束时间", "", "add", "")
				End If
			End If
			If d.Exists("start_time") And AB.C.isNul(d("start_time")) Then d("start_time") = Null
			If d.Exists("end_time") And AB.C.isNul(d("end_time")) Then d("end_time") = Null
			Select Case s_type
				Case "text" : content = Trim(App.Req("text"))
				Case "image" : content = Trim(App.Req("img"))
				Case "code" : content = Trim(App.Req("code"))
				Case "flash" : content = Trim(App.Req("flash"))
				Case Else : App.Out.Put dataReturn(0, "非法广告类型", "", "add", "")
			End Select
			d("content") = content
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "add", "")
			End If
		Else
			Dim sid, i, res, f, adboard_types, adboards, tpl, allow_type : i = 0
			Set res = adboard_model().Where("status=1").Order("id desc").Fetch()
			Set adboard_types = adboard_model.get_tpl_list()
			Set adboards = AB.C.Dict()
			Do While Not res.Eof
				sid = res("id").Value
				tpl = res("tpl").Value
				allow_type = adboard_types(tpl)("allow_type")
				If IsArray(allow_type) Then allow_type = Join(allow_type, "|")
				Set adboards(sid) = AB.C.Dict()
				For i = 0 To res.Fields.Count-1
					f = res.Fields(i).Name
					adboards(sid).Add f, res(f).Value
				Next
				adboards(sid)("allow_type") = allow_type
				res.MoveNext
			Loop
			App.View.Assign "adboards", adboards
			DisplayTpl "add.html", 1
		End If
    End Sub

	'View: index.asp?m=admin&c=ad&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		Dim board_id, board_info
		Dim s_type, content, img : s_type = App.Req("type")
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Not AB.C.isNul(d("start_time")) And Not AB.C.isNul(d("end_time")) Then
				If CDate(d("start_time")) >= CDate(d("end_time")) Then
					App.Out.Put dataReturn(0, "开始时间必须小于结束时间", "", "edit", "")
				End If
			End If
			If d.Exists("start_time") And AB.C.isNul(d("start_time")) Then d("start_time") = Null
			If d.Exists("end_time") And AB.C.isNul(d("end_time")) Then d("end_time") = Null
			Select Case s_type
				Case "text" : content = Trim(App.Req("text"))
				Case "image" : content = Trim(App.Req("img"))
				Case "code" : content = Trim(App.Req("code"))
				Case "flash" : content = Trim(App.Req("flash"))
				Case Else : App.Out.Put dataReturn(0, "非法广告类型", "", "edit", "")
			End Select
			d("content") = content
			If Err.Number=0 And model().Find(id).Set(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			board_id = IntVal( model().Where("id="& id).getField("board_id") )
			Set board_info = adboard_model().Field("name,width,height").Where("id="& board_id).Fetch()
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			App.View.Assign "board_info", board_info
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		model().Find(id).setField field, val
		ajaxReturn 1, ""
    End Sub

    Public Sub ajax_upload_img()
		Dim Upload, upimg, file, savepath, rndDir, result, s_type
		Set Upload = Admin_Base.Upload.Uploader
		If Upload.ErrorID>0 then
			ajaxReturn 0, Upload.Description
		Else
			s_type = App.Req("type")
			If AB.C.isNul(s_type) Then s_type = "img"
			rndDir = AB.C.DateTime(Now(),"yymm/dd")
			savepath = img_dir & rndDir '上传到临时目录
			Set file = upload.files(s_type)
			if not(file is nothing) then
				result = file.saveToFile(savepath,0,true)
				if result then
					upimg = file.filename
					App.Out.Put dataReturn(1, "操作成功", rndDir & "/" & upimg, "", "")
				else
					ajaxReturn 0, file.Exception
				end if
			end if
			ajaxReturn 0, "操作失败"
		End If
		Set Upload = Nothing
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>