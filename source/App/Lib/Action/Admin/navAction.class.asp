<%
Class NavAction_Admin

	Private model, model_cate

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("nav")
		Set model_cate = M("nav_cate")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=nav&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加导航"
		big_menu("iframe") = U("nav/add")
		big_menu("id") = "add"
		big_menu("width") = 550
		big_menu("height") = 350
		App.View.Assign "big_menu", big_menu
		'-- e --
		Dim cat, e, str
		Set cat = model.get_tree(0,0)
		str = ""
		For Each e In cat
			str = str & "<tr>" & VbCrlf
			str = str & "<td align='center'><input type='checkbox' value='"& cat(e)("id") &"' class='J_checkitem'></td>" & VbCrlf
			str = str & "<td align='center'>"& cat(e)("id") &"</td>" & VbCrlf
			str = str & "<td>"& cat(e)("tree_spacer") &"<span data-tdtype='edit' data-field='name' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("name") &"</span></td>" & VbCrlf
			str = str & "<td align='center'><span data-tdtype='edit' data-field='alias' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("alias") &"</span></td>" & VbCrlf
			str = str & "<td align='center'>"& model_cate().Where("name='"&cat(e)("cate")&"'").getField("title") &"</td>" & VbCrlf
			str = str & "<td align='center'><span data-tdtype='edit' data-field='link' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("link") &"</span></td>" & VbCrlf
			str = str & "<td align='center'><span data-tdtype='edit' data-field='ordid' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("ordid") &"</span></td>" & VbCrlf
			str = str & "<td align='center'><img data-tdtype='toggle' data-id='"& cat(e)("id") &"' data-field='target' data-value='"& cat(e)("target") &"' "
			str = str & " src='"& App.StaticPath &"/images/admin/toggle_"& AB.C.IIF(cat(e)("target")=0,"disabled","enabled") &".gif' /></td>" & VbCrlf
			str = str & "<td align='center'><img data-tdtype='toggle' data-id='"& cat(e)("id") &"' data-field='status' data-value='"& cat(e)("status") &"' "
			str = str & " src='"& App.StaticPath &"/images/admin/toggle_"& AB.C.IIF(cat(e)("status")=0,"disabled","enabled") &".gif' /></td>" & VbCrlf
			str = str & "<td align='center'><img data-tdtype='toggle' data-id='"& cat(e)("id") &"' data-field='istop' data-value='"& cat(e)("istop") &"' "
			str = str & " src='"& App.StaticPath &"/images/admin/toggle_"& AB.C.IIF(cat(e)("istop")=0,"disabled","enabled") &".gif' /></td>" & VbCrlf
			str = str & "<td align='center'><a href='javascript:;' class='J_showdialog' "
			str = str & "	data-uri='"& U(MODULE_NAME & "/add?pid=" & cat(e)("id") ) &"' "
			str = str & "	data-title='添加子导航' "
			str = str & "	data-id='add' data-width='550' data-height='350'>添加子导航</a> |"
			str = str & "	<a href='javascript:;' class='J_showdialog' "
			str = str & "	data-uri='"& U(MODULE_NAME & "/edit?id=" & cat(e)("id") ) &"' "
			str = str & "	data-title='编辑导航 - "& cat(e)("name") &"' "
			str = str & "	data-id='edit' data-width='550' data-height='350'>编辑</a> |"
			str = str & "	<a href='javascript:;' class='J_confirmurl' data-acttype='ajax' "
			str = str & "	data-uri='"& U(MODULE_NAME & "/delete?id=" & cat(e)("id") ) &"' "
			str = str & "	data-msg='确定要删除导航“<font color=blue>"& cat(e)("name") &"”</font>吗？<br />该导航的所有子级导航将会被删除！'>删除</a></td>"
			str = str & "</tr>" & VbCrlf & VbCrlf
		Next
		If Trim(str)="" Then str = "<tr style='height:30px;'> <td align='left' colspan='15'>&nbsp;&nbsp; <font color=red>暂无数据！</font></td> </tr>"
		App.View.Assign "nav_list", str
		App.View.Assign "module_name", MODULE_NAME
		App.View.Assign "action_name", ACTION_NAME
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=nav&a=add&pid=1
    Public Sub add()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, pid, cat, e, str, selected, color, cate, rst
		pid = IntVal(App.Req("pid")) : cate = LCase(Trim(App.Req("type")))
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "添加成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			str = ""
			Set rst = model_cate().Where("status=1").Order("ordid asc, id asc").Fetch()
			Do While Not rst.Eof
				str = str & "<option value='"& rst("name") &"'>"& rst("title") &"</option>" & VbCrlf
				rst.MoveNext
			Loop
			App.View.Assign "select_cates", str
			str = ""
			If pid>0 Then
				If cate="" Then cate = model().Find(pid).getField("cate")
				Set cat = model.get_tree2(cate,0,0)
				For Each e In cat
					selected = AB.C.IIF(cat(e)("id")=pid, "selected", "")
					str = str & "<option value='"& cat(e)("id") &"' "& selected &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
				Next
			End If
			App.View.Assign "select_navs", str
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=nav&a=edit&id=1
    Public Sub edit()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, id, pid, spid_arr, cat, e, str, selected, color, s_type, rst, cate : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			Set rs = model().Find(id).Fetch()
			pid = IntVal(App.Req("pid"))
			'If pid = id Then App.Out.Put dataReturn(0, "操作失败, 上级导航不能是自己", "", "edit", "")
			spid_arr = model.get_child_ids(id,true)
			AB.Use "A"
			If AB.A.InArray(pid, spid_arr) Then
				App.Out.Put dataReturn(0, "不能移动到本导航或者子导航下面", "", "edit", "")
			End If
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			If (Rs.Bof And Rs.Eof) Then App.Out.Put toJson("error! no data id("& id &").",1,"")
			pid = rs("pid").Value
			cate = rs("cate").Value
			App.View.Assign "rs", rs
			str = ""
			Set rst = model_cate().Where("status=1").Order("ordid asc, id asc").Fetch()
			Do While Not rst.Eof
				selected = AB.C.IIF(rst("name")=cate, "selected", "")
				color = AB.C.IIF(rst("name")=cate, " style='background:#92C2D9'", "")
				str = str & "<option value='"& rst("name") &"' "& selected & color &">"& rst("title") &"</option>" & VbCrlf
				rst.MoveNext
			Loop
			App.View.Assign "select_cates", str
			str = ""
			Set cat = model.get_tree2(cate,0,0)
			For Each e In cat
				selected = AB.C.IIF(cat(e)("id")=pid, "selected", "")
				color = AB.C.IIF(cat(e)("id")=pid, " style='background:#92C2D9'", "")
				str = str & "<option value='"& cat(e)("id") &"' "& selected & color &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
			Next
			App.View.Assign "select_navs", str
			DisplayTpl "edit.html", 1
		End If
		On Error Goto 0
    End Sub

    Public Sub delete()
		App.noCache() '强制不缓存
		Dim id, a, t, rs, ids : id = App.Req("id")
		If CStr(id)<>"0" And CStr(id)<>"" Then '包含子导航
			AB.Use "A" : a = Array()
			' t = AB.A.Fetch(id,",")
			' For i=0 To UBound(t) '多个id(批量)
				' id = t(i)
				' a = AB.A.Push(a, IntVal(id))
				' Set rs = model().Find("pid in("& id &")").Fetch()
				' Do While Not rs.Eof
					' a = AB.A.Push(a, rs("id").Value)
					' rs.MoveNext
				' Loop
			' Next
			' id = Join(a, ",")
			t = AB.A.Fetch(id,",")
			For i=0 To UBound(t) '多个id(批量)
				id = t(i)
				ids = model.get_child_ids(id,true)
				a = AB.A.Merge(a, ids)
			Next
			a = AB.A.Unique(a) '移除相同元素
			id = Join(a, ",")
		End If
		If Trim(id)="" Then ajaxReturn 0, "操作失败"
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub ajax_load_pids()
		App.noCache() '强制不缓存
		Dim cat, e, str : cate = Trim(App.Req("cate"))
		str = "<option value=""0"">作为一级导航</option>"
		If cate<>"" Then
			Set cat = model.get_tree2(cate,0,0)
			For Each e In cat
				str = str & "<option value='"& cat(e)("id") &"'>"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
			Next
		End If
		App.Out.Put str
    End Sub

    Public Sub ajax_get_cate()
		App.noCache() '强制不缓存
		Dim cat, e, str : id = IntVal(App.Req("id")) : str = ""
		If id>0 Then str = model().Find(id).getField("cate")
		App.Out.Put str
    End Sub

    Public Sub [empty]()
		App.Out.Print "Nav Empty Action"
    End Sub

End Class
%>