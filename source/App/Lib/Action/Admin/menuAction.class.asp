<%
Class MenuAction_Admin

	Private model, s_cache_menu

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		s_cache_menu = "admin_menu_cat"
		Response.Charset = App.CharSet
		Set model = M("menu")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=menu&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加菜单"
		big_menu("iframe") = U("menu/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 350
		App.View.Assign "big_menu", big_menu
		'-- e --
		Dim cat, e, str
		'--启用缓存
		If Not AB.C.IsNul(AB.Session(s_cache_menu)) Then
			str = AB.Session(s_cache_menu) & ""
		Else
			Set cat = model.get_tree(0,0)
			str = ""
			For Each e In cat
				str = str & "<tr>" & VbCrlf
				str = str & "<td align='center'><input type='checkbox' value='"& cat(e)("id") &"' class='J_checkitem'></td>" & VbCrlf
				str = str & "<td align='center'>"& cat(e)("id") &"</td>" & VbCrlf
				str = str & "<td>"& cat(e)("tree_spacer") &"<span data-tdtype='edit' data-field='name' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("name") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='module_name' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("module_name") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='action_name' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("action_name") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='data' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("data") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='ordid' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("ordid") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><img data-tdtype='toggle' data-id='"& cat(e)("id") &"' data-field='display' data-value='"& cat(e)("display") &"' "
				str = str & " src='"& App.StaticPath &"/images/admin/toggle_"& AB.C.IIF(cat(e)("display")=0,"disabled","enabled") &".gif' /></td>" & VbCrlf
				str = str & "<td align='center'><a href='javascript:;' class='J_showdialog' "
				str = str & "	data-uri='"& U(MODULE_NAME & "/add?pid=" & cat(e)("id") ) &"' "
				str = str & "	data-title='添加子菜单' "
				str = str & "	data-id='add' data-width='500' data-height='350'>添加子菜单</a> |"
				str = str & "	<a href='javascript:;' class='J_showdialog' "
				str = str & "	data-uri='"& U(MODULE_NAME & "/edit?id=" & cat(e)("id") ) &"' "
				str = str & "	data-title='编辑 - "& cat(e)("name") &"' "
				str = str & "	data-id='edit' data-width='500' data-height='350'>编辑</a> |"
				str = str & "	<a href='javascript:;' class='J_confirmurl' data-acttype='ajax' "
				str = str & "	data-uri='"& U(MODULE_NAME & "/delete?id=" & cat(e)("id") ) &"' "
				str = str & "	data-msg='确定要删除“<font color=blue>"& cat(e)("name") &"</font>”吗？'>删除</a></td>"
				str = str & "</tr>" & VbCrlf & VbCrlf
			Next
			If Trim(str)="" Then str = "<tr style='height:30px;'> <td align='left' colspan='15'>&nbsp;&nbsp; <font color=red>暂无数据！</font></td> </tr>"
			DropCache() '删除缓存
			AB.Session.Set s_cache_menu, str
		End If
		App.View.Assign "menu_list", str
		App.View.Assign "module_name", MODULE_NAME
		App.View.Assign "action_name", ACTION_NAME
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=menu&a=add&pid=1
    Public Sub add()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, pid, cat, e, str, selected, color : pid = IntVal(App.Req("pid")) : str = ""
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			DropCache() '删除缓存
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "添加成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			Set cat = model.get_tree2(0,0)
			For Each e In cat
				selected = AB.C.IIF(cat(e)("id")=pid, "selected", "")
				str = str & "<option value='"& cat(e)("id") &"' "& selected &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
			Next
			App.View.Assign "select_menus", str
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=menu&a=edit&id=1
    Public Sub edit()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, id, pid, spid_arr, cat, e, str, selected, color : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			Set rs = model().Find(id).Fetch()
			pid = IntVal(App.Req("pid"))
			'If pid = id Then App.Out.Put dataReturn(0, "操作失败, 上级菜单不能是自己", "", "edit", "")
			spid_arr = model.get_child_ids(id,true)
			AB.Use "A"
			If AB.A.InArray(pid, spid_arr) Then
				App.Out.Put dataReturn(0, "不能移动到本菜单或者子菜单下面", "", "edit", "")
			End If
			DropCache() '删除缓存
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			If (Rs.Bof And Rs.Eof) Then App.Out.Put toJson("error! no data id("& id &").",1,"")
			pid = rs("pid").Value
			App.View.Assign "rs", rs
			Set cat = model.get_tree2(0,0)
			str = ""
			For Each e In cat
				selected = AB.C.IIF(cat(e)("id")=pid, "selected", "")
				color = AB.C.IIF(cat(e)("id")=pid, " style='background:#92C2D9'", "")
				str = str & "<option value='"& cat(e)("id") &"' "& selected & color &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
			Next
			App.View.Assign "select_menus", str
			DisplayTpl "edit.html", 1
		End If
		On Error Goto 0
    End Sub

    Public Sub delete()
		App.noCache() '强制不缓存
		Dim id, a, t, rs, ids : id = App.Req("id")
		DropCache() '删除缓存
		If CStr(id)<>"0" And CStr(id)<>"" Then '包含子菜单
			AB.Use "A" : a = Array()
			' t = AB.A.Fetch(id,",")
			' For i=0 To UBound(t) '多个id(批量)
				' id = t(i)
				' a = AB.A.Push(a, IntVal(id))
				' Set rs = model().Find("pid in("& id &")").Fetch()
				' Do While Not rs.Eof
					' a = AB.A.Push(a, rs("id").Value)
					' rs.MoveNext
				' Loop
			' Next
			' id = Join(a, ",")
			t = AB.A.Fetch(id,",")
			For i=0 To UBound(t) '多个id(批量)
				id = t(i)
				ids = model.get_child_ids(id,true)
				a = AB.A.Merge(a, ids)
			Next
			a = AB.A.Unique(a) '移除相同元素
			id = Join(a, ",")
		End If
		If Trim(id)="" Then ajaxReturn 0, "操作失败"
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		DropCache() '删除缓存
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

	Private Function DropCache() '删除缓存
		On Error Resume Next
		AB.Session.Remove s_cache_menu
		On Error Goto 0
	End Function

    Public Sub [empty]()
		App.Out.Print "Menu Empty Action"
    End Sub

End Class
%>