<%
Class UserAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("user")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
	End Sub

	'View: index.asp?m=admin&c=user&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, t, s_where, a_where, s_order : a_where = Array()
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加会员"
		big_menu("iframe") = U("user/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 330
		App.View.Assign "big_menu", big_menu
		'-- e --
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(AB.E.URLDecode(App.Req("keyword")))
			s_where = " [username] LIKE '%"& t &"%' "
			's_where = s_where & " OR [email] LIKE '%"& t &"%' "
			a_where = AB.A.Push(a_where, s_where)
		End If
		s_order = Admin_Base.GetOrderBy("user")
		Set rs = model().Where(a_where).Order(s_order).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=user&a=add
    Public Sub add()
		On Error Resume Next
		Dim d, img, id, avatar_dir, avatar_img, temp_file
		If REQUEST_METHOD = "POST" Then '提交方式：POST
			If Trim(AB.Form("password"))="" Then App.Out.Put dataReturn(0, "密码不能为空", "", "add", "")
			Set d = model.dictForm()
			If d.Exists("username") Then
				If model.name_exists(d("username"), 0) Then
					App.Out.Put dataReturn(0, "操作失败，用户名重复", "", "add", "")
				End If
			ElseIf d.Exists("email") Then
				If model.email_exists(d("email"), 0) Then
					App.Out.Put dataReturn(0, "操作失败，邮箱重复", "", "add", "")
				End If
			End If
			d("reg_ip") = AB.C.GetIP() '注册IP
			d("reg_time") = Now() '注册时间
			If model().Add(d) Then
				id = model().LastId '获取插入ID
				img = Trim(d("img"))
				If img<>"" Then
					avatar_dir = avatarDir(id) '头像目录
					avatar_img = avatar_dir & AB.E.md5.To16(id & Now()) &".jpg" '头像地址
					If Instr(img,"/")<=0 Then temp_file = App.UploadPath & "avatar/temp/" & img
					If AB.C.isFile(temp_file) Then '移动临时图片
						AB.Use "Fso"
						If AB.Fso.isExists(avatar_img) Then AB.Fso.DelFile avatar_img
						If AB.Fso.MoveFile(temp_file, avatar_img) Then
							img = avatar_img
							'生成各种规格的缩略图
							'规格：24*24, 32*32, 48*48, 64*64, 100*100, 200*200
						End If
					End If
					d.RemoveAll()
					d("avatar") = img
					model().Find(id).Update(d)
				End If
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=user&a=add_users
	Public Sub add_users()
		On Error Resume Next
		Dim users, data, name, password, gender, reg_time, reg_ip
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加会员"
		big_menu("iframe") = U("user/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 330
		App.View.Assign "big_menu", big_menu
		'-- e --
		If IS_POST Then '提交方式：POST
            users = Split(Trim(App.Req("username")), ",")
            password = Trim(App.Req("password"))
            gender = CInt(App.Req("gender"))
            reg_ip = AB.C.GetIP() '注册IP
            reg_time = Now() '注册时间
			If Trim(AB.Form("password"))="" Then warnTip "密码不能为空", -1, "", 1
			For Each name In users
				If Trim(name)<>"" And Not model.name_exists(name, 0) Then
					Set data = AB.C.Dict()
					data("username") = Trim(name)
					data("password") = AB.E.Md5.To16(password)
					data("gender") = gender
					If gender=3 Then data("gender") = AB.C.Rand(0,1)
					data("reg_time") = reg_time
					data("reg_ip") = reg_ip
					model().Add(data)
				End If
            Next
			winTip "操作成功", U("user/index"), "", 1
		Else
			App.View.Display("add_users.html")
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=user&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id, avatar_dir, avatar_img, temp_file : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = model.dictForm()
			If d.Exists("username") Then
				If model.name_exists(d("username"), id) Then
					App.Out.Put dataReturn(0, "操作失败，用户名重复", "", "edit", "")
				End If
			ElseIf d.Exists("email") Then
				If model.email_exists(d("email"), id) Then
					App.Out.Put dataReturn(0, "操作失败，邮箱重复", "", "edit", "")
				End If
			End If
			If Trim(d("img"))<>"" Then
				d("img") = Trim(d("img"))
				avatar_dir = avatarDir(id) '头像目录
				avatar_img = avatar_dir & AB.E.md5.To16(id & Now()) &".jpg" '头像地址
				If Instr(d("img"),"/")<=0 Then temp_file = App.UploadPath & "avatar/temp/" & d("img")
				If AB.C.isFile(temp_file) Then '移动临时图片
					AB.Use "Fso"
					If AB.Fso.isExists(avatar_img) Then AB.Fso.DelFile avatar_img
					If AB.Fso.MoveFile(temp_file, avatar_img) Then
						d("img") = avatar_img
						'生成各种规格的缩略图
						'规格：24*24, 32*32, 48*48, 64*64, 100*100, 200*200
					End If
				End If
				d("avatar") = d("img")
			End If
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="username" Then
			If model.name_exists(val, id) Then
				ajaxReturn 0, "用户名重复"
			End If
		ElseIf field="email" Then
			If model.email_exists(val, id) Then
				ajaxReturn 0, "邮箱重复"
			End If
		End If
		model().Find(id).setField field, val
		ajaxReturn 1, ""
    End Sub

    Public Sub ajax_upload_imgs()
		Dim Upload, upimg, file, savepath, rndDir, result
		Set Upload = Admin_Base.Upload.Uploader
		If Upload.ErrorID>0 then
			ajaxReturn 0, Upload.Description
		Else
			savepath = App.UploadPath & "avatar/temp/" '上传到临时目录
			Set file = upload.files("img")
			if not(file is nothing) then
				result = file.saveToFile(savepath,0,true)
				if result then
					upimg = file.filename
					App.Out.Put dataReturn(1, "操作成功", upimg, "", "")
				else
					ajaxReturn 0, file.Exception
				end if
			end if
			ajaxReturn 0, "操作失败"
		End If
		Set Upload = Nothing
    End Sub

    Public Sub ajax_check_name()
		Dim username, id
		username = Trim(App.Req("username"))
		id = Trim(App.Req("id"))
		If model.name_exists(username, id) Then
			ajaxReturn 0, "该会员已存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

    Public Sub ajax_check_email()
		Dim email, id
		email = Trim(App.Req("email"))
		id = Trim(App.Req("id"))
		If model.email_exists(email, id) Then
			ajaxReturn 0, "该邮箱已存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>