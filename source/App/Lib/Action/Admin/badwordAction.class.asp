<%
Class BadwordAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("badword")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=badword&a=index
    Public Sub index()
		Dim rs, s_order, s_where, a_where, search : a_where = Array()
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加敏感词"
		big_menu("iframe") = U("badword/add")
		big_menu("id") = "add"
		big_menu("width") = 500
		big_menu("height") = 130
		App.View.Assign "big_menu", big_menu
		'-- e --
		s_order = Admin_Base.GetOrderBy("badword")
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(App.Req("keyword"))
			't = AB.E.URLDecode(t)
			s_where = " badword LIKE '%"& t &"%' "
			a_where = AB.A.Push(a_where, s_where)
		End If
		If IntVal(App.Req("word_type"))>0 Then
			s_where = " word_type In(" & IntVal(App.Req("word_type")) & ")"
			a_where = AB.A.Push(a_where, s_where)
        End If
		Set search = AB.C.Dict()
		search("keyword") = Trim(App.Req("keyword"))
		search("word_type") = App.Req("word_type")
		Set rs = model().Order(s_order).Where(a_where).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		App.View.Assign "search", search
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=badword&a=add
    Public Sub add()
		On Error Resume Next
		Dim d
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("add_time") = Now()
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=badword&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("word_type") = IntVal(d("word_type"))
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="badword" Then
			If model.name_exists(val, id) Then
				ajaxReturn 0, "敏感词重复"
			End If
		End If
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_check_name()
		Dim name, id
		name = Trim(App.Req("badword"))
		id = Trim(App.Req("id"))
		If model.name_exists(name, id) Then
			ajaxReturn 0, "该敏感词已存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

End Class
%>