<%
Class SqlAction_Admin

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		AB.Use "Form"
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=sql&a=index
    Public Sub index()
		App.noCache()
		Dim sql : sql = Trim(AB.Form("sql"))
		sql = Trim(AB.D.NsSpace(Replace(sql,VBCrlf," ")))
		sql = AB.C.RP(sql, Array("\\","\#","\@"), Array(Chr(21),Chr(22),Chr(23)) )
		sql = AB.C.RP(sql, "#@", App.Dao.tbPrefix)
		sql = AB.C.RP(sql, Array(Chr(21),Chr(22),Chr(23)), Array("\","#","@") )
		App.View.Assign "sql", sql
		App.View.Display("index.html")
    End Sub

End Class
%>