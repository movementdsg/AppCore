<%
Class seoAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("seo")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=seo&a=page
    Public Sub page()
		On Error Resume Next
		Dim d, k, o, str
		If App.Req("isok")=1 Then
			winTip "更新成功", U("seo/page"), "", ""
		End If
		If IS_POST Then '提交方式：POST
			App.Out.Flush()
			msgTip "正在更新中，请稍后.."
			App.Out.Flush()
			AB.Use "json"
			Set d = model.dictForm()
			If d.Exists("__hash__") Then d.Remove("__hash__")
			If d.Exists("dosubmit") Then d.Remove("dosubmit")
			If d.Exists("menuid") Then d.Remove("menuid")
			For Each k In d
				If IsObject(d(k)) Then
					str = AB.Json.toJson( d(k) )
				Else
					str = d(k)
				End If
				If model().Where("name='"& k &"'").Count()>0 Then
					Set o = AB.C.Dict()
					o("data") = str
					model().Where("name='"& k &"'").Update(o)
				Else
					Set o = AB.C.Dict()
					o("name") = k
					o("data") = str
					model().Add(o)
				End If
			Next
			SeoCfgDataRefresh() '刷新SeoCfg数据
			'winTip "更新成功", U("seo/page"), "", ""
			App.Out.Echo "<script type='text/javascript'>location='"& U("seo/page?isok=1") &"';</script>"
			App.Out.Flush()
		Else
			App.View.Display("page.html")
			App.Exit()
		End If
		On Error Goto 0
    End Sub

End Class
%>