<%
'公用核心
Class BaseAction_Admin

	Public login_url, Upload

	Private Sub Class_Initialize()
		On Error Resume Next
		AB.Use "Session" : AB.Session.Mark = "appss_" : AB.Session.Timeout=500
		AB.Use "Cookie" : AB.Cookie.Mark = "appcki_" : AB.Cookie.Timeout=60*24 '改用Cookie登陆
		AB.Use "E" : AB.E.Use "Md5"
		AB.Use "Form"
		login_url = U("index/login") '登陆地址
		'-- 视图设置
		App.View.Locate = True
		'App.View.LayerOut = False
		'App.View.Theme = "default"
		App.View.LayerFile = "layout.html" '设置layerout模板
		'-- 分页设置
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Pager.Format = "{first} {prev} {list} {next} {last}"
		App.View.Pager.addStyle "first{display:auto;class:first;text:第一页;}"
		App.View.Pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:&lt\;上一页;}"
		App.View.Pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页&gt\;;}"
		App.View.Pager.addStyle "last{display:auto;class:last;dis-tag:span;text:最后一页;}"
		App.View.Pager.addStyle "list{display:show;curr-tag:label;curr-class:current;link-text:'&nbsp\;$n&nbsp\;';link-class:'';link-space:' ';dot-val:'..';dot-for:txt#xy;index:2|2;min:5;}"
		'-- e --
		'-- 公用变量
		AB.Use "Form"
		If IS_POST And AB.Form.FormType="file" Then
			Set Upload = App.Helper("Upload")
			Upload.Way = "up.an" '使用艾恩无组件
			Upload.SingleSize = 10*1024*1024 '10M, 单个文件最大上传限制
			Upload.MaxSize = 10*1024*1024 '10M, 最大上传限制
			Upload.Allowed = "jpg|jpeg|gif|png" '合法扩展名,不限制则为"*"
			Upload.Start()
		End If
		App.View.Assign "menuid", IntVal(Trim(App.Req("menuid")))
		App.View.Assign "sizeLimit", "10M"
		App.View.Assign "minSizeLimit", "0KB"
		'设置子链接 b
		Dim sub_menu, rsm, fields, f, selected, i, d : selected = "" : i = 0
		Dim menuid : menuid = App.ReqInt("menuid",0)
		If menuid>0 Then
            Set rsm = M("menu").admin_menu(menuid)
			If Not AB.C.IsNul(rsm) Then
				Set sub_menu = AB.C.Dict()
				Do While Not rsm.Eof
					Set d = AB.C.Dict()
					fields = App.Dao.RsFields(rsm)
					For Each f In fields
						d(f) = rsm(f)
					Next
					d("class") = ""
					If MODULE_NAME=rsm("module_name") AND ACTION_NAME=rsm("action_name") AND ( Instr(URL(),Trim(rsm("data")))>0 ) Then
						selected = "on"
						d("class") = "on"
					End If
					Set sub_menu(i) = d
					i = i + 1
					rsm.MoveNext
				Loop
				If AB.C.IsNul(selected) Then
					For Each key In sub_menu
						If MODULE_NAME=rsm("module_name") AND ACTION_NAME=rsm("action_name") Then
							sub_menu(key)("class") = "on"
							Exit For
						End If
					Next
				End If
			End If
		End If
		App.View.Assign "sub_menu", sub_menu
		'推荐位下拉
		Dim posid_list : Set posid_list = AB.C.Dict()
		Dim pos : Set pos = M("posid")().Where("status=1").Fetch()
		Do While Not pos.Eof
			posid_list(pos("id").Value) = pos("name").Value
			pos.MoveNext
		Loop
		App.View.Assign "posid_list", posid_list
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'验证登陆
	Public Sub LoginCheck()
		Dim t, tmp : t = U( URL() )
		tmp = AB.C.IIF(t=U("index/index"), "", "&backurl=" & AB.E.URLEncode(t))
		If Not IsLogin Then App.RR(login_url & tmp)
	End Sub

	'判断是否已登陆
	Public Function IsLogin()
		IsLogin = False
		If AB.Cookie("admin_name")<>"" Then
			IsLogin = True
		End If
	End Function

	'获取listTable公共部分的order by
	Public Function GetOrderBy(ByVal p)
		Dim sort, order, s_orderby, s_pk, s_alias
		s_pk = M(p)().getPk()
		s_alias = M(p)().GetAlias()
		If Trim(App.Req("sort"))<>"" Then
			sort = Trim(App.Req("sort"))
		ElseIf Trim(p)<>"" Then
			sort = s_pk
		End If
		If sort="" Then sort = "id"
		order = Trim(App.Req("order"))
		If sort = s_pk and order="" Then
			order = "desc"
		Else
			If order = "" Then order = "asc"
			If order<>"asc" And order<>"desc" Then order = "asc"
		End If
		If s_alias<>"" Then sort = s_alias & "." & sort
		GetOrderBy = sort & " " & order
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d : Set d = App.dictForm()
		Set dictForm = d
		On Error Goto 0
	End Function

    Public Sub index()
		AB.C.Print "Error Loading File : " & GROUP_NAME & "/" & MODULE_NAME & "Action" & ".class.asp"
    End Sub

    Public Sub [empty]()
		AB.C.Print "Admin GROUP Error !"
    End Sub

End Class
%>