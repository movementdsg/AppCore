<%
Class CacheAction_Admin

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		AB.Use "Fso"
		AB.Use "Cache"
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=cache&a=index
    Public Sub index()
		App.View.Display("index.html")
    End Sub

    Public Sub clear()
		Dim pt : pt = App.Req("type")
		Dim DATA_PATH : DATA_PATH = App.CachePath & "data/"
		Dim TEMP_PATH : TEMP_PATH = App.CachePath & "tpl/"
		Select Case pt
			Case "data"
				If AB.C.isFolder(TEMP_PATH) Then AB.Fso.DelFolder TEMP_PATH
				If AB.C.isFolder(DATA_PATH) Then AB.Fso.DelFolder DATA_PATH
			Case "field"
				'..
			Case "runtime"
				'If AB.Cache("admin_menu_cat").Ready Then AB.Cache("admin_menu_cat").Del
				AB.Session.Remove "admin_menu_cat"
				SiteCfgDataRefresh() '刷新SiteCfg数据
				SeoCfgDataRefresh() '刷新SeoCfg数据
			Case "logs"
				'..
			Case "js"
				'..
		End Select
		ajaxReturn 1, ""
    End Sub

    Public Sub qclear()
		Dim DATA_PATH : DATA_PATH = App.CachePath & "data/"
		Dim TEMP_PATH : TEMP_PATH = App.CachePath & "tpl/"
		If AB.C.isFolder(TEMP_PATH) Then AB.Fso.Del TEMP_PATH
		If AB.C.isFolder(DATA_PATH) Then AB.Fso.Del DATA_PATH
		'If AB.Cache("admin_menu_cat").Ready Then AB.Cache("admin_menu_cat").Del
		AB.Session.Remove "admin_menu_cat"
		SiteCfgDataRefresh() '刷新SiteCfg数据
		SeoCfgDataRefresh() '刷新SeoCfg数据
		ajaxReturn 1, "清除成功！"
    End Sub

End Class
%>