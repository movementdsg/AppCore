<%
Class MenuAction_Admin

	Private o_cache, s_cache_menu, s_cache_select

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		s_cache_menu = "admin_menu_cat"
		s_cache_select = "admin_menu_select"
		AB.Use "Cache" : Set o_cache = AB.Cache.New : o_cache.Expires = 30
		Response.Charset = App.CharSet
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=menu&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim cat, e, str
		'--启用缓存
		' If o_cache(s_cache_menu).Ready Then
			' Set cat = o_cache(s_cache_menu).Value
		' Else
			' Set cat = M("menu").get_tree(0,0)
			' DropCache() '删除缓存
			' o_cache(s_cache_menu) = cat
			' o_cache(s_cache_menu).SaveApp
		' End If
		' App.View.Assign "cat", cat
		If o_cache(s_cache_menu).Ready Then
			str = o_cache(s_cache_menu).Value
		Else
			Set cat = M("menu").get_tree(0,0)
			str = ""
			For Each e In cat
				str = str & "<tr>" & VbCrlf
				str = str & "<td align='center'><input type='checkbox' value='"& cat(e)("id") &"' class='J_checkitem'></td>" & VbCrlf
				str = str & "<td align='center'>"& cat(e)("id") &"</td>" & VbCrlf
				str = str & "<td>"& cat(e)("tree_spacer") &"<span data-tdtype='edit' data-field='name' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("name") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='module_name' data-id='"& cat(e)("id") &"' class='tdedit'>"& MODULE_NAME &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='action_name' data-id='"& cat(e)("id") &"' class='tdedit'>"& ACTION_NAME &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='data' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("data") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><span data-tdtype='edit' data-field='ordid' data-id='"& cat(e)("id") &"' class='tdedit'>"& cat(e)("ordid") &"</span></td>" & VbCrlf
				str = str & "<td align='center'><img data-tdtype='toggle' data-id='"& cat(e)("id") &"' data-field='display' data-value='"& cat(e)("display") &"' "
				str = str & " src='"& App.StaticPath &"/images/admin/toggle_"& AB.C.IIF(cat(e)("display")=0,"disabled","enabled") &".gif' /></td>" & VbCrlf
				str = str & "<td align='center'><a href='javascript:;' class='J_showdialog' "
				str = str & "	data-uri='"& U(MODULE_NAME & "/add?pid=" & cat(e)("id") ) &"' "
				str = str & "	data-title='添加子菜单' "
				str = str & "	data-id='add' data-width='500' data-height='350'>添加子菜单</a> |"
				str = str & "	<a href='javascript:;' class='J_showdialog' "
				str = str & "	data-uri='"& U(MODULE_NAME & "/edit?id=" & cat(e)("id") ) &"' "
				str = str & "	data-title='编辑 - "& cat(e)("name") &"' "
				str = str & "	data-id='edit' data-width='500' data-height='350'>编辑</a> |"
				str = str & "	<a href='javascript:;' class='J_confirmurl' data-acttype='ajax' "
				str = str & "	data-uri='"& U(MODULE_NAME & "/delete?id=" & cat(e)("id") ) &"' "
				str = str & "	data-msg='确定要删除“"& cat(e)("name") &"”吗？'>删除</a></td>"
				str = str & "</tr>" & VbCrlf & VbCrlf
			Next
			DropCache() '删除缓存
			o_cache(s_cache_menu) = str
			o_cache(s_cache_menu).SaveApp
		End If
		App.View.Assign "menu_list", str
		App.View.Assign "module_name", MODULE_NAME
		App.View.Assign "action_name", ACTION_NAME
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=menu&a=add
    Public Sub add()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, id, cat, e, str, selected, color : id = IntVal(App.Req("id"))
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			DropCache() '删除缓存
			'If o_cache(s_cache_select).Ready Then o_cache(s_cache_select).Del
			If Err.Number=0 And M("menu")().Add(d) Then
				App.Out.Put dataReturn(1, "添加成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			If o_cache(s_cache_select).Ready Then
				str = o_cache(s_cache_select).Value
			Else
				M("menu").get_tree2(0,0)
				str = ""
				For Each e In cat
					selected = AB.C.IIF(cat(e)("id")=id, "selected", "")
					color = AB.C.IIF(cat(e)("id")=id, " style='color:#54A9FF'", "")
					str = str & "<option value='"& cat(e)("id") &"' "& selected & color &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
				Next
				DropCache() '删除缓存
				'If o_cache(s_cache_select).Ready Then o_cache(s_cache_select).Del
				o_cache(s_cache_select) = str
				o_cache(s_cache_select).SaveApp
			End If
			App.View.Assign "select_menus", str
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=menu&a=edit&id=1
    Public Sub edit()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, id, cat, e, str, selected, color : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			DropCache() '删除缓存
			'If o_cache(s_cache_select).Ready Then o_cache(s_cache_select).Del
			If Err.Number=0 And M("menu")().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set rs = M("menu")().Find(id).Fetch()
			If (Rs.Bof And Rs.Eof) Then App.Out.Put toJson("error! no data id("& id &").",1,"")
			App.View.Assign "rs", rs
			If o_cache(s_cache_select).Ready Then
				str = o_cache(s_cache_select).Value
			Else
				M("menu").get_tree2(0,0)
				str = ""
				For Each e In cat
					selected = AB.C.IIF(cat(e)("id")=id, "selected", "")
					color = AB.C.IIF(cat(e)("id")=id, " style='color:#54A9FF'", "")
					str = str & "<option value='"& cat(e)("id") &"' "& selected & color &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
				Next
				DropCache() '删除缓存
				'If o_cache(s_cache_menu).Ready Then o_cache(s_cache_menu).Del
				o_cache(s_cache_select) = str
				o_cache(s_cache_select).SaveApp
			End If
			App.View.Assign "select_menus", str
			DisplayTpl "edit.html", 1
		End If
		On Error Goto 0
    End Sub

    Public Sub delete()
		App.noCache() '强制不缓存
		Dim id, a : id = App.Req("id")
		''AB.Use "A" : a = AB.A.Fetch(id,",") : id = Join(a, ",")
		DropCache() '删除缓存
		If M("menu")().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		DropCache() '删除缓存
		If M("menu")().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

	Private Function DropCache() '删除缓存
		On Error Resume Next
		If o_cache(s_cache_select).Ready Then o_cache(s_cache_select).Del
		If o_cache(s_cache_menu).Ready Then o_cache(s_cache_menu).Del
		On Error Goto 0
	End Function

    Public Sub [empty]()
		App.Out.Print "Menu Empty Action"
    End Sub

End Class
%>