<%
Class LogAction_Admin

	Private model, admin_model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("admin_log")
		Set admin_model = M("admin")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set model = Nothing
		Set admin_model = Nothing
	End Sub

	'View: index.asp?m=admin&c=log&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim rs, p, d_where : Set d_where = AB.C.Dict()
		Dim search,time_start,time_end,keyword,uid_arr,r
		p = App.ReqInt("p",1)
        App.View.Assign "p", p
        App.View.Assign "k", 1
		'--条件
		time_start = Trim(App.Req("time_start"))
		time_end = Trim(App.Req("time_end"))
		keyword = Trim(App.Req("keyword"))
		If time_start<>"" Then d_where("log.login_time>=") = "#"& CDate(time_start) & "#"
		If time_end<>"" Then d_where("log.login_time<=") = "#"& CDate(time_end) & "#"
		If keyword<>"" Then
			AB.Use "A" : uid_arr = Array()
			uid_arr = AB.A.Push(uid_arr, 0)
			Set r = admin_model().Field("id").Where(" [username] LIKE '%"& keyword &"%' ").Fetch()
			Do While Not r.Eof
				uid_arr = AB.A.Push(uid_arr, r("id").Value)
				r.MoveNext
			Loop
			d_where("log.uid[in]") = uid_arr
		End If
		Set search = AB.C.Dict()
		search("time_start") = time_start
		search("time_end") = time_end
		search("keyword") = keyword
		'--
		'Set rs = model().A("log").Where(d_where).Order("log.id desc").Fetch()
		Set rs = model().A("log") _
			.Field("log.*, admin.username as username") _
			.Relation( Array("admin admin", "admin.id = log.uid", true) ) _
			.Where(d_where).Order("log.id desc").Fetch()
		App.View.Assign "search", search
		App.View.Assign "rs", rs
		App.View.Display("index.html")
		App.Exit()
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print MODULE_NAME & "Action error!"
    End Sub

End Class
%>