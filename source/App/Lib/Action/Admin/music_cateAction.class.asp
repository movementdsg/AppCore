<%
Class Music_CateAction_Admin

	Private model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("music_cate")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=music_cate&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim cat : Set cat = model.get_tree(0,0)
		App.View.Assign "cat", cat
		App.View.Assign "module_name", MODULE_NAME
		App.View.Assign "action_name", ACTION_NAME
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=music&a=add
    Public Sub add()
		On Error Resume Next
		App.noCache() '强制不缓存
		Dim rs, d, pid, spid, cat, e, str, selected, color : pid = App.ReqInt("pid",0) : str = ""
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			If model.name_exists(d("name"),d("pid"),0) Then
				ajaxReturn 0, "操作失败"
			End If
			d("spid") = model.get_ssid(d("pid"))
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "添加成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			If pid>0 Then
				spid = model.get_ssid(pid)
			End If
			App.View.Assign "spid", spid
			Set cat = model.get_tree(0,0)
			For Each e In cat
				selected = AB.C.IIF(cat(e)("id")=pid, "selected", "")
				str = str & "<option value='"& cat(e)("id") &"' "& selected &">"& cat(e)("tree_spacer") &" "& cat(e)("name") &"</option>" & VbCrlf
			Next
			App.View.Assign "select_menus", str
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=music&a=edit&id=1
    Public Sub edit()
		Dim rs, d, dd, id, spid_arr
		If IS_POST Then '提交方式：POST
			Set d = Admin_Base.dictForm()
			d("pid") = IntVal(d("pid"))
			If model.name_exists(d("name"),d("pid"),d("id")) Then
				ajaxReturn 0, "操作失败"
			End If
			Set rs = model().Field("img,pid").Find("id="& d("id")).Fetch()
			If rs.RecordCount>0 Then
				AB.Use "A"
				If d("pid") <> rs("pid") Then
					spid_arr = model.get_child_ids(d("id"),true)
					If AB.A.InArray(d("pid"), spid_arr) Then
						ajaxReturn 0, "不能移动到本分类或者子分类下面"
					End If
				End If
				d("spid") = model.get_ssid(d("pid"))
			End If
			If Err.Number=0 And model().Find(d("id")).Update(d) Then
				'Set dd = AB.C.Dict() : dd("spid") = model.get_spid(d("id"))
				'model().Find(d("id")).Update(dd)
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			id = App.ReqInt("id",0)
			Set rs = model().Find(id).Fetch()
			'spid = rs("spid")
			spid = model.get_spid(id)
			App.View.Assign "spid", spid
			App.View.Assign "id", id
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, field, val
        id = IntVal(App.Req("id"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_getchilds()
		Dim rs, id : id = App.ReqInt("id",0)
		Set rs = model().Field("id,name").Where("pid="& id).Fetch()
		If rs.RecordCount>0 Then
			App.Out.Put dataReturn(1, "操作成功", rs, "", "")
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print "Music Cate Empty Action"
    End Sub

End Class
%>