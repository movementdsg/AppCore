<%
Class TagAction_Admin

	Private model, module_model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set model = M("tag")
		Set module_model = M("modules")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	'View: index.asp?m=admin&c=tag&a=index
    Public Sub index()
		Dim rs, s_order, d_where, search, p, module_list
		'设置子菜单 b
		Dim big_menu : Set big_menu = AB.C.Dict()
		big_menu("title") = "添加标签"
		big_menu("iframe") = U("tag/add")
		big_menu("id") = "add"
		big_menu("width") = 450
		big_menu("height") = 50
		App.View.Assign "big_menu", big_menu
		'-- e --
		s_order = Admin_Base.GetOrderBy("tag")
		Set d_where = AB.C.Dict()
		If Trim(App.Req("module_id"))<>"" Then
			d_where("module_id") = IntVal(App.Req("module_id"))
		End If
		If Trim(App.Req("keyword"))<>"" Then
			t = Trim(App.Req("keyword"))
			't = AB.E.URLDecode(t)
			d_where("name[%]") = "%"& t &"%"
		End If
		Set search = AB.C.Dict()
		search("keyword") = Trim(App.Req("keyword"))
		Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
		Set rs = model().Order(s_order).Where(d_where).Fetch()
		App.View.PageSize = 15
		App.View.PageMark = "p"
		App.View.Assign "rs", rs
		p = App.ReqInt("p",1)
        App.View.Assign "p", p
        App.View.Assign "k", 1
		App.View.Assign "search", search
		App.View.Assign "module_list", module_list
		App.View.Assign "module_id", App.ReqInt("module_id",0)
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=tag&a=add
    Public Sub add()
		On Error Resume Next
		Dim d, module_list
		If IS_POST Then '提交方式：POST
			Set d = model.dictForm()
			If Err.Number=0 And model().Add(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "add", "")
			Else
				App.Out.Put dataReturn(0, "添加失败", "", "add", "")
			End If
		Else
			Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
			App.View.Assign "module_list", module_list
			DisplayTpl "add.html", 1
		End If
		On Error Goto 0
    End Sub

	'View: index.asp?m=admin&c=tag&a=edit&id=1
    Public Sub edit()
		Dim rs, d, id, module_list : id = IntVal(App.Req("id"))
		App.View.Assign "id", id
		If IS_POST Then '提交方式：POST
			Set d = model.dictForm()
			If Err.Number=0 And model().Find(id).Update(d) Then
				App.Out.Put dataReturn(1, "操作成功", "", "edit", "")
			Else
				App.Out.Put dataReturn(0, "操作失败", "", "edit", "")
			End If
		Else
			Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
			App.View.Assign "module_list", module_list
			Set rs = model().Find(id).Fetch()
			App.View.Assign "rs", rs
			DisplayTpl "edit.html", 1
		End If
    End Sub

    Public Sub ajax_edit()
		App.noCache() '强制不缓存
		Dim id, module_id, arcid, field, val
        id = IntVal(App.Req("id"))
		module_id = IntVal(App.Req("module_id"))
		arcid = IntVal(App.Req("arcid"))
        field = Trim(App.Req("field"))
        val = App.Req("val")
        'val = AB.E.UnEscape(val)
		If field="name" Then
			If model.name_exists(val, id, module_id, arcid) Then
				ajaxReturn 0, "标签名称重复"
			End If
		End If
		If model().Find(id).setField(field, val) Then
			ajaxReturn 1, ""
		Else
			ajaxReturn 0, ""
		End If
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub ajax_check_name()
		Dim name, id, module_id, arcid
		name = Trim(App.Req("name"))
		id = Trim(App.Req("id"))
		module_id = IntVal(App.Req("module_id"))
		arcid = IntVal(App.Req("arcid"))
		If model.name_exists(name, id, module_id, arcid) Then
			ajaxReturn 0, "标签已存在"
		Else
			ajaxReturn 1, ""
		End If
    End Sub

End Class
%>