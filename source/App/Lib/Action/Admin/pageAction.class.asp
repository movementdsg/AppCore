<%
Class PageAction_Admin

	Private art_model, cate_model, page_model, module_model

	Private Sub Class_Initialize()
		On Error Resume Next
		Admin_Base.LoginCheck() '验证登陆
		'App.View.LayerOut = False
		'App.View.Locate = True
		Response.Charset = App.CharSet
		Set art_model = M("article")
		Set cate_model = M("class")
		Set page_model = M("page")
		Set module_model = M("modules")
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set art_model = Nothing
		Set cate_model = Nothing
		Set page_model = Nothing
	End Sub

	'View: index.asp?m=admin&c=page&a=index
    Public Sub index()
		App.noCache() '强制不缓存
		Dim cat : Set cat = page_model.get_cate_tree(0,0)
		App.View.Assign "cat", cat
		Dim module_list : Set module_list = module_model().Where("status=1").Order("ordid asc, id asc").Fetch()
		App.View.Assign "module_list", module_list
		App.View.Assign "module_id", App.ReqInt("module_id",0)
		App.View.Display("index.html")
		App.Exit()
    End Sub

	'View: index.asp?m=admin&c=page&a=edit&classid=1
    Public Sub edit()
		App.noCache() '强制不缓存
		Dim classid, cate_info, rs, d
		Dim title, info, seo_title, seo_keys, seo_desc
		If IS_POST Then '提交方式：POST
			classid = App.ReqInt("classid",0)
			If page_model().Where("classid="& classid).Count>0 Then
				page_model().Where("classid="& classid).Update(Request)
				Set d = AB.C.Dict() : d("last_time") = Now()
				page_model().Where("classid="& classid).Update(d)
			Else
				page_model().Add(Request)
			End If
			winTip "操作成功", U("page/index"), "", ""
		Else
			classid = App.ReqInt("classid",0)
			Set cate_info = cate_model().Field("id,name").Where("typeid=1 And id="& classid).Fetch()
			If Not cate_info.RecordCount>0 Then
				App.RR( U("page/index") )
			Else
				App.View.Assign "classid", cate_info("id")
				App.View.Assign "cate_name", cate_info("name")
			End If
			Set rs = page_model().Find("classid="& classid).Fetch()
			If rs.RecordCount>0 Then
				title = Cstr(rs("title"))
				info = Cstr(rs("info"))
				seo_title = Cstr(rs("seo_title"))
				seo_keys = Cstr(rs("seo_keys"))
				seo_desc = Cstr(rs("seo_desc"))
			End If
			App.View.Assign "rs", rs
			App.View.Assign "title", title
			App.View.Assign "info", info
			App.View.Assign "seo_title", seo_title
			App.View.Assign "seo_keys", seo_keys
			App.View.Assign "seo_desc", seo_desc
			App.View.Display("edit.html")
		End If
		App.Exit()
    End Sub

    Public Sub delete()
		Dim id : id = App.Req("id")
		If page_model().Delete(id) Then
			ajaxReturn 1, "操作成功"
		Else
			ajaxReturn 0, "操作失败"
		End If
    End Sub

    Public Sub [empty]()
		App.Out.Print "Page Empty Action"
    End Sub

End Class
%>