<%
Class BaseAction

	Private Sub Class_Initialize()
		'-- 视图设置
		'App.View.LayerOut = False
		'App.View.Locate = True
		If LCase(GROUP_NAME)<>"admin" Then
			Me.SqlIn() 'SQL防注入
		End If
	End Sub

	'全局公共函数：Base.LoadCfg() '加载站点配置
	Public Function LoadCfg()
		Dim name, siteCfg, seoCfg
		'SITE配置信息
		Set siteCfg = SiteCfgData()
		For Each name In siteCfg
			App.View.Assign name, siteCfg(name)
		Next
		'SEO配置信息
		Set seoCfg = SeoCfgData()
		For Each name In seoCfg
			App.View.Assign name, seoCfg(name)
		Next
	End Function

    Public Sub SqlIn()
		Stop_SqlIn() 'SQL防注入函数
    End Sub

    Public Sub index()
		App.Out.Print "全局缺省模块Action. 可设置404页面"
		'Set404()
    End Sub

    Public Sub Set404()
		App.Out.Print "<br>404 错误页面！"
    End Sub

    Public Sub SetErr()
		App.Out.Print "<br>系统出错！"
    End Sub

End Class
%>