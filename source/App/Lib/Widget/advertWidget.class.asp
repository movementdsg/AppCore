<%
'广告单元组件
Class advertWidget

	Private UnitPath

	Private Sub Class_Initialize()
		On Error Resume Next
		Response.Charset = App.CharSet
		UnitPath = App.WidgetPath() '单元组件Widget的路径
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

    Public Function index(ByVal id)
		On Error Resume Next
		Dim tpl, board_info, fpath, str, time_now, map, limit, f, ad, ad_list, d, i, k : time_now = now()
		If Not AB.C.isNul(id) Then id = IntVal(id)
		Set board_info = M("adboard")().Where(array("id="&id, "status=1")).Fetch()
		If AB.C.isNul(id) Or AB.C.isNul(board_info) Then Exit Function
		fpath = UnitPath & "advert/" & board_info("tpl") & ".config.asp"
		Dim ad_cfg : str = AB.C.IncCode(fpath) : Execute(str)
		Set map = AB.C.Dict()
		map("board_id") = id
		map("_string") = "(start_time<=#"&time_now&"# or start_time IS NULL) AND (end_time>=#"&time_now&"# or end_time IS NULL)"
		map("status") = 1
		If AB.C.IsDict(ad_cfg) Then limit = AB.C.IIF(CBool(ad_cfg("option")),"","1")
        Set ad = M("ad")().Field("id,type,name,url,content,desc,extimg,extval").Where(map).Order("ordid").Limit(0,limit).Fetch()
		k = 0
		Set ad_list = AB.C.Dict()
		Do While Not ad.Eof
			Set d = AB.C.Dict()
			For i = 0 To ad.Fields.Count-1
				f = ad.Fields(i).Name
				d(f) = ad(f).Value
			Next
			d("html") = get_html__(d, board_info)
			Set ad_list(ad("id").Value) = d
			ad.MoveNext
		Loop
		App.View.Assign "board_info", board_info
		App.View.Assign "ad_list", ad_list
		tpl = UnitPath & "advert/" & board_info("tpl") & ".html"
		'App.View.Display(tpl)
		index = App.View.GetTplHtml(tpl)
		On Error Goto 0
    End Function

    Private Function get_html__(ByVal ad, ByVal board_info)
		On Error Resume Next
		Dim size_html, html : html = ad("content")
		Dim updir : updir = App.UploadPath & "ad/"
		size_html = ""
		If Not AB.C.isNul(board_info("width")) Then size_html = size_html & " width="""& board_info("width")& """"
		If Not AB.C.isNul(board_info("height")) Then size_html = size_html & " height="""& board_info("height")& """"
		Select Case ad("type")
            Case "image"
				html = ""
                If Not AB.C.isNul(ad("name")) Then html = html & "<a title="""& ad("name") & """ href="""& U("Public:advert/tgo?id="& ad("id")) & """ target=""_blank"">"
                html = html & "<img alt="""& ad("name") & """ id=""adimg"& ad("id") & """ src="""& updir & ad("content") & size_html & """ />"
                html = html & "</a>"
            Case "flash"
                html  = "<a title="""& ad("name") & """ href="""& U("Public:advert/tgo?id="& ad("id")) & """ target=""_blank"">"
                html = html & "<object classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" "& size_html &" codebase=""http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"" id=""adflash"& ad("id") &""">"
                html = html & "<param name=""movie"" value="""& updir & ad("content") & """ />"
                html = html & "<param name=""quality"" value=""autohigh"" />"
                html = html & "<param name=""wmode"" value=""opaque"" />"
                html = html & "<embed src="""& ad("content") & """ quality=""autohigh"" wmode=""opaque"" name=""flashad"" swliveconnect=""TRUE"" pluginspage=""http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"" type=""application/x-shockwave-flash"""& size_html &"></embed>"
                html = html & "</object>"
                html = html & "</a>"
        End Select
		get_html__ = html
		On Error Goto 0
    End Function

End Class
%>