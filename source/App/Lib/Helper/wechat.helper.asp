<%
Class App_Helper_Wechat

	Public Token,isBug,Xml
	Public toUser,fromUser,Content,Text
	Public num,Descriptions,PicUrl,UrlHttp
	Public title,url,images
	Private data

	private sub class_initialize()
		Token = "aspwx"
		isBug = True
		AB.Use "Xml" : Set Xml = AB.Xml.New
		Set Data = AB.C.Dict()
    end sub

	private sub class_terminate() 
		'..
    end sub
	
	Public Function GetXmlData()
		Xml.Load Request
		If Xml("ToUserName").Length>0 Then Data("ToUserName") = Xml("ToUserName")(0).Text
		If Xml("FromUserName").Length>0 Then Data("FromUserName") = Xml("FromUserName")(0).Text
		If Xml("MsgType").Length>0 Then Data("MsgType") = Xml("MsgType")(0).Text
		If Xml("Content").Length>0 Then Data("Content") = Xml("Content")(0).Text
		If Xml("MsgId").Length>0 Then Data("MsgId") = Xml("MsgId")(0).Text
		If Xml("Title").Length>0 Then Data("Title") = Xml("Title")(0).Text
		If Xml("Description").Length>0 Then Data("Description") = Xml("Description")(0).Text
		If Xml("Url").Length>0 Then Data("Url") = Xml("Url")(0).Text
		If Xml("Format").Length>0 Then Data("Format") = Xml("Format")(0).Text
		If Xml("MediaId").Length>0 Then Data("MediaId") = Xml("MediaId")(0).Text
		If Xml("ThumbMediaId").Length>0 Then Data("ThumbMediaId") = Xml("ThumbMediaId")(0).Text
		If Xml("Location_X").Length>0 Then Data("Location_X") = Xml("Location_X")(0).Text
		If Xml("Location_Y").Length>0 Then Data("Location_Y") = Xml("Location_Y")(0).Text
		If Xml("Scale").Length>0 Then Data("Scale") = Xml("Scale")(0).Text
		If Xml("Label").Length>0 Then Data("Label") = Xml("Label")(0).Text
		If Xml("EventKey").Length>0 Then Data("EventKey") = Xml("EventKey")(0).Text
		If Xml("Ticket").Length>0 Then Data("Ticket") = Xml("Ticket")(0).Text
		If Xml("Latitude").Length>0 Then Data("Latitude") = Xml("Latitude")(0).Text
		If Xml("Longitude").Length>0 Then Data("Longitude") = Xml("Longitude")(0).Text
		If Xml("Precision").Length>0 Then Data("Precision") = Xml("Precision")(0).Text
		Dim str, dict, o, t, i, j
		p = "xml"
		If Xml(p).Length>1 Then '含有子节点
			Set dict = AB.C.Dict()
			For i=0 To Xml(p)(0).Length-1
				If Xml(p)(0).Child(i).IsNode Then
					If Xml(p)(0).Child(i).Length>1 Then '再包含子节点
						Set t = Xml(p)(0).Child(i)
						Set o = AB.C.Dict()
						For j=0 To t(0).Length-1
							If t(0).Child(j).IsNode Then
								name = t(0).Child(j).Name
								text = t(0).Child(j).Text
								o(name) = text
							End If
						Next
						Set dict(trim(t.Name)) = o
						Set o = Nothing
						Set t = Nothing
					ElseIf Xml(p)(0).Child(i).Length=1 Then
						name = Xml(p)(0).Child(i).Name
						text = Xml(p)(0).Child(i).Text
						dict(trim(name)) = text
					End If
				End If
			Next
			Set str = dict
		ElseIf Xml(p).Length=1 Then '不含子节点
			str = Xml(p)(0).Text
		End If
		
		Set GetXmlData = Data
	End Function
	

	Public Sub WriteText()'返回文本数据
		Dim strxml
		strxml=strxml& "<xml>" & VbCrlf
		strxml=strxml& "<ToUserName><![CDATA["&fromUser&"]]></ToUserName>" & VbCrlf
		strxml=strxml& "<FromUserName><![CDATA["&toUser&"]]></FromUserName> " & VbCrlf
		strxml=strxml& "<CreateTime>1</CreateTime>" & VbCrlf
		strxml=strxml& "<MsgType><![CDATA[text]]></MsgType>" & VbCrlf
		strxml=strxml& "<Content><![CDATA["& Content &"]]></Content>" & VbCrlf
		strxml=strxml& "<FuncFlag>0</FuncFlag>" & VbCrlf
		strxml=strxml& "</xml>"
		App.Out.Print strxml
		If isBug=True Then showBugs strxml
	End Sub

	Public Sub WriteImgage()'返回图片数据
		Dim strxml
		strxml=strxml& "<xml>"
		strxml=strxml& "<ToUserName><![CDATA["&fromUser&"]]></ToUserName>"
		strxml=strxml& "<FromUserName><![CDATA["&toUser&"]]></FromUserName> "
		strxml=strxml& "<CreateTime>"&Now()&"</CreateTime>"
		strxml=strxml& "<MsgType><![CDATA[image]]></MsgType>"
		strxml=strxml& "<PicUrl><![CDATA["& PicUrl &"]]></PicUrl>"
		strxml=strxml& "<MsgId>"&(Application("MsgId")+1)&"</MsgId>"
		strxml=strxml& "</xml>"
		App.Out.Print strxml
		If isBug=True Then showBugs strxml
	End Sub

	Public Sub WriteLink()'返回地图数据
		Dim strxml
		strxml=strxml&"<xml>"
		strxml=strxml& "<ToUserName><![CDATA["&fromUser&"]]></ToUserName>"
		strxml=strxml& "<FromUserName><![CDATA["&toUser&"]]></FromUserName> "
		strxml=strxml& "<CreateTime>"&Now()&"</CreateTime>"
		strxml=strxml& "<MsgType><![CDATA[link]]></MsgType>"
		strxml=strxml& "<Title><![CDATA["&title&"]]></Title>"
		strxml=strxml& "<Description><![CDATA["&title&"]]></Description>"
		strxml=strxml& "<Url><![CDATA["&url&"]]></Url>"
		strxml=strxml& "<MsgId>"
		strxml=strxml& "</xml>"
		App.Out.Print strxml
		If isBug=True Then showBugs strxml
	End Sub

	Public Sub WriteWebText()'返回条目数据
		Dim getLiNEData,AdvTitle
		Dim strXml
		strXml="<xml>"
		strXml=strXml&"<ToUserName><![CDATA["&fromUser&"]]></ToUserName>"
		strXml=strXml&"<FromUserName><![CDATA["&toUser&"]]></FromUserName>"
		strXml=strXml&"<CreateTime>"&Now()&"</CreateTime>"
		strXml=strXml&"<MsgType><![CDATA[news]]></MsgType>"
		getRtempDATA=ResultDate
		strXml=strXml&"<ArticleCount>"&(ubound(split(getRtempDATA,"[|]"))+1)&"</ArticleCount>"
		strXml=strXml&"<Articles>"
		If ubound(split(getRtempDATA,"[|]"))>0 Then
			getImgagePATH "Adv","(select top 1 "&ffix&"id from "&tfix&"Adv where "&ffix&"KeyWords='"&text&"')"
			Set RsAd=Cn.Execute("select top 1 "&ffix&"Title from "&tfix&"Adv where "&ffix&"KeyWords='"&text&"'")
			If Not RsAd.eOF Then AdvTitle=RsAd(0)
			RsAd.close
			set RsAd=Nothing
			Set RsAd=Cn.Execute("select "&ffix&"val from "&tfix&"property where "&ffix&"table='Adv' and "&ffix&"tbid=(select top 1 "&ffix&"id from "&tfix&"Adv where "&ffix&"KeyWords='"&text&"') and instr("&ffix&"val,'http')<>0")
			If Not RsAd.eOF Then UrlHttp=RsAd(0)
			RsAd.close
			set RsAd=Nothing
		Else
			PicUrl="/UploadFile/None.jpg"
		End If
		If text<>"" Then'展示广告
			If PicUrl="/UploadFile/None.jpg" Then 
				AdvTitle="广告展示位"
			End If
			strXml=strXml&"<item>"
			strXml=strXml&"<Title><![CDATA["&AdvTitle&"]]></Title>"
			strXml=strXml&"<Description><![CDATA["&AdvTitle&"]]></Description>"
			strXml=strXml&"<PicUrl><![CDATA["&thisUrl&Replace("/"&PicUrl,"//","/")&"]]></PicUrl>"
			strXml=strXml&"<Url><![CDATA["&UrlHttp&"]]></Url>"
			strXml=strXml&"</item>"
		End If
		PicUrl="//"
		for i=0 to ubound(split(getRtempDATA,"[|]"))
			getLiNEData=split(getRtempDATA,"[|]")(i)
			If getLiNEData<>"" Then
				getImgagePATH "seachInfor",split(getLiNEData,"[,]")(0)
				If PicUrl="" Then PicUrl="//"
				strXml=strXml&"<item>"
				strXml=strXml&"<Title><![CDATA["&split(getLiNEData,"[,]")(1)&"]]></Title>"
				strXml=strXml&"<Description><![CDATA["&split(getLiNEData,"[,]")(2)&"]]></Description>"
				strXml=strXml&"<PicUrl><![CDATA["&thisUrl&Replace("/"&PicUrl,"//","/")&"]]></PicUrl>"
				strXml=strXml&"<Url><![CDATA["&thisUrl&"showInfor.asp?title="&split(getLiNEData,"[,]")(1)&"&ID="&split(getLiNEData,"[,]")(0)&"]]></Url>"
				strXml=strXml&"</item>"
				PicUrl=""
			End If
		Next
		strXml=strXml&"</Articles>"
		strXml=strXml&"</xml>"
		App.Out.Echo strXml
		If isBug=True Then showBugs strXml
	End Sub 

	Private Sub getImgagePATH(tb,id)
		Dim RsImga
		Set RsImga=Cn.Execute("select "&ffix&"val from "&tfix&"property where "&ffix&"table='"&tb&"' and "&ffix&"tbid="&id&" and (instr("&ffix&"val,'.jpg')<>0 or instr("&ffix&"val,'.gif')<>0 or instr("&ffix&"val,'.png')<>0 or instr("&ffix&"val,'.bmp')<>0)")
		If Not RsImga.Eof Then PicUrl=RsImga(0)
		RsImga.close
		Set RsImga=Nothing
	End Sub

	Sub saveBugs(ByVal Content)
		Dim t, f : f = "/Cache/logs/wx/wx.log"
		AB.Use "Fso"
		t = AB.C.DateTime(Now(),"yyyy-mm-dd h:i:s")
		If Not AB.Fso.IsFile(f) Then
			AB.Fso.CreateFile f, t & VBCrlf & Content & VBCrlf
		Else
			AB.Fso.AppendFile f, VBCrlf & t & VBCrlf & Content & VBCrlf
		End If
	End Sub

	Public Function checkSignature()
		Dim signature, timestamp, nonce, tmpArr, tmpStr, bool : bool = False
		signature = App.Req("signature")
		timestamp = App.Req("timestamp")
		nonce = App.Req("nonce")
		tmpArr = Array(token, timestamp, nonce)
		'tmpArr = AB.A.Sort(tmpArr)
		tmpStr = Join(tmpArr,"")
		AB.Use "E" : AB.E.Use "SHA1"
		tmpStr = AB.E.SHA1.E(tmpStr)
		If tmpStr=signature Then bool = True
		checkSignature = bool
	End Function

End Class
%>