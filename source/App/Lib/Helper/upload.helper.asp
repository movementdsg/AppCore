<%
Class App_Helper_Upload

	Private o_upload
	Private i_singlesize, i_maxsize, s_allowed
	Private s_way

	Private Sub Class_Initialize()
		On Error Resume Next
		s_way = "upload"
		i_singlesize = 2 * 1024 * 1024 '2M, 单个文件最大上传限制
		i_maxsize = 2 * 1024 * 1024 '2M, 最大上传限制
		s_allowed = "jpg|jpeg|gif|png" '合法扩展名,不限制则为"*"
		AB.Use "Form"
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		'..
	End Sub

	Public Property Let Way(ByVal f)
		s_way = LCase(f)
		AB.Form.Up = s_way
	End Property
	Public Property Get Way()
		Way = s_way
	End Property

	Public Property Let SingleSize(ByVal f)
		i_singlesize = CLng(f)
	End Property
	Public Property Get SingleSize()
		SingleSize = i_singlesize
	End Property

	Public Property Let MaxSize(ByVal f)
		i_maxsize = CLng(f)
	End Property
	Public Property Get MaxSize()
		MaxSize = i_maxsize
	End Property

	Public Property Let Allowed(ByVal f)
		s_allowed = CLng(f)
	End Property
	Public Property Get Allowed()
		Allowed = s_allowed
	End Property

	Public Property Let Fun(ByVal f)
		AB.Form.Fun = f
	End Property

	Public Property Get Form()
		Set Form = AB.Form
	End Property

	Public Property Let Uploader(ByVal o)
		Set o_upload = o
	End Property
	Public Property Get Uploader()
		Set Uploader = o_upload
	End Property

	Public Sub Start()
		Select Case LCase(s_way)
			Case "up.an"
				If Not IsObject(o_upload) Then
					AB.Use "up" : AB.Up.Use "An"
					Set o_upload = AB.Up.An
				End If
				o_upload.SingleSize = i_singlesize
				o_upload.MaxSize = i_maxsize
				o_upload.Exe = s_allowed
				o_upload.Charset = "utf-8"
				o_upload.GetData()
			Case "upload"
				'..
			Case Else
				'..
		End Select
	End Sub

End Class
%>