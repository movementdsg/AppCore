<%
Class App_Helper_Tree

	Public dict, icon, nbsp, sql

	Private Sub Class_Initialize()
		On Error Resume Next
		Set dict = AB.C.Dict()
		icon = array("│","├","└")
		nbsp = "&nbsp;"
		sql = ""
		'nbsp = "　"
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set dict = Nothing
	End Sub

	'调用方法：
	'dim tree : set tree = App.Helper("tree")
	'tree.sql = "select * from @menu where pid = {*} order by ordid, id"
	'dim cat : set cat = tree.getTree("", 0, 0)
	Public Function getTree(ByRef dtree, ByVal pid, ByVal deep)
		If Not Trim(Me.sql)="" Then
			Set getTree = getTree__(dtree, pid, deep, 0)
		Else
			Set getTree = Nothing
		End If
	End Function

	Private Function getTree__(ByRef types, ByVal pid, ByVal deep, ByVal path)
		Dim sql, rs, d, i, f, v, str, j, k, num, total, spacer
		If Not ab.c.isdict(types) Then Set types = AB.C.Dict()
		pid = AB.C.IIF(AB.C.IsNum(pid),IntVal(pid),0)
		deep = AB.C.IIF(AB.C.IsNum(deep),IntVal(deep),0)
		path = AB.C.IIF(AB.C.IsNum(path),IntVal(path),0)
		sql = AB.C.RP(Me.sql, "{*}", pid)
		num = 0
		Set rs = App.Dao.Query(sql)
		Do While Not rs.eof
			If deep>0 And path+1>deep Then Exit Do
			Set d = AB.C.Dict()
			j = ""
			total = rs.RecordCount
			num = num+1
			If checkTypes__(num, total) Then
				j = Me.icon(2)
				k = Me.icon(0)
			Else
				j = Me.icon(1)
				k = Me.icon(0)
			End If
			spacer = AB.C.StrRepeat(Me.nbsp & k, path-1) & (Me.nbsp & j)
			If path=0 Then spacer = ""
			'str = AB.C.StrRepeat(Me.nbsp, path)
			For i = 0 To rs.Fields.Count-1
				f = rs.Fields(i).Name
				v = rs.Fields(i).Value
				d(f) = v
			Next
			d("tree_spacer") = spacer
			types.add "id_"&rs("id"), d
			Call getTree__(types,rs("id"),deep,path+1)
			rs.MoveNext
		Loop
		Set getTree__ = types
	End Function

	Private Function checkTypes__(ByVal num, Byval total)
		Dim b : b = False
		If IntVal(num) = IntVal(total) Then
			b = True
		End if
		checkTypes__ = b
	End Function

End Class
%>