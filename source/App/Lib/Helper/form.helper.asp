<%
Class App_Helper_Form

	Public Elem

	Private Sub Class_Initialize()
		On Error Resume Next
		Set Elem = AB.C.Dict()
		Elem("input-text") = ""
		Elem("input-password") = ""
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set Elem = Nothing
	End Sub

	Public Function text(ByVal field, ByVal value, ByVal size, ByVal sclass, ByVal extra)
		Dim inputtext, size, s_sclass, s_size : inputtext = "text"
		If AB.C.isNul(value) Then value = Elem("input-text")
		value = AB.C.RP("", "", value)
		s_sclass = AB.C.IIF(AB.C.isNul(sclass), " "& sclass &"""", "")
		s_size = AB.C.IIF(AB.C.isNul(size), " size="""& size &"""", "")
		parseStr   = "<input type="""& inputtext &""" class=""input-text"& s_sclass &""" name="""& field &"""  id="""& field &""" value="""& value &"""" & s_size & extra &" /> "
		text = parseStr
	End Function

	Public Function password(ByVal password, ByVal value, ByVal size, ByVal sclass, ByVal extra)
		Dim inputtext, size, s_sclass, s_size : inputtext = "password"
		If AB.C.isNul(value) Then value = Elem("input-password")
		value = AB.C.RP(Array("\",""""), Array("\\","\"""), value)
		s_sclass = AB.C.IIF(AB.C.isNul(sclass), " "& sclass &"""", "")
		s_size = AB.C.IIF(AB.C.isNul(size), " size="""& size &"""", "")
		parseStr   = "<input type="""& inputtext &""" class=""input-text"& s_sclass &""" name="""& field &"""  id="""& field &""" value="""& value &"""" & s_size & extra &" /> "
		password = parseStr
	End Function

End Class
%>