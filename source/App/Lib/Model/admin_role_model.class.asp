<%
Class Model_Admin_Role

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "admin_role"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	Public Function get_list_tree(pid, deep)
		Dim sort, order, s_order, tree : Set tree = App.Helper("tree")
		s_order = "ordid, id"
		tree.sql = M("menu")().Where("pid={*}").Order(s_order).getSQL()
        'tree.icon = array("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ","&nbsp;&nbsp;&nbsp;&nbsp;├─ ","&nbsp;&nbsp;&nbsp;&nbsp;└─ ")
        'tree.nbsp = "&nbsp;"
        tree.icon = array("&nbsp;"," ├─ "," └─ ")
        tree.nbsp = " "
		Set get_list_tree = tree.getTree("",pid,deep)
	End Function

    Public Function name_exists(name, id)
		Dim bool : id = IntVal(id) : bool = False
		If Me.Dao.Where("name='"& name &"'"& AB.C.IIF(id>0," AND id<>"& id,"")).Count() > 0 Then
			bool = True
		End If
		name_exists = bool
	End Function

End Class
%>