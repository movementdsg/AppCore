<%
Class Model_Ipban

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "ipban"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d, t : Set d = AB.C.Dict()
		AB.Use "Form"
		'AB.Form.Fun = "AB.E.UnEscape" '设置Form函数
		d("type") = AB.Form("type")
		d("name") = AB.Form("name")
		t = Trim(AB.Form("expires_time"))
		If t<>"" Then t = CDate(t) Else t = Null
		d("expires_time") = t
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>