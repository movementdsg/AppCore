﻿<%
Class Model_Class

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "class"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	Public Function get_tree(ByVal pid, ByVal deep)
		Dim tree : Set tree = App.Helper("tree")
		Dim where : where = "pid = {*}"
		Dim module_id : module_id = App.ReqInt("module_id",0)
		Dim typeid : typeid = App.Req("typeid")
		If module_id>0 Then where = where & " and module_id=" & module_id
		If typeid<>"" Then where = where & " and typeid=" & IntVal(typeid)
		tree.sql = "select * from "& GetTable(Me.Table) &" where "& where &" order by module_id asc, ordid, id"
        'tree.icon = array("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ","&nbsp;&nbsp;&nbsp;&nbsp;├─ ","&nbsp;&nbsp;&nbsp;&nbsp;└─ ")
        tree.icon = array("&nbsp; ","&nbsp; ├─ ","&nbsp; └─ ")
        tree.nbsp = " "
		Set get_tree = tree.getTree("",pid,deep)
	End Function

	Public Function get_child_ids(ByVal id, ByVal self)
		get_child_ids = App_System_Data.get_child_ids(id, CBool(self))
	End Function

	'根据pid获取spid
	Public Function get_ssid(ByVal id)
		get_ssid = App_System_Data.get_ssid(id)
	End Function

	'根据id获取spid
	Public Function get_spid(ByVal id)
		get_ssid = App_System_Data.get_spid(id)
	End Function

	Public Function name_exists(name, pid, id)
		Dim where : pid = intval(pid) : id = intval(id)
		where = "name='"& name &"' AND pid<>"& pid & " AND id<>"& id
		If Me.Dao.Where(where).Count()>0 Then
			name_exists = True
		Else
			name_exists = False
		End If
	End Function

End Class
%>