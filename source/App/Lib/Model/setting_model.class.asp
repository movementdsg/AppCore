﻿<%
Class Model_Setting

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "setting"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d, i, f, v, o, regex, match, a, b : Set d = AB.C.Dict()
		AB.Use "Form"
		'AB.Form.Fun = "AB.E.UnEscape" '设置Form函数
		For i=0 To AB.Form.Count-1
			f = AB.Form(i).Name
			v = AB.Form(i).Value
			'If f="reg_protocol" Then val = AB.C.RP(val, Array(vbCrLf,vbCr,vbLf), "{br}") '注册协议
			If InStr(f,"setting[")>0 Then
				Set regex = AB.C.RegMatch(f, "^setting\[(\w+)\]\[(\w+)\]")
				For Each match In regex
					a = match.SubMatches(0)
					b = match.SubMatches(1)
					If AB.C.IsNul(o) Then Set o = AB.C.Dict()
					o(b) = v
					Set d(a) = o
					f = AB.C.RP(f, match.Value, "")
				Next
				Set regex = Nothing
				f = AB.C.RegReplace(f,"^setting\[(\w+)\]","$1")
				If Trim(f)<>"" Then d(f) = v
			End If
		Next
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>