﻿<%
Class Model_Article

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "article"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d : Set d = App.dictForm()
		Dim spl : spl = "<hr class=""ke-pagebreak"" style=""page-break-after:always;"" />" '分页分隔符
		If d.Exists("content") Then
			d("content") = AB.C.RP(d("content"), spl, "[page]")
		End If
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>