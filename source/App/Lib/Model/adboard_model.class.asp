﻿<%
Class Model_Adboard

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "adboard"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

    Public Function get_tpl_list()
		On Error Resume Next
		Dim flist, file, name, content, i, tpl_list, key : Set tpl_list = AB.C.Dict()
		AB.Use "Fso"
        flist = AB.Fso.glob(App.LibPath & "Widget/advert/*.config.asp",1)
		If Not AB.C.IsNul(flist) Then
			For i = 0 To UBound(flist,2)
				Dim ad_cfg
				file = flist(0,i)
				name = AB.Fso.fileName(file)
				key = AB.C.RP(name,".config.asp","")
				content = AB.C.IncCode(file)
				Execute(content)
				If AB.C.IsDict(ad_cfg) Then ad_cfg("alias")=key
				If IsObject(ad_cfg) Then Set tpl_list(key) = ad_cfg Else tpl_list(key) = ad_cfg
			Next
		End If
        Set get_tpl_list = tpl_list
		On Error Goto 0
    End Function

	Public Function name_exists(name, id)
		Dim where : id = IntVal(id)
		where = "name='"& name &"' AND id<>"& id
		If Me.Dao.Where(where).Count()>0 Then
			name_exists = True
		Else
			name_exists = False
		End If
	End Function

End Class
%>