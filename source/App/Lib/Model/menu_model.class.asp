<%
Class Model_Menu

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "menu"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	Public Function admin_menu(Byval pid)
		On Error Resume Next
		Dim where, role_id
        pid = IntVal(pid)
        where = "pid="& pid &" and display=1 "
		If Trim(AB.Cookie("admin_role_id"))="" Then role_id = 0
		role_id = IntVal(AB.Cookie("admin_role_id"))
		If role_id>1 Then
			where = where & " and (select count(a.menu_id) from "& GetTable("admin_auth") &" as a where a.role_id="& role_id &" and a.menu_id=id)>0"
		End If
		Set admin_menu = Me.Dao.Where(where).Order("ordid asc, id asc").Fetch()
		On Error Goto 0
	End Function

	Public Function get_menu_data()
		On Error Resume Next
		Dim where, rs, dict, d, data
        where = "display=1"
		If Trim(AB.Cookie("admin_role_id"))<>"" And IntVal(AB.Cookie("admin_role_id"))>1 Then
            where = where & " and (select count(a.menu_id) from "& GetTable("admin_auth") &_
			" as a where a.role_id="& IntVal(AB.Cookie("admin_role_id")) &" and a.menu_id=id)>0"
		End If
		Set rs = Me.Dao.Where(where).Field("id,name,module_name as c,action_name as a,data").Fetch()
		Set dict = AB.C.Dict()
		Set d = AB.C.Dict()
		d.Add "name", "后台首页" : d.Add "c", "index" : d.Add "a", "panel" : d.Add "data", ""
		dict.Add "id_0", d
		Do While Not rs.eof
			Set d = AB.C.Dict()
			data = rs("data").value
			If AB.C.isNul(data) Then data = ""
			d.Add "name", rs("name").value : d.Add "c", rs("c").value : d.Add "a", rs("a").value : d.Add "data", data
			dict.Add "id_"& rs("id"), d
			rs.MoveNext
		Loop
		Set rs = Nothing
		Set get_menu_data = dict
		On Error Goto 0
	End Function

	Public Function TopMenu()
		On Error Resume Next
		dim r, i, id, f, d
		Set r = Me.admin_menu(0)
		AB.Use "A"
		Do While Not r.Eof
			Set d = AB.C.Dict()
			For i = 0 To r.Fields.Count-1
				f = r.Fields(i).Name
				d(f) = r.Fields(i).Value
				'd(f) = r(f).Value
			Next
			Set d = Nothing
			r.MoveNext
		Loop
		Set TopMenu = d
		On Error Goto 0
	End Function

	Public Function menu_ids(Byval pid)
		On Error Resume Next
		Dim tRs, id, a : a = Array()
		Set tRs = Me.admin_menu(pid)
		AB.Use "A"
		Do While Not tRs.Eof
			id = tRs("id").Value
			a = AB.A.Push(a, id)
			tRs.MoveNext
		Loop
		Set tRs = Nothing
		menu_ids = Join(a, ",")
		On Error Goto 0
	End Function

	Public Function get_tree(pid, deep)
		Dim tree : Set tree = App.Helper("tree")
		tree.sql = "select * from "& GetTable(Me.Table) &" where pid = {*} order by ordid, id"
        tree.icon = array("&nbsp;&nbsp;&nbsp;│ ","&nbsp;&nbsp;&nbsp;├─ ","&nbsp;&nbsp;&nbsp;└─ ")
        tree.nbsp = "&nbsp;&nbsp;&nbsp;"
		Set get_tree = tree.getTree("",pid,deep)
	End Function

	Public Function get_tree2(pid, deep)
		Dim tree : Set tree = App.Helper("tree")
		tree.sql = "select * from "& GetTable(Me.Table) &" where pid = {*} order by ordid, id"
		tree.icon = array("&nbsp;│","&nbsp;├─","&nbsp;└─")
		tree.nbsp = "&nbsp;&nbsp;"
		Set get_tree2 = tree.getTree("",pid,deep)
	End Function

	Public Function get_child_ids(ByVal id, ByVal self)
		Dim a : a = get_child__("", id)
		a = AB.A.Push(a, intval(id))
		a = AB.A.Unique(a)
		If self = False Then a = AB.A.Del(a, id)
        get_child_ids = a
	End Function

		Private Function get_child__(ByRef a, ByVal id)
			Dim ids, i, rs : AB.Use "A" : id = intval(id)
			If AB.C.IsNul(a) Then a = Array()
			Set rs = Me.Dao.Field("id").Where("pid="& id &"").Fetch()
			If rs.RecordCount>0 Then
				Do While Not rs.Eof
					ids = get_child__(a, rs("id").Value)
					For Each i In ids
						a = AB.A.Push(a, i)
					Next
					rs.MoveNext
				Loop
			End If
			rs.Close() : Set rs = Nothing
			a = AB.A.Push(a, id)
			a = AB.A.Unique(a)
			get_child__ = a
		End Function

	'根据pid获取spid
	Public Function get_ssid(Byval pid)
		Dim spid : spid = get_spid(pid)
		If AB.C.IsNul(spid) Then spid = "0"
		spid = AB.C.IIF(spid="" Or spid="0", pid & "|", spid & pid & "|")
		If IntVal(pid)=0 Then spid = 0
		get_ssid = spid
	End Function

	'根据id获取spid
	Public Function get_spid(Byval id)
		Dim rs, str : id = intval(id)
		Set rs = Me.Dao.Where("id=" & id).Fetch()
		If rs.RecordCount>0 Then
			If rs("pid")>0 Then
				id = IntVal(rs("pid"))
				str = id & "|" & str
				If get_spid(id)<>"0" Then str = get_spid(id) & str
			Else
				str = "0|" & str
			End If
		Else
			If id=0 Then str = "0|" & str
		End If
		str = AB.C.RegReplace(str, "^0\|", "")
		If str="" Then str = "0"
		rs.Close() : Set rs = Nothing
		get_spid = str
	End Function

	Public Function get_level(Byval id, Byval d, Byval topid, Byval n)
		topid = IntVal(topid) : n = IntVal(n)
		For Each k In d
			If d(k)("id")=IntVal(id) Then
				If d(k)("pid")=topid Then
					get_level = n
					Exit Function
				End If
				n = n + 1
				get_level = Me.get_level(d(k)("pid"), d, topid, n)
			End If
		Next
	End Function

End Class
%>