<%
Class Model_Admin

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "admin"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

    Public Function name_exists(name, id)
		Dim bool : id = IntVal(id) : bool = False
		If Me.Dao.Where("username='"& name &"'"& AB.C.IIF(id>0," AND id<>"& id,"")).Count() > 0 Then
			bool = True
		End If
		name_exists = bool
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d : Set d = App.dictForm()
		AB.Use "Form"
		If d.Exists("password") Then
			If AB.Form("password")<>"" Then
				If AB.Form("password")<>AB.Form("repassword") Then
					App.Out.Put dataReturn(0, "两次输入的密码不一致", "", "add", "")
				Else
					d("password") = AB.E.Md5.To32(Trim(AB.Form("password")))
				End If
			Else
				d.Remove("password")
			End If
		End If
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>