﻿<%
Class Model_Seo

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "seo"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d, i, f, v, t : Set d = AB.C.Dict()
		AB.Use "Form"
		'AB.Form.Fun = "AB.E.UnEscape" '设置Form函数
		For i=0 To AB.Form.Count-1
			f = AB.Form(i).Name
			v = AB.Form(i).Value
			If AB.C.RegTest(f, "^\w+(\[\w*\])*$") Then
				t = AB.C.CLeft(f,"[")
				If Not d.Exists(t) Then
					If IsObject( App.Req(t) ) Then
						Set d(t) = App.Req(t)
					Else
						d(t) = App.Req(t)
					End If
				End If
			End If
		Next
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>