﻿<%
Class Model_Tag

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "tag"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

    Public Function name_exists(name, id, module_id, arcid)
		Dim bool : id = IntVal(id) : module_id = IntVal(module_id) : arcid = IntVal(arcid) : bool = False
		If Me.Dao.Where( _
					"name='"& name &"'" _
					& AB.C.IIF(id>0," AND id<>"& id,"") _
					& AB.C.IIF(module_id>0," AND module_id="& module_id,"") _
					& AB.C.IIF(arcid>0," AND arcid="& arcid,"") _
				).Count() > 0 Then
			bool = True
		End If
		name_exists = bool
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d : Set d = App.dictForm()
		If d.Exists("arcid") Then d("arcid") = IntVal(d("arcid"))
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>