<%
Class Model_Nav

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "nav"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	Public Function nav_ids(Byval pid)
		On Error Resume Next
		Dim tRs, id, a : a = Array()
		Set tRs = Me.admin_nav(pid)
		AB.Use "A"
		Do While Not tRs.Eof
			id = tRs("id").Value
			a = AB.A.Push(a, id)
			tRs.MoveNext
		Loop
		Set tRs = Nothing
		nav_ids = Join(a, ",")
		On Error Goto 0
	End Function

	Public Function get_tree(pid, deep)
		Dim tree : Set tree = App.Helper("tree")
		Dim st, cate : cate = LCase(Trim(App.Req("type")))
		If cate<>"" Then st = " AND cate='"& cate &"'"
		tree.sql = "select * from "& GetTable(Me.Table) &" where pid = {*} "& st &" order by cate desc, ordid, id"
        tree.icon = array("&nbsp;&nbsp;&nbsp;│ ","&nbsp;&nbsp;&nbsp;├─ ","&nbsp;&nbsp;&nbsp;└─ ")
        tree.nbsp = "&nbsp;&nbsp;&nbsp;"
		Set get_tree = tree.getTree("",pid,deep)
	End Function

	Public Function get_tree2(st, pid, deep)
		Dim tree : Set tree = App.Helper("tree")
		If st<>"" Then st = " AND cate='"& st &"'"
		tree.sql = "select * from "& GetTable(Me.Table) &" where pid = {*} "& st &" order by cate desc, ordid, id"
		tree.icon = array("&nbsp;│","&nbsp;├─","&nbsp;└─")
		tree.nbsp = "&nbsp;&nbsp;"
		Set get_tree2 = tree.getTree("",pid,deep)
	End Function

	Public Function get_child_ids(ByVal id, ByVal self)
		Dim a : a = get_child__("", id)
		a = AB.A.Push(a, intval(id))
		a = AB.A.Unique(a)
		If self = False Then a = AB.A.Del(a, id)
        get_child_ids = a
	End Function

		Private Function get_child__(ByRef a, ByVal id)
			Dim ids, i, rs : AB.Use "A" : id = intval(id)
			If AB.C.IsNul(a) Then a = Array()
			Set rs = Me.Dao.Field("id").Where("pid="& id &"").Fetch()
			If rs.RecordCount>0 Then
				Do While Not rs.Eof
					ids = get_child__(a, rs("id").Value)
					For Each i In ids
						a = AB.A.Push(a, i)
					Next
					rs.MoveNext
				Loop
			End If
			rs.Close() : Set rs = Nothing
			a = AB.A.Push(a, id)
			a = AB.A.Unique(a)
			get_child__ = a
		End Function

	'根据pid获取spid
	Public Function get_ssid(Byval pid)
		Dim spid : spid = get_spid(pid)
		If AB.C.IsNul(spid) Then spid = "0"
		spid = AB.C.IIF(spid="" Or spid="0", pid & "|", spid & pid & "|")
		If IntVal(pid)=0 Then spid = 0
		get_ssid = spid
	End Function

	'根据id获取spid
	Public Function get_spid(Byval id)
		Dim rs, str : id = intval(id)
		Set rs = Me.Dao.Where("id=" & id).Fetch()
		If rs.RecordCount>0 Then
			If rs("pid")>0 Then
				id = IntVal(rs("pid"))
				str = id & "|" & str
				If get_spid(id)<>"0" Then str = get_spid(id) & str
			Else
				str = "0|" & str
			End If
		Else
			If id=0 Then str = "0|" & str
		End If
		str = AB.C.RegReplace(str, "^0\|", "")
		If str="" Then str = "0"
		rs.Close() : Set rs = Nothing
		get_spid = str
	End Function

	Public Function get_level(Byval id, Byval d, Byval topid, Byval n)
		topid = IntVal(topid) : n = IntVal(n)
		For Each k In d
			If d(k)("id")=IntVal(id) Then
				If d(k)("pid")=topid Then
					get_level = n
					Exit Function
				End If
				n = n + 1
				get_level = Me.get_level(d(k)("pid"), d, topid, n)
			End If
		Next
	End Function

End Class
%>