<%
Class Model_User

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "user"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

    Public Function name_exists(name, id)
		Dim bool : id = IntVal(id) : bool = False
		If Me.Dao.Where("username='"& name &"'"& AB.C.IIF(id>0," AND id<>"& id,"")).Count() > 0 Then
			bool = True
		End If
		name_exists = bool
	End Function

    Public Function email_exists(email, id)
		Dim bool : id = IntVal(id) : bool = False
		If Me.Dao.Where("email='"& email &"'"& AB.C.IIF(id>0," AND id<>"& id,"")).Count() > 0 Then
			bool = True
		End If
		email_exists = bool
	End Function

	'Form表单转为字典集
	Public Function dictForm()
		On Error Resume Next
		Dim d, t : Set d = AB.C.Dict()
		AB.Use "Form"
		'AB.Form.Fun = "AB.E.UnEscape" '设置Form函数
		If AB.Form("password")<>"" Then
			If AB.Form("password")<>AB.Form("repassword") Then
				App.Out.Put dataReturn(0, "两次输入的密码不一致", "", "add", "")
			Else
				d("password") = AB.E.Md5.To16(Trim(AB.Form("password")))
			End If
		End If
		If AB.Form.Has("username") And Trim(AB.Form("username"))<>"" Then d("username") = Trim(AB.Form("username"))
		d("email") = Trim(AB.Form("email"))
		d("img") = Trim(AB.Form("img"))
		d("gender") = CInt(AB.Form("gender"))
		d("status") = CInt(AB.Form("status"))
		If AB.Form.Has("birthday") And Trim(AB.Form("birthday"))<>"" Then
			t = Trim(AB.Form("birthday"))
			t = Split(t,"-")
			If AB.C.IsNum(t(0)) Then d("byear") = CInt(t(0))
			If d("byear")>0 Then
				If d("byear")>=100 and d("byear")<1000 Then
					d("byear") = 1000 + d("byear")
				ElseIf d("byear")<100 Then
					d("byear") = 2000 + d("byear")
					If d("byear")>year(Now()) Then
						d("byear") = d("byear") - 100
					End If
				End If
			End If
			If UBound(t)>0 Then
				If AB.C.IsNum(t(1)) Then d("bmonth") = CInt(t(1))
				If d("bmonth")>12 Or d("bmonth")<0 Then d("bmonth") = 0
			End If
			If UBound(t)>1 Then
				If AB.C.IsNum(t(2)) Then d("bday") = CInt(t(2))
				If d("bday")>31 Or d("bday")<0 Then d("bday") = 0
			End If
		End If
		Set dictForm = d
		On Error Goto 0
	End Function

End Class
%>