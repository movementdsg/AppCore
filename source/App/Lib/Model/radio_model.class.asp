﻿<%
Class Model_Radio

	Public Table '数据表
	Public module_id

	Private Sub Class_Initialize()
		Table = "radio"
		module_id = M("modules")().Where("name='radio'").getField("id")
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	Public Function get_list_tree(pid, deep)
		Dim sort, order, s_order, tree : Set tree = App.Helper("tree")
		s_order = "ordid, id"
		tree.sql = M("class")().Where("module_id="& module_id &" and typeid=0 and pid={*}").Order(s_order).getSQL()
        'tree.icon = array("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ","&nbsp;&nbsp;&nbsp;&nbsp;├─ ","&nbsp;&nbsp;&nbsp;&nbsp;└─ ")
        tree.icon = array("&nbsp; ","&nbsp; ├─ ","&nbsp; └─ ")
        tree.nbsp = " "
		Set get_list_tree = tree.getTree("",pid,deep)
	End Function

	Public Function name_exists(name, id)
		Dim where : id = IntVal(id)
		where = "name='"& name &"' AND id<>"& id
		If Me.Dao.Where(where).Count()>0 Then
			name_exists = True
		Else
			name_exists = False
		End If
	End Function

End Class
%>