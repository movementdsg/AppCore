﻿<%
Class Model_Page

	Public Table '数据表

	Private Sub Class_Initialize()
		Table = "page"
	End Sub

	'默认缺省方法为Dao，注意：此方法必不可少！
	Public Default Function Dao()
		Set Dao = App.Dao.New
		Dao.Table = Table
	End Function

	Public Function get_cate_tree(pid, deep)
		Dim tree : Set tree = App.Helper("tree")
		Dim sort, order, s_order
		's_order = Admin_Base.GetOrderBy("user")
		'sort = AB.C.IIF(Trim(App.Req("sort"))<>"",App.Req("sort"),"ordid")
		'order = AB.C.IIF(Trim(App.Req("order"))<>"",App.Req("order"),"DESC")
		's_order = sort & " " & order
		s_order = "ordid, id"
		Dim where : where = "typeid=1 and pid = {*}"
		Dim module_id : module_id = App.ReqInt("module_id",0)
		If module_id>0 Then where = where & " and module_id=" & module_id
		tree.sql = M("class cat")().Field("id,pid,name,last_time,module_id").Join(GetTable("page")& " page").On("page.classid = cat.id").Where(where).Order(s_order).getSQL()
        tree.icon = array("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ","&nbsp;&nbsp;&nbsp;&nbsp;├─ ","&nbsp;&nbsp;&nbsp;&nbsp;└─ ")
        tree.nbsp = "&nbsp;"
		Set get_cate_tree = tree.getTree("",pid,deep)
	End Function

End Class
%>