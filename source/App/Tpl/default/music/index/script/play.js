/**
 * Interface Elements[Append Song to Fav Box]
 * require jQuery
 * window.tobox(url,'_box')
 */

var zhuanjiid = 0;

$(document).ready(function() {

	$('body').append('<div id="joverlay" style="display:none;' + 'position:absolute; background:#333; top:0; width:100%;' + ' opacity:0.5; filter:alpha(opacity=50); z-index:2147483647;left:0"></div>');
	var joverlay = $('#joverlay');
	var jbox;

	function boxinit(w, h) {
		var d = document.documentElement;
		var top = d.scrollTop + ((d.clientHeight - h) / 2) + 'px';
		var left = (d.clientWidth - w) / 2 + 'px';
		jbox.css({
			top: top,
			left: left
		});
	};

	window.tobox = function(url, name) {
		if (name === '_box') {
			$('body').append('<iframe id="jbox" name="_box" src="' + url + '"' + ' allowtransparency="no" scrolling="no"' + ' frameborder="0" style="display:none;position:absolute;' + ' z-index:2147483647; width:500px; height:300px;"></iframe>');
			jbox = $('#jbox');
		} else if (name === '_boxly') {
			$('body').append('<iframe id="jboxly" name="_boxly" src="' + url + '"' + ' allowtransparency="no" scrolling="no"' + ' frameborder="0" style="display:none;position:absolute;' + ' z-index:2147483647; width:500px; height:300px;"></iframe>');
			jbox = $('#jboxly');
		} else if (name === '_boxtj') {
			$('body').append('<iframe id="jboxtj" name="_boxtj" src="' + url + '"' + ' allowtransparency="no" scrolling="no"' + ' frameborder="0" style="display:none;position:absolute;' + ' z-index:2147483647; width:500px; height:300px;"></iframe>');
			jbox = $('#jboxtj');
		} else if (name === '_lsdown') {
			$('body').append('<iframe id="lsdown" name="_lsdown" src="' + url + '"' + ' allowtransparency="no" scrolling="no"' + ' frameborder="0" style="display:none;position:absolute;' + ' z-index:2147483647; width:500px; height:300px;"></iframe>');
			jbox = $('#lsdown');
			agjustDlg(300, 300);
		} else if (name === '_qzonedown') {
			$('body').append('<iframe id="qzonedown" name="_qzonedown" src="' + url + '"' + ' allowtransparency="no" scrolling="no"' + ' frameborder="0" style="display:none;position:absolute;' + ' z-index:2147483647; width:690px; height:530px;"></iframe>');
			jbox = $('#qzonedown');
			parent.agjustDlg(690, 530);
		} else if (name === '_noplay') {
			$('body').append('<iframe id="noplay" name="_qzonedown" src="' + url + '"' + ' allowtransparency="no" scrolling="no"' + ' frameborder="0" style="display:none;position:absolute;' + ' z-index:2147483647; width:100%; height:1000px;"></iframe>');
			jbox = $('#noplay');
			parent.agjustDlg(screen.width + 100, screen.height);
		} else {
			window.open(url, name);
		}
		return true;
	};

	window.closeDialog = function(t) {
		if (!t) {
			joverlay.hide();
			jbox.remove();
			$(window).unbind('.box');
		}
	};

	window.agjustDlg = function(w, h) {
		jbox.css({
			width: w + 'px',
			height: h + 'px',
			display: 'block'
		});
		joverlay.css({
			height: $(document).height() + 'px',
			display: 'block'
		}).click(function() {
			closeDialog();
		});
		var fn = function() {
			var t;
			return function() {
				if (t) window.clearTimeout(t);
				t = window.setTimeout(function() {
					boxinit(w, h);
				},
				5);
			} ();
		};
		$(window).bind('scroll.box', fn).bind('resize.box', fn).trigger('scroll.box');
	};

	window.commonMsg = function(msg, callBack) {
		eval(callBack);
		var s = msg.indexOf('<');
		if (s != -1) {
			$('body').append('<div id="jboxs" ' + ' style="display:none;position:absolute;' + ' z-index:2147483647;background:#F2F7FC;border: 1px solid rgb(137, 212, 248);">' + '<div style="background: transparent url() repeat-x scroll 0% 50%;font-size:14px;font-weight:800;height:28px;line-height:28px;text-indent:1em;"><span style="float:right;display:inline-block;"><img alt="关闭" style="cursor: pointer;margin:2px 2px 0 0" id="close_button" onclick="closeDialog()" src=""/></span>收藏歌曲</div>' + '<div style="text-align:center;' + 'padding:20px 10px 5px 10px;line-height:150%;">' + msg + '</p></div>');
			jbox = $('#jboxs');
			window.agjustDlg(300, 150);
		} else {
			alert(msg);
		}
	};

	if (zhuanjiid == 69) {
		setTimeout("lsdown();", 3000);
	}

});


// 删除歌曲列表功能
function del_list(str_l) {
	//if(confirm("您确认要删除吗?")){
	var str = document.cookie;
	var numStar = str.indexOf("l_music");
	if (numStar != -1) {
		var numEnd = str.indexOf("l_end");
		if (numEnd > numStar) {
			//设置时间
			var nextyear = new Date();
			nextyear.setFullYear(nextyear.getFullYear() + 1);
			var str_time = nextyear.toGMTString();
			var strCoki = str.substring(numStar, numEnd).replace(/l_music=/ig, "");
			if (str_l.indexOf(",") != -1) {
				var str_ls = str_l.split(",");
				for (var ix = 0; ix < str_ls.length; ix++) {
					strCoki = strCoki.replace(str_ls[ix], "");
					strCoki = strCoki + ",";
					strCoki = strCoki.replace(str_ls[ix], "");
					strCoki = strCoki.replace(",,", ",");
				}
				strCoki = strCoki.replace(",,", ",");
				strCoki = strCoki.replace(",,", ",");
				strCoki = strCoki.replace(",,", ",");
				strCoki = strCoki.replace(",,", ",");
			} else {
				strCoki = strCoki.replace(str_l, "");
				strCoki = strCoki + ",";
				strCoki = strCoki.replace(str_l, "");
				strCoki = strCoki.replace(",,", "");
			}
			document.cookie = "l_music=" + strCoki + "l_end;version=" + document.lastModified + ";expires=" + str_time + ";path=/";

		}
	}
	//}
}
//清空所有播放歌曲记录Cookie
function delcok() {
	if (confirm("确定要清空历史试听记录吗？\r\r如果您的历史记录不正常，建议全部清空！")) {
		//设置时间
		var nextyear = new Date();
		nextyear.setFullYear(nextyear.getFullYear() - 10);
		var str_time = nextyear.toGMTString();
		document.cookie = "l_music=l_end;expires=" + str_time + ";path=/";
		optlist(1);
	}
}
//记录cookie
function write_cookie(str) {
	var num_start = str.indexOf("l_music");
	var nextyear = new Date();
	nextyear.setFullYear(nextyear.getFullYear() + 1);
	var str_time = nextyear.toGMTString();
	var coknums = 17 * 3;
	if (num_start == -1) {
		document.cookie = "l_music=" + cokstr + "l_end;version=" + document.lastModified + ";expires=" + str_time + ";path=/";
	} else {
		if (str.indexOf("" + cokstr + "") == -1) {
			var num_end = str.indexOf("l_end");
			if (num_end > num_start) {
				var str_list = str.substring(num_start, num_end).replace(/l_music=/ig, "");
				var arr_list = str_list.split(",");
				if (arr_list.length < coknums) {
					document.cookie = "l_music=" + cokstr + "," + str_list + "l_end;version=" + document.lastModified + ";expires=" + str_time + ";path=/";
				} else {
					var arr_new = new Array();
					for (j = 0; j < arr_list.length; j++) {
						if (j == 0) {
							arr_new[j] = "" + cokstr + "";
						} else {
							arr_new[j] = arr_list[j - 1];
						}
					}

					document.cookie = "l_music=" + arr_new + "l_end;version=" + document.lastModified + ";expires=" + str_time + ";path=/";
				}
			} else {
				return ("参数错误!");
			}
		}
	}
}
function GetCookie(sMainName, sSubName) {
	var sCookieName = sMainName + "=";
	var sSubCookieName = (sSubName) ? sSubName + "=": null;
	var sCookie;
	var sWholeCookie = document.cookie;

	var nValueBegin = sWholeCookie.indexOf(sCookieName);
	if (nValueBegin != -1) {
		var nValueEnd = sWholeCookie.indexOf(";", nValueBegin);
		if (nValueEnd == -1) nValueEnd = sWholeCookie.length;

		var sValue = sWholeCookie.substring(nValueBegin + sCookieName.length, nValueEnd); //获得Cookie值
		if (sSubCookieName) {
			var nSubValueBegin = sValue.indexOf(sSubCookieName);
			if (nSubValueBegin != -1) {
				var nSubValueEnd = sValue.indexOf("&", nSubValueBegin);
				if (nSubValueEnd == -1) nSubValueEnd = sValue.length;
				var sSubValue = sValue.substring(nSubValueBegin + sSubCookieName.length, nSubValueEnd); //获得指定的子键值
				return sSubValue;
			}
		}
		if (!sSubCookieName) return sValue;
	}
	return null;
}
function jkSetCookie(sName, sValue) {
	var expires = new Date();
	expires.setFullYear(expires.getFullYear() + 1);
	document.cookie = sName + "=" + escape(sValue) + ";path=/;expires=" + expires.toGMTString();
}
// 获取指定名称的cookie值 
function jkGetCookie(name) {
	var result = null;
	var myCookie = document.cookie + ";";
	var searchName = name + "=";
	var startOfCookie = myCookie.indexOf(searchName);
	var endOfCookie;
	if (startOfCookie != -1) {
		startOfCookie += searchName.length;
		endOfCookie = myCookie.indexOf(";", startOfCookie);
		result = unescape(myCookie.substring(startOfCookie, endOfCookie));
	}
	return result;
}

function GetO() {
	var ajax = false;
	try {
		ajax = new ActiveXObject("Msxml2.XMLHTTP");
	} catch(e) {
		try {
			ajax = new ActiveXObject("Microsoft.XMLHTTP");
		} catch(E) {
			ajax = false;
		}
	}
	if (!ajax && typeof XMLHttpRequest != 'undefined') {
		ajax = new XMLHttpRequest();
	}
	return ajax;
}

//更新点击数、下载数、报错数
function handle(module, id) {
	switch(module) {
		case 'hits' :
			var url = serverurl_dohits; break;
		case 'downs' :
			var url = serverurl_dodowns; break;
		case 'errors' :
			var url = serverurl_doerrors; break;
		default:
			var url = '';
	}
	var ajax = GetO();
	var url = url + id + "&" + Math.random();
	ajax.open("Get", url, true);
	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4 && ajax.status == 200) {}
	}
	ajax.send(null)
}

//删除我的收藏
function delmyfav(favstr) {
	var ajax = GetO();
	var url = "/2009/delfav.asp?act=delmusic&musicid=" + favstr + "&" + Math.random();
	ajax.open("Get", url, true);
	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4 && ajax.status == 200) {}
	}
	ajax.send(null)
}

//批量收藏
function plsc(_scurl) {
	login_name = GetCookie("Login", "ccUserName");
	/*
	if (login_name != null) {
		window.tobox("http://my.9ku.com/comm/addfav.asp?act=add&i=" + _scurl + "&" + Math.random(), "_box")
	} else {
		window.tobox("/2009/userlogin.htm?" + _scurl, "_box")
	}
	*/
}
//铃声下载
function lsdown() {
	//window.tobox("http://www.9ku.com/a9/youday/20101227/index.html?webownerId=15105&childid=10000&siteurl=http://www.9ku.com/&songname="+MusicName+"","_lsdown");	
	//window.tobox("http://code.woiring.com/mmscode/MusicSingleRing/04081727560923439869.htm?webownerId=15105&childid=10000&siteurl=http://www.9ku.com/&songname=" + encodeURI(MusicName) + "", "_lsdown");
}
//参数获取
function getQuery(queryStringName) {
	var returnValue = "";
	var URLString = new String(document.location);
	var serachLocation = -1;
	var queryStringLength = queryStringName.length;
	do {
		serachLocation = URLString.indexOf(queryStringName + "\=");
		if (serachLocation != -1) {
			if ((URLString.charAt(serachLocation - 1) == '#') || (URLString.charAt(serachLocation - 1) == '&')) {
				URLString = URLString.substr(serachLocation);
				break;
			}
			URLString = URLString.substr(serachLocation + queryStringLength + 1);
		}
	} while ( serachLocation != - 1 ) if (serachLocation != -1) {
		var seperatorLocation = URLString.indexOf("&");
		if (seperatorLocation == -1) {
			returnValue = URLString.substr(queryStringLength + 1);
		} else {
			returnValue = URLString.substring(queryStringLength + 1, seperatorLocation);
		}
	}
	returnValue = returnValue.replace(/\?/g, '');
	return returnValue;
}
function isNumber(s) { //数字判断函数
	if (s == "") return false;
	var digits = "0123456789";
	var i = 0;
	var sLength = s.length;

	while ((i < sLength)) {
		var c = s.charAt(i);
		if (digits.indexOf(c) == -1) return false;
		i++;
	}

	return true;
}
//相似歌曲部分
var old = "";
function quanxuan(obj) {
	if (old != obj.toString()) {
		document.form.reset();
		old = obj.toString();
	}
	with(document.getElementById(obj)) {
		var ins = getElementsByTagName("input");
		for (var i = 0; i < ins.length; i++) {
			if (i < 100) ins[i].checked = !ins[i].checked;
		}
	}
}
function lbplay() {
	var newx = "",
	firstid = "";
	var ok = false;
	var lens = eval("document.form.Url").length;
	if (lens == undefined) {
		lens = 1
	}
	if (lens > 1) {
		for (var i = 0; i < lens; i++) {

			var temp = eval("document.form.Url[" + i + "]");
			if (temp.checked) {
				if (firstid == "") firstid = temp.value.replace("@", "");
				newx = newx + temp.value.replace("@", "") + "/";
				ok = true;
			}
		}
	} else {
		ok = true;
		newx = document.form.Url.value.replace("@", "") + "/";
	}
	if (ok) {
		if (newx.length > 2009) {
			alert("您选择的太多，请保持在70首以内，以便达到最佳效果！");
		} else {
			if (newx.indexOf("/").length - 1 > 1) {
				window.open(serverurl_play + '#!' + newx);
			} else {
				top.location.href = serverurl_view + '' + newx.split("/")[0] + '';
			}
		}
	} else {
		alert('请选择后再播放');
	}
}
function clkplay(type, id) {
	var url = "";
	var urli = 0;
	var object2;
	var object = getTags(document.getElementById(id), "input");
	if (object.length > 133) {
		objectlength = 133;
	} else {
		objectlength = object.length;
	}
	for (var i = 0; i < object.length; i++) {
		object2 = object[i];
		if (object[i].value.replace("@", "") > 0 && object[i].checked) {
			url += object[i].value.replace("@", "") + "/";
			urli = urli + 1;
		}
	}
	url = url.substring(0, url.length - 1);

	if (type == "playadd") {
		if (url.length > 0) {
			Addplayplay(url);
		} else {
			alert("请选择要添加的歌曲!");
		}
	}
}

function Addplayplay(url) {
	var ifplay = JKSite.Cookie.get("jk_ifplay");
	if (ifplay == "1") {
		var now = new Date();
		var ss = now.getTime();
		now.setTime(ss + 12 * 60 * 60 * 1000); //12 hours
		document.cookie = "jk_addplay=" + (url) + ";domain="+server_domain+";path=/;expires=" + now.toGMTString();
		alert("恭喜，已经添加到连续播放列表！");
	} else {
		window.open(serverurl_play + '' + url + '', "jk_play");
	}
}

//****************************


//document.domain = server_domain;
var addrlocation = document.URL.toLowerCase();
if (addrlocation.indexOf("?qq-pf-to") != -1) addrlocation = addrlocation.split("?qq-pf-to")[0];
var isIE = false; //document.all? true:false;
var DzUrl = "http://mp3.19www.com";
var Dzmp3 = "http://mp3.19www.com";
var meida = {mp3: ""}; //存放歌曲地址
var $song_Lrc = new Array();
var $song_Lrci = new Array();
var $data_Url = new Array();
var id_list = 0;
var id_list_play = 0;
var id_list_fy = 1;
var song_play_id = 0;
var song_gs_id = 0;
var song_qumu_id = 0;
var fast = false;
var maxperpage = 9;
var totalpage = 1,
currentPage = 1;
var $referrer = document.referrer.toLowerCase();
var _url = server_url;
var playlrcid;
var playbfqid;
var isloadlrc, loadlrcnum = 0;
var MusicName = '';
var Singer = '';
var timeupd1=0;
var oldsokey = "";
var isTmpList = 0; //0临时列表,1播放历史
var firstplay="";
var onplay=0;//是否在播放
var checkisplaytime=10;//10秒检测一次是否在播放，建议在10秒以上进行检测
var stval;

var JKSite = new Object();
JKSite.Cookie = {
	set: function(name, value, path) {
		var expires = new Date(new Date().getTime() + (120 * 3600 * 200 * 7));
		var domain = server_domain;
		document.cookie = name + "=" + value + ((expires) ? "; expires=" + expires.toGMTString() : "") + ((path) ? "; path=" + path: "; path=/") + ((domain) ? ";domain=" + domain: "");
	},
	set2: function(name, value, path, time) {
		var expires = new Date(new Date().getTime() + time);
		var domain = server_domain;
		document.cookie = name + "=" + value + ((expires) ? "; expires=" + expires.toGMTString() : "") + ((path) ? "; path=" + path: "; path=/") + ((domain) ? ";domain=" + domain: "");
	},
	get: function(name) {
		var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
		if (arr != null) {
			return unescape(arr[2]);
		}
		return null;
	}
};
function IsNum(s) {
	return (new RegExp(/^(\+|-)?(0|[1-9]\d*)(\.\d*[1-9])?$/).test(s));
}
function formatFloat(src, pos) {
	var gyc = Math.round(src * Math.pow(10, pos)) / Math.pow(10, pos);
	if (src > gyc) {
		return gyc + 1;
	} else {
		return gyc;
	}
}
function $waitbehind(b) {
	document.getElementById('song_list').innerHTML = '<div id="landing"><div id="lan"><div class="h3"><p>' + b + '</p></div></div></div>';
}
function User_landing(b) {
	var html = '<div id="landing"><div id="lan"><div class="h2">会员登录</div>';
	if (b) {
		html += '<div class="h4" id="landing_error">' + b + '</div>';
	}
	html += ' <form action="" method="POST" onSubmit="return User_attestation(this);"><div class="h3">用户名称：  <input name="username" type="text" id="username" value="" size="15" /></div><div class="h3">登录密码： <input name="password" type="password" id="password" value="" size="15" /></div><div class="h5"><input type="submit" id="B12" class="bu ok" value="登 录"/>  <input type="button" class="bu ok" id="B13" onclick="javascript:window.open(\'http://my.9ku.com/reg.htm\',\'_blank\',\'\')" value="注 册"/></div></form></div></div>';
	document.getElementById('song_list').innerHTML = html;
}
function User_attestation(b) {
	if (b.username.value == "") {
		document.getElementById('landing_error').innerHTML = '<span style="color: #009900">注意！</span><br />请输入会员名称。';
		b.username.focus();
		return (false);
	}
	if (b.password.value == "") {
		document.getElementById('landing_error').innerHTML = '<span style="color: #009900">注意！</span><br />请输入登录密码。';
		b.password.focus();
		return (false);
	}
	b.B12.disabled = true;
	b.B13.disabled = true;
	var url = "http://my.9ku.com/UserBoxMini.asp?Action=showmusic&username=" + escape(b.username.value) + "&password=" + escape(b.password.value) + "&s=";
	$waitbehind('正在登录，请稍候....');
	$download(url);
	return (false);
}
function slTop(b) {
	document.getElementById("song_list").scrollTop = b;
}
function ctlent(hf) {
	var ie = (document.all) ? true: false;
	if (ie) {
		if (event.ctrlKey && window.event.keyCode == 13) {
			document.getElementById('bofang').value = '1';
			optlist(3);
			document.getElementById('bofang').value = '0';

		}
	}
}
function song_list(x, fi) {
	x = parseInt(x);
	fi = parseInt(fi);
	eval("id_list_fy=fi");
	if (x == id_list) {
		var listHtml = '';
		var ni = maxperpage;
		var ki = 0;
		if ($song_data[x]) {
			var song_data = $song_data[x].split('$$');
			var n = song_data.length - 1
			var gy = formatFloat(n / ni, 0);
			if (fi > gy) {
				fi = gy;
			}
			if (fi > 1) {
				ki = ni * (fi - 1);
			}
			if (n > ni) {
				ni = ni * fi;
			}
			if (ni > n) {
				ni = n;
			}
			for (var i = ki; i < ni; i++) {
				var song_n = song_data[i].split('|');
				listHtml += '<li id="song_' + song_n[0] + '_p" onMouseOut="pu.MouseOut(this,' + i + ');" onMouseOver="pu.MouseOver(this,' + i + ');"'
				if (song_n[0] == song_play_id && x == id_list_play && timeupd1>0) {
					listHtml += ' class="current"';
				}
				listHtml += '><input name="id" id="id" class="check" type="checkbox" value="' + song_data[i] + '$$' + i + '" />';
				listHtml += '<span class="num">' + (i + 1) + '</span>';
				listHtml += '<a onClick="pu.utils(' + id_list + ',' + i + ',1);" title="歌手：' + song_n[3] + '&#10;歌曲：' + song_n[1] + '" style="CURSOR: pointer" class="playList-songName">' + song_n[1] + '</a>';
				listHtml += '<a onclick="pu.getgsmusic(' + song_n[2] + ',\'' + song_n[3] + '\');" style="cursor:pointer;" class="playList-singerName">' + song_n[3] + '</a>';
				listHtml += '<span class="playListBtn">';
				if (x == 0) {
					listHtml += '<a onClick="pu.DelPlayer(' + i + ');" style="CURSOR: pointer" title="移除" class="playListBtn-delete">移除</a>';
				} else {
					var gqidAndName=song_n[0]+"|"+song_n[1].replace("(已下线)","")+"|";
					if ($song_data[0].indexOf(gqidAndName) == -1) {
					//if ($song_data[0].indexOf(song_data[i] + '$$') == -1) {
						listHtml += '<a class="playListBtn-add" onClick="pu.$AddPlayer(\'' + song_data[i] + '$$\');this.className=\'playListBtn-add-ok\';this.title=\'该歌曲已经在临时列表了\';" style="CURSOR: pointer" title="添加到“临时列表”"></a>';
					} else {
						listHtml += '<a class="playListBtn-add-ok" href="javascrit:void(0)" title="该歌曲已经在临时列表了"></a>';
					}
				}
				listHtml += '</span></li>';
			}
			if (x == 3 && totalpage > 1) {
				//搜索其他页
				listHtml += '<li class="turn-page">';
				if (currentPage == 1) {
					listHtml += '<a href="javascript:void(0);">上一页</a>';
				} else {
					listHtml += '<a onClick="searchnextpage(' + (currentPage - 1) + ');slTop(0)" style="CURSOR: pointer">上一页</a> ';
				}
				listHtml += '<span class="page_total">' + currentPage + '/' + totalpage + '</span>';
				if (currentPage == totalpage) {
					listHtml += '<a href="javascript:void(0);">下一页</a>';
				} else {
					listHtml += '<a onClick="searchnextpage(' + (currentPage + 1) + ');slTop(0)" style="CURSOR: pointer">下一页</a>';
				}
				listHtml += '</li>';
				//搜索页结束
			} else {
				if (gy != 1) {
					listHtml += '<li class="turn-page">';
					if (fi == 1) {
						listHtml += '<a href="javascript:void(0);">上一页</a>';
					} else {
						listHtml += '<a onClick="song_list(' + x + ',' + (fi - 1) + ');slTop(0)" style="CURSOR: pointer">上一页</a>';
					}
					listHtml += '<span class="page_total">' + fi + '/' + gy + '</span>';
					if (fi == gy) {
						listHtml += '<a href="javascript:void(0);">下一页</a>';
					} else {
						listHtml += '<a onClick="song_list(' + x + ',' + (fi + 1) + ');slTop(0)" style="CURSOR: pointer">下一页</a>';
					}
					listHtml += '</li>';
				} else {
					//临时列表广告
					if (n <= 5) {
						listHtml += "<div id='p_list5' style='text-align:center;'></div>";
					}
				}
			}
			document.getElementById('song_list').innerHTML = listHtml;

		} else {
			document.getElementById('song_list').innerHTML = '<p class="el">正在加载歌曲列表，请稍候。</p>';
		}
	}
	if (x == 3) {
		var qString = new String(document.getElementById('ww').value);
		var keywords = qString.split(" ");
		/*setAllColor('light', keywords, 'CC0033');*/
	}
}
function searchnextpage(pages) {
	currentPage = pages;
	optlist(3);
	return (false);
}
function $download(URL) {
	if (URL == undefined) return;
	Tag = document.createElement("script");
	Tag.type = "text/javascript";
	Tag.src = URL;
	document.getElementsByTagName("head")[0].appendChild(Tag);
}
function so_jk() {
	var select1 = "default"; //document.getElementById('select').value;
	if (select1 != "default") {
		var key = document.getElementById('ww').value;
		var bofang = document.getElementById('bofang').value;
		window.open("http://baidu.9ku.com/s.aspx?k=" + escape(key), "_blank");
		return (false);
	}
	optlist(3);
	return (false);
}
function optlist(n) {
	slTop(0);
	var num = new Date().getTime();
	this._download1 = function(st) {
		if (st == id_list_play) {
			song_list(st, formatFloat((song_qumu_id + 1) / 50, 0));
		} else {
			song_list(st, 1);
		}
	};
	this._download2 = function(st, URL) {
		if (!$song_data[st]) {
			$data_Url[st] = '';
		}
		if (st > 0 && $data_Url[st] != URL) {
			$waitbehind('获取歌曲数据 请稍候...');
			$download(URL);
			eval("$data_Url[st]=URL");
		} else {
			this._download1(st);
		}
	};
	this.download_data = function(st, URL) {
		if (st == 1) {
			if (st != id_list_play) {
				this._download2(st, URL);
			} else {
				this._download1(st);
			}
		} else {
			this._download2(st, URL);
		}
	};
	document.getElementById('song_list').innerHTML = '';
	if (document.getElementById(id_list + '_list')) document.getElementById(id_list + '_list').className = "";
	document.getElementById(n + '_list').className = "current";
	if (n != id_list_play) {
		if (document.getElementById(id_list_play + '_list')) document.getElementById(id_list_play + '_list').className = "";
	}
	eval("id_list=n");
	if (n == 0) {
		song_list(0, 1);
	} else if (n == 1) {
		var url = "dd";
	} else if (n == 3) {
		var key = document.getElementById('ww').value;
		var bofang = document.getElementById('bofang').value;
		if (key == "" || key == "输入关键字进行搜索") {
			document.getElementById('song_list').innerHTML = '<p class="el"><span style="color: #FF0000">请输入关键字 ！</span></p>';
			return (false);
		}
		if (oldsokey == "") oldsokey = key;
		if (oldsokey != key) {
			oldsokey = key;
			currentPage = 1
		}
		var url = "http://baidu.9ku.com/SearchApiHandle.ashx?action=song3&key=" + escape(key) + "&page=" + currentPage;
	} else if (n == 4) {
		var url = list_song_js + "&op=rnddata"; //网友推荐,随机
	} else if (n == 5) {
		var url = list_song_js + "&op=newdata"; //新歌速递
	} else if (n == 6) {
		var url = list_song_js + "&op=hotdata"; //热门歌曲
	} else if (n == 90 && song_gs_id > 0) {
		var url = _url + "html/gsjs/" + song_gs_id + ".js";
	}
	var x_list = '';
	var c_list = '<label class="allXuan" id="allXuan" style="cursor:pointer;"><input name="allXuan" onclick="clk(\'reverse\',\'song_list\');" class="check" type="checkbox"> 全选/反选</label>';
	c_list += '';
	//c_list += '<input type="button" class="bu ok" ';
	/*if(n==0){c_list += 'value="删除播放" title="将选择删除" onclick="clk(\'playdel\',\'song_list\');';
}else{
	if(n==1){
		c_list += 'value="删除所选" title="删除选中的歌曲" onclick="clk(\'delsel\',\'song_list\');';
	}else{
		c_list += 'value="加入列表" title="将选择的歌曲加入“临时列表”" onclick="clk(\'playadd\',\'song_list\');';
	}
}
c_list += '"'+x_list+'/>';*/
	if (n == 0) {
		/*c_list += '<input type="button" class="bu ok" value="批量收藏" title="快速批量收藏歌曲" onclick="clk(\'plsc\',\'song_list\');"'+x_list+'/>';*/
	}
	if (n == 1) {
		c_list += '<input type="button" class="bu ok" value="清空所有" title="清理所有历史播放记录" onclick="delcok();"' + x_list + '/>';
	}
	if (n == 2) {
		/*c_list += '<input type="button" class="bu ok" value="删除收藏" title="删除我收藏的歌曲" onclick="clk(\'delfav\',\'song_list\');"'+x_list+'/>';*/
	}
	if (n > 2) c_list += '<a onclick="clk(\'playadd\',\'song_list\');" class="allAdd" style="cursor:pointer;">加入列表</a>';
	c_list += '<a onclick="clk(\'play\',\'song_list\');" class="allPlay" style="cursor:pointer;">全部播放</a>';
	if(n==0) c_list += '<input type="button" class="button" value="删除播放" title="将选择删除" onclick="clk(\'playdel\',\'song_list\');" />';
	
	document.getElementById('control_list').innerHTML = c_list;
	if (url == "dd") {
		isTmpList = 1
		//music_list(document.cookie);
		var str = document.cookie;
		var num_start = str.indexOf("l_music");
		if (num_start != -1) {
			var num_end = str.indexOf("l_end");
			if (num_end > num_start) {
				var str_list = str.substring(num_start, num_end).replace(/l_music=/ig, "");
				var arr_list = str_list.split(",");
				if (arr_list.length > 0) {
					for (i = 0; i < arr_list.length; i++) {
						var str_ti = arr_list[i];
						if (str_ti != undefined || str_ti != "undefined" || str_ti != null || str_ti != "") {
							var str_ti_path = parseInt(str_ti / 10000 + 1);
							var url = _url + data_song_js + str_ti + '';
							this.download_data(n, url);
						}
					}
				}
			} else {
				$song_data[1] = '';
			}
		} else {
			$song_data[1] = '';
		}
	} else {
		this.download_data(n, url);
	}
	return (false);
}
function play_optlist(n)
{
	bang=n;
	document.getElementById(id_list_play + '_list').className = "";
	optlist(n);
	cl = setTimeout("checkloaded();", 5000);  // 检测加载列表的时间
	try { pu.utils(n,0,1); } catch(e) {}
}
function PlayerUtils() {
	var p = 0;
	var song_id;
	var song_u = new Array();
	var rnd_id = 0;
	var song_u_1;
	var total = 0;
	var lrctimea = 8888888;
	var Stat_drag = 0;
	var Stat_Time = 0;
	var Stat_inn = '-1';
	var Stat_num = '';
	var utils_s = 0;
	var stnum = 1190351137140;
	var stnumk = 0;
	var ding1 = 0;
	var cai1 = 0;

	this.showPlayingTime = function(seconds) {
		var minute = parseInt(seconds / 60);
		var second = parseInt(seconds - minute * 60);
		if (minute < 10) {
			minute = '0' + minute;
		}
		if (second < 10) {
			second = '0' + second;
		}
		return minute + ':' + second;
	};

	this.MouseOver = function(c, d) {
		c.style.backgroundColor = "#FFF5CA";
	};
	this.MouseOut = function(c, d) {
		c.style.backgroundColor = "";
	};
	this.showDelIcon = function(c, d) {
		c.getElementsByTagName("li")[2].style.display = "block";
	};
	this.hideDelIcon = function(c, d) {
		c.getElementsByTagName("li")[2].style.display = "none";
	};
	this.DelPlayer = function(d) {
		d += '/';
		var x = d.split('/');
		var c = $song_data[0];
		for (var i = 0; i < x.length - 1; i++) {
			c = c.replace($song_data[0].split('$$')[parseInt(x[i])] + '$$', '');
			if (p >= parseInt(x[i])) {
				p = p - 1;
			}
		}
		$song_data[0] = c;
		total = c.split('$$').length - 1;
		song_list(0, id_list_fy);
	};
	this.DelPlayer_cok = function(d) {
		d += '/';
		var x = d.split('/');
		var c = $song_data[1];
		for (var i = 0; i < x.length - 1; i++) {
			c = c.replace($song_data[1].split('$$')[parseInt(x[i])] + '$$', '');
			if (p >= parseInt(x[i])) {
				p = p - 1;
			}
		}
		$song_data[1] = c;
		total = c.split('$$').length - 1;
		song_list(1, id_list_fy);
	};
	this.DelPlayer_fav = function(d) {
		d += '/';
		var x = d.split('/');
		var c = $song_data[2];
		for (var i = 0; i < x.length - 1; i++) {
			c = c.replace($song_data[2].split('$$')[parseInt(x[i])] + '$$', '');
			if (p >= parseInt(x[i])) {
				p = p - 1;
			}
		}
		$song_data[2] = c;
		total = c.split('$$').length - 1;
		song_list(2, id_list_fy);
	};
	this.$AddPlayer = function(cli) {
		if (cli) {
			if (total == 1 && id_list_play == 0) {
				document.getElementById('Playleixin').value = 0;
			}
			var dc = $song_data[0];
			if ($song_data[0].indexOf(cli) == -1) $song_data[0] = dc + cli;
			total = $song_data[0].split('$$').length - 1;
			if (id_list == 0) {
				optlist(0);
			}
		}
	};
	this.AddPlayer = function(id) {
		isTmpList = 0
		if (id) {
			JKSite.Cookie.set("jk_addplay", "", "/");
			id = "@0/" + id + "/0@";
			var song_data = $song_data[0].split('$$');
			for (var i = 0; i < song_data.length; i++) {
				id = id.replace('/' + song_data[i].split('|')[0] + '/', '/');
			}
			id = id.replace('/0@', '').replace('@0/', '').replace('@0', '');
			if (id) {
				var idss = id.split("/");
				for (var is = 0; is < idss.length; is++) {
					var idss_path = parseInt(idss[is] / 10000 + 1);
					$download(_url + data_song_js + idss[is] + '')
				}
			}
		}
	};
	this.$AddPlayer_cok = function(cli) {
		if (cli) {
			var dc = $song_data[1];
			if ($song_data[1].indexOf(cli) == -1) $song_data[1] = cli + dc;
		}
	};
	this._Stat_go = function() {
		Stat_drag = 0;
		Stat_Time = 0;
		Stat_num = new Date().getTime();
		if (Stat_inn == '-1') {
			Stat_inn = 0;
			pu.Stat_go();
		}
		Stat_inn = 0;
	};
	this.Stat_go = function() {
		if (90 == Stat_inn && Stat_drag == 0) {
			var num = new Date().getTime();
		}
	};
	//播放完下一首
	this.playnextsong = function() {
		clearInterval(stval);
		var leixin = document.getElementById('Playleixin').value;
		if (leixin == 0) {
			this.PlayNext(1);
		} else if (leixin == 1) {
			this.PlayNext(0);
		} else if (leixin == 2) {
			this.utils(id_list_play, rnd_id, 0);
		}
	};
	this.ssPlay = function() {
		fast = false;
		try {
			var leixin = document.getElementById('Playleixin').value;
		} catch(e) {
			return (false);
		}
		if (leixin == 2) {
			if (rnd_id >= total - 1) {
				rnd_id = 0;
			} else {
				rnd_id++;
			}
		}
		this.AddPlayer(JKSite.Cookie.get("jk_addplay"));
		//if(stnumk>2){
		//}else{
		playbfqid = setTimeout("pu.ssPlay()", 200);
		//}
	};
	//暂停
	this.pause = function() {
		try {
			document.getElementById('mediaPlayerObj').controls.pause();
		} catch(e) {
			return (false);
		}
	}
	//播放按钮
	this.play = function() {
		try {
			document.getElementById('mediaPlayerObj').controls.play();
		} catch(e) {
			return (false);
		}
	}
	//最大音量
	this.maxvolume = function() {
		try {
			document.getElementById('mediaPlayerObj').settings.volume = 100;
			$('.jp-volume-bar-value').css('width', '62px');
			$('.jp-unmute').hide();
			$('.jp-mute').show();
		} catch(e) {
			return (false);
		}
	}
	//静音
	this.mute = function() {
		try {
			document.getElementById('mediaPlayerObj').settings.volume = 0;
			$('.jp-volume-bar-value').css('width', '0px');
			$('.jp-unmute').show();
			$('.jp-mute').hide();
		} catch(e) {
			return (false);
		}
	}
	//取消静音
	this.unmute = function() {
		try {
			var d = $('.jp-volume-bar-value').attr('vol');
			document.getElementById('mediaPlayerObj').settings.volume = d;
			$('.jp-volume-bar-value').css('width', d + 'px');
			$('.jp-unmute').hide();
			$('.jp-mute').show();
		} catch(e) {
			return (false);
		}
	}
	this._Next = function(t) {
		p += t;
		if (p > (total - 1)) {
			p = 0;
		} else if (p < 0) {
			p = (total - 1);
		}
		this.utils(id_list_play, p, 0);
	};
	this.PlayNext = function(t) {
		clearInterval(stval);
		this._Next(t);
	};
	this.utils = function(t, r, s) {
		clearInterval(stval);
		clearTimeout(playlrcid);
		playlrcid = setTimeout("pu.PlayLrc()", 200);
		if (fast) {
			return (false);
		}
		fast = true;
		if (t != id_list) {
			document.getElementById(t + '_list').className = "current";
		}
		if (t != id_list_play) {
			document.getElementById(id_list_play + '_list').className = "";
		}
		if ($song_data[t]) {
			var song_data = $song_data[t].split('$$');
			total = song_data.length - 1;
			song_u = song_data[r].split('|');
			var num = new Date().getTime();
			var url = song_u[4];
			firstplay=url;
			if (url.indexOf("http://") != -1) {
				Media_wma(url);
			} else {
				Media_wma(Dzmp3 + url);
			}
			if ((num - stnum) > 3000) {
				stnumk = 0;
			} else {
				stnumk++;
			}
			stnum = num;
			song_u_1 = song_u[1];
			if (document.getElementById('play_title')) document.getElementById('play_title').innerHTML = '歌曲：<a href="' + serverurl_play + song_u[0] + '" target="_blank">' + song_u[1] + '</a>&nbsp;&nbsp;歌手：<a href="' + serverurl_artist + song_u[2] + '" target="_blank">' + song_u[3] + '</a>';

			if (document.getElementById('botmusic')) document.getElementById('botmusic').innerHTML = '<a href="' + serverurl_artist + song_u[2] + '" target="_blank">' + song_u[3] + '</a>歌曲<a href="' + serverurl_play + song_u[0] + '" target=_blank >《' + song_u[1] + '》</a>';

			if (document.getElementById('seegc')) document.getElementById('seegc').innerHTML = '<a href="' + serverurl_lrctxt + song_u[0] + '" target="_blank">查<br>看<br>歌<br>词</a>';

			if (document.getElementById('play_musicname')) document.getElementById('play_musicname').innerHTML = '<a href="' + serverurl_play + song_u[0] + '" target="jk_play">' + song_u[1] + '</a>';

			if (gqzhuangtai == "1") {
				if (document.getElementById('play_title')) document.getElementById('play_title').innerHTML = '歌曲：' + song_u[1] + '';
			}

			Singer = song_u[3];
			MusicName = song_u[1];
			//if(s!=2&&s!=3){}
			document.title = song_u[1] + ' ' + song_u[3] + ' - mp3下载 - 歌曲试听 永久网络音乐盒';

			try {
				if (song_u[0] != song_play_id || addrlocation.indexOf("play.htm") > 0) {

					//读取喜欢歌曲的人也喜欢和您可能喜欢TA们的歌曲
					if (document.getElementById("singergequ") || document.getElementById("simlarsinger")) {
						$.ajax({
							type: "GET",
							url: "/html/playmusic/" + song_u[0] + ".js",
							data: "",
							success: function(res) {
								if(res!=""){
									var result = eval("(" + res + ")");
									if (document.getElementById("singergequ")) {
										document.getElementById("singergequ").innerHTML = result.data0;
										$("#singergequ").show();
									}
									if (document.getElementById("simlarsinger")) {
										document.getElementById("simlarsinger").innerHTML = result.data1;
										$("#simlarsinger").show();
	
									}
								}
							}
						});
					}
				}
			} catch(e) {}

			p = r;
			utils_s = s;
			this._Stat_go();
			this.changeBg(r, t, song_id, song_u[0]);
			this.downloadlrc(song_u[0]);
			this.listBg(t, id_list, id_list_play);
			//if(nolrc==false){pu.PlayLrc();}
			//cokstr=song_u[0]+"|"+encodeURI(song_u[1])+"|"+song_u[2]+"|"+encodeURI(song_u[3])+"|"+song_u[4]+"";
			cokstr = song_u[0];
			write_cookie(document.cookie); //this.Playerhistory(song_u[0]);
			/*if(song_id!=song_u[0]){song_id=song_u[1];}*/
			song_id = song_u[0];
			eval("song_qumu_id=r");
			eval("song_play_id=song_id");
			eval("id_list_play=t");
			eval("song_gs_id=" + song_u[2]);
			if (t == id_list) {
				var gy = formatFloat((r + 1) / maxperpage, 0);
				//song_list(t,gy);
			}
		}
		clearInterval(stval);
		stval = setInterval(function(){
			try {
				//if($('#kuPlayer').data('jPlayer').status.seekPercent<=0) { }
				if($('#kuPlayer').data('jPlayer').status.currentTime<=0) {
					/*pu.PlayNext(1);*/ pu.playnextsong(); handle('errors',song_play_id); 
				} else {
					clearInterval(stval);
				}
			} catch(e) {}
		}, 5000);
	};
	this.getgsmusic = function(gsid, gsname) {
		eval("song_gs_id=" + gsid);
		//document.getElementById("90_list").innerHTML="<div style='width:56px;height:20px;overflow:hidden;text-overflow:ellipsis;'>"+gsname+"</div>";
		optlist(90);
	};
	this.changeBg = function(r, t, song_id, song_u) {
		if (t == id_list) {
			var gy = formatFloat((r + 1) / maxperpage, 0);
			if (gy == 1) {
				xx = (r - 0);
			} else {
				xx = r - (maxperpage * (gy - 1));
			}
			if (gy == id_list_fy) {
				slTop((xx - 5) * 22);
			}
			if (gy != formatFloat((song_qumu_id + 1) / maxperpage, 0)) {
				song_list(t, gy);
			}
			if (document.getElementById('song_' + song_id + '_p')) {
				document.getElementById('song_' + song_id + '_p').className = "";
			}
			if (document.getElementById('song_' + song_u + '_p')) {
				document.getElementById('song_' + song_u + '_p').className = "current";
			}
		}
	};
	this.listBg = function(t, a, b) {
		if (t != a) {
			document.getElementById(t + '_list').className = "current";
		}
		if (t != b) {
			if (document.getElementById(id_list_play + '_list')) document.getElementById(id_list_play + '_list').className = "";
		}
	};
	this.Playerhistory = function(id) {
		var flag = id + ',';
		var history = ',' + JKSite.Cookie.get("Playerhistory") + ',';
		history = history.replace(',' + flag, ",");
		history = flag + history;
		var history_data = history.split(',');
		if (history_data.length) {
			var historyc = "";
			var n = 0;
			for (var i = 0; i < history_data.length; i++) {
				if (IsNum(history_data[i]) && 50 > n) {
					historyc += history_data[i] + ',';
					n++;
				}
			}
			historyc = historyc.substring(0, historyc.length - 1);
			JKSite.Cookie.set("Playerhistory", historyc, "/play");
		}
	};
	this.led = function(s2, s3, s4, s5, s6) {
		document.getElementById("LR2").innerHTML = s2;
		document.getElementById("LR3").innerHTML = s3;
		document.getElementById("LR4").innerHTML = s4;
		document.getElementById("LR5").innerHTML = s5;
		document.getElementById("LR6").innerHTML = s6;
	};
	this.downloadlrc = function(t) {
		if (!IsNum(t)) return;
		if (!$song_Lrc[t]) {
			this.led('', '', '正在载入歌词...', '', '');
			//$download(_url + 'html/lrc/' + tfolder + '/' + t + '.js');
			$download(serverurl_lrcjs + t + '');
			isloadlrc = setTimeout("pu.checklrcisload(" + t + ")", 3000);
		}
		lrctimea = 8888888;
	};
	this.doSp = function(oT) {
		if (parseInt(oT) > 1) {
			var zongsj = $(".jp-duration").html();
			var b1 = zongsj.split(":");
			var b2 = b1[0];
			var b3 = b1[1];
			b2 = parseFloat(b2);
			b3 = parseFloat(b3);
			var b4 = b2 * 60 + b3;
			b4 = parseFloat(b4);
			var rat = 285 / b4;
			rat = parseFloat(rat);
			$("#kuPlayer").jPlayer("playHead", 100 * (oT / 285 * rat));
		}
	};
	this.lrci = function(xi, i) {
		if (_l[i]) {
			var l_path = _t[i] / 1000;
			document.getElementById(xi).innerHTML = '<a style="CURSOR: pointer" onclick="pu.doSp(\'' + l_path + '\')" >' + _l[i] + '</a>';
		} else {
			document.getElementById(xi).innerHTML = '';
		}
	};
	this.PlayLrc = function() {
		var nolrc = false;
		if ($song_Lrc[song_id]) {
			try {
				var bfqtime = $(".jp-current-time").html();
				var b1 = bfqtime.split(":");
				var b2 = b1[0];
				var b3 = b1[1];
				b2 = parseFloat(b2);
				b3 = parseFloat(b3);
				var b4 = b2 * 60 + b3;
				b4 = parseFloat(b4);
				var curTime = b4 * 1000 + 500;
			} catch(e) {
				var curTime = 0;
			}
			if (Stat_drag == 1 || curTime - 1000 > Stat_Time || Stat_Time > curTime) {
				Stat_drag = 1;
			} else {
				Stat_drag = 0;
			}
			Stat_Time = curTime;
			if ($song_Lrc[song_id] == 0) {
				if (8888888 == lrctimea) {
					this.led('', '<span style="color: #FF9900">歌手：' + song_u[3] + '</span>', '<span style="color: #FF9900">歌曲：' + song_u[1] + '</span>', '没找到相关歌词', '');
				}
				lrctimea = 0;
				nolrc = false;
			} else if ($song_Lrc[song_id].indexOf("文本歌词[/]") != -1) {
				lrctimea = 0;
				nolrc = true;
				try {
					document.getElementById("lyric").style.display = "none";
					document.getElementById("txtword").style.display = "block";
					if ($song_Lrc[song_id].indexOf("暂时找不到") != -1) {
						//...
					}
					var textword = $song_Lrc[song_id].split("[/]")[1].replace('　', '');
					textword = textword.replace(/\&lt;/g, "<");
					textword = textword.replace(/\&gt;/g, ">");
					textword = textword.replace(/\&nbsp;/g, " ");
					document.getElementById("txtword").innerHTML = textword;
				} catch(e) {
					return (false);
				}
				if ($song_Lrc[song_id].indexOf("暂时找不到这首歌曲的歌词") != -1) {
					document.getElementById("txtword").style.overflowY = "hidden";
				} else {
					document.getElementById("txtword").style.overflowY = "";
				}
			} else if ($song_Lrc[song_id].length > 0) {
				nolrc = false;
				try {
					document.getElementById("lyric").style.display = "block";
					document.getElementById("txtword").style.display = "none";
				} catch(e) {
					return (false);
				}
				if (8888888 == lrctimea) {
					if ($song_Lrci[song_id]) {} else {
						var lrc1 = $song_Lrc[song_id].split("[");
						var array = [];
						for (var i = 0; i < lrc1.length; i++) {
							var g = {};
							var t = lrc1[i].split("]");
							g.time = jtime(t[0]);
							if (isNaN(g.time)) continue;
							g.c = t[1];
							if (g.c == "") g.c = getnext(i, lrc1);
							array.push(g);
						}
						array.sort(function(x, y) {
							if (x.time > y.time) return 1;
							else if (x.time < y.time) return - 1;
							else return 0;
						});
						$song_Lrci[song_id] = array;
					}
					var tin = "";
					var tim = "";
					if ($song_Lrci[song_id]) {
						var array1 = $song_Lrci[song_id];
						for (var i = 0; i < array1.length; i++) {
							var g = array1[i];
							if (!g.c) {
								g.c = "";
							}
							if(g.c==".")g.c="";
							tin += g.time;
							tim += g.c;
							if (i < array1.length - 1) {
								tin += ",";
								tim += "[n]";
							}
						}
					}
					var timeH = '0,0,0,0,' + tin + ',8888888';
					var TxtH = '歌手：' + song_u[3] + '[n]歌曲：' + song_u[1] + '[n][n]' + tim + '[n]';
					_t = timeH.split(",");
					_l = TxtH.split('[n]');
				}
				for (var i = 0; i < _t.length; i++) {
					if (_t[i] < curTime && curTime < _t[i + 1] || 8888888 == lrctimea) {
						if (lrctimea != i) {
							this.lrci("LR2", i - 2);
							this.lrci("LR3", i - 1);
							this.lrci("LR4", i);
							this.lrci("LR5", i + 1);
							this.lrci("LR6", i + 2);
						}
						lrctimea = i;
					}
				}
			} else {
				if (8888888 == lrctimea) {
					this.led('', '', '载入歌词失败！', '', '');
					lrctimea = 0;
					nolrc = false;
				}
			}
		}
		if (nolrc) {
			clearTimeout(playlrcid);
		} else {
			playlrcid = setTimeout("pu.PlayLrc()", 100);
		}
	};
	this.checklrcisload = function(t) {
		if (!$song_Lrc[t]) {
			loadlrcnum += 1;
			if (loadlrcnum > 3) {
				loadlrcnum = 0;
				clearTimeout(isloadlrc);
			} else {
				//this.led('','','正在加载歌词....','','');
				isloadlrc = setTimeout("pu.downloadlrc(" + t + ")", 3000);
			}
		} else {
			loadlrcnum = 0;
			clearTimeout(isloadlrc);
		}
	};
	this.diange = function() {
		//window.clipboardData.setData("text","歌曲ID: "+song_id+"");alert("网址复制成功啦!");
	};
	this.downmp3 = function() { }
	this.share = function() {}
}
function Media_wma(src) {
	meida.mp3 = src;
	if(isqq==-1){
		$("#kuPlayer").jPlayer("setMedia", meida).jPlayer("play");
	}else{
		if (isqq == 1 && mygqstat == 0) {
			$("#kuPlayer").jPlayer("setMedia", meida).jPlayer("play");
			document.getElementById("mydiv2").style.display = "none";
			document.getElementById("mydiv1").style.display = "block";
		}	
	}
}
function getnext(y, lrc) {
	var result = "";
	var i = y + 1;
	if (lrc[i]) {
		t = lrc[i].split("]");
		if (t[1] == "") result = getnext(i, lrc);
		else result = t[1]
	}
	return result;
};

function jtime(tn) {
	var time = 0;
	var ta = tn.split(":");
	if (ta.length < 2) return time;

	if (ta[1].indexOf(".") > 0) {
		var tb = ta[1].split(".");
		time = ta[0] * 60 * 1000 + tb[0] * 1000 + tb[1] * 10;
	} else time = ta[0] * 60 * 1000 + ta[1] * 1000;
	return time;
}
//####################################################
function epen(URL, W, H) {
	var Ws = screen.availWidth / 2;
	var Hs = screen.availHeight / 2;
	window.open(URL, "showDlg", "scrollbars=yes,width=" + W + ",height=" + H + ",top=" + (Hs - (H / 1.5)) + ",left=" + (Ws - (W / 2)) + ",");
}
function epen2(URL) {
	window.open(URL)
}
//登录后执行
function UserCollect2(){
	$.close('popLogin');
	login_name = dlGetCookie("nickname");
	loginid = GetCookie("Login","uid");
	if(loginid!=""&&loginid!=null){
		LoginYes(loginid,login_name);
		//再次调用顶部下拉菜单
		$(function(){
			$(".top-user li.down-menu").hover(function () { $(this).toggleClass("hover"); })
		});
		asyncbox.open({
			id: "gdscbox",
			title: '收藏歌曲到指定的歌单',
			url: 'http://my.9ku.com/box/gedan.asp?refer=goplay&gqid=' + song_play_id,
			width: 500,
			height: 350,
			modal: true,
			btnsbar: $.btn.close
		});
	}
}
//登录前执行
function UserCollect3(){
	LoginNo();
	asyncbox.open({		
		id:"popLogin", 
		modal:true,
		drag:true,
		title:"登录九酷",
		width:500,
		height:268, 
		html:'<form name="myform" method="post" action="http://my.9ku.com/login/ChkLogin.asp" target="denglubox" id="myform"><input type="hidden" name="action" value="Login" /><input type="hidden" name="backurl" value="goplay" /><div class="popLoginForm form-box"><div class="fl"><ul><li class="form-item clearfix"><label class="label">帐户：</label><input type="text" class="input" id="username" name="username" /></li><li class="form-item clearfix"><label class="label">密码：</label><input type="password" class="input" id="passwd" name="passwd" /></li><li class="form-item clearfix remember "><label class="label">&nbsp;</label><label for="autologin"><input type="checkbox" id="autologin" name="autologin" value="1" /> 下次自动登录</label>&nbsp;&nbsp;&nbsp;<a href="http://my.9ku.com/reg/getpass.asp" target="_blank" class="no-pass">忘记密码</a></li></ul><div class="popLoginBtn"><a href="javascript:void(0);" id="popLoginBtn">登录</a></div></div><div class="fr"><h4><a href="http://my.9ku.com/reg/" target="_blank">还没有注册？</a></h4><a href="http://my.9ku.com/reg/" target="_blank" class="go-regedit"></a></div></div></form><iframe name="denglubox" width="0" height="0" style="display:none;"></iframe>',		
		callback : function(action){}		
	});	
	//点击登录按钮      
	$("#popLoginBtn").click(function(){
		var u=$("#username").val();
		var p=$("#passwd").val();
		if($("#autologin").attr("checked")){
			var a=$("#autologin").val();
		}else{
			var a="";	
		}
		if(u==""||p==""){
			alert("请输入账户和密码！");
			return;	
		}
		document.myform.submit();
	});
}
//点收藏
function UserCollect(obj) {
	return ;
	login_name = dlGetCookie("nickname");
	loginid = GetCookie("Login","uid");
	if(loginid!=""&&loginid!=null){
		UserCollect2();
	}else{
		UserCollect3();
	}
}
function getTags(parentobj, tag) {
	if (parentobj == null) {
		return new Array();
	} else if (typeof parentobj.getElementsByTagName != 'undefined') {
		return parentobj.getElementsByTagName(tag);
	} else if (parentobj.all && parentobj.all.tags) {
		return parentobj.all.tags(tag);
	} else {
		return new Array();
	}
}
function clk(type, id) {
	var url = "";
	var urlcok = "",
	urlfav = "";
	var urli = 0;
	var object = getTags(document.getElementById(id), "input");
	if (object.length > 133) {
		objectlength = 133;
	} else {
		objectlength = object.length;
	}
	for (var i = 0; i < object.length; i++) {
		if (type == "all") {
			if (objectlength > i) {
				object[i].checked = true;
			} else {
				object[i].checked = false;
			}
		} else if (type == "reverse") {
			if (object[i].checked) {
				object[i].checked = false;
			} else {
				object[i].checked = true;
			}
		} else if (type == "delsel") {
			if (type == "delsel") {
				var url_value = object[i].value.split('$$')[1];
				var url_value_cok0 = object[i].value.split('$$')[0].split("|")[0];
				var url_value_cok1 = object[i].value.split('$$')[0].split("|")[1];
				var url_value_cok2 = object[i].value.split('$$')[0].split("|")[2];
				var url_value_cok3 = object[i].value.split('$$')[0].split("|")[3];
				var url_value_cok4 = object[i].value.split('$$')[0].split("|")[4];
			}
			if (object[i].checked) {
				url += url_value + "/";
				urli++;
				urlcok += url_value_cok0 + ",";
			}
		} else if (type == "delfav") { //收藏删除
			if (type == "delfav") {
				var url_value = object[i].value.split('$$')[1];
				var url_value_fav0 = object[i].value.split('$$')[0].split("|")[0];
			} else {
				var url_value = object[i].value.split('|')[0];
			}
			if (object[i].checked) {
				url += url_value + "/";
				urli++;
				urlfav += url_value_fav0 + "-";
			}
		} else {
			if (type == "playdel") {
				var url_value = object[i].value.split('$$')[1];
			} else {
				var url_value = object[i].value.split('|')[0];
			}
			if (url_value > -1 && object[i].checked) {
				url += url_value + "/";
				urli++;
			}
		}
	}
	url = url.substring(0, url.length - 1);
	if (type == "play") {
		if (url.length == 0) {
			alert("请选择歌曲!");
		} else if (urli > 80) {
			alert("连续播放不能超过80首!");
		} else {
			if (url.indexOf("/") != -1) {
				window.open(serverurl_play + '#!' + url);
			} else {
				top.location.href = serverurl_view + '' + url + '';
			}
		}
	}
	if (type == "playadd") {
		if (url.length == 0) {
			alert("请选择添加歌曲!");
		} else {
			var ifplay = JKSite.Cookie.get("jk_ifplay");
			if (ifplay == "1") {
				JKSite.Cookie.set("jk_addplay", url, "/");
				alert("已经添加到“临时列表”！");
			} else {
				if (url.indexOf("/") != -1) {
					window.open(serverurl_play + '#!' + url);
				} else {
					top.location.href = serverurl_view + '' + url + '';
				}
			}
		}
	}
	if (type == "playdel") {
		if (url.length == 0) {
			alert("请选择删除歌曲!");
		} else {
			pu.DelPlayer(url);
		}
	}
	if (type == "delsel") {
		if (url.length == 0) {
			alert("请选择删除歌曲!");
		} else {
			pu.DelPlayer_cok(url);
			urlcok = urlcok.substring(0, urlcok.length - 1);
			del_list(urlcok);
		}
	}
	if (type == "delfav") {
		if (url.length == 0) {
			alert("请选择删除歌曲!");
		} else {
			if (confirm("真的要删除您收藏的歌曲吗？")) {
				pu.DelPlayer_fav(url);
				delmyfav(urlfav);
			}
		}
	} //收藏删除
	if (type == "plsc") {
		if (url.length == 0) {
			alert("请选择歌曲!");
		} else {
			if (confirm("真的要批量收藏所选歌曲吗？")) {
				plsc(url);
			}
		}
	} //批量收藏
}
JKSite.Cookie.set("jk_ifplay", "1", "/"); (function() { (function(el, cb) {
		if (el.addEventListener) {
			el.addEventListener('unload', cb, false);
		} else if (el.attachEvent) {
			el.attachEvent('onunload', cb);
		}
	})(window,
	function() {
		if (window.sidebar) {
			JKSite.Cookie.set("jk_ifplay", "0", "/");
		} else if (window.external) {
			JKSite.Cookie.set("jk_ifplay", "0", "/");
		} else if (window.opera && window.print) {
			return true;
		}
	});
})();

//判断是否清空过
var yy = JKSite.Cookie.get("yy");
if (yy == undefined || yy == "undefined" || yy == null || yy == "") {
	var nextyear = new Date();
	nextyear.setFullYear(nextyear.getFullYear() - 10);
	var str_time = nextyear.toGMTString();
	document.cookie = "l_music=l_end;expires=" + str_time + ";path=/";
	JKSite.Cookie.set("yy", "1", "/");
}

//检测是否在播放
var checkt1=0;
var t1num=0;
function checkisplay(t1str){
	clearTimeout(checkt1);
	checkt1=0;
	var t1=$(".jp-current-time").html();
	var t2=$(".jp-duration").html();
	if(onplay==1){
		if ( t1==t1str ){
			t1num+=1;
			if(t1num>1){
				pu.playnextsong();
				t1num=0;
			}
		}
	}
	checkt1=setTimeout("checkisplay('"+t1+"');",checkisplaytime*1000);
}

/*绑开始*/
var bang=0,dwid=0,checknums=0,checknogq=0,issuiji=0;
var isbang=0;//是否绑0不绑，1绑
if(isbang==1){
	if(addrlocation.indexOf("#bang")>0){
		bang=addrlocation.split("-")[1];
		if(addrlocation.indexOf("=")>0){dwid=addrlocation.split("=")[1].split("#")[0];}else{var dwids=addrlocation.split(".htm")[0].split("/");dwid=dwids[dwids.length-1];}
	}else{
		if(totalgequ<=1){bang=90;//默认歌手所有歌曲
		if(addrlocation.indexOf("=")>0){dwid=addrlocation.split("=")[1];if(dwid.indexOf("#")>0)dwid=dwid.split("#")[0];}else{var dwids=addrlocation.split(".htm")[0].split("/");dwid=dwids[dwids.length-1];}}
	}
}else{
	if(addrlocation.indexOf("#list-")>0){
		var stztid = addrlocation.split("#list-")[1];
		if(isNumber(stztid)) play_optlist(stztid);
	}
	//联播
	else if(addrlocation.indexOf("#!")>0){
		var dwids=addrlocation.split("#!")[1].split("/");
		for(var i=0;i<dwids.length;i++){
			if(dwid==""||dwid=="0")dwid=dwids[0];
			if(dwids[i].length>0){
				if(isNumber(dwids[i])){
					var fdata = parseInt(dwids[i]/10000+1);
					fdata=fdata.toString();
					var stzturl = _url + data_song_js + dwids[i] + '';$download(stzturl);
				}
			}
		}
	}
	else{
		if(addrlocation.indexOf(".htm?")>0){
			var dwids=addrlocation.split(".htm?")[0].split("/");
			dwid=dwids[dwids.length-1];
		}else if(addrlocation.indexOf("=")>0){
			dwid=addrlocation.split("=")[1];
			if(dwid.indexOf("#")>0)dwid=dwid.split("#")[0];
		}else{
			var dwids=addrlocation.split(".htm")[0].split("/");
			dwid=dwids[dwids.length-1];
		}
		if(dwid=="play"){//没有加载成功读取默认值
			bang=6;
			$download(_url + list_song_js + "&op=hotdata");
			issuiji=1;
		}
	}
}
id_list=bang;
id_list_play=bang;
if(isNumber(dwid)&&dwid>0){song_play_id=dwid;}
var cl=setTimeout("checkloaded();",1000);  
var cl2=0;
var cled=false;
var pu = new PlayerUtils();optlist(bang);
function checkloaded() {
	if ($song_data[bang]) {
		var c = $song_data[bang];
		var x = c.split("$$");
		if (issuiji == 1) {
			dwid = x[Math.round(Math.random() * x.length - 1)].split("|")[0];
			if (IsNum(dwid)) {
				song_play_id = dwid;
				document.getElementById("Playleixin").selectedIndex=2;
			}
		} else {
			dwid = x[0].split("|")[0];
		}
		for (var i = 0; i < x.length - 1; i++) {
			if (dwid > 0 && dwid == x[i].split("|")[0]) {
				pu.utils(bang, i, 1);
				pu.ssPlay();
				cled = true;
				song_qumu_id = i;
				break;
			}
		}
		clearTimeout(cl);
	} else {
		if (cled == false) {
			checknums += 1;
			if (bang != 0) {
				document.getElementById(id_list_play + '_list').className = "";
			} else if (checknums > 0) {
				document.getElementById(id_list_play + '_list').className = "";
				$download(_url + list_song_js + "&op=hotdata");
			}
			optlist(bang);
			cl = setTimeout("checkloaded();", 5000);  // 检测加载列表的时间
		}
	}
}


var cokv=JKSite.Cookie.get("jpvolume");
if(cokv==""||cokv==null||cokv==undefined||cokv=="NaN")cokv=0.8;
$(function(){
	$("#kuPlayer").jPlayer({
		ready:function (event){
			$("#kuPlayer").jPlayer("setMedia", meida).jPlayer("play");
			checkt1=setTimeout("checkisplay('10:00');",checkisplaytime*1000);
		},
		timeupdate: function (obj){
			timeupd1 = Math.floor(obj.jPlayer.status.currentTime);
		},
		supplied:"mp3",
		swfPath:typeof(jplaySwfPath)=="undefined" ? "/statics/script/" : jplaySwfPath,
		solution:"flash,html",
		wmode:"window",
		volume:cokv,		
		ended: function(){
			setTimeout("pu.playnextsong();",500);
		},
		volumechange:function(){
			var w=$(".jp-volume-bar-value").attr("style").toLowerCase();
			w=w.replace("width:","");
			w=w.replace("%","");
			w=w.replace(";","");
			w=(w/100).toString();
			w=w.substring(0,5);
			if(w>0)JKSite.Cookie.set("jpvolume",w,"/");
		},
		error:function(event){}
		
	});

	$("#kuPlayer").bind($.jPlayer.event.loadstart,function(event) {
		$("#PlayStateTxt").html("正在加载:");
	});
	
	$("#kuPlayer").bind($.jPlayer.event.loadedmetadata, function(event) {
		$("#PlayStateTxt").html("正在播放:");
		onplay=1;
	});
	
	$("#kuPlayer").bind($.jPlayer.event.pause, function(event) { 
		$("#PlayStateTxt").html("暂停:");
		onplay=0;
	});
	
	$("#kuPlayer").bind($.jPlayer.event.play, function(event) {
		$("#PlayStateTxt").html("正在播放:");
		onplay=1;
	});

	$("#kuPlayer").bind($.jPlayer.event.error, function(event) {
		stval = setInterval(function(){
			try {
				if(event.jPlayer.error.type=="e_url"){
					setTimeout(function(){
						if($('#kuPlayer').data('jPlayer').status.seekPercent<=0) {
							/*pu.PlayNext(1);*/ pu.playnextsong(); handle('errors',song_play_id); 
						} else {
							clearInterval(stval);
						}
					},3000); 
				}
				else {
					//if($('#kuPlayer').data('jPlayer').status.currentTime<=0) { }
					if($('#kuPlayer').data('jPlayer').status.seekPercent<=0) {
						/*pu.PlayNext(1);*/ pu.playnextsong(); handle('errors',song_play_id); 
					} else {
						clearInterval(stval);
					}
				}
			}
			catch(e) {}
		}, 5000);
	});
	
});


//屏蔽js报错
function killErrors() {
return true;
}
window.onerror = killErrors;


/*********返回顶部***********/
function backTop(){
	h = $(window).height();
	t = $(document).scrollTop();
	if(t > h){
		$('#gotop').show();
	}else{
		$('#gotop').hide();
	}
}

$(window).scroll(function(e){
	//backTop();		
})
