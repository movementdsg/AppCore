/*
* Ajax Play 主执行文件
*/

/*————————————————————————————————————————————————————————————————————*/

	var gs = new getServer();
	var host = gs.host;
	var d_u = gs.dir_path_url; //文件所在目录
	var p_d_u = gs.parent_dir_url; //文件所在上级目录
	var _page = gs.pagename; //当前文件名

	var player_dir;
	var player_server;

	//player_dir = './musicbox/';
	player_dir = 'http://' + host + d_u + '';

	//player_server = './musicbox/asp/';
	//player_server = 'http://' + host + d_u + 'module/player/musicbox/asp/';
	player_server = abpath + 'asp/';

/*————————————————————————————————————————————————————————————————————*/

	player_dir = ''+ cmspath +'/musicbox/';

/*————————————————————————————————————————————————————————————————————*/


function ReplaceClassName(str) {
	var _cname_ = '';

	var _page_ = _page.toLowerCase();
	var _id_ = request("listid").toLowerCase();
	var _item_ = request("item").toLowerCase();
	
	if (_id_ != '') {
		_cname_ = eval( 'BigClassName_ID' + _id_ );
	}
	else if (_item_ == 'top') {
		_cname_ = '推荐歌曲';
	}
	else {
		_cname_ = '所有歌曲';
	}
	_cname_ = '' + _cname_ + '';

	str = str.replace('<!--[歌曲类别]-->', _cname_);
	str = str.replace('[歌曲类别]', _cname_);

	return str;
}

function LoadList() {
	var sListPage = play_list;

	var _page_ = _page.toLowerCase();
	var _id_ = request("listid").toLowerCase();
	var _item_ = request("item").toLowerCase();

	if (_id_ != '') {
		sListPage = play_list + '&listid=' + _id_ ;
	}
	else if (_item_ == 'top') {
		sListPage = play_list + '&item=top';
	}
	else {
		sListPage = play_list + '';
	}
	
	/* 
	//这段代码 可能导致获取不到 也容易造成死循环cpu飙高，所以暂时不用了, 换成下面 DoJSLoaded()方法
	// LoadScript(sListPage);
	// Process_Script();
	*/

	//DoJSLoaded(sListPage,function(){doplay();});
	scriptLoader.load([sListPage+''],0,function(){setTimeout(function(){
		try{
			if(SongList.selectedIndex < 0){ SongList.options[0].selected = true; play(); }
		}catch(e){}
	},5000);});
}
/*————————————————————————————————————————————————————————————————————*/

function do_act() {
	LoadList();
	e_loaded+=1;
}

function Ajax_ShowBox() {
	var SendURL;
	e_loaded+=1;
	SendURL = box_main + '&t='+ (new Date).getTime();
	var xmlhttp = new HttpRequest(); // 兼容的 XMLHttpRequest 创建方法
	xmlhttp.onfinish = function(){
		ShowBox_Result = this.responseText;
		ShowBox_Result = unescape(ShowBox_Result);
		the("BoxMain").innerHTML = ReplaceClassName(ShowBox_Result);
		do_act();
	}
	xmlhttp.onerror=function(e){alert(e.message);}
	xmlhttp.open("get",SendURL,true);
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=GB2312');
	try{
		xmlhttp.send(null);
	}
	catch(e) { }
}
