/*
* URLEncode解析库文件(URLEncode.js)
*/

loadjs(abpath + 'js/Unicode2ANSI.js'); //载入Unicode与ANSI编码互转库文件

function str2asc(str)
{
    var n = UnicodeToAnsi(str.charCodeAt(0));
    var s = n.toString(16);
    return s.toUpperCase();
}

function asc2str(code)
{
    var n = AnsiToUnicode(code);
    return String.fromCharCode(n);
}

function urlEncode(str)
{
    var ret = "";
	var strSpecial = "!\"#$%&'()*+,/:;<=>?[]^`{|}~%";
    var tt = "";
    for(var i = 0; i < str.length; i++)
    {
        var chr = str.charAt(i);
        var c = str2asc(chr);
        tt += chr + ":" + c + "n";
        if (parseInt("0x" + c) > 0x7f)
        {
            ret += "%" + c.slice(0,2) + "%" + c.slice(-2);
        }
        else
        {
            if (chr == " ")
                ret += "+";
            else if (strSpecial.indexOf(chr) != -1)
                ret += "%" + c.toString(16);
            else
                ret += chr;
        }
    }
    return ret;
}
 
function urlDecode(str)
{
    var ret = "";
    for (var i = 0; i < str.length; i++)
    {
        var chr = str.charAt(i);
        if (chr == "+")
        {
            ret += " ";
        }
        else if (chr == "%")
        {
            var asc = str.substring(i+1, i+3);
            if (parseInt("0x"+asc) > 0x7f)
            {
                ret += asc2str(parseInt("0x" + asc+str.substring(i+4, i+6)));
                i += 5;
            }
            else
            {
                ret += asc2str(parseInt("0x"+asc));
                i += 2;
            }
        }
        else
        {
            ret += chr;
        }
    }
    return ret;
}