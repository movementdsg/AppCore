/************************** 自定义部分 Begin ********************************************/
var AutoSwfPlayHander = true; //是否开启Flash播放器自动处理(URL失效和播放完成自动转入下一首)
var IsSwfAutoPlay = true; //是否开启Flash播放器自动播放(若开启则就不支持暂停功能)
var ServerUrl = ""; //音乐服务器地址
/************************** 自定义部分 End ********************************************/

/*————————————————————————————————————————————————————————————————————*/

/*___________________________________________________________________________________________*/

var TotalSongs, MusicName, SingerName, SongID, BgColor;
var PlayID; //当前播放ID
var url; //当前播放媒体地址

//判断浏览器类型
var MSIE = navigator.userAgent.indexOf("MSIE");

function GetPlayID(pid) { //获取当前播放ID
	var tempID;
	if( pid == null && pid == '' ) {
		tempID = 0;
	}else {
		tempID = parseInt(pid);
	}
	return tempID;
}

//插入歌曲列表入口函数
function InsertList(url, MusicName, SingerName, SongID, BgColor, PlayerType){
	var myTo = the("SongList");

	if(!TotalSongs){
		TotalSongs = myTo.options.length + 1;
		the("playlist").style.display = "none";
		myTo.style.display = "block";
	}else{
		TotalSongs++;
	}

	var tStr = TotalSongs + "." + MusicName + "**" + SingerName;
	var sTip = TotalSongs + "." + MusicName + " - " + SingerName;;
	//if(tStr.length < 21){ 
	//if(NumLen(tStr) < 21){
	if(StrTemp.GetLength(tStr) < 37){
		tStr = tStr.replace("**"," - ");
	}else{
		tStr = tStr.replace("**"," - ");
		//tStr = tStr.substr(0, 21) + "..";
		//tStr = NumLenStr(tStr,21) + "..";
		tStr = getInterceptedStr(tStr,37) + "..";
	}
	var tO = new Option;
	tO.text = tStr;
	tO.value = SongID;
	tO.style.background = BgColor;
	tO.title = sTip;

	myTo.options[TotalSongs - 1] = tO;
}

//初始执行
function doplay(){
	/*
	try{
		if( document.getElementById && the("Exobud") ) {
			if(the("Exobud").name == "MediaAudioPlayer"){
				the("Exobud").controls.stop();
				//the("Exobud").close();
				if(document.body){
					document.body.removeChild(the("Exobud"));
				}
				else {
					if(the("Exobud").parentNode){ the("Exobud").parentNode.removeChild(the("Exobud")); }
				}
			}
		}
	} catch(e) {}
	*/
	try{
		ready(function(){
			var SongList = the("SongList");
			if(SongList.selectedIndex < 0){ SongList.options[0].selected = true; }
			play();
		});
	} catch(e) {/*alert(e.message);*/ alert('播放清单上没有添加任何歌曲'); }
}

//播放歌曲
function play(){
	var SongList = the("SongList");
	if(SongList.selectedIndex < 0){ alert('请选择你要播放的曲目!'); return false;}
	else{
		SongID = SongList.options[SongList.selectedIndex].value;
		SongID = parseInt(SongID);
		url	= ServerUrl + eval("url_" + SongID);
		MusicName 	= eval("music_" + SongID);
		SingerName 	= eval("singer_" + SongID);
		var PlayerType 	= eval("PlayerType_" + SongID);
		PlayID = GetPlayID(SongID); //当前播放ID(全局通用)

 			if(PlayerType == 'default'){
				do_what(url,'default',SongID);
			}
 			else if(PlayerType == 'pt_mp3'){
				do_what(url,'mp3',SongID);
			}
 			else if(PlayerType == 'pt_wma'){
				do_what(url,'wma',SongID);
			}
 			else if(PlayerType == 'pt_swf'){
				do_what(url,'swf',SongID);
			}
 			else if(PlayerType == 'pt_flv'){
				do_what(url,'flv',SongID);
			}
 			else if(PlayerType == 'pt_wmv'){
				do_what(url,'wmv',SongID);
			}
 			else if(PlayerType == 'pt_rm'){
				do_what(url,'rm',SongID);
			}
 			else if(PlayerType == 'pt_rmvb'){
				do_what(url,'rmvb',SongID);
			}
 			else if(PlayerType == 'pt_mp4'){
				do_what(url,'mp4',SongID);
			}
 			else if(PlayerType == 'pt_3gp'){
				do_what(url,'3gp',SongID);
			}
 			else if(PlayerType == 'pt_mid'){
				do_what(url,'mid',SongID);
			}
 			else if(PlayerType == 'pt_wav'){
				do_what(url,'wav',SongID);
			}
 			else if(PlayerType == 'pt_asf'){
				do_what(url,'asf',SongID);
			}
 			else if(PlayerType == 'pt_avi'){
				do_what(url,'avi',SongID);
			}
 			else if(PlayerType == 'pt_ra'){
				do_what(url,'ra',SongID);
			}
 			else if(PlayerType == 'pt_mov'){
				do_what(url,'mov',SongID);
			}
 			else if(PlayerType == 'pt_qt'){
				do_what(url,'qt',SongID);
			}
 			else if(PlayerType == 'pt_mpg'){
				do_what(url,'mpg',SongID);
			}
 			else if(PlayerType == 'pt_mpeg'){
				do_what(url,'mpg',SongID);
			}
 			else if(PlayerType == 'pt_dat'){
				do_what(url,'dat',SongID);
			}
 			else if(PlayerType == 'pt_vob'){
				do_what(url,'vob',SongID);
			}
 			else if(PlayerType == 'pt_flac'){
				do_what(url,'flac',SongID);
			}
			else{
				do_what(url,'default',SongID);
			}
	}
}

	function doTLab(SongID) {
		//根据不同的浏览器控制播放器
		if(MSIE > -1){  //IE浏览器		
			/*
			//var ExO = document.cm_Player1;
			var ExO = the("Exobud");
			ExO.style.display = "block";
			//ExO.url = url;
			ExO.controls.play(); */
			showTLab();
			HandPlayNum(SongID);
		}else{  //非IE浏览器
			var tB = '';
			tB += '<embed name="cm_Player" type="audio/x-ms-wma" '; 
			tB += ' codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" ';
			tB += ' width="370" height="64" ';
			tB += ' src="' + url + '">';
			tB += '</embed>';

			//the("PlayerObj").innerHTML = tB;
			setTimeout("showTLabNoIE()", 3000);
			HandPlayNum(SongID);
		}
	}

var ewt;
ewt = 0;
var xwt = null;
var PlayModeRadio = document.getElementsByName("PlayModeRadio");

function showTLab(){
 state = setInterval("updatetime()",1000);
 if(the("Exobud").name == "MediaAudioPlayer" || the("Exobud").name == "MediaVideoPlayer") {
	the("Exobud").controls.play();
	//the("Exobud").playState 返回值 0:Undefined,1:Stopped[停止],2:Paused[暂停中],3:Playing[正在播放],4:ScanForward[向前搜索],5:ScanReverse[向后搜索]
	// 6:Buffering[缓冲中],7:Waiting[等待中],8:MediaEnded[播放完毕],9:Transitioning[转换曲目],10:Ready[准备就绪],11:Reconnecting[重新连接]
	//the("Exobud").openState 返回值  0(Undefined) 8(MediaChanging) 9(MediaLocating) 10(MediaConnecting) 11(MediaLoading) 
	// 12(MediaOpening) 13(MediaOpen) 20(MediaWaiting) 21(OpeningUnknownURL)
	clearInterval(xwt);
	xwt = null;
   function CheckMediaPlayerPlaying(){
		ewt=ewt+1;
		if(ewt>=3){
			if(the("Exobud").playState == 1 || the("Exobud").playState == 8) {
				clearInterval(xwt);
				xwt = null;
				if (PlayModeRadio[1].checked == true) {
					RandomPlay();
				}
				else if (PlayModeRadio[2].checked == true) {
					OneRepeatPlay();
				}
				else {
					Next_OneOK();
				}
			}
			if(the("Exobud").playState == 10 || the("Exobud").openState == 6 || the("Exobud").openState == 21) {
				clearInterval(xwt);
				xwt = null;
				SList = the("SongList");
				songid = SList.options[SList.selectedIndex].value;
				//setTimeout("HandErrorNum('"+songid+"')",1000);
				HandErrorNum(songid);
				if (PlayModeRadio[1].checked == true) {
					RandomPlay();
				}
				else if (PlayModeRadio[2].checked == true) {
					OneRepeatPlay();
				}
				else {
					Next_OneOK();
				} 
			}
		}
	}
	function AutoCheckMediaPlay() {
		ewt=0;
		if (xwt === null) xwt = setInterval( function(){ CheckMediaPlayerPlaying();}, 3000);
	}
	AutoCheckMediaPlay();
 }
 if(the("Exobud").name == "RealAudioPlayer") {
	//alert(the("Exobud").GetPlayState());
	//GetPlayState() 返回值 0:停止,1:连接,2:缓冲,3:播放,4:暂停,5:寻找(定位中)
   function CheckRealPlayerPlaying(){
	if(the("Exobud").GetPlayState() == 5) {
		SList = the("SongList");
		songid = SList.options[SList.selectedIndex].value;
		//setTimeout("HandErrorNum('"+songid+"')",1000);
		HandErrorNum(songid);
 		if (PlayModeRadio[1].checked == true) {
			RandomPlay();
		}
		else if (PlayModeRadio[2].checked == true) {
			OneRepeatPlay();
		}
		else {
			Next_OneOK();
		} 
	}
	if(the("Exobud").GetPlayState() == 0) {
 		if (PlayModeRadio[1].checked == true) {
			RandomPlay();
		}
		else if (PlayModeRadio[2].checked == true) {
			OneRepeatPlay();
		}
		else {
			Next_OneOK();
		} 
	}
   }
   setInterval(CheckRealPlayerPlaying, 5000);
 }

 if(the("Exobud").name == "SwfPlayer") {
   function IsSwfPlaying(){
	   if(IsSwfAutoPlay == true){
		//if( SwfPlayer.PercentLoaded() ==100 ) {
			if(!SwfPlayer.IsPlaying()) { SwfPlayer.Play(); } 
		//}
	   }
	   else{
	   
	   }
   }
	//window.setTimeout(IsSwfPlaying,8000); 
	setInterval(IsSwfPlaying, 8000);
	setInterval(SwfPlayerStateHandle, 5000);
 }

 //setTimeout("showTLab()", 5000);
}

function showTLabNoIE(){
	showTLab();
}

/*________________________________________________*/

//控制按钮 上一首
function Last_OneOK(){
	var SongList = the("SongList");
	var oItem = getSelectItem('SongList',PlayID); //当前播放项
	var mItem = getSelectItemMove('SongList',PlayID,-1); //当前播放项的上一项
	var nItem = getSelectItemMove('SongList',PlayID,1); //当前播放项的下一项
	var prevItem,nextItem;
	if (PlayModeRadio[1].checked == true) {
		prevItem = SongList.options[GetRandom(TotalSongs)];
	}
	else {
		prevItem = mItem;
		// prevItem = SongList.options[SongList.selectedIndex - 1];
	} 	
	/**_________*/
	if(the("Exobud").name == "MediaAudioPlayer" || the("Exobud").name == "MediaVideoPlayer") {
		if (prevItem != null) {
			if(the("Exobud").playState != 1) {
				the("Exobud").controls.stop();
				//先停止Media Player控件播放,避免内存堆积
			}
		}
	}
	/**_________*/
	if(prevItem != null){
		prevItem.selected = true;
		play();
	}else {}
	/*
	if((SongList.selectedIndex > 0) && (SongList.selectedIndex < TotalSongs)){
		prevItem.selected = true;
		play();
	}
	*/
}

//控制按钮 下一首
function Next_OneOK(){
	var SongList = the("SongList");
	var oItem = getSelectItem('SongList',PlayID); //当前播放项
	var mItem = getSelectItemMove('SongList',PlayID,-1); //当前播放项的上一项
	var nItem = getSelectItemMove('SongList',PlayID,1); //当前播放项的下一项
	var prevItem,nextItem;
	if (PlayModeRadio[1].checked == true) {
		nextItem = SongList.options[GetRandom(TotalSongs)];
	}
	else {
		nextItem = nItem;
		// nextItem = SongList.options[SongList.selectedIndex + 1];
	} 
	/**_________*/
	if(the("Exobud").name == "MediaAudioPlayer" || the("Exobud").name == "MediaVideoPlayer") {
		if (nextItem != null) {
			if(the("Exobud").playState != 1) {
				the("Exobud").controls.stop();
				//先停止Media Player控件播放,避免内存堆积
			}
		}
	}
	/**_________*/
	if(getSelectItemIndex('SongList',PlayID) >= 0){
		if( getSelectItemIndex('SongList',PlayID) < TotalSongs - 1){
			//SongList.options[getSelectItemIndex('SongList',PlayID) + 1].selected = true;
			if(nextItem != null){ 
				nextItem.selected = true;
			}else {
				//getSelItem('SongList',0).selected = true;
			}
	 		play();
		}else{
			//SongList.options[0].selected = true;
			if( getSelItem('SongList', 0) != null ) { getSelItem('SongList',0).selected = true; }
	 		play();
		}
	}
	/*
	if(SongList.selectedIndex >= 0){
		if(SongList.selectedIndex < TotalSongs - 1){
			nextItem.selected = true;
	 		play();
		}else{
			SongList.options[0].selected = true;
	 		play();
		}
	}
	*/
}

//随机播放 函数
function RandomPlay(){
	if(the("Exobud").name == "MediaAudioPlayer" || the("Exobud").name == "MediaVideoPlayer") {
		if(the("Exobud").playState != 1) {
			the("Exobud").controls.stop();
			//先停止Media Player控件播放,避免内存堆积
		}
	}
	var SongList = the("SongList");
	if(SongList.selectedIndex >= 0){
		SongList.options[GetRandom(TotalSongs)].selected = true;
		play();
	}
}

//单曲循环 函数
function OneRepeatPlay(){
	var SongList = the("SongList");
	SongList.options[SongList.selectedIndex].selected = true;
	play();
}

function SwfPlayerStateHandle() {
 state = setInterval("updatetime2()",100);
 if(AutoSwfPlayHander == true){
	//if( SwfPlayer.CurrentFrame() <=0 ) {
  function IsLoaded(){
	//if( SwfPlayer.CurrentFrame() <=0 ) { //当一段时间后flash当前帧数小于等于0,判为链接失效,自动转入下一首
	if( SwfPlayer.PercentLoaded() <=0 ) { //当一段时间后flash文件载入百分比还是0的话,判为链接失效,自动转入下一首
		SList = the("SongList");
		songid = SList.options[SList.selectedIndex].value;
		//setTimeout("HandErrorNum('"+songid+"')",1000);
		HandErrorNum(songid);
 		if (PlayModeRadio[1].checked == true) {
			RandomPlay();
		}
		else if (PlayModeRadio[2].checked == true) {
			OneRepeatPlay();
		}
		else {
			Next_OneOK();
		} 
	}
  }
  window.setTimeout(IsLoaded,8000); //设定8秒后
  function IsLoadedAll(){
	if( SwfPlayer.PercentLoaded() ==100 ) { //必须是载入进度达到100%
	//if(!SwfPlayer.IsPlaying()) { //当播放完成后,状态转为停止,则自动转入下一首
	//if(SwfPlayer.TotalFrames - SwfPlayer.CurrentFrame() <=5) { //当播放快结束,则自动转入下一首
	if(SwfPlayer.CurrentFrame() / SwfPlayer.TotalFrames >=0.99 ) { //当播放进度达到99%,自动转入下一首
 		if (PlayModeRadio[1].checked == true) {
			RandomPlay();
		}
		else if (PlayModeRadio[2].checked == true) {
			OneRepeatPlay();
		}
		else {
			Next_OneOK();
		} 
	}
	}
  }
  window.setTimeout(IsLoadedAll,3000); //设定3秒后
 }
}

function updatetime(){ 
	if(the("Exobud")){
		if(the("Exobud").name == "MediaAudioPlayer" || the("Exobud").name == "MediaVideoPlayer") {
			if(the("Exobud").controls.currentPositionString=='') { 
			the("StateInfo").innerHTML = "正在缓冲";
			} 
			else { 
			the("StateInfo").innerHTML = the("Exobud").controls.currentPositionString + "/" + the("Exobud").currentMedia.durationString; 
			}
		}
	}
}

 function updatetime2(){ 
 if(SwfPlayer.PercentLoaded()!=100) { 
  //the("StateInfo").innerHTML = "载入中...";
  the("StateInfo").innerHTML = ""+SwfPlayer.PercentLoaded()+"%";
 } 
 else {
  the("StateInfo").innerHTML = SwfPlayer.CurrentFrame()+"/"+SwfPlayer.TotalFrames; 
 }
}

/*
** Extra Function____________________
*/

/*
* event.shiftKey ==true: Shift键;	
* event.ctrlKey ==true: Ctrl键; 
* event.altKey ==true: Alt键; 
*/
document.onkeydown = function(){
   if ( event.ctrlKey ==true && event.keyCode=='37' ) //当按下了 Ctrl键和左方向键, 跳到上一首
   {
    Last_OneOK();
   }
   if ( event.ctrlKey ==true && event.keyCode=='39' ) //当按下了 Ctrl键和右方向键, 跳到下一首
   {
    Next_OneOK();
   }
};

/*
** ____________________Extra Function
*/

/*_____________________No Use___________________________*/

//Fill the display DIV with state information when called
function evtOnOpenState() {
    var openState;
    openState = divMedia.openState;
	var openDiv = document.getElementById("openDiv");
    openDiv.innerHTML = getOpenState(openState);
}

//Fill the display DIV with state information when called
function evtOnPlayState() {
    var playState;
    playState = divMedia.playState;
	var openDiv = document.getElementById("openDiv");
    playDiv.innerHTML = getPlayState(playState);
}

//Provide a meaningful description for the playState integer
function getPlayState(state) {
    var stateText;
    var stateText = "(" + state + "): ";
    switch (state) {
    case 0:  stateText += "The playback state is undefined."; break;             
    case 1:  stateText += "Playback is stopped."; break;          
    case 2:  stateText += "Playback is paused."; break;           
    case 3:  stateText += "The player is playing a stream."; break;            
    case 4:  stateText += "The player is scanning a stream forward."; break;     
    case 5:  stateText += "The player is scanning a stream in reverse."; break; 
    case 6:  stateText += "The player is buffering media."; break;     
    case 7:  stateText += "The player is waiting for streaming data."; break;    
    case 8:  stateText += "The player has reached the end of the media."; break;          
    case 9:  stateText += "The player is preparing new media."; break;           
    case 10: stateText += "The player is ready to begin playback."; break;    
    default: stateText += "No value"; break;
    }
    return stateText;
}

//Provide an meaningful description for the openState integer
function getOpenState(state)
{
    var stateText = "(" + state + "): ";
    switch (state) {
    case 0:  stateText += "Undefined"; break;                           
    case 1:  stateText += "The player is about to load a new playlist."; break;  
    case 2:  stateText += "The player is locating the playlist."; break;              
    case 3:  stateText += "The player is connecting to the server that holds a playlist."; break;  
    case 4:  stateText += "The player is loading a playlist."; break;                  
    case 5:  stateText += "The player is opening a playlist."; break;                
    case 6:  stateText += "The player's playlist is open."; break;                  
    case 7:  stateText += "The player's playlist has changed."; break;            
    case 8:  stateText += "The player is about to load new media."; break;    
    case 9:  stateText += "The player is locating the media file."; break;                  
    case 10: stateText += "The player is connecting to the server holding the media file."; 
    break;  
    case 11: stateText += "The player is loading a media file."; break;                       
    case 12: stateText += "The player is opening a media file."; break;                     
    case 13: stateText += "The media file is open."; break;                       
    case 14: stateText += "The player is starting codec acquisition."; break;          
    case 15: stateText += "The player is ending codec acquisition."; break;               
    case 16: stateText += "The player is starting license acquisition."; break;         
    case 17: stateText += "The player is ending license acquisition."; break;            
    case 18: stateText += "The player is starting individualization."; break;           
    case 19: stateText += "The player is ending individualization."; break;                
    case 20: stateText += "The player is waiting for media."; break;                    
    case 21: stateText += "The player is opening a URL whose type is not known."; break;  
    default: stateText += "No value"; break;
    }
    return stateText;
}
