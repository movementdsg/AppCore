/*
** function.js
*/

/*------------------------------------------------------------------------------------
* Block Name       :       Common Block
* Version          :       1.0.0 (2010-03)
* Author           :       Lajox
* Description      :       常用函数执行模块
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/

	/**
	 * @func getScriptPath()
	 * @desc js获取自身所在文件的目录路径
	 */
	function getScriptPath() {
		var isie=(window.ActiveXObject ? true : false); /*-1 != navigator.userAgent.indexOf('MSIE')*/
		var js=document.scripts;
		var jsPath='';
		if(isie){ //IE
			/*
			for(var i=js.length;i>0;i--){
				if(js[i-1].src.indexOf("musicbox/do.js")>-1){
					jsPath=js[i-1].src.substring(0,js[i-1].src.lastIndexOf("/")+1);
				}
			}
			return jsPath;
			*/
			return js[js.length-1].src.substring(0,js[js.length-1].src.lastIndexOf("/")+1);
		} else { //Others
			var jsSrc = document.getElementsByTagName('script')[document.getElementsByTagName('script').length-1].src;
			var jsName = jsSrc.split('/')[jsSrc.split('/').length-1];
			return jsSrc.replace(jsName,'');
			//return (document.documentElement.lastChild.lastChild.src).replace(/(.*\/){0,}([^\.]+.*)/ig, "$1");
		}
	}

	var the = function(em) {
		if (document.getElementById){ return document.getElementById(em); }
		else if (document.all){ return document.all[em]; }
		else if (document.layers){ return document.layers[em]; }
		else{ return null; }
	};

	function getObjectById(id) {  //获取对象ID, 浏览器兼容IE,FF等
		if (typeof(id) != "string" || id == "") return null;
		if (document.all) return document.all(id); /* return eval('window.'+id); */
		if (document.getElementById) return document.getElementById(id);
		try {return eval(id);} catch(e){ return null;}
	}

	// 安全检测JavaScript基本数据类型和内置对象
	// 参数：o表示检测的值
	// 返回值：返回字符串"undefined"、"number"、"boolean"、"string"、"function"、
	// "regexp"、"array"、"date"、"error"、"object"或"null"
	function _typeOf(o){
		// 获取对象的toString()方法引用
		var _toString = Object.prototype.toString;
		// 列举基本数据类型和内置对象类型，你还可以进一步补充该数组的检测数据类型范围
		var _type ={
			"undefined" : "undefined",
			"number" : "number",
			"boolean" : "boolean",
			"string" : "string",
			"[object Function]" : "function",
			"[object RegExp]" : "regexp",
			"[object Array]" : "array",
			"[object Date]" : "date",
			"[object Error]" : "error"
		}
		return _type[typeof o] || _type[_toString.call(o)] || (o ? "object" : "null");
		// 通过把值转换为字符串，然后匹配返回字符串中是否包含特定字符进行检测
	}

	/**
	 * @func isArray(o)
	 * @desc 检测是否为数组
	 */
	function isArray(o) {
		//return Object.prototype.toString.call(o) === '[object Array]';
		return o instanceof Array || Object.prototype.toString.call(o) == '[object Arguments]';
	}

	/**
	 * @func getType(o)
	 * @desc 判断数据类型(类型有：string|boolean|number|array|object|function|date|regexp|math|null)
	 */
	function getType(o) {
		var _t; return ((_t = typeof(o)) == "object" ? o == null && "null" || Object.prototype.toString.call(o).slice(8,-1):_t).toLowerCase();
	}

	/**
	 * @func typename(o)
	 * @desc 检测对象数据类型, 推荐使用上面的getType方法。
	 */
	var typename = function(o) {
		var is = {
			types: ["Array", "RegExp", "Date", "Number", "String", "Object", "Boolean", "Function", "Math", "Regexp", "Error", "HTMLDocument", "Undefined", "Null"]
		};
		for (var i = 0, c; c = is.types[i++]; ) {
			is[c] = (function(type) {
				return function(o) {
					return Object.prototype.toString.call(o) == "[object " + type + "]";
					//此外还可以用：(typeof o=="object") && o.constructor == eval('' + type) 进行判断检测
				}
			})(c);
		}
		for (var i = 0, c; c = is.types[i++]; ) {
			if( is[c](o) ) { return c; break }
		}
	}

	function getObject(objectId) {
		if(document.getElementById && document.getElementById(objectId)) {
		// W3C DOM
		return document.getElementById(objectId);
		} else if (document.all && document.all(objectId)) {
		// MSIE 4 DOM
		return document.all(objectId);
		} else if (document.layers && document.layers[objectId]) {
		// NN 4 DOM.. note: this won't find nested layers
		return document.layers[objectId];
		} else {
		return false;
		}
	}

	//得到字符串的真实长度(双字节换算为两个单字节)
	function getStrActualLen(sChars)
	{
		return sChars.replace(/[^\x00-\xff]/g,"xx").length;
	}

	//截取固定长度子字符串
	function getInterceptedStr(sSource, iLen)
	{
		if(sSource.replace(/[^\x00-\xff]/g,"xx").length <= iLen)
		{
			return sSource;
		}

		var str = "";
		var l = 0;
		var schar;
		for(var i=0; schar=sSource.charAt(i); i++)
		{
			str += schar;
			l += (schar.match(/[^\x00-\xff]/) != null ? 2 : 1);
			if(l >= iLen)
			{
				break;
			}
		}

		return str;
	}

	var StrTemp = {};
	StrTemp.GetLength = function(str) { //获得字符串实际长度[固定长度], 中文2, 英文1
		//要获得长度的字符串: Str
		var realLength = 0, len = str.length, charCode = -1;
		for (var i = 0; i < len; i++) {
			charCode = str.charCodeAt(i);
			if (charCode >= 0 && charCode <= 128) realLength += 1;
			else realLength += 2;
		}
		return realLength;
	};
	//alert(StrTemp.GetLength('测试ceshi'));

	function NumLen(str) //计算字符数,汉字为1,英文为0.5
	{
		 var i,sum;
		 sum=0;
		 for(i=0;i<str.length;i++)
		 {
			 if ((str.charCodeAt(i)>=0) && (str.charCodeAt(i)<=255))
				 sum=sum+0.5;
			 else
				 sum=sum+1;
		 }
		 sum = parseInt(sum);
		 return sum;
	}

	function NumLenStr(str,n) //截取字符,汉字为1,英文为0.5
	{
		 var i,sum;
		 sum=0;
		 for(i=0;i<str.length;i++)
		 {
			 if ((str.charCodeAt(i)>=0) && (str.charCodeAt(i)<=255))
				 sum=sum+0.5;
			 else
				 sum=sum+1;
		 }
		 sum = parseInt(sum);

		 var j,sumB;
		 sumB=0;
		 var nStr="";
		 for(i=0;i<str.length;i++)
		 {
			 if ((str.charCodeAt(i)>=0) && (str.charCodeAt(i)<=255))
				 sumB=sumB+0.5;
			 else
				 sumB=sumB+1;
			 nStr += str.charAt(i);
			 if(parseInt(sumB)+1>=n) break;
		 }

		 if(n>sum) { str = str + ""; }
		 //else { str = str.substr(0, n) + ".."; }
		 else { str = nStr + ""; }
		 return str;
	}

	/*
	var GetRandom = 1; 	//声明一个随机数变量，默认为1
	GetRandom = function(n){ //获取随机范围内数值的函数
		return Math.floor(Math.random()*n+0);
	};
	// 调用实例: var RndNum = 0; RndNum = GetRandom(100); //获得一个0-99的随机数(缺点: 可能和上次发生重复)
	*/

	var GetRandom = 1; 	//声明一个随机数变量，默认为1
	GetRandom = function(m){ //获取随机范围内数值的函数
		//var a = [0,1,2,3,4,5,6,7,8,9];
		var n = m;

		var b=new Array; //组建临时数组
		for (var i=0;i<n;i++){
			b[i]=i;
		}

		var a=new Array;
		a = b;

		 if(0 == n){
		 a = b;
		 n = a.length;
		 }

		 var i = Math.floor(Math.random()*n);
		 return a[i];
		 a = a.slice(0,i).concat(a.slice(i + 1));
		 n --;
	};

	//设置cookie值, 调用方法: setCookie("auth", "admin", null, "/", "19www.com", false);
	function setCookie(name, value)
	{
		var expdate = new Date();
		var argv = setCookie.arguments;
		var argc = setCookie.arguments.length;
		var expires = (argc > 2) ? argv[2] : null;
		var path = (argc > 3) ? argv[3] : null;
		var domain = (argc > 4) ? argv[4] : null;
		var secure = (argc > 5) ? argv[5] : false;
		if(expires!=null) expdate.setTime(expdate.getTime() + ( expires * 1000 ));
		document.cookie = name + "=" + escape (value) +((expires == null) ? "" : ("; expires="+ expdate.toGMTString()))+((path == null) ? "" : ("; path=" + path)) +((domain == null) ? "" : ("; domain=" + domain))+((secure == true) ? "; secure" : "");
	}

	//获取cookie值
	function getCookie(NameOfCookie) {
		if (document.cookie.length > 0) {
			var begin = document.cookie.indexOf(NameOfCookie + "=");
			if (begin != -1) {
				begin += NameOfCookie.length + 1; //cookie值的初始位置
				end = document.cookie.indexOf(";", begin); //结束位置
				if (end == -1) end = document.cookie.length; //没有;则end为字符串结束位置
				return unescape(document.cookie.substring(begin, end));
			}
		}
		return null;
	}

	//删除所有下个结点
	function delNextElement(e){
		if(e){
			var next = e.nextSibling;
			var parent = e.parentNode;
			if(parent){
				parent.removeChild(e);
			}
			delNextElement(next);
		}
	}

	//删除子元素
	function delNodes(o){
		if(o && typeof o != 'undefined') return;
		var e = o.childNodes;
		for(var i=0; i < e.length; i++){
			if(e[i]) parent.removeChild(e[i]);
		}
	}

/*------------------------------------------------------------------------------------
* Block Name       :       Auto Load Script and Css Files Block
* Version          :       1.0.0 (2011-11-25)
* Author           :       Lajox
* Description      :       动态载入JS或Css文件，并执行
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/

	function LoadScript1(srcUrl) { //载入JS文件
		var arr = document.getElementsByTagName("script");
		var isLoad = true;
		if (arr)
		{
		   for(i=0; i<arr.length; i++)
		   {
			var src = arr[i].src;
			if (src)
			{
			 if (src.toLowerCase() == srcUrl.toLowerCase()) { isLoad = false; }
			}
		   }
		}
		if (isLoad)
		{
		   //document.writeln("<script src=\"" + srcUrl + "\" language=\"javascript\" type=\"text\/javascript\"><\/script>");
		   document.writeln(unescape("%3Cscript src=\'" + srcUrl + "\' language=\'javascript\' type=\'text\/javascript\'%3E%3C\/script%3E"));
		}
	}

	function LoadScript(srcUrl) { //载入JS文件
		//重定向document.write的输出方向
		document.write = function(s) {
			document.innerHTML += s;
			return false;
		}

		//var head = document.getElementsByTagName('head').item(0);
		var head = document.getElementsByTagName('head')[0];

		var script = document.createElement("script")
		script.type = "text/javascript";
		script.language='javascript';
		script.defer = true;

		if (script.readyState){  //IE
			script.onreadystatechange = function(){
				if (script.readyState == "loaded" ||
					script.readyState == "complete") {
					script.onreadystatechange = null;
					//callback();
				}
			};
		}
		else {  //Others
			script.onload = function(){
				//callback();
			};
		}

		script.src = srcUrl;
		void(head.appendChild(script));
		// window.setTimeout(function(){head.appendChild(script)}, 100);

	/*
		if( document.createElement && document.childNodes ) {
			document.write('<script type="text\/javascript" src="'+srcUrl+'"><\/script>');
		}
	*/
	}

	function LoadCSS(srcUrl) { //载入CSS文件
		//重定向document.write的输出方向
		document.write = function(s) {
			document.innerHTML += s;
			return false;
		}
		var arr = document.getElementsByTagName("link");
		var isLoad = true;
		if (arr)
		{
		   for(i=0; i<arr.length; i++)
		   {
			var type = arr[i].type;
			if (type && type.toLowerCase() == "text/css")
			{
			 var href = arr[i].href;
			 if (href)
			 {
			  if (href.toLowerCase() == srcUrl.toLowerCase()) { isLoad = false; }
			 }
			}
		   }
		}
		if (isLoad)
		{
		   document.writeln("<link href=\"" + srcUrl + "\" type=\"text\/css\" rel=\"stylesheet\"\/>");
		}
	}

	//e.g. includeJs("test.js",function(){alert('js loaded!')});
	var includeJs = (function(){
		var uid = 0;
		var remove = function(id){
		var head = document.getElementsByTagName('head')[0];
		head.removeChild( document.getElementById('jsInclude_'+id) );
		};
		return function(file,callback){
			var callback;
			var id = ++uid;
			var head = document.getElementsByTagName('head')[0];
			var js = document.createElement('script');
			js.setAttribute('type','text/javascript');
			js.setAttribute('src',file);
			js.setAttribute('id','jsInclude_'+id);
			if( document.all )
			  js.onreadystatechange = function(){
				if(/(complete|loaded)/.test(js.readyState)){ callback(id);remove(id);}
			  };
			else
			  js.onload = function(){callback(id); remove(id); };
			head.appendChild(js);
			return uid;
		};
	})();

	function DoJSLoaded(js,callback) { //JS载入完毕执行函数
		//重定向document.write的输出方向
		document.write = function(s) {
			document.innerHTML += s;
			return false;
		}
		var script = document.createElement("script")
		script.type = "text/javascript";
		script.language='javascript';
		//script.defer = true;
		script.src=''+js+'';
		//var head = document.getElementsByTagName('head').item(0);
		var head = document.getElementsByTagName('head')[0];
		head.appendChild(script);
		if (script.readyState){  //IE
			//script.onreadystatechange = function(){
				if (script.readyState == "loaded" || script.readyState == "complete") {
					if (callback!=null && callback!=''){
						try {
							callback();
						}
						catch(e){}
					}
				}
			//};
		}
		else {  //Others
			script.onload = function(){
				if (callback!=null && callback!=''){
					try {
						callback();
					}
					catch(e){}
				}
			};
		}
	}

	/*
	 * Chainable external javascript file loading
	 */
	var scriptLoader = {
		_loadCount: 0,
		_loadScript: function (_url, _callback) {
			var head = document.getElementsByTagName('head')[0];
			var script = document.createElement('script');
			script.type = 'text/javascript';
			//script.defer = true;
			script.src = _url;
			if (_callback) {
				try {
					if (document.all) {
						script.onreadystatechange = function () {
							if (this.readyState == 'loaded' || this.readyState == "complete") _callback();
						}
					}
					else { script.onload = _callback; }
				} catch(e) { _callback(); }
			}
			head.appendChild(script);
		},
		load: function (a, b, c) {
			var items, iteration, callback;
			items = a; iteration = b; callback = c;
			if(typeof b == 'function'){ callback = b; iteration = c; if (!iteration) iteration = 0; } else { if (!iteration) iteration = 0; }
			if (isArray(items)) {
				if (items[iteration]) {
					scriptLoader._loadScript(
						items[iteration],
						function () {
							if(iteration+1<items.length) {scriptLoader.load(items, iteration+1, callback);return false;}
						}
					);
					++scriptLoader._loadCount;
				}
				if(scriptLoader._loadCount>=items.length||iteration>=items.length-1){
					if(callback) {try{callback();}catch(e){setTimeout(function(){callback()},100);}}; return false;
				}
			} else {
				if(items) {
					scriptLoader._loadScript(
						items,
						function () {
							//if(callback) {callback();} return false;
							if(callback) {try{callback();}catch(e){setTimeout(function(){callback()},100);}}; return false;
						}
					);
				}
			}
		}
	};
	/*
	e.g.
	scriptLoader.load('http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', function(){alert('js载入完毕！');} );
	scriptLoader.load([
		'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js',
		'http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js',
		'your-script.js' //"your-script.js"将在前两个js加载完再加载
	]);
	scriptLoader.load(
		[
			'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js',
			'http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js',
			'your-script.js'
		],0,function(){
			alert('所有指定js全部载入完毕！');
		}
	);
	*/

	function loadjs(jsurl,rd) { //载入JS文件([完全+同步+实时]载入)
		return (new JObject()).include(jsurl,rd);
	}

	function JObject(){
		this.isIE = (window.ActiveXObject ? true : false), /*-1 != navigator.userAgent.indexOf('MSIE')*/
		this.isFF = (-1 != navigator.userAgent.indexOf("Firefox")),
		this.jsSrc = '';
		this.createHTMLObj = function(pTag){
			try{
				return document.createElement(pTag.toUpperCase());
			}
			catch(e){
				return null;
			}
		};
		this.JSWriteType=(function(){
			return function(jsCode,n){
				if (n==1) {
					var _script = document.createElement("script");
					_script.setAttribute("language", "javascript");
					_script.setAttribute("type", "text/javascript");
					_script.setAttribute("charset", "GB2312");
					_script.setAttribute("id", "loadjs" + (new Date().getTime()));
					//_script.defer = true;
					//_script.language = "javascript";
					if(document.all) {
						//_script.setAttribute("text", jsCode);
						_script.setAttribute("src", this.jsSrc);
					}
					else {
						_script.innerHTML = jsCode;
						//_script.setAttribute("src", this.jsSrc);
					}
					document.getElementsByTagName("head")[0].appendChild(_script);
					//document.body.appendChild(_script);
				}
				else {
					if (window.execScript){ //ie
						try{
							window.execScript(jsCode);
						}catch(e){}
					}else{ //firefox
						try{
							//window.eval(jsCode,"javascript");
							window.eval(jsCode);
						}catch(e){}
					}
				}
				try{ }catch(e){}
			};
		})();
		this.createXMLHttp=function(){
			if (window.XMLHttpRequest) {
				var objXMLHttp = new XMLHttpRequest();
			}
			else {
				var MSXML = ['MSXML2.XMLHTTP.5.0', 'MSXML2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP'];
				for(var n = 0; n < MSXML.length; n ++){
					try
					{
						var objXMLHttp = new ActiveXObject(MSXML[n]);
						break;
					}
					catch(e){ }
				}
			}
			return objXMLHttp;
		};
		this.include=function(pJSFile,rd){
			var tXMLHttp = this.createXMLHttp();
			if (rd) { //加随机数防止缓存
				if(pJSFile.indexOf("?") > 0) { pJSFile += "&rnd=" + Math.random(); }
				else { pJSFile += "?rnd=" + Math.random(); }
			} else {
				pJSFile += "";
			}
			this.jsSrc = pJSFile;
			tXMLHttp.open("GET",pJSFile,false);
			tXMLHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=GB2312');
			//tXMLHttp.setRequestHeader("Content-Type","application/text/html; charset=utf-8");
			//tXMLHttp.overrideMimeType("text/html;charset=gb2312"); //IE
			//tXMLHttp.setRequestHeader("Connection", "close");
			tXMLHttp.send(null);
			//tXMLHttp.onreadystatechange = function() {
				if (tXMLHttp.readyState == 4 && (tXMLHttp.status == 200 || tXMLHttp.status == 304)) {
					if(this.isIE){
						var tJsCode = gb2utf8(tXMLHttp.responseBody);
					} else{
						var tJsCode = tXMLHttp.responseText;
					}
					this.JSWriteType(tJsCode,1)
				}
			//}
		};
	};

/*------------------------------------------------------------------------------------
* Block Name       :       CharSet Encoding Block
* Version          :       1.0.0 (2012-6-11)
* Author           :       Lajox
* Description      :       字符编码处理模块
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/

	function regReplaceLowerCase(reg,str){
		//str = str.toLowerCase();
		return str.replace(reg,function(m){return m.toLowerCase()})
	}
	function regReplaceUpperCase(reg,str){
		//str = str.toUpperCase();
		return str.replace(reg,function(m){return m.toUpperCase()})
	}

	var to16 = {
		on : function (str) {
			var a = [], i = 0;
			for (; i < str.length ;) a[i] = ("00" + str.charCodeAt(i ++).toString(16)).slice(-4);
			return "\\u" + a.join("\\u");
		},

		un : function (str) {
			return unescape(str.replace(/\\/g, "%"));
		}
	};
	//alert(to16.on("\"请输\"")); //即 \u0022\u8bf7\u8f93\u0022
	//alert(to16.un("\\u0022\\u8BF7\\u8F93\\u0022")); //即 "请输"

	var toHTML = {
		on : function (str) {
			var a = [], i = 0;
			for (; i < str.length ;) a[i] = str.charCodeAt(i ++);
			return "&#" + a.join(";&#") + ";";
		},

		un : function (str) {
				return str.replace(/&#(x)?([^&]{1,5});?/g, function (a, b, c) {
				return String.fromCharCode(parseInt(c, b ? 16 : 10));
			});
		}
	};
	//alert(toHTML.on("\"请输\"")); //即 &#34;&#35831;&#36755;&#34;
	//alert(toHTML.un("&#x22;&#x8BF7;&#x8F93;&#34;")); //即 "请输"

	function toGB(s){ //转为GB2312 ( 我们 = &#x6211;&#x4EEC; = \u6211\u4eec)
		s = unescape(s.replace(/&#x([a-fA-F0-9]{4});/g,'%u$1'));
		s = unescape(s.replace(/\\u([a-fA-F0-9]{4})/g,'%u$1'));
		return s;
	}
	function toUnicode(s){ //转为Unicode ( &#X6211;&#X4EEC; = 我们 )
		s = s.replace(/[^\u0000-\u00FF]/gi,function($0){return escape($0).replace(/(%u)(\w{4})/gi,"&#x$2;")});
		//if (/[\u0000-\u00ff]/g.test(s))
		s = s.replace(/\\u([a-fA-F0-9]{4})/gi,'&#x$1;');
		s = regReplaceUpperCase(/&#x\w{4}/g,s);
		s = s.replace(/(&#X)(\w{4});/g,"&#x$2;");
		return s;
	}
	function toJSUnicode(s){ //转为JS Unicode ( &#X6211;&#X4EEC; = 我们 )
		s = s.replace(/[^\u0000-\u00FF]/gi,function($0){return escape($0).replace(/(%u)(\w{4})/gi,"\\u$2")});
		s = s.replace(/&#x([a-fA-F0-9]{4});/gi,'\\u$1');
		s = regReplaceLowerCase(/\\u\w{4}/g,s);
		return s;
	}

	function decode(s){ //转为GB2312 ( 我们 = &#x6211;&#x4EEC; = \u6211\u4eec)
		s = toHTML.un(s);
		s = toGB(s);
		return s;
	}
	function encode(s){ //转为Unicode ( &#X6211;&#X4EEC; = 我们 )
		s = toHTML.un(s);
		s = toUnicode(s);
		return s;
	}
	function jsencode(s){ //转为JS Unicode  ( \u6211\u4eec = 我们 )
		s = toHTML.un(s);
		s = toJSUnicode(s);
		return s;
	}

	function encode2(s){ //转为Unicode ( &#X6211;&#X4EEC; = 我们 )
		s = toGB(s);
		if(s!="") s = toHTML.on(s);
		return s;
	}

	/*****/

	function gb2utf8(data){
		var glbEncode = [];
		gb2utf8_data = data;
		execScript("gb2utf8_data = MidB(gb2utf8_data, 1)", "VBScript");
		var t=escape(gb2utf8_data).replace(/%u/g,"").replace(/(.{2})(.{2})/g,"%$2%$1").replace(/%([A-Z].)%(.{2})/g,"@$1$2");
		t=t.split("@");
		var i=0,j=t.length,k;
		while(++i<j) {
			k=t[i].substring(0,4);
			if(!glbEncode[k]) {
				gb2utf8_char = eval("0x"+k);
				execScript("gb2utf8_char = Chr(gb2utf8_char)", "VBScript");
				glbEncode[k]=escape(gb2utf8_char).substring(1,6);
			}
			t[i]=glbEncode[k]+t[i].substring(4);
		}
		gb2utf8_data = gb2utf8_char = null;
		return unescape(t.join("%"));
	}

	function GB2312UTF8(){
		this.Dig2Dec=function(s){
			  var retV = 0;
			  if(s.length == 4){
				  for(var i = 0; i < 4; i ++){
					  retV += eval_r(s.charAt(i)) * Math.pow(2, 3 - i);
				  }
				  return retV;
			  }
			  return -1;
		}
		this.Hex2Utf8=function(s){
			 var retS = "";
			 var tempS = "";
			 var ss = "";
			 if(s.length == 16){
				 tempS = "1110" + s.substring(0, 4);
				 tempS += "10" + s.substring(4, 10);
				 tempS += "10" + s.substring(10,16);
				 var sss = "0123456789ABCDEF";
				 for(var i = 0; i < 3; i ++){
					retS += "%";
					ss = tempS.substring(i * 8, (eval_r(i)+1)*8);
					retS += sss.charAt(this.Dig2Dec(ss.substring(0,4)));
					retS += sss.charAt(this.Dig2Dec(ss.substring(4,8)));
				 }
				 return retS;
			 }
			 return "";
		}
		this.Dec2Dig=function(n1){
			  var s = "";
			  var n2 = 0;
			  for(var i = 0; i < 4; i++){
				 n2 = Math.pow(2,3 - i);
				 if(n1 >= n2){
					s += '1';
					n1 = n1 - n2;
				  }
				 else
				  s += '0';
			  }
			  return s;
		}

		this.Str2Hex=function(s){
			  var c = "";
			  var n;
			  var ss = "0123456789ABCDEF";
			  var digS = "";
			  for(var i = 0; i < s.length; i ++){
				 c = s.charAt(i);
				 n = ss.indexOf(c);
				 digS += this.Dec2Dig(eval_r(n));
			  }
			  return digS;
		}
		this.Gb2312ToUtf8=function(s1){
			var s = escape(s1);
			var sa = s.split("%");
			var retV ="";
			if(sa[0] != ""){
			  retV = sa[0];
			}
			for(var i = 1; i < sa.length; i ++){
			  if(sa[i].substring(0,1) == "u"){
				retV += this.Hex2Utf8(this.Str2Hex(sa[i].substring(1,5)));
		   if(sa[i].length){
			retV += sa[i].substring(5);
		   }
			  }
			  else{
			 retV += unescape("%" + sa[i]);
		   if(sa[i].length){
			retV += sa[i].substring(5);
		   }
		   }
			}
			return retV;
		}
		this.Utf8ToGb2312=function(str1){
				var substr = "";
				var a = "";
				var b = "";
				var c = "";
				var i = -1;
				i = str1.indexOf("%");
				if(i==-1){
				  return str1;
				}
				while(i!= -1){
			if(i<3){
						substr = substr + str1.substr(0,i-1);
						str1 = str1.substr(i+1,str1.length-i);
						a = str1.substr(0,2);
						str1 = str1.substr(2,str1.length - 2);
						if(parseInt("0x" + a) & 0x80 == 0){
						  substr = substr + String.fromCharCode(parseInt("0x" + a));
						}
						else if(parseInt("0x" + a) & 0xE0 == 0xC0){ //two byte
								b = str1.substr(1,2);
								str1 = str1.substr(3,str1.length - 3);
								var widechar = (parseInt("0x" + a) & 0x1F) << 6;
								widechar = widechar | (parseInt("0x" + b) & 0x3F);
								substr = substr + String.fromCharCode(widechar);
						}
						else{
								b = str1.substr(1,2);
								str1 = str1.substr(3,str1.length - 3);
								c = str1.substr(1,2);
								str1 = str1.substr(3,str1.length - 3);
								var widechar = (parseInt("0x" + a) & 0x0F) << 12;
								widechar = widechar | ((parseInt("0x" + b) & 0x3F) << 6);
								widechar = widechar | (parseInt("0x" + c) & 0x3F);
								substr = substr + String.fromCharCode(widechar);
						}
			 }
			 else {
			  substr = substr + str1.substring(0,i);
			  str1= str1.substring(i);
			 }
					  i = str1.indexOf("%");
				}

				return substr+str1;
		}
	}

/*------------------------------------------------------------------------------------
* Block Name       :       SelectOptions Block
* Version          :       1.0.0 (2010-05-27)
* Author           :       Lajox
* Description      :       Select下拉菜单操作模块
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/

    function getSelItem(SelectID,SelItemIndex) { //获取select元素:某一项(按索引获取)
        var o = document.getElementById(SelectID).options;
		var vx = SelItemIndex;
		var rt = null;
		try { rt = o[vx]; } catch(e){ rt = null;}
		return rt;
    }
	/* 调用示例:
		var oItem = getSelItem('SongList',0); //获取首项
		if( oItem != null){
			alert(oItem.value);
		} else {
			alert("No Found");
		}
	*/

    function getSelectItem(SelectID,SelItemValue) { //获取select元素:某一项(按项值获取)
        var o = document.getElementById(SelectID).options;
        var myValue = SelItemValue;
		var rt = null;
        for(var i = 0; i < o.length; i++) {
            if (o[i].value == myValue) {
				rt = o[i];
				break;
			}
        }
		return rt;
    }
	/* 调用示例:
		var oItem = getSelectItem('SongList',20);
		if( oItem != null){
			alert(oItem.value);
		} else {
			alert("No Found");
		}
	*/

    function getSelectItemIndex(SelectID,SelItemValue) { //获取select元素:某一项索引值
        var sel = document.getElementById(SelectID);
		var rt = -1;
		var k=0;
        for(var i = 0; i < sel.options.length; i++) {
            if (sel.options[i].value == SelItemValue) {
				rt = k;
				break;
			}
			++k;
        }
		return k;
    }
	//调用示例:
	// if( getSelectItemIndex('SongList',20) != -1){ alert(getSelectItemIndex('SongList',20)); } else { alert("No Index"); }

    function getSelectItemMove(SelectID,SelItemValue,SN) { //获取select元素:某一项索引值
		var sel = document.getElementById(SelectID);
        var o = document.getElementById(SelectID).options;
		var oIndex = getSelectItemIndex(SelectID,SelItemValue); //当前项索引值
		var oMoveItem;
		var sv;
		if( oIndex==-1 ){
			sv = null;
		}
		else {
			if(!isNaN(SN)){
				if(SN==0) {
					sv = getSelectItem(SelectID,SelItemValue);
				}
				else if (SN>0) {
					try { oMoveItem = sel.options[oIndex + SN]; } catch(e){oMoveItem = null;}
					sv = oMoveItem;
				}
				else {
					try { oMoveItem = sel.options[oIndex - (-SN)]; } catch(e){oMoveItem = null;}
					sv = oMoveItem;
				}
			} else {
				sv = null;
			}
		}
		return sv;
    }
	/* 调用示例:
		var oItem = getSelectItem('SongList',PlayID); //当前播放项
		var mItem = getSelectItemMove('SongList',PlayID,-1); //当前播放项的上一项
		var nItem = getSelectItemMove('SongList',PlayID,1); //当前播放项的下一项
	*/

	function jsSelectIsExitItem(SelectID, objItemValue) { //判断select元素某项是否存在
		var objSelect = document.getElementById(SelectID);
		var isExit = false;
		for (var i = 0; i < objSelect.options.length; i++) {
		 if (objSelect.options[i].value == objItemValue) {
			 isExit = true;
			 break;
		 }
		}
		return isExit;
	}

    function setSelectColor(SelectID,Color) { //设置select元素:所有项-颜色
        var o = document.getElementById(SelectID).options;
        for(var i = 0; i < o.length; i++) {
			o[i].style.color = Color;
        }
    }

    function setSelectItemColor(SelectID,SelItemValue,Color) { //设置select元素:某一项-颜色
        var o = document.getElementById(SelectID).options;
        var myValue = SelItemValue;
        for(var i = 0; i < o.length; i++) {
            if (o[i].value == myValue) {
				o[i].style.color = Color;
			} else {
				o[i].style.color = '#000000';
			}
        }
    }

    function setSelSelectedItemColor(SelectID,Color) { //设置select元素:选中项-颜色
        var o = document.getElementById(SelectID).options;
		//o.options[o.selectedIndex].style.color = Color;
		/**/
        for(var i = 0; i < o.length; i++) {
            if (o[i].selected == true) {
				o[i].style.color = Color;
			} else {
				o[i].style.color = '#000000';
			}
        }
    }

	function delOption(oSel){ // 删除option (可一次性删除多个选中的options)
		if (oSel.selectedIndex>=0) {
			oSel.remove(oSel.selectedIndex);
		} else {
			alert("请先至少选择一项");
		};
	}

	function addOption(oSel,sVal){ // 新增option项
		for(var i=0;i<oSel.length;i++){
			if (oSel.options[i].value==sVal){
				alert("已存在："+sVal+"\n请不要添加重复数据！");
				return false;
			}
		}
		var oOption=new Option(sVal,sVal);
		oSel[oSel.length]=oOption;
	}


/*------------------------------------------------------------------------------------
* Block Name       :       Class Block
* Version          :       1.0.0 (2010-02)
* Author           :       Lajox
* Description      :       常用操作类执行模块
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/

	var getHost = function(URL) {
		var host = "null";
		if(typeof URL == "undefined"|| null == URL)
		URL = window.location.href;
		var regex = /.*\:\/\/([^\/]*).*/;
		var match = URL.match(regex);
		if(typeof match != "undefined" && null != match)
		host = match[1];
		return host;
	};
	//alert(getHost()); //当前服务器名(如果有端口则包括端口) 如: 127.0.0.1:8080

	var getServer = function(){ //JS获取服务器信息类
		//var url = window.location.href;
		var url = document.URL; //获取文档URL完整地址[包括参数]
		//(如返回: http://localhost:8080/webtest/demo/test.html?action=add&id=5#top )
		//var host = url.replace('http://','').split('/')[0]; //服务器地址 (返回: localhost:8080 )
		var host = window.location.host; //URL主机部分(如果有端口则包括端口) (返回: localhost:8080 )
		var dir_full_url = url.substring(0, url.lastIndexOf('/')) + '/'; //文档所在目录完整地址
		//(返回: http://localhost:8080/webtest/demo/ )
		var dir_path_url = dir_full_url.replace('http://'+host,''); //文档目录相对地址 (返回: /webtest/demo/ )
		var page_path_url = url.replace('http://'+host,''); //文档相对地址[包括参数] (返回: /webtest/demo/test.html?action=add&id=5#top )
		//var pagename = page_path_url.replace(dir_path_url,'').split('?')[0].split('#')[0]; //文档文件名 (返回: test.html )
		var pagename = url.substring(url.lastIndexOf('/')+1, url.length).split('?')[0].split('#')[0];
		var search = window.location.search; //URL地址中从?开始的参数部分 (返回 ?action=add&id=5 )
		//var paraS = (url.indexOf("?")==-1)?'':url.substring(url.indexOf("?")+1,url.length);
		var paraS = window.location.search.substr(1); //URL地址中的参数部分 (返回 action=add&id=5 )
		var parent_dir_url = dir_path_url.replace(dir_path_url.split('/')[dir_path_url.split('/').length-2]+'/','');
		//上一层目录相对地址 (返回 /webtest/ )
		if(dir_path_url=='/'){ parent_dir_url = '/';}
		this.url = url;
		this.host = host;
		this.dir_full_url = dir_full_url;
		this.dir_path_url = dir_path_url;
		this.page_path_url = page_path_url;
		this.pagename = pagename;
		this.search = search;
		this.paraS = paraS;
		this.parent_dir_url = parent_dir_url;
		this.protocol = window.location.protocol; //URL协议部分 (返回 http: )
		this.port = window.location.port; //URL端口部分,使用默认的80端口时,(包括URL中包含“:80”),返回值是空字符""
		this.pathname = window.location.pathname; //URL路径部分 (返回 /webtest/demo/test.html )
		this.hash = window.location.hash; //URL锚点 (返回 #top )
		this.domain = document.domain; //当前服务器域名 (返回 localhost )
	};

	var HttpRequest = function() { // XMLHttpRequest的兼容代码(兼容各种浏览器)
		if(this==window)throw new Error(0,"HttpRequest is unable to call as a function.")
		var me=this;
		   var asyncFlag=false;
		   var typeFlag=false;
		var r;
		function onreadystatechange(){
			if(me.onreadystatechange)me.onreadystatechange.call(r);
			if(r.readyState==4)
			{
				if(Number(r.status)>=300)
				{
					if(me.onerror)me.onerror.call(r,new Error(0,"Http error:"+r.status+" "+r.statusText));
					if(typeFlag)r.onreadystatechange=Function.prototype;
					else r.onReadyStateChange=Function.prototype;
					r=null;
					return;
				}
				me.status=r.status;
				me.statusText=r.statusText;
				me.responseText=r.responseText;
				me.responseBody=r.responseBody;
				me.responseXML=r.responseXML;
				me.readyState=r.readyState;
				if(typeFlag)r.onreadystatechange=Function.prototype;
				else r.onReadyStateChange=Function.prototype;
				r=null;
				if(me.onfinish)me.onfinish();
			}
		}
		function creatHttpRequest(){
			var e;
			try{
				r=new window.XMLHttpRequest();
				typeFlag=true;
			} catch(e) {
				var ActiveXName=[
					'MSXML2.XMLHttp.6.0',
					'MSXML2.XMLHttp.5.0',
					'MSXML2.XMLHttp.4.0',
					'MSXML2.XMLHttp.3.0',
					'Msxml2.XMLHTTP',
					'MSXML.XMLHttp',
					'Microsoft.XMLHTTP'
				]
				function XMLHttpActiveX()
				{
					var e;
					for(var i=0;i<ActiveXName.length;i++)
					{
						try{
							var ret=new ActiveXObject(ActiveXName[i]);
							typeFlag=false;
						} catch(e) {
							continue;
						}
						return ret;
					}
					throw {"message":"XMLHttp ActiveX Unsurported."};
				}
				try{
					r=new XMLHttpActiveX();
					typeFlag=false;
				} catch(e) {
					throw new Error(0,"XMLHttpRequest Unsurported.");
				}
			}
		}
		creatHttpRequest();
		this.abort=function(){
			r.abort();
		}
		this.getAllResponseHeaders=function(){
			r.getAllResponseHeaders();
		}
		this.getResponseHeader=function(Header){
			r.getResponseHeader(Header);
		}
		this.open=function(Method,Url,Async,User,Password){
			asyncFlag=Async;
			try{
				r.open(Method,Url,Async,User,Password);
			} catch(e) {
				if(me.onerror)me.onerror(e);
				else throw e;
			}
		}
		this.send=function(Body){
			try{
				if(typeFlag)r.onreadystatechange=onreadystatechange;
				else r.onReadyStateChange=onreadystatechange;

				r.send(Body);
				//alert("sended");
				if(!asyncFlag){
					this.status=r.status;
					this.statusText=r.statusText;
					this.responseText=r.responseText;
					this.responseBody=r.responseBody;
					this.responseXML=r.responseXML;
					this.readyState=r.readyState;
					if(typeFlag)r.onreadystatechange=Function.prototype;
					else r.onReadyStateChange=Function.prototype;
					r=null;
				}
			} catch(e) {
				if(me.onerror)me.onerror(e);
				else throw e;
			}
			//alert("sended");
		}
		this.setRequestHeader=function(Name,Value){
			r.setRequestHeader(Name,Value);
		}
	};

	//e.g. XMLHttp.sendReq('GET', path, '', function(ServerObj){ alert(ServerObj.responseText);});
	var XMLHttp = {
		_objPool: [],
		_getInstance: function () {
			for (var i = 0; i < this._objPool.length; i ++) {
				if (this._objPool[i].readyState == 0 || this._objPool[i].readyState == 4) {
					return this._objPool[i];
				}
			}
			// IE5中不支持push方法
			this._objPool[this._objPool.length] = this._createObj();
			return this._objPool[this._objPool.length - 1];
		},
		_createObj: function () {
			if (window.XMLHttpRequest) {
				var objXMLHttp = new XMLHttpRequest();
			}
			else {
				var MSXML = ['MSXML2.XMLHTTP.5.0', 'MSXML2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP'];
				for(var n = 0; n < MSXML.length; n ++) {
					try {
						var objXMLHttp = new ActiveXObject(MSXML[n]);
						break;
					}
					catch(e){}
				}
			 }
			// mozilla某些版本没有readyState属性
			if (objXMLHttp.readyState == null) {
				objXMLHttp.readyState = 0;
				objXMLHttp.addEventListener("load", function () {
						objXMLHttp.readyState = 4;
						if (typeof objXMLHttp.onreadystatechange == "function") {
							objXMLHttp.onreadystatechange();
						}
					},  false);
			}

			return objXMLHttp;
		},
		// 发送请求(方法[post,get], 地址, 数据, 回调函数)
		sendReq: function (method, url, data, callback) {
			var objXMLHttp = this._getInstance();
			with(objXMLHttp) {
				try {
					// 加随机数防止缓存
					if (url.indexOf("?") > 0) {
						url += "&randnum=" + Math.random();
					}
					else {
						url += "?randnum=" + Math.random();
					}
					open(method, url, true);
					// 设定请求编码方式
					setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
					send(data);
					onreadystatechange = function () {
						if (objXMLHttp.readyState == 4 && (objXMLHttp.status == 200 || objXMLHttp.status == 304)) {
							callback(objXMLHttp);
						}
					}
				}
				catch(e){alert(e.message);}
			}
		}
	};

	function xmlHttpObject() {
		var objType = false;
		try {
			objType = new ActiveXObject('Msxml2.XMLHTTP');
		} catch(e) {
			try {
				objType = new ActiveXObject('Microsoft.XMLHTTP');
			} catch(e) {
				objType = new XMLHttpRequest();
			}
		}
		return objType;
	}

	function CreateAjaxObject()
	{
		var XMLHttp;
		try { XMLHttp = new ActiveXObject("Microsoft.XMLHTTP"); } //IE的创建方式
		catch(e) {
			try { XMLHttp = new XMLHttpRequest(); } //FF等浏览器的创建方式
			catch(e) { XMLHttp = false; } //创建失败，返回false
		}
		return XMLHttp; //返回XMLHttp实例
	}

	/**______Ajax 调用实例 Begin______

	function getAjaxExample(surl) {
		var sendurl = surl';
		var theHttpRequest = xmlHttpObject();
		theHttpRequest.open("GET", sendurl, true);
		theHttpRequest.send(null);
		theHttpRequest.onreadystatechange = function() {
		   if (theHttpRequest.readyState == 4) {
			 if (theHttpRequest.status == 200) {
				//dowhat();
			 }
			 else {
				//alert("网络链接失败");
			 }
		   }
		};
		return;
	};

	function doAjaxExample(surl) {
		var posturl = surl;
		var data = 'action=dohits&n='+Math.random()+'';
		theHttpRequest = xmlHttpObject();
		theHttpRequest.open('POST',posturl,true);
		theHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		theHttpRequest.send(data);
		theHttpRequest.onreadystatechange = function() {processAJAX();};
		function processAJAX() {
		   if (theHttpRequest.readyState == 4) {
			 if (theHttpRequest.status == 200) {
				//dowhat();
			 }
			 else {
				//alert("网络链接失败");
			 }
		   }
		}
		return;
	};

	______Ajax 调用实例 End______*/

	/*
	function createXMLHttpRequest() {
		var request = false;
		if(window.XMLHttpRequest) {
			request = new XMLHttpRequest();
			if(request.overrideMimeType) {
				request.overrideMimeType('text/xml');
			}
		}
		else if(window.ActiveXObject) {
			var versions = ['Microsoft.XMLHTTP', 'MSXML.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.7.0', 'Msxml2.XMLHTTP.6.0', 'Msxml2.XMLHTTP.5.0', 'Msxml2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP'];
			for(var i=0; i<versions.length; i++) {
				try {
					request = new ActiveXObject(versions[i]);
					if(request) {
						return request;
						break;
					}
				} catch(e) {}
			}
		}
		return request;
	}
	function xmlHttpSend(page)
	{
		xmlHttp=createXMLHttpRequest();
		xmlHttp.onreadystatechange = handleStateChange;
		xmlHttp.open("GET","./Ajax.do?page="+page);
		xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
		xmlHttp.send(null); //发送一定是domDoc文档对象,如果只发送xml将出错”statue=500”
	}
	*/

	// ************************************
	// 类名: CheckExitsAjaxClass
	// 功能: Ajax检查URL是否存在(无法跨域请求)
	// ************************************
	var CheckExitsAjaxClass = function() {
		// 初始化、指定处理函数、发送请求的函数
		this.oXMLHttp = function(){
			var oXML = null;
			// 开始初始化XMLHttpRequest对象
			if(window.XMLHttpRequest)
			{
				// Mozilla 浏览器
				oXML = new XMLHttpRequest();
				if(oXML.overrideMimeType){
					// 设置MiME类别
					oXML.overrideMimeType('text/xml');
				}
				return oXML;
			}
			else if(window.ActiveXObject){
				// IE浏览器
				try { oXML = new ActiveXObject("Msxml2.XMLHTTP"); }
				catch (e){
					try{ oXML = new ActiveXObject("Microsoft.XMLHTTP"); }
					catch (e) {}
				}
			}
			if(!oXML){
				// 异常，创建对象实例失败
				window.alert("不能创建XMLHttpRequest对象实例.");
				return false;
			}
			return oXML;
		};

		this.ajaxRequest = function(strURL,strMode,strContent,returnFunction){
			// 确定发送请求的方式和strURL以及是否同步执行下段代码
			if(strMode=="GET"){
				var d = new Date();
				var s = "";
				s += d.getHours();
				s += d.getMinutes();
				s += d.getSeconds();
				s += d.getMilliseconds();
				//var arr = strURL.split("?");
				//if(arr.length=="1"){
				if(strURL.indexOf("?")!=-1){
					strURL+="&time="+s;
				}else{
					strURL+="?time="+s;
				}
			}
			var oXML = this.oXMLHttp();
			var getData = "";
			if (oXML){
				oXML.open(strMode, strURL, false);
				// 处理返回信息的函数
				oXML.onreadystatechange = function(){
					var intReadyState = oXML.readyState;
					var oReturnTime = null;
					if(intReadyState == 4){
						// 判断对象状态
						if (oXML.status == 200){
							// 信息已经成功返回，开始处理信息
							getData = oXML.responseText;
							//getData = oXML.responseXML; //获取XML文档
							//getData = oXML.getAllResponseHeaders();
							//getData = oXML.getResponseHeader("Content-Type");
							//getData = oXML.getResponseHeader("Content-Length");
							//getData = oXML.getResponseHeader("Last-Modified");
							//getData = oXML.getResponseHeader("Server");
							try { returnFunction(getData); }
							catch (e){ eval(returnFunction+"(getData)"); }
						}
						else{
							// 页面不正常
							try { returnFunction(""); }
							catch (e) { eval(returnFunction+"('')"); }
						}
					}
				}
				if (strMode=="POST"){
					oXML.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
				}
				oXML.send(strContent);
			}
		};

		this.ajaxHttpRequest = function(strURL,strMode,strContent,returnFunction){
			// 确定发送请求的方式和strURL以及是否同步执行下段代码
			if(strMode=="GET"){
				var d = new Date();
				var s = "";
				s = s + d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
				if(strURL.indexOf("?")!=-1){
					strURL+="&time="+s;
				}else{
					strURL+="?time="+s;
				}
			}

			var oXML = this.oXMLHttp();
			var getData = "";
			oXML.Open(strMode, strURL, false);
			if (strMode=="POST"){ oXML.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); }
			try{ oXML.Send(strContent); }
			catch(e){}
			finally{
				var sts = oXML.Status;
				//alert(sts);
				//oXML.onreadystatechange = function(){
					//if(oXML.readyState == 4){
						// 判断对象状态
						if (sts == 200){
							// 信息已经成功返回，开始处理信息
							getData = oXML.responseText;
							//getData = oXML.getAllResponseHeaders();
							//getData = oXML.responseXML; //获取XML文档
							try { returnFunction(getData); }
							catch (e){ eval(returnFunction +"(getData)"); }
						}
						else{
							// 页面不正常
							try { returnFunction(""); }
							catch (e) { eval(returnFunction +"('')"); }
						}
					//}
				//}
				if(sts==200){return(true);}
				else{return(false);}
			}
		};
	};

	function getXML(URL) { //判断文件是否存在,当文件存在返回true,不存在返回false;
		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		xmlhttp.Open("GET",URL,false);
		try{
			xmlhttp.Send(null);
		}
		catch(e) { }
		finally {
			var result = xmlhttp.responseText;
			if(result) {
			var sts=xmlhttp.Status;
			if(sts!=200){
				return(true);
			}
			else {
				return(false);
			}
			}
			else {
				return(false);
			}
		}
	}


	 /*
	 ***___________________
	 */

	 /*
	 *--------------------------------------------------------------------
	 *
	 *创建一个XMLHttp实例
	 *return object 成功创建返回一个XMLHttp对象实例,否则返回false
	 *
	 *--------------------------------------------------------------------
	 */
	 function CreateAjax()
	 {
		var XMLHttp;
		try
		{
			XMLHttp = new ActiveXObject("Microsoft.XMLHTTP");   //IE的创建方式
		}
		catch(e)
		{
			try
			{
				XMLHttp = new XMLHttpRequest();     //FF等浏览器的创建方式
			}
			catch(e)
			{
				XMLHttp = false;        //创建失败，返回false
			}
		}
		return XMLHttp;     //返回XMLHttp实例
	 }

	function HandErrorNum(id)
	{
		_xmlhttp = CreateAjax();
		if(typeof(cmspath) == "undefined") { var cmspath = './'; }
		var url = ''+ error_dourl +'&st=error&id='+id+'&n='+Math.random()+'';		//这里添加了一个参数n,表示为一个随机数，以避免浏览器缓存
		if(_xmlhttp)    //判断XmlHttp是否创建成功
		{
			_xmlhttp.open('GET',url,true);
			_xmlhttp.send(null);
	/*
	向服务器发送请求，因为是get请求，会直接附在URL后面，所以这里括号中的数据为null，
	IE中也可以不写，但FF就必须加上null，否则会发送失败。
	*/
			//alert("报错成功,次数增加1");
		}
		else    //创建未成功
		{
			alert("您的浏览器不支持或未启用 XMLHttp!");
		}
	}

	function HandPlayNum(id)
	{
		_xmlhttp = CreateAjax();
		if(typeof(cmspath) == "undefined") { var cmspath = './'; }
		var url = ''+ error_dourl +'&st=hits&id='+id+'&n='+Math.random()+'';		//这里添加了一个参数n,表示为一个随机数，以避免浏览器缓存
		if(_xmlhttp)    //判断XmlHttp是否创建成功
		{
			_xmlhttp.open('GET',url,true);
			_xmlhttp.send(null);
	/*
	向服务器发送请求，因为是get请求，会直接附在URL后面，所以这里括号中的数据为null，
	IE中也可以不写，但FF就必须加上null，否则会发送失败。
	*/
			//alert("播放次数增加1");
			}
		else    //创建未成功
		{
			alert("您的浏览器不支持或未启用 XMLHttp!");
		}
	}

	 /*
	 ***___________________
	 */

	var tempString = "这是参数内容。"
	function MyFunc(str){
		alert("参数值: " + str);
	}
	//*=============================================================
	//*   功能： 修改 window.setInterval ，使之可以传递参数和对象参数
	//*   方法： setInterval (回调函数,时间,参数1,,参数n)  参数可为对象:如数组等
	//*=============================================================
	/*
	var __sto = setInterval;
	window.setInterval = function(callback,timeout,param){
		var args = Array.prototype.slice.call(arguments,2);
		var _cb = function(){
			callback.apply(null,args);
		}
		__sto(_cb,timeout);
	}
	*/
	//window.setInterval(MyFunc,3000,tempString);


/*------------------------------------------------------------------------------------
* Block Name       :       domReady Block
* Version          :       1.0.1 (2010-07-16)
* Author           :       Lajox
* Description      :       domReady()函数 执行模块
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/

	function __domReady(a,b)
	{
		var isvar=true,f=null,ida=null,idx=null,idn=0,arg=arguments,i=0;
		if(arg.length==0){ isvar=true; f=null; }
		else if(arg.length==1){ isvar=true; f=(a==''||a==null)?null:a; }
		else {
			isvar=true;
			if(a==''||a==null){ isvar=true; }
			else {
				ida = a.split(',');
				idn = ida.length;
				for(i=0;i<=idn-1;i++) {
					idx = ida[i].replace(' ','');
					if(idx!='' && idx!=null) {
						//isvar = isvar && (document.getElementById(idx+'') != null);
						isvar = isvar && (eval(idx+'') != null);
					}
				}
			}
			f=(b==''||b==null)?null:b;
		}
		this.n = typeof this.n == 'undefined' ? 0 : this.n + 1;
		if (
			typeof document.getElementsByTagName != 'undefined'
			&& (document.getElementsByTagName('body')[0] != null || document.body != null)
			&& isvar==true
		)
		{
			if(f!=null) {return f();}
		}
		else if(this.n < 60) { setTimeout('__domReady()', 250); }
	};


	var domtime = 0; //初始化设定domReady重试次数
	var domitem = null; //初始化domReady需要加载额外项
	function domReady(a,b)
	{
		var item=null;pa=[];f=null,ida=null,idx=null,idn=0,arg=arguments,i=0;
		if(arg.length==0){ item=null; f=null; }
		else if(arg.length==1){ item=null; f=(a==''||a==null)?null:a; }
		else {
			if(a==''||a==null){ item=null; }
			else {
				ida = a.split(',');
				idn = ida.length;
				for(i=0;i<=idn-1;i++) {
					idx = ida[i].replace(' ','');
					if(idx!='' && idx!=null) {
						pa[i] = idx+'';
					}
				}
				item = pa;
			}
			f=(b==''||b==null)?null:b;
		}
		domitem = item;
		if ( domReady.done && f!=null ) return f(); // 假如 DOM 已经加载，马上执行函数
		if ( domReady.timer && f!=null ) { // 假如我们已经增加了一个函数
			domReady.ready.push( f ); // 把它加入待执行函数清单中
		} else {
			// 为页面加载完毕绑定一个事件，
			// 以防它最先完成。使用addEvent
			//addEvent(window,"load", isDOMReady);
			if(window.attachEvent) {
				window.attachEvent("onload",function() {
					isDOMReady();
				});
			}
			else {
				window.addEventListener("load",function() {
					isDOMReady();
				},true);
			}
			if(f!=null) domReady.ready = [ f ]; // 初始化待执行函数的数组
			domReady.timer = setInterval( isDOMReady, 13 ); // 尽可能快地检查DOM是否已可用
		}

	};

	function isDOMReady() {	// 检查DOM是否已可操作
		var domisvar=true,isvar=true;
		if (domitem!=null) {
			for(i=0;i<=domitem.length-1;i++) {
				if(domitem[i]!='' && domitem[i]!=null) {
					//isvar = isvar && (document.getElementById(pa[i]) != null);
					isvar = isvar && (eval(pa[i]) != null);
				}
			}
		}
		domisvar = isvar;
		domtime = domtime + 1;
		if ( domReady.done ) return false; // 如果我们能判断出DOM已可用，忽略
		if(
			document && document.getElementsByTagName
			&& document.getElementById && document.body
			&& domisvar || domtime >= 100
		){ // 检查若干函数和元素是否可用
			clearInterval( domReady.timer ); // 如果可用，我们可以停止检查
			domReady.timer = null;
			for ( var i = 0; i < domReady.ready.length; i++ ){
				domReady.ready[i]();// 执行所有正等待的函数
			}
			domReady.ready = null;
			domReady.done = true; // 记录我们在此已经完成
		}
	}


	function relayFunction(f,v,t) {
		//功能: 再次获取变量值(延时值,已改变),直至条件满足执行函数;
		//f为函数名,v为变量,t为时间间隔
		t=(t==null)?250:t; //t缺省为250(毫秒)
		var n = 0;
		var t = setInterval(function() {
			var c = true;
			n++;
			c = false;
			if(v != '' && v != null) {
				c = true;
			}
			if(!c) { f(); clearInterval(t); }
			if(n >= 60) {
				clearInterval(t);
			}
		}, t); //这里根据情况调节时间间隔(单位毫秒)
	};
	/* 调用实例:
	var tempname = '';
	var foobar = new relayFunction(
		function() {
			tempname = document.getElementById("tempid");
			alert(tempname);
		}
		, tempname
	);
	*/

	//ready(func);//func为已存在的函数
	//ready(function(){alert('aaa')});
	(function(){
		var isReady=false; //判断onDOMReady方法是否已经被执行过
		var readyList= [];//把需要执行的方法先暂存在这个数组里
		var timer;//定时器句柄
		ready=function(fn) {
			if (isReady )
				fn.call( document);
			else
				readyList.push( function() { return fn.call(this);});
			return this;
		}
		var onDOMReady=function(){
			for(var i=0;i<readyList.length;i++){
				readyList.apply(document);
			}
			readyList = null;
		}
		var bindReady = function(evt){
			if(isReady) return;
			isReady=true;
			onDOMReady.call(window);
			if(document.removeEventListener){
				document.removeEventListener("DOMContentLoaded", bindReady, false);
			}else if(document.attachEvent){
				document.detachEvent("onreadystatechange", bindReady);
				if(window == window.top){
					clearInterval(timer);
					timer = null;
				}
			}
		};
		if(document.addEventListener){
			document.addEventListener("DOMContentLoaded", bindReady, false);
		}else if(document.attachEvent){
			document.attachEvent("onreadystatechange", function(){
				if((/loaded|complete/).test(document.readyState))
					bindReady();
			});
			if(window == window.top){
				timer = setInterval(function(){
					try{
							isReady||document.documentElement.doScroll('left');//在IE下用能否执行doScroll判断dom是否加载完毕
					}catch(e){
							return;
					}
					bindReady();
				},5);
			}
		}
	})();


/*------------------------------------------------------------------------------------
* Block Name       :       GetInfo Block
* Version          :       1.0.0 (2010-02)
* Author           :       Lajox
* Description      :       获取客户端信息操作模块
* WebSite          :       http://www.19www.com
* Email            :       lajox@19www.com
* Copyright        :       版权所有,源代码公开,各种用途均可免费使用,但是修改后必须
*                          把修改后的文件发送一份给作者.并且保留此版权信息
==DESC=================================================================================
* 暂无
---------------------------------------------------------------------------------------*/


/*___________获取URL参数 Begin___________*/

function request(paras){
	var pageURL = location.href;
	var paraString = pageURL.substring(pageURL.indexOf("?")+1,pageURL.length).split("&");
	var paraObj = {}
	for (i=0; j=paraString[i]; i++){
		paraObj[j.substring(0,j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=")+1,j.length);
	}
	var returnValue = paraObj[paras.toLowerCase()];
	if(typeof(returnValue)=="undefined"){ return ""; }
	else{ return returnValue; }
}
/*
//调用示例:
  var theurl=request("id");
*/

function GetRequest() { // 获取URL各个参数类
	var qs = location.search; //获取url中从?开始之后的字串
	var str = "";
	if (document.URL.split("?")[1] != undefined) {
		str = document.URL.split("?")[1].replace(/\#/g, "");
	}
	var theRequest = new Object();
	if (qs.indexOf("?") != -1) {
	  //str = qs.substr(1);
	  strs = str.split("&");
	  for(var i = 0; i < strs.length; i ++) {
		 theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
	  }
	}
	return theRequest;
}
/*
//调用示例:
  var Request = new GetRequest();
  var action = Request['action'];
  var id = Request['id'];
*/

function GetQueryString(name) { //获取URL各个参数 正则方法
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
	var r = window.location.search.substr(1).match(reg);
	if (r!=null) return unescape(r[2]); return null;
}
/*
//调用示例:
  var id = GetQueryString("id");
*/

/*___________获取URL参数 Begin___________*/

/*_________________________URL处理: [URL 编码 和 解码] Begin_________________________*/

var GetRealUrl = function(URL) { //获取URL完整路径
	URL = URL.replace(/\\/g,'/');
	var g = new getServer();
	var host = g.host;
	var dir_path_url = g.dir_path_url;
	var parent_dir_url = g.parent_dir_url;
	var LURL = URL.toLowerCase();
	var is_http = (LURL.indexOf('http://') != -1); //是否为 http://开头的地址
	var is_https = (LURL.indexOf('https://') != -1); //是否为 https:// SSL安全套层开头的地址
	var is_ftp = (LURL.indexOf('ftp://') != -1); //是否为 ftp://协议开头的地址
	var is_mms = (LURL.indexOf('mms://') != -1); //是否为 mms://协议开头的地址
	var is_rtsp = (LURL.indexOf('rtsp://') != -1); //是否为 rtsp://协议开头的地址
	var is_file1 = (LURL.indexOf('file:///') != -1); //是否为 本地文件地址(以file:///开头)
	var is_file2 = (LURL.indexOf('c:/') != -1 || LURL.indexOf('d:/') != -1 || LURL.indexOf('e:/') != -1 || LURL.indexOf('f:/') != -1 || LURL.indexOf('g:/') != -1 || LURL.indexOf('h:/') != -1 || LURL.indexOf('i:/') != -1 || LURL.indexOf('j:/') != -1 ); //是否为 本地文件地址(以盘符开头)
	var s = '';
	if(is_http||is_https||is_ftp||is_mms||is_rtsp||is_file1) { //如果是以 http://开头 或 mms://开头 或rtsp://开头 地址 则地址不需改变
		s = URL;
	}
	else if(is_file2) { //如果是以 以盘符开头 开头地址 则地址在最前面补上file:///
		//s = URL;
		s = 'file:///'+URL;
	}
	else if(URL.indexOf('/')==0) { //如果是以/开头 就补全 网站服务器地址
		s = s+'http://' + host + URL + '';
	}
	else { //如果不是以http://开头 且 不是以/开头 的相对地址 就补全 当前所在地址
		var s = '';
		//s = s+'http://' + host + dir_path_url + URL + '';
		s = s+'http://' + host + parent_dir_url + URL + '';
	}
	return s;
};

var Browser = function(){
	var browserName = navigator.userAgent.toLowerCase();
	var mybrowser = {
			"version": (browserName.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [0, '0'])[1],
			"safari": /webkit/i.test(browserName) && !this.chrome,
			"opera": /opera/i.test(browserName),
			"firefox":/firefox/i.test(browserName),
			"ie": /msie/i.test(browserName) && !/opera/.test(browserName),
			"mozilla": /mozilla/i.test(browserName) && !/(compatible|webkit)/.test(browserName) && !this.chrome,
			"chrome": /chrome/i.test(browserName) && /webkit/i.test(browserName) && /mozilla/i.test(browserName),
			"webkit": /webkit/i.test(browserName)
		}

	//浏览器版本号函数
	var br=navigator.userAgent.toLowerCase();
	var browserVer=(br.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [0, '0'])[1];

	//js浏览器判断函数
	var userBrowser = function(){
		var browserName=navigator.userAgent.toLowerCase();
		if(/msie/i.test(browserName) && !/opera/.test(browserName)){ //IE
			return "ie";
		}else if(/firefox/i.test(browserName)){ //Firefox
			return "firefox";
		}else if(/chrome/i.test(browserName) && /webkit/i.test(browserName) && /mozilla/i.test(browserName)){ //Chrome
			return "chrome";
		}else if(/opera/i.test(browserName)){ //Opera
			return "opera";
		}else if(/webkit/i.test(browserName) &&!(/chrome/i.test(browserName) && /webkit/i.test(browserName) && /mozilla/i.test(browserName))){ //Safari
			return "safari";
		}else{ //unknown
			return "";
		}
	}

	this.version = mybrowser["version"];
	this.ie = mybrowser["ie"];
	this.ff = mybrowser["firefox"];
	this.webkit = mybrowser["webkit"];
	this.bs = userBrowser();

	var Sys={};
	var na=navigator.userAgent.toLowerCase();
	if(window.activeXObject){ //IE
		Sys.ie=na.match(/msie([\d.]+)/)[1];
	}else if(document.getBoxObjectFor){ //Firefox
		Sys.firefox=na.match(/firefox\/([\d.]+)/)[1];
	}else if(window.MessageEvent&&!document.getBoxObjectFor){ //Google Chrome
		Sys.chrome=na.match(/chrome\/([\d.]+)/)[1];
	}else if(window.opera){ //opera
		Sys.opera=na.match(/opera.([\d.]+)/)[1];
	}else if (window.openDatabase) { //Safari
		Sys.safari = ua.match(/version\/([\d.]+)/)[1];
	}else {
	}
}

var Browser = new Browser(); //浏览器检测相关对象

/*
* 注册浏览器的DOMContentLoaded事件
* @param { Function } onready [必填]在DOMContentLoaded事件触发时需要执行的函数
* @param { Object } config [可选]配置项
*/
function onDOMContentLoaded(onready,config){
	//设置是否在FF下使用DOMContentLoaded（在FF2下的特定场景有Bug）
	this.conf = {
		enableMozDOMReady:true
	};
	if( config )
	for( var p in config)
	this.conf[p] = config[p];
	var isReady = false;
	function doReady(){
		if( isReady ) return;
		//确保onready只执行一次
		isReady = true;
		onready();
	}
	/*IE*/
	if( Browser.ie ){
		(function(){
			if ( isReady ) return;
			try {
				document.documentElement.doScroll("left");
			} catch( error ) {
				setTimeout( arguments.callee, 0 );
				return;
			}
			doReady();
		})();
		window.attachEvent('onload',doReady);
	}
	/*Webkit*/
	else if (Browser.webkit && Browser.version < 525){
		(function(){
			if( isReady ) return;
			if (/loaded|complete/.test(document.readyState))
			doReady();
			else
			setTimeout( arguments.callee, 0 );
		})();
		window.addEventListener('load',doReady,false);
	}
	/*FF Opera 高版webkit 其他*/
	else{
		if( !Browser.ff || Browser.version != 2 || this.conf.enableMozDOMReady)
		document.addEventListener( "DOMContentLoaded", function(){
				document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
				doReady();
			}, false );
		window.addEventListener('load',doReady,false);
	}
}

  /*
 ***___________________
 */
