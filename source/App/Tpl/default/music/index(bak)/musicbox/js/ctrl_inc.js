/*
* MusicBox其他控件配制
*/

/*
* ctrl_show(SongID,pType) 函数相关说明:
* SongID: 即媒体ID
* pType: 即后台设置的播放器格式类型
* pType的值有:
  'default','mp3','wma','swf','flv','wmv','rm','rmvb','mp4','3gp','mid'
  'wav','asf','avi','ra','mov','qt','mpg','dat','vob','flac'
*/

function ctrl_show(SongID,pType) {
	MusicName 	= eval("music_" + SongID);
	SingerName 	= eval("singer_" + SongID);
	MediaURL = eval("url_" + SongID);
	MediaURL = GetRealUrl(MediaURL);
	var MediaNameFull = ""; //显示媒体信息
	if(SingerName+''=='') MediaNameFull = ""+MusicName+"";
	if(SingerName+''!='') MediaNameFull = ""+MusicName+" ["+SingerName+"]";
		xStr = MediaNameFull;
		//if(xStr.length < 16){
		//if(NumLen(xStr) < 16){
		if(StrTemp.GetLength(xStr) < 28){
			xStr = xStr;
		}else{
			//xStr = xStr.substr(0, 16) + "..";
			//xStr = NumLenStr(xStr,16) + "..";
			xStr = getInterceptedStr(xStr,28) + "..";
		}
	the("Lrc_Down").innerHTML = "<input type=\"button\" class=\"DownBtn OnBtn1\" value=\"歌词\" onClick=\"window.open(\'"+ lrc_txt +"&id="+SongID+"\',\'\',\'width=488,height=450,top=150,left=300,resizable=yes,,status=no,scrollbars=yes,toolbar=no,menubar=no,location=no\')\" onMouseOut=\"this.className='DownBtn OnBtn1'\" onMouseOver=\"this.className='DownBtn OnBtn2' alt=\"点此下载["+MusicName+"]\" /><input type=\"button\" class=\"DownBtn OnBtn1\" value=\"下载\" onClick=\"window.open(\'"+ down_url +"&id="+SongID+"\',\'\',\'width=488,height=300,top=180,left=300,resizable=no\')\" onMouseOut=\"this.className='DownBtn OnBtn1'\" onMouseOver=\"this.className='DownBtn OnBtn2' alt=\"点此下载["+MusicName+"]\" />";
	the("MusicInfo").innerHTML = "<font color=#669900>&#x5A92;&#x4F53;:</font>&nbsp;<a href=\""+playcx+"&id="+SongID+"\" target=\"_blank\" title=\""+MediaNameFull+"\">"+xStr+"</a>";
	the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"280\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ lrc_show +"&id=" + SongID  + "\" allowTransparency=true>对不起，你的浏览器不支持框架。</iframe>";

	if(pType == 'default') {
		if ( getPlayerType(the("PlayerObj").title) == 'default' || getPlayerType(the("PlayerObj").title) == '' ) {
			if (sPlayerList['default']=="auto") {
				var is_mms = (MediaURL.toLowerCase().indexOf('mms:') != -1); //是否为 mms://协议地址
				var is_rtsp = (MediaURL.toLowerCase().indexOf('rtsp:') != -1); //是否为 rtsp://协议地址
				if (is_mms) {
					if(MediaURL.toLowerCase().indexOf('.wmv') != -1) {
						the("MusicGeCi").innerHTML = '';
						the("LrcShow_DownAndLrc").style.display = 'block';
						the("MMShow").style.display = 'block';
						the("MMShow").style.width = 370;
						the("StateInfo").innerHTML = "";
						the("Lrc_Down").style.display = 'none';
						the("Geci_Content").style.display = 'none';
						the("Exobud").style.filter = '';
						the("MusicGeCi").style.display = 'none';
					}
					else {
						the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"280\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ lrc_show +"&id=" + SongID  + "&player=media\" allowTransparency=true>对不起，你的浏览器不支持框架。</iframe>";
						the("LrcShow_DownAndLrc").style.display = 'block';
						the("MMShow").style.display = 'block';
						the("MMShow").style.width = 370;
						the("Lrc_Down").style.display = 'block';
						the("Geci_Content").style.display = 'block';
						the("Exobud").style.filter = 'xray()';
						the("MusicGeCi").style.bgColor = '#000000';
						the("MusicGeCi").style.display = 'block';
					}
				}
				else if(is_rtsp) {
					if(MediaURL.toLowerCase().indexOf('.rmvb') != -1) {
						the("MusicGeCi").innerHTML = '';
						the("LrcShow_DownAndLrc").style.display = 'block';
						the("MMShow").style.display = 'block';
						the("MMShow").style.width = 370;
						the("StateInfo").innerHTML = "";
						the("Lrc_Down").style.display = 'none';
						the("Geci_Content").style.display = 'none';
						the("Exobud").style.filter = '';
						the("MusicGeCi").style.display = 'none';
					}
					else {
						the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"278\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ lrc_show +"&id=" + SongID  + "&player=real\" allowTransparency=true>对不起，你的浏览器不支持框架。</iframe>";
						the("LrcShow_DownAndLrc").style.display = 'block';
						the("MMShow").style.display = 'block';
						the("MMShow").style.width = 292;
						the("Lrc_Down").style.display = 'block';
						the("Geci_Content").style.display = 'block';
						the("Exobud").style.filter = 'xray()';
						the("MusicGeCi").style.bgColor = '#000000';
						the("MusicGeCi").style.display = 'block';
					}
				}
				else { // 媒体地址为 http或ftp协议地址等
					var xmlhttp = new HttpRequest(); // 兼容的 XMLHttpRequest 创建方法
					var SendURL;
					SendURL = encodeURL(MediaURL);
					SendURL = checkurl + '&enurl=' + SendURL;
					xmlhttp.onfinish = function(){
						var result = this.responseText;
						var is_real_file = (result.indexOf('FileExt:') != -1);
						var is_unknown_file = (result.indexOf('Content-Type:') != -1);
						if (is_real_file) {
							eReturnStr = result.replace('FileExt:','');
						}
						if (is_unknown_file) {
							try{
								eReturnStr = sFileType[result].split(',')[0];
							}
							catch(e) {
								eReturnStr = 'html'; //可能是网页文档,包括404返回页
							}
						}
						if (result=='Error') {
							eReturnStr = eReturnStr_other;
						}
						Crrl_ShowByExt(SongID,eReturnStr);

					}
					xmlhttp.onerror=function(e){/*alert(e.message);*/}
					xmlhttp.open("get",SendURL,true);
					try{
						xmlhttp.send(null);
					}
					catch(e) { }
				}
			}
		}
		else {

		}
	}
	else {
		Crrl_ShowByExt(SongID,pType);
	}
}

function Crrl_ShowByExt(SongID,pType){
	if(pType == 'mp3' || pType == 'wma'){
	//对于 windows media player音频格式播放器类型 下面进行显示
		the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"280\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ lrc_show +"&id=" + SongID  + "&player=media\" allowTransparency=true>对不起，你的浏览器不支持框架。</iframe>";
		the("LrcShow_DownAndLrc").style.display = 'block';
		the("MMShow").style.display = 'block';
		the("MMShow").style.width = '292px';
		the("Lrc_Down").style.display = 'block';
		the("Geci_Content").style.display = 'block';
		the("Exobud").style.filter = 'xray()';
		the("MusicGeCi").style.bgColor = '#000000';
		the("MusicGeCi").style.display = 'block';
		setTimeout( function(){setSelectItemColor('SongList',SongID,'#ff0000');}, 100);
	}
	else if(pType == 'rm'){
	//对于 Real Player音频格式播放器类型 下面进行显示
		the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"280\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ lrc_show +"&id=" + SongID  + "&player=real\" allowTransparency=true>对不起，你的浏览器不支持框架。</iframe>";
		the("LrcShow_DownAndLrc").style.display = 'block';
		the("MMShow").style.display = 'block';
		the("MMShow").style.width = '292px';
		the("Lrc_Down").style.display = 'block';
		the("Geci_Content").style.display = 'block';
		the("Exobud").style.filter = 'xray()';
		the("MusicGeCi").style.bgColor = '#000000';
		the("MusicGeCi").style.display = 'block';
		setTimeout( function(){setSelectItemColor('SongList',SongID,'#ff0000');}, 100);
	}
	else if(pType == 'mid'){
	//对于 windows media player （Radio类型） 下面进行隐藏
		the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"308\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ intro_url +"&id=" + SongID  + "\">对不起，你的浏览器不支持框架。</iframe>";
		the("LrcShow_DownAndLrc").style.display = 'none';
		the("MMShow").style.display = 'none';
		the("MMShow").style.width = '0px';
		the("Lrc_Down").style.display = 'none';
		the("Geci_Content").style.display = 'block';
		the("Exobud").style.filter = 'invert()';
		the("MusicGeCi").style.bgColor = '#000000';
		the("MusicGeCi").style.display = 'block';
		setTimeout( function(){setSelectItemColor('SongList',SongID,'#ff0000');}, 100);
		}
	else if(pType == 'wmv' || pType == 'swf' || pType == 'flv'){
	//对于 WMV SWF FLV 视频播放器
		the("MusicGeCi").innerHTML = '';
		the("LrcShow_DownAndLrc").style.display = 'block';
		the("MMShow").style.display = 'block';
		the("MMShow").style.width = '370px';
		the("StateInfo").innerHTML = "";
		the("Lrc_Down").style.display = 'none';
		the("Geci_Content").style.display = 'none';
		the("Exobud").style.filter = '';
		the("MusicGeCi").style.display = 'none';
		setTimeout( function(){setSelectItemColor('SongList',SongID,'#ff0000');}, 100);
	}
	else if(pType == 'avi' || pType == 'mpeg'){
	//对于 AVI MPEG 视频播放器
		the("MusicGeCi").innerHTML = '';
		the("LrcShow_DownAndLrc").style.display = 'block';
		the("MMShow").style.display = 'block';
		the("MMShow").style.width = '370px';
		the("StateInfo").innerHTML = "";
		the("Lrc_Down").style.display = 'none';
		the("Geci_Content").style.display = 'none';
		the("Exobud").style.filter = '';
		the("MusicGeCi").style.display = 'none';
		setTimeout( function(){setSelectItemColor('SongList',SongID,'#ff0000');}, 100);
	}
	else{
		the("MusicGeCi").innerHTML = "<iframe name=\"frameshow\" frameborder=\"0\" border=\"0\" scrolling=\"no\" width=\"370\" height=\"280\"  marginWidth=\"0\" marginHeight=\"0\" align=\"middle\" src=\""+ lrc_show +"&id=" + SongID  + "&player=media\" allowTransparency=true>对不起，你的浏览器不支持框架。</iframe>";
		the("LrcShow_DownAndLrc").style.display = 'block';
		the("MMShow").style.display = 'block';
		the("MMShow").style.width = '292px';
		the("Lrc_Down").style.display = 'block';
		the("Geci_Content").style.display = 'block';
		the("Exobud").style.filter = 'xray()';
		the("MusicGeCi").style.bgColor = '#000000';
		the("MusicGeCi").style.display = 'block';
		setTimeout( function(){setSelectItemColor('SongList',SongID,'#ff0000');}, 100);
	}
}

function setFrameBgColor(color) {
	window.frameshow.document.body.bgColor = color;
}