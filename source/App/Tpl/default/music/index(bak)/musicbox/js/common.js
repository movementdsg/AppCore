
/*————————————————————————————————————————————————————————————————————*/

var player_server = ''+ cmspath +'/musicbox/asp/';

/*————————————————————————————————————————————————————————————————————*/

// 检查指定的地址是否存在.
function CheckURLExits(sUrl){
	var oXML = new CheckExitsAjaxClass();
	//oXML.ajaxRequest(sUrl,"GET","",ajaxReturn);
	oXML.ajaxHttpRequest(sUrl,"GET","",ajaxReturn);
	// 处理返回的信息
	function ajaxReturn(sSource){
		if (sSource.length>0) { alert("文件存在."); }
		else { alert("文件不存在!"); }
	}
}

var eReturnValue = false;

function CheckUrlActive(sUrl){
	var fstr;
	if(getXML(sUrl)){
		fstr = "文件存在";
		//alert(fstr);
		return true;
	}
	else {
		fstr = "文件不存在";
		//alert(fstr);
		return false;
	}
}

var sFileType = { //文件类型 与 Content-Type对应值
	"Content-Type:audio/mpeg" : "mp3" , // "Content-Type:audio/mpeg" 对应的格式有: mp3
	"Content-Type:audio/x-ms-wma" : "wma" , // "Content-Type:audio/x-ms-wma" 对应的格式有: wma
	"Content-Type:video/x-ms-wmv" : "wmv" , // "Content-Type:video/x-ms-wmv" 对应的格式有: wmv
	"Content-Type:audio/mid" : "mid" , // "Content-Type:audio/mid" 对应的格式有: mid
	"Content-Type:audio/wav" : "wav" , // "Content-Type:audio/wav" 对应的格式有: wav
	"Content-Type:video/x-ms-asf" : "asf" , // "Content-Type:video/x-ms-asf" 对应的格式有: asf
	"Content-Type:video/avi" : "avi" , // "Content-Type:video/avi" 对应的格式有: avi
	"Content-Type:application/x-shockwave-flash" : "swf" ,
	// "Content-Type:application/x-shockwave-flash" 对应的格式有: swf
	"Content-Type:audio/x-pn-realaudio" : "ra" , // "Content-Type:audio/x-pn-realaudio" 对应的格式有: ra
	"Content-Type:video/quicktime" : "mov,qt" , // "Content-Type:video/quicktime" 对应的格式有: mov,qt
	"Content-Type:video/mpeg" : "mpg,mpeg" , // "Content-Type:video/mpeg" 对应的格式有: mpg,mpeg
	"Content-Type:application/octet-stream" : "rm,rmvb,flv,dat,mp4,3gp,vob,flac,js,asa,mdb,doc,xls" ,
	// "Content-Type:application/octet-stream" 对应的格式为一系列未知类型有:
	//  媒体类型: rm,rmvb,flv,dat,mp4,3gp,vob,flac 和 其他类型: js,asa,mdb,doc,xls等
	"Content-Type:text/html" : "html,htm,asp,php,jsp,aspx" ,
	// "Content-Type:text/html" 对应的格式为一系列文档类型有: html,htm.asp.php.jsp.aspx等
	"Content-Type:text/plain" : "txt" , // "Content-Type:text/plain" 对应的格式有: txt
	"Content-Type:text/css" : "css" , // "Content-Type:text/css" 对应的格式有: css
	"Content-Type:text/xml" : "xml" , // "Content-Type:text/xml" 对应的格式有: xml
	"Content-Type:image/gif" : "gif" , // "Content-Type:image/gif" 对应的格式有: gif
	"Content-Type:image/jpeg" : "jpg,jpeg" , // "Content-Type:image/jpeg" 对应的格式有: jpg,jpeg
	"Content-Type:image/png" : "png" , // "Content-Type:image/png" 对应的格式有: png
	"Content-Type:image/bmp" : "bmp" , // "Content-Type:image/bmp" 对应的格式有: bmp
	"Content-Type:image/x-icon" : "ico"  // "Content-Type:image/x-icon" 对应的格式有: ico
};

/*
	js对文字进行编码涉及3个函数: escape,encodeURI,encodeURIComponent，
	           相应3个解码函数: unescape,decodeURI,decodeURIComponent
	ASP代码要进行编码用 Server.UrlEncode()
	escape对0-255以外的unicode值进行编码时输出%u****格式,其它情况下escape,encodeURI,encodeURIComponent编码结果相同
	encodeURIComponent将中文、韩文等特殊字符转换成utf-8格式的url编码
	escape不编码字符有69个：*，+，-，.，/，@，_，0-9，a-z，A-Z
	encodeURI不编码字符有82个：!，#，$，&，'，(，)，*，+，,，-，.，/，:，;，=，?，@，_，~，0-9，a-z，A-Z
	encodeURIComponent不编码字符有71个：!， '，(，)，*，-，.，_，~，0-9，a-z，A-
	js使用数据时可以使用escape
	进行url跳转时可以整体使用encodeURI ( location.href="/encodeURI"("http://cang.baidu.com/do/s?word=百度&ct=21"); )
	传递参数时需要使用encodeURIComponent，这样组合的url才不会被#等特殊字符截断
*/
function encodestr(str){
	return encodeURIComponent(encodeURIComponent(str));
}
function decodestr(str){
	return decodeURIComponent(decodeURIComponent(str));
}

function Encode_MediaURL(u){
	var sURL;
	sURL=u;
	sURL=(sURL==null)?'':sURL;
	sURL = sURL.replace(/\\/g,'\\\\');
	//sURL = escape(sURL);
	//sURL = encodeURI(sURL);
	//sURL = urlEncode(sURL);
	return sURL;
}
function Decode_MediaURL(u){
	var sURL;
	sURL=u;
	sURL=(sURL==null)?'':sURL;
	sURL = sURL.replace(/\//g,'\\');
	//sURL = unescape(sURL);
	//sURL = urlDecode(sURL);
	return sURL;
}
function encodeURL(u){
	var sURL;
	sURL=u;
	sURL=(sURL==null)?'':sURL;
	/*
	sURL = sURL.replace(/\//g,'\\');
	//sURL = urlEncode(sURL);
	return sURL;
	*/
	sURL=base64encode(sURL);
	return sURL;
}
function decodeURL(u){
	var sURL;
	sURL=u;
	sURL=(sURL==null)?'':sURL;
	/*
	sURL = sURL.replace(/\\/g,'/');
	//sURL = urlDecode(sURL);
	return sURL;
	*/
	sURL=base64decode(sURL);
}
/*_________________________ URL处理: [URL 编码 和 解码] End_________________________*/

var getFileInfo;
getFileInfo = '';

var eReturnStr;
eReturnStr = '';

var eReturnStr_other;
var eReturnStr_unknown;
/*
 * eReturnStr_other, eReturnStr_unknown 的值表示:
 * 无法直接判断后缀名的媒体文件通过服务器进一步判断时，不存在,则
 * ''表示: 默认显示的播放器类型为 "" 设置对应的播放器;
 * 'unknown'表示: 默认显示的播放器类型为"unknown"设置的对应播放器;
*/
eReturnStr_other = '';
eReturnStr_unknown = 'unknown';

/*<![CDATA[*/
/*______________ 基于JSONP原理调用方法 Begin_______________*/

function JSONP_GetFileExt(URL,pType,SongID){
	var jsonpURL,MediaURL;
	//jsonpURL = encodeURL(URL);
	jsonpURL = encodestr(URL);
	jsonpURL = jsonpurl + '&url=' + jsonpURL + '&ptype=' + pType + '&SongID=' + SongID ;
	var JSONP = document.createElement("script");
	//FF:onload IE:onreadystatechange
	JSONP.onload = JSONP.onreadystatechange = function(){ //onreadystatechange, 仅IE
		if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
			JSONP.onload = JSONP.onreadystatechange = null;//请内存，防止IE memory leaks
		}
	};
	JSONP.type = "text/javascript";
	JSONP.src = jsonpURL;
	document.getElementsByTagName("head")[0].appendChild(JSONP); //在head之后添加js文件
	//document.write(jsonpURL);
}

function jsonpHandle(URL,ReturnText,pType,SongID){ // JSONP 方法, 可跨域
	//MediaURL = decodeURL(URL);
	MediaURL = decodestr(URL);
	//alert(MediaURL);
	var result = ReturnText;
	var is_real_file = (result.indexOf('FileExt:') != -1);
	var is_unknown_file = (result.indexOf('Content-Type:') == -1 || result == "Content-Type:" );
	if (is_real_file) {
		eReturnStr = result.replace('FileExt:','');
	}
	else if (!is_unknown_file) {
		try{
			eReturnStr = sFileType[result].split(',')[0];
		}
		catch(e) {
			eReturnStr = 'html'; //可能是网页文档,包括404返回页
		}
	}
	else if (result=='Error') {
		eReturnStr = eReturnStr_unknown;
	}
	else if (result=='Content-Type:') {
		eReturnStr = eReturnStr_other;
	}
	else {
		eReturnStr = eReturnStr_unknown;
	}
	Return_Func(MediaURL,eReturnStr,pType,SongID);
}

/*______________ 基于JSONP原理调用方法 End_______________*/

/*______________ CrossJS调用方法 Begin_______________*/

/*
  脚本跨域函数
  必填参数: script_e, path_s, callback_f
*/
function script_cross_domain_f(script_e, path_s, callback_f) { /* var:name，void return，func:none */
  if(typeof script_e==='string'){ script_e = document.getElementById(script_e); }
  if(!script_e){ document.body.appendChild( script_e = document.createElement('script') ); }
  if(!callback_f){ callback_f = function(){} };
  script_e.onload = function() {
    callback_f()
  };
  script_e.onreadystatechange = function() {
    if(script_e.readyState=='loaded')
    {
      callback_f()
    }
  };
  script_e.src = path_s;
}

var cb_url = "";
var cb_ext = "";
var cb_type = "";
var cb_songid = "";
function CrossJS_GetFileExt(URL,pType,SongID){
	var jsURL,MediaURL;
	//jsURL = encodeURL(URL);
	jsURL = encodestr(URL);
	jsURL = crossurl + '&url=' + jsURL + '&ptype=' + pType + '&SongID=' + SongID ;
	//document.write(jsURL);
	script_cross_domain_f
	(
	  'cross_domain_e'
	  , jsURL
	  , function()
		{
			//MediaURL = decodeURL(cb_url);
			MediaURL = decodestr(cb_url);
			var result = cb_ext;
			var pType = cb_type;
			var SongID = cb_songid;
			//alert(MediaURL);
			var is_real_file = (result.indexOf('FileExt:') != -1);
			var is_unknown_file = (result.indexOf('Content-Type:') == -1 || result == "Content-Type:" );
			if (is_real_file) {
				eReturnStr = result.replace('FileExt:','');
			}
			else if (!is_unknown_file) {
				try{
					eReturnStr = sFileType[result].split(',')[0];
				}
				catch(e) {
					eReturnStr = 'html'; //可能是网页文档,包括404返回页
				}
			}
			else if (result=='Error') {
				eReturnStr = eReturnStr_unknown;
			}
			else if (result=='Content-Type:') {
				eReturnStr = eReturnStr_other;
			}
			else {
				eReturnStr = eReturnStr_unknown;
			}
			Return_Func(MediaURL,eReturnStr,pType,SongID);
		}
	);
}

/*______________ CrossJS调用方法 End_______________*/

/*]]>*/

/*_________________________ IE的方法: <id.startDownload> Begin_________________________*/

function show_result(s){
	//var re=/^FileExt\:/i;
	//var is_real_file = (re.test(s));
	var is_real_file = (s.indexOf('FileExt:') != -1);
	var is_unknown_file = (s.indexOf('Content-Type:') == -1 || s == "Content-Type:" );
	if (is_real_file) {
		eReturnStr = s.replace('FileExt:','');
	}
	else if (!is_unknown_file) {
		try{
			eReturnStr = sFileType[s].split(',')[0];
		}
		catch(e) {
			eReturnStr = 'html'; //可能是网页文档,包括404返回页
		}
	}
	else if (s=='Error') {
		eReturnStr = eReturnStr_unknown;
	}
	else if (s=='Content-Type:') {
		eReturnStr = eReturnStr_other;
	}
	else {
		eReturnStr = eReturnStr_unknown;
	}
	getFileInfo = eReturnStr ;
	return eReturnStr;
}

function IE_GetFileExt(URL,pType,SongID) {
	var SendURL,MediaURL;
	MediaURL = GetRealUrl(URL);
	SendURL = encodeURL(URL);
	SendURL = checkurl + '&enurl=' + SendURL;
	getFileInfo = '';
	var is_mms = (MediaURL.indexOf('mms:') != -1); //是否为 mms://协议地址
	var is_rtsp = (MediaURL.indexOf('rtsp:') != -1); //是否为 rtsp://协议地址
	if (is_mms) { //媒体地址为 mms协议地址
		if(MediaURL.toLowerCase().indexOf('.wmv') != -1){
			Return_Func(MediaURL,'mms_audio',pType,SongID);
		}
		else {
			Return_Func(MediaURL,'mms_video',pType,SongID);
		}
	}
	else if(is_rtsp) { //媒体地址为 rtsp协议地址
		if(MediaURL.toLowerCase().indexOf('.rmvb') != -1){
			Return_Func(MediaURL,'rtsp_video',pType,SongID);
		}
		else {
			Return_Func(MediaURL,'rtsp_audio',pType,SongID);
		}
	}
	else { // 媒体地址为 http协议地址
		getObjectById("oDownload").startDownload(SendURL,show_result); //仅IE 5以上版本浏览器支持
		var foobar = new relayFunction(
			function() {
				Return_Func(MediaURL,getFileInfo,pType,SongID);
			}
			, getFileInfo
			, 250
		);
	}
}

/*______________ IE的方法 End_______________*/

/*_________________________ Ajax调用方法 Begin_________________________*/

function Ajax_GetFileExt(URL,pType,SongID) {
	var SendURL,MediaURL;
	MediaURL = GetRealUrl(URL);
	SendURL = encodeURL(URL);
	SendURL = checkurl + '&enurl=' + SendURL;
	//window.open(SendURL,"_blank");
	var is_mms = (MediaURL.indexOf('mms:') != -1); //是否为 mms://协议地址
	var is_rtsp = (MediaURL.indexOf('rtsp:') != -1); //是否为 rtsp://协议地址
	if (is_mms) {
		if(MediaURL.toLowerCase().indexOf('.wmv') != -1){
			Return_Func(MediaURL,'mms_video',pType,SongID);
		}
		else {
			Return_Func(MediaURL,'mms_audio',pType,SongID);
		}
	}
	else if(is_rtsp) {
		if(MediaURL.toLowerCase().indexOf('.rmvb') != -1){
			Return_Func(MediaURL,'rtsp_video',pType,SongID);
		}
		else {
			Return_Func(MediaURL,'rtsp_audio',pType,SongID);
		}
	}
	else { // 媒体地址为 http或ftp协议地址等
		var xmlhttp = new HttpRequest(); // 兼容的 XMLHttpRequest 创建方法
		xmlhttp.onfinish = function(){
			var result = this.responseText;
			var is_real_file = (result.indexOf('FileExt:') != -1);
			var is_unknown_file = (result.indexOf('Content-Type:') == -1 || result == "Content-Type:" );
			if (is_real_file) {
				eReturnStr = result.replace('FileExt:','');
			}
			else if (!is_unknown_file) {
				try{
					eReturnStr = sFileType[result].split(',')[0];
				}
				catch(e) {
					eReturnStr = 'html'; //可能是网页文档,包括404返回页
				}
			}
			else if (result=='Error') {
				eReturnStr = eReturnStr_unknown;
			}
			else if (result=='Content-Type:') {
				eReturnStr = eReturnStr_other;
			}
			else {
				eReturnStr = eReturnStr_unknown;
			}
			Return_Func(MediaURL,eReturnStr,pType,SongID);
		}
		xmlhttp.onerror=function(e){/*alert(e.message);*/}
		xmlhttp.open("get",SendURL,true);
		try{
			xmlhttp.send(null);
		}
		catch(e) {/*alert(e.message);*/}
/*
 		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); //IE的 XMLHttp ActiveX 创建方法
		xmlhttp.Open("GET",SendURL,false);
		try{
			xmlhttp.Send();
		}
		catch(e) { }
		finally {
			var result = xmlhttp.responseText;
			var is_real_file = (result.indexOf('FileExt:') != -1);
			var is_unknown_file = (result.indexOf('Content-Type:') == -1 || result == "Content-Type:" );
			if (is_real_file) {
				eReturnStr = result.replace('FileExt:','');
			}
			else if (!is_unknown_file) {
				try{
					eReturnStr = sFileType[result].split(',')[0];
				}
				catch(e) {
					eReturnStr = 'html'; //可能是网页文档,包括404返回页
				}
			}
			else if (result=='Error') {
				eReturnStr = eReturnStr_unknown;
			}
			else if (result=='Content-Type:') {
				eReturnStr = eReturnStr_other;
			}
			else {
				eReturnStr = eReturnStr_unknown;
			}
			Return_Func(MediaURL,eReturnStr,pType,SongID);
		}
*/
	}
}

/*_________________________ Ajax调用方法 End_________________________*/

function getPlayerType(cStr) {
	cStr=(cStr==null)?"":cStr;
	var s="";
/* 	var strRegex = "(player\:)([a-zA-Z]{1}([a-zA-Z0-9]|[_]){0,})(\])$";
	var re=new RegExp(strRegex);
	if ((arr = re.exec(cStr))!=null){
		s = RegExp.$2;
	} */
	var is_tag = (cStr.indexOf('[player:') != -1);
	if (is_tag) {
		//s = cStr.replace('[player:','').replace(']','').replace(' ','');
		s = cStr.replace('[player:','').replace(']','').replace(/\s/g,'');
	}
	else {
		s="default";
	}
	return s;
}

function FileExt_Func(MediaURL,$ext,pType,SongID) {
	var showplayer = '';
	if($ext=='' || $ext==null || $ext=='unknown') {
		//output_what('文件地址出错',pType,SongID,MediaURL,$ext);
		//showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
		showplayer = eval(""+sPlayerList[$ext]+"('"+MediaURL+"')");
		output_what(showplayer,pType,SongID,MediaURL,$ext);
	}
	else {
		try{
			if (sPlayerList[$ext] =="") {
				showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
			}
			else {
				showplayer = eval(""+sPlayerList[$ext]+"('"+MediaURL+"')");
			}
		}
		catch(e) {
			showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
		}
		output_what(showplayer,pType,SongID,MediaURL,$ext);
	}
}

function Return_Func(MediaURL,$ext,pType,SongID) {
	var p_s = getObjectById("PlayerObj");
	var p_tag = p_s.title;
	//if ( p_s.title == '[player:default]' ) { }
	p_tag = getPlayerType(p_tag);
	var showplayer = '';
	var is_mms_audio = ($ext == 'mms_audio'); //是否为 mms://协议地址(音频)
	var is_mms_video = ($ext == 'mms_video'); //是否为 mms://协议地址(视频)
	var is_rtsp_audio = ($ext == 'rtsp_audio'); //是否为 rtsp://协议地址(音频)
	var is_rtsp_video = ($ext == 'rtsp_video'); //是否为 rtsp://协议地址(视频)
	if ( p_tag=='default' ) {
		if ( pType=='default' ) {
			if (sPlayerList['default']=="auto") {
				FileExt_Func(MediaURL,$ext,pType,SongID);
			}
			else {
				try{
					var d_p = sPlayerList['default'];
					showplayer = eval(""+sPlayerList[d_p]+"('"+MediaURL+"')");
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
				catch(e) {
					FileExt_Func(MediaURL,$ext,pType,SongID);
				}
			}
		}
		else {
			try{
				showplayer = eval(""+sPlayerList[pType]+"('"+MediaURL+"')");
				output_what(showplayer,pType,SongID,MediaURL,$ext);
			}
			catch(e) {
				if (is_mms_audio) { //媒体地址为 mms协议地址
					var mms_p = sPlayerList['mms_audio'];
					//alert("mms协议地址:"+MediaURL);
					try{
						if (mms_p != "") {
							showplayer = eval(""+sPlayerList['mms_audio']+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
						else {
							showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
					}
					catch(e) {
						showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				else if(is_mms_video) { //媒体地址为 mms协议地址
					var mms_p = sPlayerList['mms_video'];
					//alert("mms协议地址:"+MediaURL);
					try{
						if (mms_p != "") {
							showplayer = eval(""+sPlayerList['mms_video']+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
						else {
							showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
					}
					catch(e) {
						showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				else if(is_rtsp_audio) { //媒体地址为 rtsp协议地址
					var rtsp_p = sPlayerList['rtsp_audio'];
					//alert("rtsp协议地址:"+MediaURL);
					try{
						if (rtsp_p != "") {
							showplayer = eval(""+sPlayerList['rtsp_audio']+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
						else {
							showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
					}
					catch(e) {
						showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				else if(is_rtsp_video) { //媒体地址为 rtsp协议地址
					var rtsp_p = sPlayerList['rtsp_video'];
					//alert("rtsp协议地址:"+MediaURL);
					try{
						if (rtsp_p != "") {
							showplayer = eval(""+sPlayerList['rtsp_video']+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
						else {
							showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
							output_what(showplayer,pType,SongID,MediaURL,$ext);
						}
					}
					catch(e) {
						showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				else { // 媒体地址为 http协议地址
					if( pType != '' ){
						try{
							showplayer = eval(""+sPlayerList[pType]+"('"+MediaURL+"')");
						}
						catch(e) {
							try{
								if (sPlayerList[$ext] =="") {
									showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
								}
								else {
									showplayer = eval(""+sPlayerList[$ext]+"('"+MediaURL+"')");
								}
							}
							catch(e) {
								showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
							}
						}
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
					else {
						showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
			}
		}
	}
	else {
		try{
			showplayer = eval(""+sPlayerList[p_tag]+"('"+MediaURL+"')");
			output_what(showplayer,pType,SongID,MediaURL,$ext);
		}
		catch(e) {
			if (is_mms_audio) { //媒体地址为 mms协议地址
				var mms_p = sPlayerList['mms_audio'];
				//alert("mms协议地址:"+MediaURL);
				try{
					if (mms_p != "") {
						showplayer = eval(""+sPlayerList['mms_audio']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
					else {
						showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				catch(e) {
					showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
			}
			else if(is_mms_video) { //媒体地址为 mms协议地址
				var mms_p = sPlayerList['mms_video'];
				//alert("mms协议地址:"+MediaURL);
				try{
					if (mms_p != "") {
						showplayer = eval(""+sPlayerList['mms_video']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
					else {
						showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				catch(e) {
					showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
			}
			else if(is_rtsp_audio) { //媒体地址为 rtsp协议地址
				var rtsp_p = sPlayerList['rtsp_audio'];
				//alert("rtsp协议地址:"+MediaURL);
				try{
					if (rtsp_p != "") {
						showplayer = eval(""+sPlayerList['rtsp_audio']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
					else {
						showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				catch(e) {
					showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
			}
			else if(is_rtsp_video) { //媒体地址为 rtsp协议地址
				var rtsp_p = sPlayerList['rtsp_video'];
				//alert("rtsp协议地址:"+MediaURL);
				try{
					if (rtsp_p != "") {
						showplayer = eval(""+sPlayerList['rtsp_video']+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
					else {
						showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
						output_what(showplayer,pType,SongID,MediaURL,$ext);
					}
				}
				catch(e) {
					showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
			}
			else { // 媒体地址为 http协议地址
				if( p_tag != '' ){
					try{
						showplayer = eval(""+sPlayerList[p_tag]+"('"+MediaURL+"')");
					}
					catch(e) {
						try{
							if (sPlayerList[$ext] =="") {
								showplayer = eval(""+sPlayerList[""]+"('"+MediaURL+"')");
							}
							else {
								showplayer = eval(""+sPlayerList[$ext]+"('"+MediaURL+"')");
							}
						}
						catch(e) {
							showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
						}
					}
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
				else {
					showplayer = eval(""+sPlayerList['unknown']+"('"+MediaURL+"')");
					output_what(showplayer,pType,SongID,MediaURL,$ext);
				}
			}
		}
	}
}

function output_what(OutStr,pType,SongID,MediaURL,$ext) { //最终输出
	var isIE = (window.ActiveXObject ? true : false);
	if($ext=='swf' || $ext=='flv') {
		try{
			if(getCookie("LBMediaExt")!=""&&(getCookie("LBMediaExt")!="swf"&&getCookie("LBMediaExt")!="flv")) {
				try{
					the("Exobud").controls.stop();
					//the("Exobud").close();
				}catch(e){}
			}
			delNodes(getObjectById("PlayerObj"));
			getObjectById("PlayerObj").innerHTML = "";
			getObjectById("PlayerObj").innerHTML = OutStr;
			doTLab(SongID);
			ctrl_show(SongID,pType);
		}catch(e){}
	}
	else {
		if(getCookie("LBMediaExt")=="wma"||getCookie("LBMediaExt")=="mp3") {
			try{
				if( document.getElementById && the("Exobud") ) {
					if(this.isIE) {
						//先停止Media Player控件播放,避免内存堆积
						the("Exobud").controls.stop();
						//the("Exobud").close();
						the("Exobud").URL = MediaURL;
						the("Exobud").controls.play();
					} else {
						getObjectById("PlayerObj").innerHTML = "";
						getObjectById("PlayerObj").innerHTML = OutStr;
					}
				} else{
					getObjectById("PlayerObj").innerHTML = "";
					getObjectById("PlayerObj").innerHTML = OutStr;
				}
				doTLab(SongID);
				ctrl_show(SongID,pType);
			}catch(e){}
		}
		else{
			try{
				if( document.getElementById && the("Exobud") ) {
					if(the("Exobud").name == "MediaAudioPlayer"){
						the("Exobud").controls.stop();
						//the("Exobud").close();
						if(document.body){
							document.body.removeChild(the("Exobud"));
						}
						else {
							if(the("Exobud").parentNode){ the("Exobud").parentNode.removeChild(the("Exobud")); }
						}
					}
				}
				getObjectById("PlayerObj").innerHTML = "";
				getObjectById("PlayerObj").innerHTML = OutStr;
				doTLab(SongID);
				ctrl_show(SongID,pType);
			}catch(e){}
		}
	}
	setCookie("LBMediaExt", $ext + "", 24*60*60);
}

function do_what(url,sType,SongID) { // 起始执行函数
	if (sType=='default' || sType=='' || sType==null) { sType = 'default'; }
	else { sType = sType; }
		//IE_GetFileExt(url,sType,SongID); // IE 的方法 （IE 5以上版本）
		//CrossJS_GetFileExt(url,sType,SongID); // CrossJS调用方法
		//JSONP_GetFileExt(url,sType,SongID); // 基于JSONP原理调用方法
		Ajax_GetFileExt(url,sType,SongID); // Ajax调用方法
}
