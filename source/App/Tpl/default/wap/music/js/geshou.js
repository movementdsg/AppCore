var geshoudata={"total":"15","pn":"0","rn":"15","reclist":[{"id":"h0","source":"8","name":"热门歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs0.jpg","intro":""},{"id":"h90","source":"8","name":"90后喜欢","pic":"./App/Tpl/default/wap/music/images/geshou/gs90.jpg","intro":""},{"id":"h80","source":"8","name":"80后喜欢","pic":"./App/Tpl/default/wap/music/images/geshou/gs80.jpg","intro":""},{"id":"hynan","source":"8","name":"华语男歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs1.jpg","intro":""},{"id":"hynv","source":"8","name":"华语女歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs2.jpg","intro":""},{"id":"hyzh","source":"8","name":"华语组合","pic":"./App/Tpl/default/wap/music/images/geshou/gs3.jpg","intro":""},{"id":"omnan","source":"8","name":"欧美男歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs4.jpg","intro":""},{"id":"omnv","source":"8","name":"欧美女歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs5.jpg","intro":""},{"id":"omzh","source":"8","name":"欧美组合","pic":"./App/Tpl/default/wap/music/images/geshou/gs6.jpg","intro":""},{"id":"rhnan","source":"8","name":"日韩男歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs7.jpg","intro":""},{"id":"rhnv","source":"8","name":"日韩女歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs8.jpg","intro":""},{"id":"rhzh","source":"8","name":"日韩组合","pic":"./App/Tpl/default/wap/music/images/geshou/gs9.jpg","intro":""},{"id":"h70","source":"8","name":"70后喜欢","pic":"./App/Tpl/default/wap/music/images/geshou/gs10.jpg","intro":""},{"id":"wlgs","source":"8","name":"网络歌手","pic":"./App/Tpl/default/wap/music/images/geshou/gs11.jpg","intro":""},{"id":"hjqt","source":"8","name":"合辑/其它","pic":"./App/Tpl/default/wap/music/images/geshou/gs15.jpg","intro":""}]};
var gsclsPage = 1;
var gsclsRn = 20;
var gsclsTotal = 0;
var currentClsId=-1,currentClsName="";
var currentArtistId = -1,currentArtistName="",currentArtistPic="",currentArtistIntro="";
var musicPageNum = 1;
var musicRn = 100;
var musicTotal = -1;
function loadGeshou(){
	var gsclsArr=geshoudata.reclist;
	var html=[];
	for(var i=0;i<gsclsArr.length;i++){
		var tobj=gsclsArr[i];
		var click='getClsGeshou(\''+tobj.id+'\',\''+tobj.name+'\')';
		html[html.length]='<div class="gsbox" onclick="'+click+'">';
		html[html.length]='<p class="img"><img src="'+tobj.pic+'" onerror="this.src=\'./App/Tpl/default/wap/music/images/my_list.jpg\'"></p>';
		html[html.length]='<p class="txt">'+tobj.name+'</p>';
		html[html.length]='</div>';
	}
	$("#gshtmlId").html(html.join(""));
}
function getClsGeshou(id,name){
	if(isJump){
		isJump = false;
	}else{
		gretArr[gretArr.length]='getClsGeshou(\''+id+'\',\''+name+'\')';
	}
	config.qk_type=8;
	gopage(8,name);	
	if(currentClsId==id){
		$("#gsContextId").show();
		jQuery(window).scrollTop(lastposarr["gscon"]);
		$("#morebtnId").html(lastposarr["gsconmore"]);
		if(lastposarr["gsconmore"].indexOf("更多")!=-1){
			allowLoadMore=1;	
		}
		return;
	}
	if(currentClsId!=id){
		currentClsId=id;
		currentClsName=name;	
		gsclsPage=1;
		gsclsTotal=0;
		$("#returnId .tit").html(name);
		$("#gsContextId").html(loadinghtml());
	}	
		
	loadClsGeshou(id,name);
}
function loadClsGeshou(id,name){
	if(gsclsPage>1){
		moreloadhtml(3);
	}
	var url="./App/Tpl/default/wap/music/htm/geshou/"+id+"/"+gsclsPage+".js";
	ku9.jsonp(url,"r="+getDate(1)+".js",function(json){log("成功")},function(){log("出错了")});
}
function backgeshou(data){
	var gsList = data.geshoulist;
	gsclsTotal = data.total;
	var html=[];
	for(var i=0;i<gsList.length;i++){
		var pic=gsList[i].pic; if(pic.indexOf("://")<0) pic=picUrl+pic;
		var click='commonClick(\''+gsList[i].source+'\',\''+gsList[i].intro+'\',\''+gsList[i].name+'\','+gsList[i].gsid+',\'\',\''+gsList[i].pic+'\',1)';
		html[html.length]='<div class="listone" onclick="'+click+'">';
		html[html.length]='<div class="listpic"><img src="'+pic+'" width="60" height="60" onerror="this.src=\'./App/Tpl/default/wap/music/images/my_list.jpg\'"/></div>';
		html[html.length]='<div class="listdec">';	
		html[html.length]='<p>'+gsList[i].name+'</p>';	
		html[html.length]='<p class="listtext">共'+gsList[i].totalmusic+'首歌曲</p>';
		html[html.length]='</div>';
		html[html.length]='<div class="moreone"><img src="./App/Tpl/default/wap/music/images/disclosure_indicator@2x.png"/></div>';		
		html[html.length]='</div>';
	}
	if(gsclsPage==1){
		$("#gsContextId").html(html.join(""));
	}else{
		$("#gsContextId").append(html.join(""));
	}
	if(parseInt(gsclsTotal,10)==0){
		setToast3('<div style="color:#fff;background: rgba(0, 0, 0, 0.6);border-radius: 2px;padding: 2px;text-align:center;width:160px;margin: 0 auto;">没有歌手</div>');
		return;
	}
	gsclsPage++;
	if(Math.ceil((parseInt(gsclsTotal,10)+(gsclsRn-1))/gsclsRn) > gsclsPage){
		moreloadhtml(2);
	}else{
		moreloadhtml(1);
	}	
}
//读取歌手歌曲
function showGsMusic(id,name,pic,intro){
	if(isJump){
		isJump = false;
	}else{
		gretArr[gretArr.length]='showGsMusic('+id+',\''+name+'\',\''+pic+'\',\''+intro+'\')';
	}
	config.qk_type=9;
	gopage(9);
	if(currentArtistId==id){
		jQuery(window).scrollTop(lastposarr["gsmcon"]);
		$("#morebtnId").html(lastposarr["gsmconmore"]);
		if(lastposarr["gsmconmore"].indexOf("更多")!=-1){
			allowLoadMore=1;	
		}
		goplayposition();
		return;
	}
	if(currentArtistId!=id){
		musicPageNum=1;
		currentArtistId = id;
		currentArtistName=name;
		currentArtistPic=pic;
		currentArtistIntro=intro;
		$("#returnId .tit").html(name);
		if(pic!=''&&intro!=''){
			$("#con_artid .bangdan img").attr("src",picUrl+pic);
			$("#con_artid .bangdan p").html(cutStrByGblen(intro,75));
			$("#con_artid .bangdan").show();
		}else{
			$("#con_artid .bangdan").hide();	
		}
		$("#con_artid .play_listall").html(loadinghtml());
	}	
	loadGsMusic(id);
}
function loadGsMusic(id){
	if(musicPageNum>1){
		moreloadhtml(3);
	}
	var url = "./App/Tpl/default/wap/music/htm/geshoumusic/"+id+"/"+musicPageNum+".js";
	ku9.jsonp(url,"r="+getDate(2),function(json){log("成功")},function(){log("出错了")});
}
//加载歌手单曲
function backArtistMusic(jsondata){
	var data = jsondata;
	if(data=="" || data==null){
		return;
	}
	var musicList = data.musiclist;
	musicTotal = data.total;
	var bigStr = musicBigString(musicList,musicPageNum,musicRn);
	if(musicPageNum==1){
		$("#con_artid .play_listall").html(bigStr);
	}else{
		$("#con_artid .play_listall").append(bigStr);
	}
	bigStr="";
	musicPageNum++;
	if(Math.ceil((parseInt(musicTotal,10)+(musicRn-1))/musicRn)> musicPageNum){
		moreloadhtml(2);
	}else{
		moreloadhtml(1);
	}
}