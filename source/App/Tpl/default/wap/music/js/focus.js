﻿try {
    var jsondata = {
        "total": "4",
        "child": [{
            "id": "3",
            "name": "经典老歌大全",
            "intro": "经典老歌大全，汇聚最好听的经典老歌。从经典欧美老歌到经典粤语老歌，从60年代70年代到80年代90年代，每一首都是一段回忆。听腻了快餐式的网络歌曲，就回到经典老歌里去细细品味吧！",
            "source": "4",
            "img": "./App/Tpl/default/wap/music/images/focus/1.jpg",
            "small_img": "./App/Tpl/default/wap/music/images/focus/s1.jpg"
        },
        {
            "id": "55",
            "name": "网络歌曲大全",
            "intro": "找2014最新最好听的网络歌曲，就到永久音乐网！这里有最火的网络歌曲，而且根据网络歌曲人气智能排序网络歌曲排行榜！",
            "source": "4",
            "img": "./App/Tpl/default/wap/music/images/focus/2.jpg",
            "small_img": "./App/Tpl/default/wap/music/images/focus/s2.jpg"
        },
        {
            "id": "66",
            "name": "英文歌曲大全",
            "intro": "找2014最新最好听的英文歌曲，就到永久音乐网！这里有最火的英文歌曲，而且根据英文歌曲人气智能排序英文歌曲排行榜！",
            "source": "4",
            "img": "./App/Tpl/default/wap/music/images/focus/3.jpg",
            "small_img": "./App/Tpl/default/wap/music/images/focus/s3.jpg"
        },
        {
            "id": "15",
            "name": "好听的儿歌大全",
            "intro": "永久儿童乐园，这里有最新的儿歌，最好看的儿歌视频！宝宝喜欢的睡前故事，儿童唐诗三百首等适合婴儿、幼儿、少儿等各个阶段的儿童在线使用，支持下载。",
            "source": "4",
            "img": "./App/Tpl/default/wap/music/images/focus/4.jpg",
            "small_img": "./App/Tpl/default/wap/music/images/focus/s4.jpg"
        }]
    };
    backfocus(jsondata);
} catch(e) {
    jsonError(e)
}