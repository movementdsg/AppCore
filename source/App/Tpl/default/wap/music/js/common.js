/*
* 智能机浏览器版本信息:
*/
var browser={
    versions:function(){ 
		var u = navigator.userAgent, app = navigator.appVersion; 
		return {//移动终端浏览器版本信息 
			trident: u.indexOf('Trident') > -1, //IE内核
			presto: u.indexOf('Presto') > -1, //opera内核
			webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
			gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
			mobile: !!u.match(/AppleWebKit.*Mobile.*/)||!!u.match(/AppleWebKit/), //是否为移动终端
			ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
			android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
			iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
			iPad: u.indexOf('iPad') > -1, //是否iPad
			webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
		};
    }(),
	language:(navigator.browserLanguage || navigator.language).toLowerCase()
}
var config={};
config.stopBubble=function(e){
	if(window.event && window.event.cancelBubble){
		window.event.cancelBubble = true;
	} 
	if (e && e.stopPropagation){
		e.stopPropagation();
	}
}
var ku9={
	jsonp:function(url,data,success,error){
		var add  = /\?/.test( url ) ? "&" : "?";
           var script =  document.createElement("script");
		script.jsonp = 1;
		if(url.lastIndexOf(".htm")==-1 && url.lastIndexOf(".js")==-1){
			var url = url + add + data;
		}
           script.src = url;
		script.onload = script.onreadystatechange = function(){
             if( -[1,] || /loaded|complete/i.test(this.readyState)){
              ku9.removeScript(script);
             }
           }
           script.onerror = function(){
		  	  requestErr();
              ku9.removeScript(script);
           }
           var head = document.getElementsByTagName("head")[0];
           head.appendChild(script);
	},
	removeScript: function(script){
         if(typeof script.jsonp === "undefined" ){
			alert("请求出错");
         }
         if (script.clearAttributes) {
           script.clearAttributes();
         } else {
           script.onload = script.onreadystatechange = script.onerror = null;
         }
         try{script.parentNode.removeChild(script);}catch(e){}
     }
}
function IsNum(s){return(new RegExp(/^(\+|-)?(0|[1-9]\d*)(\.\d*[1-9])?$/).test(s));}
String.prototype.gblen = function() {
	var len = 0;
	for ( var i = 0; i < this.length; i++) {
		if (this.charCodeAt(i) > 127 || this.charCodeAt(i) == 94) {
			len += 2;
		} else {
			len++;
		}
	}
	return len;
};
function cutStrByGblen(str, gblen){
	if (str.gblen() <= gblen) {
		return str;
	} else {
		var baseCut = Math.floor((gblen - 1) / 2);
		var result = str.substring(0, baseCut);
		var nowGblen = result.gblen();
		if (nowGblen < gblen) {
			for ( var i = baseCut, len = str.length; i < len; i++) {
				var charGblen = str.charAt(i).gblen();
				if (nowGblen + charGblen <= gblen - 1) {
					result += str.charAt(i);
					nowGblen += charGblen;
				} else {
					var dotNum = gblen - nowGblen;
					if (dotNum == 1) {
						result += '...';
					} else if (dotNum == 2) {
						result += '...';
					} else {
						result += '...';
					}
					return result;
				}
			}
		}
	}
}
function getDate(flag){
  var dateTime=new Date();
  var yy=dateTime.getFullYear();
  var MM=dateTime.getMonth()+1;  //因为1月这个方法返回为0，所以加1
  if(MM<10)MM="0"+MM;
  var dd=dateTime.getDate();
  if(dd<10)dd="0"+dd;
  var week=dateTime.getDay();
  var hh=dateTime.getHours();
  if(flag==1){
  		return yy+""+MM+""+dd;
  }else if(flag==2){
	return dateTime.getTime();
  }
  return yy+""+MM+""+dd+""+hh;
}
function getTimeM(totalTime){
	var totalTimeStr="00:00";
	if(!isNaN(totalTime)){
		var totalTimeStr=totalTime/60>=10?parseInt(totalTime/60):"0"+parseInt(totalTime/60);
		totalTimeSec=(totalTime%60>=10?parseInt(totalTime%60):"0"+parseInt(totalTime%60));
		if(totalTimeStr>99){
			totalTimeStr="00";
		}
		totalTimeStr=totalTimeStr+":"+totalTimeSec;
	}
	return totalTimeStr;
	
}
function isHTML5(){
	try{
		var hasAudio = !!(document.createElement('audio').canPlayType("audio/mpeg"));
		return hasAudio;
   }catch(e){return false;}
}
function isLocalStorage(){
	if(window.localStorage){
	   return true;
	}
	return false;
}
function getRequest() {
	var url = location.search; //获取url中"?"符后的字串
	try {
		url = decodeURIComponent(url);
	} catch (e) {
		url = unescape(url);//避免有些浏览器下传过来的&是%26...add by liyan 2011-10-11
	}
	var theRequest = {};
	if (url.indexOf("?") !== -1) {
		var str = url.slice(1);
		strs = str.split("&");
		var i;
		for (i = 0; i < strs.length; i += 1) {
			theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
		}
	}
	return theRequest;
}
//导航悬浮
$(function(){	
  $(window).scroll(function() {
	if($(window).scrollTop()>=94){
		$("#header").addClass("headerFix");
	}else{
		$("#header").removeClass("headerFix");
	} 
  });
});

