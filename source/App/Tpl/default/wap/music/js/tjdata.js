﻿var page=1;
var rn=10;
var total = -1;
config.qk_loading=0;
var G_PR=10;
var G_SUM=0;
var tjzjid=0,tjzjname="",tjzjpic="",tjzjintro="";
var current_zjid=0,current_name="",current_pic="",current_intro="";
var tjPageNum=1,tjRn=100;//默认显示第一页，每页100条的专辑歌曲
function loadtjData(flag){
	$("#tjContentId").html(loadinghtml());
	loadData();
}
function loadData(){
	if(config.qk_loading==1){
		return;
	}
	if(total!=-1 && Math.ceil((parseInt(total,10)+(rn-1))/rn) <= page){
		return;
	}
	config.qk_loading=1;
	if(page>1){
		moreloadhtml(3);
	}
	config.TJ_URL = "./App/Tpl/default/wap/music/htm/tjdata/all"+page+".js";
	ku9.jsonp(config.TJ_URL,"r="+getDate(1)+".js",function(json){log("成功")},function(){log("出错了")});
}
function loadBlockList(jsondata){
	var tjArr=jsondata.reclist;
	total=jsondata.total;
	var html=[];
	for(var i=0;i<tjArr.length;i++){
		var tobj=tjArr[i];
		tjzjid=tobj.id;
		tjzjname=tobj.name;
		tjzjpic=tobj.pic?"":"./App/Tpl/default/wap/music/images/my_list.jpg";
		tjzjintro=tobj.intro;
		var click='commonClick(\''+tobj.source+'\',\''+tjzjintro+'\',\''+tjzjname+'\','+tjzjid+',\'\',\'./App/Tpl/default/wap/music/images/my_list.jpg\',1)';
		html[html.length]='<div class="listone" onclick="'+click+'">';
		html[html.length]='<div class="listpic"><img src="./App/Tpl/default/wap/music/images/my_list.jpg" width="60" height="60" onerror="this.src=\'./App/Tpl/default/wap/music/images/my_list.jpg\'"></div>';
		html[html.length]='<div class="listdec">';			
		html[html.length]='<p><span style="background:none;">'+tjzjname+'</span></p>';			
		html[html.length]='<p class="listtext">'+tjzjintro+'</p>';				
		html[html.length]='</div>';
		html[html.length]='<div class="moreone"><img src="./App/Tpl/default/wap/music/images/disclosure_indicator@2x.png"/></div>';
		html[html.length]='</div>';
	}
	if(page<=1){
		$("#tjContentId").html(html.join(""));
	}else{
		$("#tjContentId").append(html.join(""));
	}
	page++;//分页自动加1
	if(Math.ceil((parseInt(total,10)+(rn-1))/rn) > page){
		moreloadhtml(2);
	}else{
		moreloadhtml(1);
	}
	dsExe();
}
//通过专辑id查找专辑
function ShowZhuantiMusic(id,name,pic,intro){
	if(isJump){
		isJump = false;
	}else{
		gretArr[gretArr.length]='ShowZhuantiMusic('+id+',\''+name+'\',\''+pic+'\',\''+intro+'\')';
	}
	config.qk_type=10;
	gopage(10);
	if(current_zjid==id){
		jQuery(window).scrollTop(lastposarr["gsmcon"]);
		$("#morebtnId").html(lastposarr["gsmconmore"]);
		if(lastposarr["gsmconmore"].indexOf("更多")!=-1){
			allowLoadMore=1;	
		}
		goplayposition();
		return;
	}
	if(current_zjid!=id){
		tjPageNum=1;
		current_zjid=id;
		current_name=name;
		current_pic=pic;
		current_intro=intro;
		$("#returnId .tit").html(cutStrByGblen(name,30));
		$("#twoContextId #con_gdtextId .bangdan img").attr("src",pic);
		$("#twoContextId #con_gdtextId .bangdan p").html(cutStrByGblen(intro,75));		
		$("#twoContextId #con_gdtextId .play_listall").html(loadinghtml());
	}
	loadZhuantiMusic(id,name,pic,intro);
}
function loadZhuantiMusic(id,name,pic,intro){
	if(tjPageNum>1){
		moreloadhtml(3);
	}
	var url="./App/Tpl/default/wap/music/htm/tjdata/"+id+"_"+tjPageNum+".js";
	ku9.jsonp(url,"r="+getDate(1)+".js",function(json){log("成功")},function(){log("出错了")});
	
}
//加载专题单曲
function backZjMusic(jsondata){
	var data = jsondata;
	if(data=="" || data==null){
		return;
	}
	var musicList = data.musiclist;
	musicTotal = data.total;
	var bigStr = musicBigString(musicList,tjPageNum,tjRn);
	if(tjPageNum==1){
		$("#twoContextId #con_gdtextId .play_listall").html(bigStr);
	}else{
		$("#twoContextId #con_gdtextId .play_listall").append(bigStr);
	}
	bigStr="";
	tjPageNum++;
	if(Math.ceil((parseInt(musicTotal,10)+(tjRn-1))/tjRn)> tjPageNum){
		moreloadhtml(2);
	}else{
		moreloadhtml(1);
	}
}