﻿//搜索
var searchMusicPage = 1;
var searchMusicRn = 20;
var searchMusicTotal = 0;
var searchKey = "";
var last_search_key='';
function searchMethod(flag){
	searchMusicPage=1;
	var str=$("#searchvalueId").val();
	if(str=='' || str=='请输入歌曲或者歌手'){
		setToast3('<div style="color:#fff;background: rgba(0, 0, 0, 0.6);border-radius: 2px;padding: 2px;text-align: center;width:200px;margin: 0 auto;">请输入歌曲名和歌手名</div>');
		return;
	}
	searchKey=str;
	if(searchKey!=last_search_key){searchMusicTotal=0;}
	if(flag==1){
		searchMusicFunction(searchKey);
	}
}
function searchMusicFunction(sourceKey){
	$("#returnId .tit").html("搜索<font color=red>"+sourceKey+"</font>,找到相关结果共<span id='serresult' style='color:red;'>"+searchMusicTotal+"</span>个。");
	config.qk_type=15;
	gopage(15);
	if(isJump){
		isJump = false;
	}else{
		if(gretArr[gretArr.length-1].indexOf('search')>-1){
			gretArr[gretArr.length-1]='searchMusicFunction(\''+sourceKey+'\')';
		}else{
			gretArr[gretArr.length]='searchMusicFunction(\''+sourceKey+'\')';
		}
	}
	if(sourceKey==last_search_key){
		if($("#searchdqId").is(":visible")){
			jQuery(window).scrollTop(lastposarr["searchdqId"]);
			$("#morebtnId").html(lastposarr["searchdqIdmore"]);
			if(lastposarr["searchdqIdmore"].indexOf("更多")!=-1){
				allowLoadMore=1;
			}
		}
		return;
	}
	last_search_key=sourceKey;
	$("#searchdqId").html(loadinghtml());
	searchMusic();
}
function searchMusic(){
	if(config.qk_loading==1){
		return;
	}
	if(searchMusicPage>1){
		moreloadhtml(3);
	}
	config.qk_loading=1;
	$("#search_tag").hide();
	var url = "http://m.9ku.com/do/getser.asp?key="+encodeURI(searchKey)+"&page="+searchMusicPage+"&callback=searchMusicResult"
	ku9.jsonp(url,"r="+getDate(2),function(json){log("成功")},function(){log("出错了")});
}
function searchMusicResult(jsondata){
	config.qk_loading=0;
	var data = jsondata;
	if(data=="" || data==null){
		return;
	}
	var musicList = data.musiclist;
	searchMusicTotal = data.total;
	var bigsig=musicBigString(musicList,searchMusicPage,searchMusicRn);
	if(searchMusicPage==1){
		if(document.getElementById("serresult"))$("#serresult").html(searchMusicTotal);
		$("#searchdqId").html(bigsig);
	}else{
		$("#searchdqId").append(bigsig);
	}
	if(parseInt(searchMusicTotal,10)==0){
		$("#search_tag").show();
		setToast3('<div style="color:#fff;background: rgba(0, 0, 0, 0.6);border-radius: 2px;padding: 2px;text-align: center;width:160px;margin: 0 auto;">没有找到相关的内容</div>');
		return;
	}
	searchMusicPage++;
	if(Math.ceil((parseInt(searchMusicTotal,10)+(searchMusicRn-1))/searchMusicRn) > searchMusicPage){
		moreloadhtml(2);
	}else{
		moreloadhtml(1);
	}
}

function showsearchdiv(){
	commHide();
	showplaybtn();
	$("#con_sreachId").show();
	if($("#searchdqId").html()!=""){
		$("#searchContextId").show();
		$("#searchdqId").show();
		$("#search_tag").hide();
		if(lastposarr["searchdqIdmore"].indexOf("更多")!=-1){
			allowLoadMore=1;	
		}
	}else{
		$("#searchContextId").hide();
		$("#search_tag").show();	
	}
	  
}
function get_search_tag(){
	var url = "http://m.9ku.com/do/getsearchtag.asp?callback=get_search_tag_result"
	ku9.jsonp(url,"r="+getDate(2),function(json){log("成功")},function(){log("出错了")});
}
function get_search_tag_result(jsondata){
	var data = jsondata;
	if(data=="" || data==null){
		return;
	}
	var str="<ul>";
	var tagList=data.taglist;
	for(var i=0;i<tagList.length;i++){
		var someObj=tagList[i];
		var tag = someObj.songName+" "+someObj.artist;
		str+="<li><a onclick=\"linktoser('"+tag+"');\" class=\"pic\">"+tag+"</a></li>";
	}
	str+="</ul>";
	document.getElementById("search_tag").innerHTML=str;
	str="";
}
function linktoser(serkey){
	searchKey=serkey;
	$("#searchvalueId").val(serkey);
	searchMusicFunction(serkey);
}