﻿function loadquku(){
	if(config.TreeArr==null){
		$("#qkhtmlId").html(loadinghtml());
		config.bangdan_url="./App/Tpl/default/wap/music/htm/bangdan/bangdan.js";
		ku9.jsonp(config.bangdan_url,"r="+getDate(1)+".js",function(json){log("成功")},function(){log("出错了")});
	}else{
		showQuKuTreeFromInternet(config.TreeArr);
	}
}
//通过网络去取tree返回方法
function showQuKuTreeFromInternet(jsondata){
	config.TreeArr=jsondata;
	var oneflobj=config.TreeArr.child;
	var html=[];
	for(var i=0;i<oneflobj.length;i++){
		var tobj=oneflobj[i];
		if(tobj.id==19){
			var click='oneCategory('+i+');';
		}else{
			var click='commonClick(\''+tobj.source+'\',\''+tobj.intro+'\',\''+tobj.name+'\','+tobj.id+',\'\',\''+tobj.pic+'\',1)';
		}
		html[html.length]='<div class="listone" onclick="'+click+'">';
		html[html.length]='<div class="listpic"><img src="'+tobj.pic+'" width="60" height="60" onerror="this.src=\'./App/Tpl/default/wap/music/images/my_list.jpg\'"/></div>';
		html[html.length]='<div class="listdec">';	
		html[html.length]='<p>'+tobj.name+'</p>';	
		html[html.length]='<p class="listtext">'+tobj.intro+'</p>';		
		html[html.length]='</div>';
		html[html.length]='<div class="moreone"><img src="./App/Tpl/default/wap/music/images/disclosure_indicator@2x.png"/></div>';		
		html[html.length]='</div>';
	}
	$("#qkhtmlId").html(html.join(''));
}
var last_index=-1000;
function oneCategory(index){
	config.curr_index=index;
	if(isJump){
		isJump = false;
	}else{
		gretArr[gretArr.length]='oneCategory('+index+')';
	}
	loadCategory(index);
}
//加载15个大分类下的子列表
function loadCategory(index){
	var sometreeobj = config.TreeArr.child[index].child;
	var len=sometreeobj.length;
	gopage(5,config.TreeArr.child[index].name);
	if(last_index==index){
		jQuery(window).scrollTop(lastposarr["onecon"]);
		$("#morebtnId").html(lastposarr["oneconmore"]);
		if(lastposarr["oneconmore"].indexOf("更多")!=-1){
			allowLoadMore=1;	
		}
		return;
	}
	last_index=index;
	$("#oneContextId").html(loadinghtml());
	if(len>G_PR){
		len=G_PR;
		G_SUM=G_PR;
		moreloadhtml(2);
	}
	if(len>0){
		$("#oneContextId").html(onefldata(0,len,index));
	}
}
function onefldata(start,end,index){
	var treeobj = config.TreeArr;
	var categoryString = "";
	var sometreeobj = treeobj.child[index].child;
	var html = [];
	for(var i=start,j=end;i<j;i++){
		var someObj = sometreeobj[i];
		var click = "commonClick(\'"+someObj.source+"','"+someObj.intro+"','"+someObj.name+"','"+someObj.id+"','"+treeobj.child[index].id+"','"+someObj.pic+"')";
		html[html.length]='<div class="listone" onclick="'+click+'">';
		html[html.length]='<div class="listpic"><img src="'+someObj.pic+'" width="60" height="60" onerror="this.src=\'./App/Tpl/default/wap/music/images/my_list.jpg\'"/></div>';
		html[html.length]='<div class="listdec">';	
		html[html.length]='<p>'+someObj.name+'</p>';	
		html[html.length]='<p class="listtext">'+someObj.intro+'</p>';		
		html[html.length]='</div>';
		html[html.length]='<div class="moreone"><img src="./App/Tpl/default/wap/music/images/disclosure_indicator@2x.png"/></div>';
		html[html.length]='</div>';
	}
	categoryString = html.join('');
	html = null;
	return categoryString;
}
//榜单心情歌曲
function xinQingGeQu(id,name,pic,intro){
	if(isJump){
		isJump = false;
	}else{
		gretArr[gretArr.length]='xinQingGeQu('+id+',\''+name+'\',\''+pic+'\',\''+intro+'\')';
	}
	config.qk_type=2;
	gopage(2);
	if(currentBangId==id){
		return;
	}
	currentBangId="taste"+id;
	$("#returnId .tit").html(cutStrByGblen(name,30));
	$("#twoContextId .bangdan img").attr("src",pic);
	$("#twoContextId .bangdan p").html(cutStrByGblen(intro,75));
	
	$("#twoContextId #con_gdtextId .play_listall").html(loadinghtml());
	
	var url="./App/Tpl/default/wap/music/htm/taste/"+id+".js";
	ku9.jsonp(url,"r="+getDate(1)+".js",function(json){log("成功")},function(){log("出错了")});
}
//榜单
var currentBangId="";
function someBang(id,name,pic,intro){
	if(isJump){
		isJump = false;
	}else{
		gretArr[gretArr.length]='someBang('+id+',\''+name+'\',\''+pic+'\',\''+intro+'\')';
	}
	config.qk_type=2;
	gopage(2);
	if(currentBangId.replace("bang","")==id){
		goplayposition();
		return;
	}
	if(currentBangId!=id){
		currentBangId="bang"+id;
		$("#returnId .tit").html(cutStrByGblen(name,30));
		$("#twoContextId .bangdan img").attr("src",pic);
		$("#twoContextId .bangdan p").html(cutStrByGblen(intro,75));		
		$("#twoContextId #con_gdtextId .play_listall").html(loadinghtml());
	}
	var url="./App/Tpl/default/wap/music/htm/bangdan/bangdan"+id+".js";
	ku9.jsonp(url,"r="+getDate(1)+".js",function(json){log("成功")},function(){log("出错了")});
}