
$(document).ready(function(){

	//插入锚链接
	$('p strong').each(function(i) {
		var p = $(this);
		var s = p.html();
		var re = /([\w\.]+)(\(\))?\s*(方法|属性)/gmi;
		if(re.test(s)) {
			var t = s.replace(re,'$1');
			t = t.replace(/\./gm,'_').toLowerCase();
			p.after('<a name="'+t+'"></a>');
		}
	});

	//var hash = window.location.hash ? window.location.hash : ($("body")[0].contentWindow.location.hash ? $("body")[0].contentWindow.location.hash : '');
	var hash = window.location.hash;
	if(hash!='') {
		hash = hash.split('#')[1];
		var p = $("a[name!='']");
		var q = $("a[name='"+hash+"']");
		var idx = p.index(q);
		p.eq(idx).parents("p").prevAll().remove();
		p.eq(idx+1).parents("p").andSelf().nextAll().remove();
	}

	$('blockquote p,blockquote div').each(function(i) {
		var str = $(this).html();
		//var str = $(this).prop('innerHTML');
		//var str = $(this).get(0).innerHTML;
		//str = str.replace(/&/gi, '&amp;');
		var re = /&#([A-Za-z0-9]+);/gmi;
		while(r = re.exec(str)) {
			str = str.replace(r[0],'&amp;#'+r[1]+';');
		}
		str = str.replace(/</gmi, '&lt;');
		str = str.replace(/>/gmi, '&gt;');
		$(this).html(str);
	});

  if($('blockquote p').length>0){
		/*
		$('blockquote p').each(function() {
			var str = $(this).html();
			var re = /([\r\n])(?!\<br \/>)/ig;
			var r = "";
			while(r = re.exec(str)) {
				var reg=new RegExp(r[0],"gmi");
				str = str.replace(reg, r[0]+'<br />');
			}
			$(this).html(str);
		});
		*/
		$('blockquote p').each(function(i) {
			var p = $(this);
			var s = p.html();
			if(s.match(/[\r\n]/gmi)) {
				//var re = /^\s*/gmi;
				var re = /^( )( )( )( )( )( )/gmi;
				s = s.replace(re,'');
				p.after('<pre><code>'+s+'</code></pre>').remove();
			}
		});
	};

});