# AppCore
> #### 相关资源
* <b>官方网站 : [永久网络](http://www.19www.com) </b> :point_left:
* <b>AspBox框架 : [框架下载](http://git.oschina.net/lajox/AspBox) </b> :point_left:

> #### 学习交流
* 框架交流群：![](https://raw.githubusercontent.com/JackJiang2011/MobileIMSDK/master/preview/more_screenshots/others/qq_group_icon_16-16.png) `41161311` :point_left:
* bug/建议发送至邮箱：`lajox@19www.com`
* 技术支持/合作/咨询请联系作者QQ：`517544292`

# 一、简介

<b>AppCore是以[AspBox框架](http://git.oschina.net/lajox/AspBox)为核心构建的基于MVC模式的APP应用：</b> 

> 带有MVC框架的特点<br>
路由功能实现，主体三个参数：m,c,a发出定位到控制器<br>
单入口，统一访问文件：index.asp<br>
Model层主要处理数据操作<br>
Controller层主要负责业务逻辑<br>
View层载入模板文件


# 二、使用方法

> #### (1) 配置相关参数(文件Inc/config.asp)，例如：

```asp
const cfg_sitepath="/"'======网站安装路径======
```

# 三、代码分布说明
<b>目录结构：</b>

* <b>api</b>                            存放第三方API接口文件 （目前文件为空）
* <b>App</b>                            应用程序文件存放目录，项目所有实现代码均放在这里。:point_left:
* <b>Cache</b>                          生成的缓存文件、日志等存放目录
* <b>Data</b>                           数据库ACCESS存放目录
* <b>Inc</b>                            程序核心运行文件，一般不能随便修改 :point_left:
* <b>res</b>                            资源定向文件，其实就是url定向跳转文件 （可删除）
* <b>statics</b>                        静态文件地址（img,js,css等文件），包括上传文件目录 :point_left:
* <b>Tester</b>                         项目代码测试工具，项目正式上线后可删除。（可删除）
* <b>demo</b>                           查看分页效果演示代码 （可删除）
* <b>index.asp</b>                      程序单入口文件，系统所有均访问这个页面
* <b>admin.asp</b>                      后台入口文件，其实就是一个url定向

<b><font color="green">App - 应用程序文件存放目录，项目所有实现代码均放在这里。</font></b>
这个目录是重中之重，基本修改代码均在这里。

> App<br>
>  - Common           用户自定义函数<br>
>  - Conf             配置文件<br>
>  - Lang             语言文件<br>
>  - Lib              核心文件<br>
>  - Tpl              模板文件（视图层View）

<b>Lib 目录用于存放 核心代码</b>

> Lib<br>
> 	 - Action   放置Controller类文件，里面具体行为(Action)函数，主要负责业务逻辑<br>
> 	 - Model    (数据库表)Model层模型文件，基本上一个数据表对应一个模型文件<br>
> 	 - Helper   Helper辅助类文件<br>
> 	 - Widget   存放单元组件（比如广告组件）

# 四、MVC 框架的特点
MVC是( Model、View、Controller )的缩写。

 - 一般都是单入口，URL运行模式（路由功能），三个参数：m,c,a <br>

举例URL访问地址： index.asp?m=admin&c=index&a=login<br>
首先从 m=admin 定位到 /App/Lib/Action/Admin 目录下<br>
其次从 c=index 定位到 /App/Lib/Action/Admin/indexAction.class.asp 文件<br>
再次从 a=login 定位到 indexAction.class.asp 文件 里面的 Sub login() 函数块<br>

> 注：<br>
> 缺省了 m 参数，默认值为 home<br>
> 缺省了 c 参数，默认值为 index<br>
> 缺省了 a 参数，默认值为 index<br>
> 所以访问 /index.asp 其实就等同访问 /index.asp?m=home&c=index&a=index<br>

 - 分工明细，Model层主要处理数据操作，Controller层主要负责业务逻辑，View层载入模板文件。


# 五、性能测试
压力测试表明，AppCore可以承载百万条数据。


